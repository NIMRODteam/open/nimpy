'''
Defines classes :class:`Scalar`, :class:`Vector` and :class:`Tensor`.

This is designed to be used with the results from
:class:`nimpy.eval_nimrod.EvalNimrod` to create and interface for symbolic
mathematical operations. For example:

.. code-block:: python

    from nimpy.plot_nimrod import PlotNimrod as pn
    from nimpy.eval_nimrod import EvalNimrod
    from nimpy.field_class import Scalar,Vector,Tensor

    dumpfile = 'dumpgll.00000.h5'
    nml = f90nml.read('nimrod.in')
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False

    eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbj')
    grid = pn.grid_1d_gen([-0.75, 0.1, 0], [0.75, 0.1, 0], 1000)
    fval = eval_nimrod.eval_field('p', grid, dmode=2, eq=2)
    p = Scalar(fval, grid, dmode=2, torgeom=gmt)
    fval = eval_nimrod.eval_field('b', grid, dmode=1, eq=2)
    B = Vector(fval, grid, dmode=1, torgeom=gmt)
    fval = eval_nimrod.eval_field('j', grid, dmode=1, eq=2)
    j = Vector(fval, grid, dmode=1, torgeom=gmt)

    jxb=j.cross(B)
    grp=p.grad()
    force=jxb-grp
    pn.plot_vector_line(grid, force.data, flabel=r'Force', f2=jxb.data,
                        f2label=r'$\mathbf{J}\times\mathbf{B}$',
                        f3=grp.data, f3label=r'$\\nabla p$',
                        comp='perp', style='varied',
                        legend_loc='lower left', ylabel=r'Force density N/m^3')

Internally to the code, the Phi derivative is defined as
$1/R$ $\partial/\partial\Phi$ when torgeom=True. This can be confusing, so
caveat emptor.

Some operations have the option to reduce the amount of differential order
It is useful to optimize execution time
'''  # noqa
import numpy as np


class Scalar:
    '''
    Basic field class for a Scalar variable
    '''

    def __init__(self, fval, grid, dmode=2, torgeom=True, iqty=-1):
        '''
        Create an instance of the scalar class.

        :param fval: input scalar values
        :param grid: corresponding R,Z,Phi values
        :param dmode: differential order of fval
        :param torgeom: True if toroidal geometry
        :param iqty: iqty of fval to use in the scalar
        '''
        if iqty > -1:
            if dmode == 0:
                size = (1,) + np.shape(fval[0])
                nqty = fval.shape[0]
            elif dmode == 1:
                size = (4,) + np.shape(fval[0])
                nqty = int(fval.shape[0] / 4)
            elif dmode == 2:
                size = (10,) + np.shape(fval[0])
                nqty = int(fval.shape[0] / 10)
            self.__fval = np.zeros(size)
            self.__fval[0] = fval[iqty]
            if dmode > 0:
                self.__fval[1:4] = fval[nqty+iqty:4*nqty:nqty]
            if dmode > 1:
                self.__fval[4:10] = fval[4*nqty+iqty:10*nqty:nqty]
        else:
            self.__fval = fval
        self.__grid = grid
        self.__dmode = dmode
        self.__torgeom = torgeom
        if torgeom:
            self.__h3 = grid[0]
        else:
            self.__h3 = 1.0

        self.__set_field()
        return

    def __set_field(self):
        '''
        Set views to the values in fval.
        '''
        self.data = self.__fval[0]
        if self.__dmode > 0:
            self.dr = self.__fval[1]
            self.dz = self.__fval[2]
            self.dp = self.__fval[3]  # 1/R d/dPhi
        if self.__dmode > 1:
            self.drr = self.__fval[4]
            self.drz = self.__fval[5]
            self.drp = self.__fval[6]  # 1/R d/dR d/dPhi
            self.dzz = self.__fval[7]
            self.dzp = self.__fval[8]  # 1/R d/dZ d/dPhi
            self.dpp = self.__fval[9]  # 1/R^2 d^2/dPhi^2
        return

    def __add__(self, other):
        '''
        Add for :class:`Scalar` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data + other
            if dmode > 0:
                fval[1] = self.dr
                fval[2] = self.dz
                fval[3] = self.dp
            if self.__dmode > 1:
                fval[4] = self.drr
                fval[5] = self.drz
                fval[6] = self.drp
                fval[7] = self.dzz
                fval[8] = self.dzp
                fval[9] = self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data + other.data
            if dmode > 0:
                fval[1] = self.dr + other.dr
                fval[2] = self.dz + other.dz
                fval[3] = self.dp + other.dp
            if self.__dmode > 1:
                fval[4] = self.drr + other.drr
                fval[5] = self.drz + other.drz
                fval[6] = self.drp + other.drp
                fval[7] = self.dzz + other.dzz
                fval[8] = self.dzp + other.dzp
                fval[9] = self.dpp + other.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot add a Scalar with {}'.format(type(other)))

    def __radd__(self, other):
        '''
        Add for :class:`Scalar` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = other + self.data
            if dmode > 0:
                fval[1] = self.dr
                fval[2] = self.dz
                fval[3] = self.dp
            if self.__dmode > 1:
                fval[4] = self.drr
                fval[5] = self.drz
                fval[6] = self.drp
                fval[7] = self.dzz
                fval[8] = self.dzp
                fval[9] = self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __sub__(self, other):
        '''
        Subtract for :class:`Scalar` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data - other
            if dmode > 0:
                fval[1] = self.dr
                fval[2] = self.dz
                fval[3] = self.dp
            if self.__dmode > 1:
                fval[4] = self.drr
                fval[5] = self.drz
                fval[6] = self.drp
                fval[7] = self.dzz
                fval[8] = self.dzp
                fval[9] = self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data - other.data
            if dmode > 0:
                fval[1] = self.dr - other.dr
                fval[2] = self.dz - other.dz
                fval[3] = self.dp - other.dp
            if self.__dmode > 1:
                fval[4] = self.drr - other.drr
                fval[5] = self.drz - other.drz
                fval[6] = self.drp - other.drp
                fval[7] = self.dzz - other.dzz
                fval[8] = self.dzp - other.dzp
                fval[9] = self.dpp - other.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            message = 'Cannot subtract a {} from Scalar'.format(type(other))
            raise Exception(message)

    def __rsub__(self, other):
        '''
        Subtract for :class:`Scalar` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = other - self.data
            if dmode > 0:
                fval[1] = -self.dr
                fval[2] = -self.dz
                fval[3] = -self.dp
            if self.__dmode > 1:
                fval[4] = -self.drr
                fval[5] = -self.drz
                fval[6] = -self.drp
                fval[7] = -self.dzz
                fval[8] = -self.dzp
                fval[9] = -self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __mul__(self, other):
        '''
        Multiply for :class:`Scalar` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data * other
            if dmode > 0:
                fval[1] = self.dr * other
                fval[2] = self.dz * other
                fval[3] = self.dp * other
            if self.__dmode > 1:
                fval[4] = self.drr * other
                fval[5] = self.drz * other
                fval[6] = self.drp * other
                fval[7] = self.dzz * other
                fval[8] = self.dzp * other
                fval[9] = self.dpp * other
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data * other.data
            if dmode > 0:
                fval[1] = self.data * other.dr + self.dr * other.data
                fval[2] = self.data * other.dz + self.dz * other.data
                fval[3] = self.data * other.dp + self.dp * other.data
            if dmode > 1:
                fval[4] = self.data * other.drr + self.drr * other.data \
                    + self.dr * other.dr + self.dr * other.dr
                fval[5] = self.data * other.drz + self.drz * other.data \
                    + self.dr * other.dz + self.dz * other.dr
                fval[6] = self.data * other.drp + self.drp * other.data \
                    + self.dr * other.dp + self.dp * other.dr
                fval[7] = self.data * other.dzz + self.dzz * other.data \
                    + self.dz * other.dz + self.dz * other.dz
                fval[8] = self.data * other.dzp + self.dzp * other.data \
                    + self.dz * other.dp + self.dp * other.dz
                fval[9] = self.data * other.dpp + self.dpp * other.data \
                    + self.dp * other.dp + self.dp * other.dp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Vector:
            dmode = np.minimum(self.__dmode, other._Vector__dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data * other.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.data * other.dr[iqty] \
                        + self.dr * other.data[iqty]
                    fval[2 * 3 + iqty] = self.data * other.dz[iqty] \
                        + self.dz * other.data[iqty]
                    fval[3 * 3 + iqty] = self.data * other.dp[iqty] \
                        + self.dp * other.data[iqty]
                if dmode > 1:
                    fval[4 * 3 + iqty] = self.data * other.drr[iqty] \
                        + self.drr * other.data[iqty] \
                        + self.dr * other.dr[iqty] + self.dr * other.dr[iqty]
                    fval[5 * 3 + iqty] = self.data * other.drz[iqty] \
                        + self.drz * other.data[iqty] \
                        + self.dr * other.dz[iqty] + self.dz * other.dr[iqty]
                    fval[6 * 3 + iqty] = self.data * other.drp[iqty] \
                        + self.drp * other.data[iqty] \
                        + self.dr * other.dp[iqty] + self.dp * other.dr[iqty]
                    fval[7 * 3 + iqty] = self.data * other.dzz[iqty] \
                        + self.dzz * other.data[iqty] \
                        + self.dz * other.dz[iqty] + self.dz * other.dz[iqty]
                    fval[8 * 3 + iqty] = self.data * other.dzp[iqty] \
                        + self.dzp * other.data[iqty] \
                        + self.dz * other.dp[iqty] + self.dp * other.dz[iqty]
                    fval[9 * 3 + iqty] = self.data * other.dpp[iqty] \
                        + self.dpp * other.data[iqty] \
                        + self.dp * other.dp[iqty] + self.dp * other.dp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Tensor:
            dmode = np.minimum(self.__dmode, other._Tensor__dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (3, 12) + np.shape(self.__fval[0])
            fval = np.zeros(size)
            fval[:, 0:3] = self.data * other.data
            if dmode > 0:
                fval[0, 3:] = self.data * other.dr + self.dr \
                    * other.data.reshape((9,) + np.shape(self.__fval[0]))
                fval[1, 3:] = self.data * other.dz + self.dz \
                    * other.data.reshape((9,) + np.shape(self.__fval[0]))
                fval[2, 3:] = self.data * other.dp + self.dp \
                    * other.data.reshape((9,) + np.shape(self.__fval[0]))
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            message = 'Cannot multiply a Scalar with {}'.format(type(other))
            raise Exception(message)

    def __rmul__(self, other):
        '''
        Multiply for :class:`Scalar` type
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = other * self.data
            if dmode > 0:
                fval[1] = other * self.dr
                fval[2] = other * self.dz
                fval[3] = other * self.dp
            if self.__dmode > 1:
                fval[4] = other * self.drr
                fval[5] = other * self.drz
                fval[6] = other * self.drp
                fval[7] = other * self.dzz
                fval[8] = other * self.dzp
                fval[9] = other * self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __truediv__(self, other):
        '''
        Divide for :class:`Scalar` type
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data / other
            if dmode > 0:
                fval[1] = self.dr / other
                fval[2] = self.dz / other
                fval[3] = self.dp / other
            if self.__dmode > 1:
                fval[4] = self.drr / other
                fval[5] = self.drz / other
                fval[6] = self.drp / other
                fval[7] = self.dzz / other
                fval[8] = self.dzp / other
                fval[9] = self.dpp / other
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.data / other.data
            if dmode > 0:
                fval[1] = ((self.dr * other.data - self.data * other.dr)
                           / other.data**2)
                fval[2] = ((self.dz * other.data - self.data * other.dz)
                           / other.data**2)
                fval[3] = ((self.dp * other.data - self.data * other.dp)
                           / other.data**2)
            if dmode > 1:
                raise Exception('Implement Scalar/Scalar division '
                                + 'with dmode=2')
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot divide a Scalar by {}'.format(type(other)))

    def __rtruediv__(self, other):
        '''
        Divide for :class:`Scalar` type
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = other / self.data
            if dmode > 0:
                fval[1] = -other * self.dr / self.data**2
                fval[2] = -other * self.dz / self.data**2
                fval[3] = -other * self.dp / self.data**2
            if dmode > 1:
                fval[4] = other * ((2.0 * self.data * self.dr * self.dr
                                    - self.drr * self.data**2)
                                   / self.data**4)
                fval[5] = other * ((2.0 * self.data * self.dr * self.dz
                                    - self.drz * self.data**2)
                                   / self.data**4)
                fval[6] = other * ((2.0 * self.data * self.dr * self.dp
                                    - self.drp * self.data**2)
                                   / self.data**4)
                fval[7] = other * ((2.0 * self.data * self.dz * self.dz
                                    - self.dzz * self.data**2)
                                   / self.data**4)
                fval[8] = other * ((2.0 * self.data * self.dz * self.dp
                                    - self.dzp * self.data**2)
                                   / self.data**4)
                fval[9] = other * ((2.0 * self.data * self.dp * self.dp
                                    - self.dpp * self.data**2)
                                   / self.data**4)
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __neg__(self):
        '''
        Negative for :class:`Scalar` type.
        '''
        dmode = self.__dmode
        if dmode == 0:
            size = (1,) + np.shape(self.__fval[0])
        elif dmode == 1:
            size = (4,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (10,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        fval[0] = -self.data
        if dmode > 0:
            fval[1] = -self.dr
            fval[2] = -self.dz
            fval[3] = -self.dp
        if self.__dmode > 1:
            fval[4] = -self.drr
            fval[5] = -self.drz
            fval[6] = -self.drp
            fval[7] = -self.dzz
            fval[8] = -self.dzp
            fval[9] = -self.dpp
        return Scalar(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)

    def __pow__(self, other):
        '''
        Power for scalar type.
        '''

        dmode = self.__dmode
        fval = np.zeros_like(self.__fval)
        if type(other) in [int, float]:
            fval[0] = np.power(self.data, other)
            if dmode > 0:
                if other == 1:
                    temp1 = 1
                else:
                    temp1 = other * np.power(self.data, other - 1)
                fval[1] = temp1 * self.dr
                fval[2] = temp1 * self.dz
                fval[3] = temp1 * self.dp
            if dmode > 1:
                if other == 1:
                    temp2 = 0
                elif other == 2:
                    temp2 = 2
                else:
                    temp2 = other * (other - 1) \
                        * np.power(self.data, other - 2)
                fval[4] = temp2 * self.dr * self.dr + temp1 * self.drr
                fval[5] = temp2 * self.dr * self.dz + temp1 * self.drz
                fval[6] = temp2 * self.dr * self.dp + temp1 * self.drp
                fval[7] = temp2 * self.dz * self.dz + temp1 * self.dzz
                fval[8] = temp2 * self.dz * self.dp + temp1 * self.dzp
                fval[9] = temp2 * self.dp * self.dp + temp1 * self.dpp
            return Scalar(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            message = 'Scalar power unreconized type {}'.format(type(other))
            raise Exception(message)

    def _test_almost_eq(self, other, rtol=1e-7, atol=0):
        if not isinstance(other, Scalar):
            raise TypeError('Can only compare two Scalars')
        self.__test_atr_eq(other)
        self.__test_data_almost_eq(other, rtol=rtol, atol=atol)

    def __test_atr_eq(self, other):
        '''
        Test if self and other have equal attributes
        '''
        assert self.__dmode == other._Scalar__dmode
        assert self.__grid.all() == other._Scalar__grid.all()
        assert self.__torgeom == other._Scalar__torgeom
        if self.__torgeom:
            assert self.__h3.all() == other._Scalar__h3.all()
        else:
            assert self.__h3 == other._Scalar__h3

    def __test_data_almost_eq(self, other, rtol=1e-7, atol=0):
        '''
        Test data arrays in self and other to see if they are equal within
        tolerance.

        Note that the allclose asseration does not commute
        a ~ b and b ~ a can yield different when atol != 0
        '''
        np.testing.assert_allclose(self.__fval, other._Scalar__fval,
                                   rtol, atol)
        np.testing.assert_allclose(other._Scalar__fval, self.__fval,
                                   rtol, atol)
        np.testing.assert_allclose(self.data, other.data, rtol, atol)
        np.testing.assert_allclose(other.data, self.data, rtol, atol)
        if self.__dmode > 0:
            np.testing.assert_allclose(self.dr, other.dr, rtol, atol)
            np.testing.assert_allclose(other.dr, self.dr, rtol, atol)
            np.testing.assert_allclose(self.dz, other.dz, rtol, atol)
            np.testing.assert_allclose(other.dz, self.dz, rtol, atol)
            np.testing.assert_allclose(self.dp, other.dp, rtol, atol)
            np.testing.assert_allclose(other.dp, self.dp, rtol, atol)
        if self.__dmode > 1:
            np.testing.assert_allclose(self.drr, other.drr, rtol, atol)
            np.testing.assert_allclose(other.drr, self.drr, rtol, atol)
            np.testing.assert_allclose(self.drz, other.drz, rtol, atol)
            np.testing.assert_allclose(other.drz, self.drz, rtol, atol)
            np.testing.assert_allclose(self.drp, other.drp, rtol, atol)
            np.testing.assert_allclose(other.drp, self.drp, rtol, atol)
            np.testing.assert_allclose(self.dzz, other.dzz, rtol, atol)
            np.testing.assert_allclose(other.dzz, self.dzz, rtol, atol)
            np.testing.assert_allclose(self.dzp, other.dzp, rtol, atol)
            np.testing.assert_allclose(other.dzp, self.dzp, rtol, atol)
            np.testing.assert_allclose(self.dpp, other.dpp, rtol, atol)
            np.testing.assert_allclose(other.dpp, self.dpp, rtol, atol)

    def grad(self, dmode=1):
        '''
        Gradient of :class:`Scalar` type.

        :param dmode: output differential order (optional)
        :return: :class:`Vector` type
        '''
        dmode = min(self.__dmode, dmode + 1)
        if dmode == 0:
            raise Exception('Cannot calculate gradient with dmode=0')
        elif dmode == 1:
            size = (3,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (12,) + np.shape(self.__fval[0])
        fval = np.zeros(size)
        if dmode > 0:
            fval[0] = self.dr
            fval[1] = self.dz
            fval[2] = self.dp
        if dmode > 1:
            fval[3] = self.drr
            fval[4] = self.drz
            fval[5] = self.drp
            fval[6] = self.drz
            fval[7] = self.dzz
            fval[8] = self.dzp
            fval[9] = self.drp
            fval[10] = self.dzp
            fval[11] = self.dpp
            if self.__torgeom:
                fval[5] -= self.dp / self.__h3
        res = Vector(fval, self.__grid, dmode=dmode - 1,
                     torgeom=self.__torgeom)
        return res

    pass


class Vector:
    '''
    Basic field class for a vector variable.
    '''

    def __init__(self, fval, grid, dmode=2, torgeom=True):
        '''
        Create an instance of the Vector class.

        :param fval: input scalar values
        :param grid: corresponding R,Z,Phi values
        :param dmode: differential order of fval
        :param torgeom: True if toroidal geometry
        '''

        self.__fval = fval
        self.__grid = grid
        self.__dmode = dmode
        self.__torgeom = torgeom
        if torgeom:
            self.__h3 = grid[0:1]
        else:
            self.__h3 = np.zeros(np.shape(grid[0:1])[0])
            self.__h3[:] = 1.0
        self.__set_field()
        return

    def __set_field(self):
        '''
        Set views to the values in fval.
        '''
        self.data = self.__fval[0:3]
        self.r = self.__fval[0]
        self.z = self.__fval[1]
        self.p = self.__fval[2]
        if self.__dmode > 0:
            self.dr = self.__fval[3:6]
            self.dz = self.__fval[6:9]
            self.dp = self.__fval[9:12]  # 1/R d/dPhi
        if self.__dmode > 1:
            self.drr = self.__fval[12:15]
            self.drz = self.__fval[15:18]
            self.drp = self.__fval[18:21]  # 1/R d/dR d/dPhi
            self.dzz = self.__fval[21:24]
            self.dzp = self.__fval[24:27]  # 1/R d/dZ d/dPhi
            self.dpp = self.__fval[27:30]  # 1/R^2 d^2/dPhi^2
        self.mod = np.sqrt(self.r**2 + self.z**2 + self.p**2)
        return

    def dyadic(self, other, dmode=1):
        '''
        Dyadic product between :class:`Vector` and :class:`Vector`

        :param other: other :class:`Vector` type
        :param dmode: output differential order (optional)
        :return: :class:`Tensor` type
        '''
        if type(other) is Vector:
            dmode_aux = np.minimum(self.__dmode, other._Vector__dmode)
            dmode = np.minimum(dmode_aux, dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0])
            fval = np.zeros(size)
            fval[0, 0] = self.r * other.r
            fval[0, 1] = self.r * other.z
            fval[0, 2] = self.r * other.p
            fval[1, 0] = self.z * other.r
            fval[1, 1] = self.z * other.z
            fval[1, 2] = self.z * other.p
            fval[2, 0] = self.p * other.r
            fval[2, 1] = self.p * other.z
            fval[2, 2] = self.p * other.p
            if dmode > 0:
                fval[0, 3] = self.dr[0] * other.r + self.r * other.dr[0]
                fval[0, 4] = self.dr[0] * other.z + self.r * other.dr[1]
                fval[0, 5] = self.dr[0] * other.p + self.r * other.dr[2]
                fval[0, 6] = self.dr[1] * other.r + self.z * other.dr[0]
                fval[0, 7] = self.dr[1] * other.z + self.z * other.dr[1]
                fval[0, 8] = self.dr[1] * other.p + self.z * other.dr[2]
                fval[0, 9] = self.dr[2] * other.r + self.p * other.dr[0]
                fval[0, 10] = self.dr[2] * other.z + self.p * other.dr[1]
                fval[0, 11] = self.dr[2] * other.p + self.p * other.dr[2]
                fval[1, 3] = self.dz[0] * other.r + self.r * other.dz[0]
                fval[1, 4] = self.dz[0] * other.z + self.r * other.dz[1]
                fval[1, 5] = self.dz[0] * other.p + self.r * other.dz[2]
                fval[1, 6] = self.dz[1] * other.r + self.z * other.dz[0]
                fval[1, 7] = self.dz[1] * other.z + self.z * other.dz[1]
                fval[1, 8] = self.dz[1] * other.p + self.z * other.dz[2]
                fval[1, 9] = self.dz[2] * other.r + self.p * other.dz[0]
                fval[1, 10] = self.dz[2] * other.z + self.p * other.dz[1]
                fval[1, 11] = self.dz[2] * other.p + self.p * other.dz[2]
                fval[2, 3] = self.dp[0] * other.r + self.r * other.dp[0]
                fval[2, 4] = self.dp[0] * other.z + self.r * other.dp[1]
                fval[2, 5] = self.dp[0] * other.p + self.r * other.dp[2]
                fval[2, 6] = self.dp[1] * other.r + self.z * other.dp[0]
                fval[2, 7] = self.dp[1] * other.z + self.z * other.dp[1]
                fval[2, 8] = self.dp[1] * other.p + self.z * other.dp[2]
                fval[2, 9] = self.dp[2] * other.r + self.p * other.dp[0]
                fval[2, 10] = self.dp[2] * other.z + self.p * other.dp[1]
                fval[2, 11] = self.dp[2] * other.p + self.p * other.dp[2]
            if dmode > 1:
                raise Exception('Cannot have tensor with dmode =2')
            res = Tensor(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        else:
            raise Exception('Cannot cross with a variable of type'
                            + ' {}'.format(type(other)))
        return res

    def __add__(self, other):
        '''
        Add for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] + other
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty]
                    fval[2 * 3 + iqty] = self.dz[iqty]
                    fval[3 * 3 + iqty] = self.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty]
                    fval[5 * 3 + iqty] = self.drz[iqty]
                    fval[6 * 3 + iqty] = self.drp[iqty]
                    fval[7 * 3 + iqty] = self.dzz[iqty]
                    fval[8 * 3 + iqty] = self.dzp[iqty]
                    fval[9 * 3 + iqty] = self.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Vector:
            dmode = np.minimum(self.__dmode, other._Vector__dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] + other.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty] + other.dr[iqty]
                    fval[2 * 3 + iqty] = self.dz[iqty] + other.dz[iqty]
                    fval[3 * 3 + iqty] = self.dp[iqty] + other.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty] + other.drr[iqty]
                    fval[5 * 3 + iqty] = self.drz[iqty] + other.drz[iqty]
                    fval[6 * 3 + iqty] = self.drp[iqty] + other.drp[iqty]
                    fval[7 * 3 + iqty] = self.dzz[iqty] + other.dzz[iqty]
                    fval[8 * 3 + iqty] = self.dzp[iqty] + other.dzp[iqty]
                    fval[9 * 3 + iqty] = self.dpp[iqty] + other.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot add a Vector with {}'.format(type(other)))

    def __radd__(self, other):
        '''
        Add for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = other + self.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty]
                    fval[2 * 3 + iqty] = self.dz[iqty]
                    fval[3 * 3 + iqty] = self.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty]
                    fval[5 * 3 + iqty] = self.drz[iqty]
                    fval[6 * 3 + iqty] = self.drp[iqty]
                    fval[7 * 3 + iqty] = self.dzz[iqty]
                    fval[8 * 3 + iqty] = self.dzp[iqty]
                    fval[9 * 3 + iqty] = self.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __sub__(self, other):
        '''
        Subtract for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] - other
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty]
                    fval[2 * 3 + iqty] = self.dz[iqty]
                    fval[3 * 3 + iqty] = self.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty]
                    fval[5 * 3 + iqty] = self.drz[iqty]
                    fval[6 * 3 + iqty] = self.drp[iqty]
                    fval[7 * 3 + iqty] = self.dzz[iqty]
                    fval[8 * 3 + iqty] = self.dzp[iqty]
                    fval[9 * 3 + iqty] = self.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Vector:
            dmode = np.minimum(self.__dmode, other._Vector__dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] - other.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty] - other.dr[iqty]
                    fval[2 * 3 + iqty] = self.dz[iqty] - other.dz[iqty]
                    fval[3 * 3 + iqty] = self.dp[iqty] - other.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty] - other.drr[iqty]
                    fval[5 * 3 + iqty] = self.drz[iqty] - other.drz[iqty]
                    fval[6 * 3 + iqty] = self.drp[iqty] - other.drp[iqty]
                    fval[7 * 3 + iqty] = self.dzz[iqty] - other.dzz[iqty]
                    fval[8 * 3 + iqty] = self.dzp[iqty] - other.dzp[iqty]
                    fval[9 * 3 + iqty] = self.dpp[iqty] - other.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            message = 'Cannot subtract a {} from a Vector'.format(type(other))
            raise Exception(message)

    def __rsub__(self, other):
        '''
        Subtract for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = other - self.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = -self.dr[iqty]
                    fval[2 * 3 + iqty] = -self.dz[iqty]
                    fval[3 * 3 + iqty] = -self.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = -self.drr[iqty]
                    fval[5 * 3 + iqty] = -self.drz[iqty]
                    fval[6 * 3 + iqty] = -self.drp[iqty]
                    fval[7 * 3 + iqty] = -self.dzz[iqty]
                    fval[8 * 3 + iqty] = -self.dzp[iqty]
                    fval[9 * 3 + iqty] = -self.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __mul__(self, other):
        '''
        Multiply for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] * other
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.dr[iqty] * other
                    fval[2 * 3 + iqty] = self.dz[iqty] * other
                    fval[3 * 3 + iqty] = self.dp[iqty] * other
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = self.drr[iqty] * other
                    fval[5 * 3 + iqty] = self.drz[iqty] * other
                    fval[6 * 3 + iqty] = self.drp[iqty] * other
                    fval[7 * 3 + iqty] = self.dzz[iqty] * other
                    fval[8 * 3 + iqty] = self.dzp[iqty] * other
                    fval[9 * 3 + iqty] = self.dpp[iqty] * other
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] * other.data
                if dmode > 0:
                    fval[1 * 3 + iqty] = self.data[iqty] * other.dr \
                        + self.dr[iqty] * other.data
                    fval[2 * 3 + iqty] = self.data[iqty] * other.dz \
                        + self.dz[iqty] * other.data
                    fval[3 * 3 + iqty] = self.data[iqty] * other.dp \
                        + self.dp[iqty] * other.data
                if dmode > 1:
                    fval[4 * 3 + iqty] = self.data[iqty] * other.drr \
                        + self.drr[iqty] * other.data \
                        + self.dr[iqty] * other.dr \
                        + self.dr[iqty] * other.dr
                    fval[5 * 3 + iqty] = self.data[iqty] * other.drz \
                        + self.drz[iqty] * other.data \
                        + self.dr[iqty] * other.dz \
                        + self.dz[iqty] * other.dr
                    fval[6 * 3 + iqty] = self.data[iqty] * other.drp \
                        + self.drp[iqty] * other.data \
                        + self.dr[iqty] * other.dp \
                        + self.dp[iqty] * other.dr
                    fval[7 * 3 + iqty] = self.data[iqty] * other.dzz \
                        + self.dzz[iqty] * other.data \
                        + self.dz[iqty] * other.dz \
                        + self.dz[iqty] * other.dz
                    fval[8 * 3 + iqty] = self.data[iqty] * other.dzp \
                        + self.dzp[iqty] * other.data \
                        + self.dz[iqty] * other.dp \
                        + self.dp[iqty] * other.dz
                    fval[9 * 3 + iqty] = self.data[iqty] * other.dpp \
                        + self.dpp[iqty] * other.data \
                        + self.dp[iqty] * other.dp \
                        + self.dp[iqty] * other.dp
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Vector:
            raise Exception("Use dot or dyadic methods to multiply vectors")
        else:
            message = 'Cannot multiply a Vector with {}'.format(type(other))
            raise Exception(message)

    def __rmul__(self, other):
        '''
        Multiply for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = other * self.data[iqty]
                if dmode > 0:
                    fval[1 * 3 + iqty] = other * self.dr[iqty]
                    fval[2 * 3 + iqty] = other * self.dz[iqty]
                    fval[3 * 3 + iqty] = other * self.dp[iqty]
                if self.__dmode > 1:
                    fval[4 * 3 + iqty] = other * self.drr[iqty]
                    fval[5 * 3 + iqty] = other * self.drz[iqty]
                    fval[6 * 3 + iqty] = other * self.drp[iqty]
                    fval[7 * 3 + iqty] = other * self.dzz[iqty]
                    fval[8 * 3 + iqty] = other * self.dzp[iqty]
                    fval[9 * 3 + iqty] = other * self.dpp[iqty]
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __truediv__(self, other):
        '''
        Divide for :class:`Vector` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0:3] = self.data[:] / other
            if dmode > 0:
                fval[3:6] = self.dr[:] / other
                fval[6:9] = self.dz[:] / other
                fval[9:12] = self.dp[:] / other
            if self.__dmode > 1:
                fval[12:15] = self.drr[:] / other
                fval[15:18] = self.drz[:] / other
                fval[18:21] = self.drp[:] / other
                fval[21:24] = self.dzz[:] / other
                fval[24:27] = self.dzp[:] / other
                fval[27:30] = self.dpp[:] / other
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            for iqty in range(3):
                fval[iqty] = self.data[iqty] / other.data
                if dmode > 0:
                    fval[3 + iqty] = ((self.dr[iqty] * other.data
                                       - self.data[iqty] * other.dr)
                                      / other.data**2)
                    fval[6 + iqty] = ((self.dz[iqty] * other.data
                                       - self.data[iqty] * other.dz)
                                      / other.data**2)
                    fval[9 + iqty] = ((self.dp[iqty] * other.data
                                       - self.data[iqty] * other.dp)
                                      / other.data**2)
                if dmode > 1:
                    fval[12 + iqty] = \
                        (self.drr[iqty] * other.data**2
                         - self.data[iqty] * other.drr * other.data
                         + 2.0 * self.data[iqty] * other.dr**2
                         - 2.0 * self.dr[iqty] * other.dr * other.data) \
                        / other.data**3
                    fval[15 + iqty] = \
                        (self.drz[iqty] * other.data**2
                         - self.data[iqty] * other.drz * other.data
                         + 2.0 * self.data[iqty] * other.dr * other.dz
                         - self.dr[iqty] * other.dz * other.data
                         - self.dz[iqty] * other.dr * other.data) \
                        / other.data**3
                    fval[18 + iqty] = \
                        (self.drp[iqty] * other.data**2
                         - self.data[iqty] * other.drp * other.data
                         + 2.0 * self.data[iqty] * other.dr * other.dp
                         - self.dr[iqty] * other.dp * other.data
                         - self.dp[iqty] * other.dr * other.data) \
                        / other.data**3
                    fval[21 + iqty] = \
                        (self.dzz[iqty] * other.data**2
                         - self.data[iqty] * other.dzz * other.data
                         + 2.0 * self.data[iqty] * other.dz**2
                         - 2.0 * self.dz[iqty] * other.dz * other.data) \
                        / other.data**3
                    fval[24 + iqty] = \
                        (self.dzp[iqty] * other.data**2
                         - self.data[iqty] * other.dzp * other.data
                         + 2.0 * self.data[iqty] * other.dz * other.dp
                         - self.dz[iqty] * other.dp * other.data
                         - self.dp[iqty] * other.dz * other.data) \
                        / other.data**3
                    fval[27 + iqty] = \
                        (self.dpp[iqty] * other.data**2
                         - self.data[iqty] * other.dpp * other.data
                         + 2.0 * self.data[iqty] * other.dp**2
                         - 2.0 * self.dp[iqty] * other.dp * other.data) \
                        / other.data**3
            return Vector(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot divide a Vector by {}'.format(type(other)))

    def __neg__(self):
        '''
        Negative for :class:`Vector` type.
        '''

        dmode = self.__dmode
        if dmode == 0:
            size = (3,) + np.shape(self.__fval[0])
        elif dmode == 1:
            size = (12,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (30,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        for iqty in range(3):
            fval[iqty] = -self.data[iqty]
            if dmode > 0:
                fval[1 * 3 + iqty] = -self.dr[iqty]
                fval[2 * 3 + iqty] = -self.dz[iqty]
                fval[3 * 3 + iqty] = -self.dp[iqty]
            if self.__dmode > 1:
                fval[4 * 3 + iqty] = -self.drr[iqty]
                fval[5 * 3 + iqty] = -self.drz[iqty]
                fval[6 * 3 + iqty] = -self.drp[iqty]
                fval[7 * 3 + iqty] = -self.dzz[iqty]
                fval[8 * 3 + iqty] = -self.dzp[iqty]
                fval[9 * 3 + iqty] = -self.dpp[iqty]
        return Vector(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)

    def _test_almost_eq(self, other, rtol=1e-7, atol=0):
        ''' Test two vector instances to see if they are equal '''
        if not isinstance(other, Vector):
            raise TypeError('Can only compare two Vectors')
        self.__test_atr_eq(other)
        self.__test_data_almost_eq(other, rtol=rtol, atol=atol)

    def __test_atr_eq(self, other):
        '''
        Test if self and other have equal attributes
        '''
        assert self.__dmode == other._Vector__dmode
        assert self.__grid.all() == other._Vector__grid.all()
        assert self.__torgeom == other._Vector__torgeom
        assert self.__h3.all() == other._Vector__h3.all()

    def __test_data_almost_eq(self, other, rtol=1e-7, atol=0):
        '''
        Test data arrays in self and other to see if they are equal within
        tolerance.

        Note that the allclose asseration does not commute
        a ~ b and b ~ a can yield different when atol != 0
        '''
        np.testing.assert_allclose(self.__fval, other._Vector__fval,
                                   rtol, atol)
        np.testing.assert_allclose(other._Vector__fval, self.__fval,
                                   rtol, atol)
        np.testing.assert_allclose(self.data, other.data, rtol, atol)
        np.testing.assert_allclose(other.data, self.data, rtol, atol)
        np.testing.assert_allclose(self.r, other.r, rtol, atol)
        np.testing.assert_allclose(other.r, self.r, rtol, atol)
        np.testing.assert_allclose(self.z, other.z, rtol, atol)
        np.testing.assert_allclose(other.z, self.z, rtol, atol)
        np.testing.assert_allclose(self.p, other.p, rtol, atol)
        np.testing.assert_allclose(other.p, self.p, rtol, atol)
        np.testing.assert_allclose(self.mod, other.mod, rtol, atol)
        np.testing.assert_allclose(other.mod, self.mod, rtol, atol)
        if self.__dmode > 0:
            np.testing.assert_allclose(self.dr, other.dr, rtol, atol)
            np.testing.assert_allclose(other.dr, self.dr, rtol, atol)
            np.testing.assert_allclose(self.dz, other.dz, rtol, atol)
            np.testing.assert_allclose(other.dz, self.dz, rtol, atol)
            np.testing.assert_allclose(self.dp, other.dp, rtol, atol)
            np.testing.assert_allclose(other.dp, self.dp, rtol, atol)
        if self.__dmode > 1:
            np.testing.assert_allclose(self.drr, other.drr, rtol, atol)
            np.testing.assert_allclose(other.drr, self.drr, rtol, atol)
            np.testing.assert_allclose(self.drz, other.drz, rtol, atol)
            np.testing.assert_allclose(other.drz, self.drz, rtol, atol)
            np.testing.assert_allclose(self.drp, other.drp, rtol, atol)
            np.testing.assert_allclose(other.drp, self.drp, rtol, atol)
            np.testing.assert_allclose(self.dzz, other.dzz, rtol, atol)
            np.testing.assert_allclose(other.dzz, self.dzz, rtol, atol)
            np.testing.assert_allclose(self.dzp, other.dzp, rtol, atol)
            np.testing.assert_allclose(other.dzp, self.dzp, rtol, atol)
            np.testing.assert_allclose(self.dpp, other.dpp, rtol, atol)
            np.testing.assert_allclose(other.dpp, self.dpp, rtol, atol)

    def grad(self, dmode=1):
        '''
        Gradient of :class:`Vector` type.

        :param dmode: output differential order (optional)
        :return: :class:`Tensor` type
        '''
        dmode = min(self.__dmode, dmode + 1)
        if dmode == 0:
            raise Exception('Cannot calculate gradient with dmode=0')
        elif dmode == 1:
            size = (3, 3,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (3, 12,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        for iqty in range(3):
            fval[0, iqty] = self.dr[iqty]
            fval[1, iqty] = self.dz[iqty]
            fval[2, iqty] = self.dp[iqty]
            if dmode > 1:
                for iqty in range(3):
                    fval[0, iqty + 3] = self.drr[iqty]
                    fval[1, iqty + 3] = self.drz[iqty]
                    fval[2, iqty + 3] = self.drp[iqty]
                    fval[0, iqty + 6] = self.drz[iqty]
                    fval[1, iqty + 6] = self.dzz[iqty]
                    fval[2, iqty + 6] = self.dzp[iqty]
                    fval[0, iqty + 9] = self.drp[iqty]
                    fval[1, iqty + 9] = self.dzp[iqty]
                    fval[2, iqty + 9] = self.dpp[iqty]
        if self.__torgeom:
            fval[2, 0] = fval[2, 0] - self.p / self.__h3
            fval[2, 2] = fval[2, 2] + self.r / self.__h3
            if dmode > 1:
                fval[0, 9] = (fval[0, 9] - self.dp[0] / self.__h3 +
                              self.p / self.__h3**2 - self.dr[2] / self.__h3)
                fval[1, 9] = fval[1, 9] - self.dz[2] / self.__h3
                fval[2, 9] = fval[2, 9] - self.dp[2] / self.__h3

                fval[0, 10] = fval[0, 10] - self.dp[1] / self.__h3

                fval[0, 11] = (fval[0, 11] - self.dp[2] / self.__h3 +
                               self.dr[0] / self.__h3 - self.r / self.__h3**2)
                fval[1, 11] = fval[1, 11] + self.dz[0] / self.__h3
                fval[2, 11] = fval[2, 11] + self.dp[0] / self.__h3

        res = Tensor(fval, self.__grid, dmode=dmode - 1,
                     torgeom=self.__torgeom)
        return res

    def div(self, dmode=1):
        '''
        Divergence of :class:`Vector` type.

        :param dmode: output differential order (optional)
        :return: :class:`Scalar` type
        '''
        dmode = min(self.__dmode, dmode + 1)
        if dmode == 0:
            raise Exception('Cannot calculate divergence with dmode=0')
        elif dmode == 1:
            size = (1,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (4,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        if dmode > 0:
            fval[0] = self.dr[0] + self.dz[1] + self.dp[2]
            if self.__torgeom:
                fval[0] = fval[0] + self.r / self.__h3
        if dmode > 1:
            fval[1] = self.drr[0] + self.drz[1] + self.drp[2]
            fval[2] = self.drz[0] + self.dzz[1] + self.dzp[2]
            fval[3] = self.drp[0] + self.dzp[1] + self.dpp[2]
            if self.__torgeom:
                fval[1] = fval[1] - self.dp[2] / self.__h3 + \
                          self.dr[0] / self.__h3 - self.r / self.__h3**2
                fval[2] = fval[2] + self.dz[0] / self.__h3
                fval[3] = fval[3] + self.dp[0] / self.__h3
        res = Scalar(fval, self.__grid, dmode=dmode - 1,
                     torgeom=self.__torgeom)
        return res

    def curl(self, dmode=1):
        '''
        Curl of :class:`Vector` type.

        :param dmode: output differential order (optional)
        :return: :class:`Vector` type
        '''
        dmode = min(self.__dmode, dmode + 1)
        if dmode == 0:
            raise Exception('Cannot calculate curl with dmode=0')
        elif dmode == 1:
            size = (3,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (12,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        if dmode > 0:
            fval[0] = self.dz[2] - self.dp[1]
            fval[1] = self.dp[0] - self.dr[2]
            fval[2] = self.dr[1] - self.dz[0]
            if self.__torgeom:
                fval[1] = fval[1] - self.p / self.__h3
        if dmode > 1:
            fval[3] = self.drz[2] - self.drp[1]
            fval[4] = self.drp[0] - self.drr[2]
            fval[5] = self.drr[1] - self.drz[0]
            fval[6] = self.dzz[2] - self.dzp[1]
            fval[7] = self.dzp[0] - self.drz[2]
            fval[8] = self.drz[1] - self.dzz[0]
            fval[9] = self.dzp[2] - self.dpp[1]
            fval[10] = self.dpp[0] - self.drp[2]
            fval[11] = self.drp[1] - self.dzp[0]
            if self.__torgeom:
                fval[3] = fval[3] + self.dp[1] / self.__h3
                fval[4] = fval[4] - self.dp[0] / self.__h3 \
                    + self.p / self.__h3**2 \
                    - self.dr[2] / self.__h3
                fval[7] = fval[7] - self.dz[2] / self.__h3
                fval[10] = fval[10] - self.dp[2] / self.__h3
        res = Vector(fval, self.__grid, dmode=dmode - 1,
                     torgeom=self.__torgeom)
        return res

    def dot(self, other, dmode=2):
        '''
        Dot :class:`Vector` type with another :class:`Vector`
        or :class:`Tensor` type.

        :param other: other :class:`Vector` or :class:`Tensor` type
        :param dmode: output differential order (optional)
        :return: :class:`Scalar` or :class:`Vector` type
        '''
        if type(other) is Vector:
            dmode = min(self.__dmode, other._Vector__dmode, dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)

            fval[0] = self.r * other.r + self.z * other.z + self.p * other.p
            if dmode > 0:
                fval[1] = (self.dr[0] * other.r + self.r * other.dr[0] +
                           self.dr[1] * other.z + self.z * other.dr[1] +
                           self.dr[2] * other.p + self.p * other.dr[2])
                fval[2] = (self.dz[0] * other.r + self.r * other.dz[0] +
                           self.dz[1] * other.z + self.z * other.dz[1] +
                           self.dz[2] * other.p + self.p * other.dz[2])
                fval[3] = (self.dp[0] * other.r + self.r * other.dp[0] +
                           self.dp[1] * other.z + self.z * other.dp[1] +
                           self.dp[2] * other.p + self.p * other.dp[2])
            if dmode > 1:
                fval[4] = \
                    (self.drr[0] * other.r + self.r * other.drr[0] +
                     self.dr[0] * other.dr[0] + self.dr[0] * other.dr[0] +
                     self.drr[1] * other.z + self.z * other.drr[1] +
                     self.dr[1] * other.dr[1] + self.dr[1] * other.dr[1] +
                     self.drr[2] * other.p + self.p * other.drr[2] +
                     self.dr[2] * other.dr[2] + self.dr[2] * other.dr[2])
                fval[5] = \
                    (self.drz[0] * other.r + self.r * other.drz[0] +
                     self.dr[0] * other.dz[0] + self.dz[0] * other.dr[0] +
                     self.drz[1] * other.z + self.z * other.drz[1] +
                     self.dr[1] * other.dz[1] + self.dz[1] * other.dr[1] +
                     self.drz[2] * other.p + self.p * other.drz[2] +
                     self.dr[2] * other.dz[2] + self.dz[2] * other.dr[2])
                fval[6] = \
                    (self.drp[0] * other.r + self.r * other.drp[0] +
                     self.dr[0] * other.dp[0] + self.dp[0] * other.dr[0] +
                     self.drp[1] * other.z + self.z * other.drp[1] +
                     self.dr[1] * other.dp[1] + self.dp[1] * other.dr[1] +
                     self.drp[2] * other.p + self.p * other.drp[2] +
                     self.dr[2] * other.dp[2] + self.dp[2] * other.dr[2])
                fval[7] = \
                    (self.dzz[0] * other.r + self.r * other.dzz[0] +
                     self.dz[0] * other.dz[0] + self.dz[0] * other.dz[0] +
                     self.dzz[1] * other.z + self.z * other.dzz[1] +
                     self.dz[1] * other.dz[1] + self.dz[1] * other.dz[1] +
                     self.dzz[2] * other.p + self.p * other.dzz[2] +
                     self.dz[2] * other.dz[2] + self.dz[2] * other.dz[2])
                fval[8] = \
                    (self.dzp[0] * other.r + self.r * other.dzp[0] +
                     self.dz[0] * other.dp[0] + self.dp[0] * other.dz[0] +
                     self.dzp[1] * other.z + self.z * other.dzp[1] +
                     self.dz[1] * other.dp[1] + self.dp[1] * other.dz[1] +
                     self.dzp[2] * other.p + self.p * other.dzp[2] +
                     self.dz[2] * other.dp[2] + self.dp[2] * other.dz[2])
                fval[9] = \
                    (self.dpp[0] * other.r + self.r * other.dpp[0] +
                     self.dp[0] * other.dp[0] + self.dp[0] * other.dp[0] +
                     self.dpp[1] * other.z + self.z * other.dpp[1] +
                     self.dp[1] * other.dp[1] + self.dp[1] * other.dp[1] +
                     self.dpp[2] * other.p + self.p * other.dpp[2] +
                     self.dp[2] * other.dp[2] + self.dp[2] * other.dp[2])
            res = Scalar(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        elif type(other) is Tensor:
            dmode = min(self.__dmode, other._Tensor__dmode, dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.r * other.rr + self.z * other.zr + self.p * other.pr
            fval[1] = self.r * other.rz + self.z * other.zz + self.p * other.pz
            fval[2] = self.r * other.rp + self.z * other.zp + self.p * other.pp
            if dmode > 0:
                fval[3] = \
                    (self.dr[0] * other.rr + self.r * other.dr[0] +
                     self.dr[1] * other.zr + self.z * other.dr[3] +
                     self.dr[2] * other.pr + self.p * other.dr[6])
                fval[4] = \
                    (self.dr[0] * other.rz + self.r * other.dr[1] +
                     self.dr[1] * other.zz + self.z * other.dr[4] +
                     self.dr[2] * other.pz + self.p * other.dr[7])
                fval[5] = \
                    (self.dr[0] * other.rp + self.r * other.dr[2] +
                     self.dr[1] * other.zp + self.z * other.dr[5] +
                     self.dr[2] * other.pp + self.p * other.dr[8])
                fval[6] = \
                    (self.dz[0] * other.rr + self.r * other.dz[0] +
                     self.dz[1] * other.zr + self.z * other.dz[3] +
                     self.dz[2] * other.pr + self.p * other.dz[6])
                fval[7] = \
                    (self.dz[0] * other.rz + self.r * other.dz[1] +
                     self.dz[1] * other.zz + self.z * other.dz[4] +
                     self.dz[2] * other.pz + self.p * other.dz[7])
                fval[8] = \
                    (self.dz[0] * other.rp + self.r * other.dz[2] +
                     self.dz[1] * other.zp + self.z * other.dz[5] +
                     self.dz[2] * other.pp + self.p * other.dz[8])
                fval[9] = \
                    (self.dp[0] * other.rr + self.r * other.dp[0] +
                     self.dp[1] * other.zr + self.z * other.dp[3] +
                     self.dp[2] * other.pr + self.p * other.dp[6])
                fval[10] = \
                    (self.dp[0] * other.rz + self.r * other.dp[1] +
                     self.dp[1] * other.zz + self.z * other.dp[4] +
                     self.dp[2] * other.pz + self.p * other.dp[7])
                fval[11] = \
                    (self.dp[0] * other.rp + self.r * other.dp[2] +
                     self.dp[1] * other.zp + self.z * other.dp[5] +
                     self.dp[2] * other.pp + self.p * other.dp[8])
            res = Vector(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        elif type(other) is list:
            dmode = min(self.__dmode, dmode)
            if dmode == 0:
                size = (1,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (4,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (10,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.r * other[0] + self.z * other[1] + self.p * other[2]
            if dmode > 0:
                fval[1] = (self.dr[0] * other[0] + self.dr[1] * other[1]
                           + self.dr[2] * other[2])
                fval[2] = (self.dz[0] * other[0] + self.dz[1] * other[1]
                           + self.dz[2] * other[2])
                fval[3] = (self.dp[0] * other[0] + self.dp[1] * other[1]
                           + self.dp[2] * other[2])
            if dmode > 1:
                fval[4] = (self.drr[0] * other[0] + self.drr[1] * other[1]
                           + self.drr[2] * other[2])
                fval[5] = (self.drz[0] * other[0] + self.drz[1] * other[1]
                           + self.drz[2] * other[2])
                fval[6] = (self.drp[0] * other[0] + self.drp[1] * other[1]
                           + self.drp[2] * other[2])
                fval[7] = (self.dzz[0] * other[0] + self.dzz[1] * other[1]
                           + self.dzz[2] * other[2])
                fval[8] = (self.dzp[0] * other[0] + self.dzp[1] * other[1]
                           + self.dzp[2] * other[2])
                fval[9] = (self.dpp[0] * other[0] + self.dpp[1] * other[1]
                           + self.dpp[2] * other[2])
            res = Vector(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        else:
            message = 'Cannot dot with a variable of type ' \
                + '{}'.format(type(other))
            raise Exception(message)
        return res

    def cross(self, other, dmode=2):
        '''
        Cross :class:`Vector` type with another :class:`Vector`
        or :class:`Tensor` type.

        :param other: other :class:`Vector` or `Tensor` type
        :param dmode: output differential order (optional)
        :return: :class:`Vector` or `Tensor` type
        '''
        if type(other) is Vector:
            dmode = min(self.__dmode, other._Vector__dmode, dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.z * other.p - self.p * other.z
            fval[1] = self.p * other.r - self.r * other.p
            fval[2] = self.r * other.z - self.z * other.r
            if dmode > 0:
                fval[3] = (self.dr[1] * other.p - self.dr[2] * other.z +
                           self.z * other.dr[2] - self.p * other.dr[1])
                fval[4] = (self.dr[2] * other.r - self.dr[0] * other.p +
                           self.p * other.dr[0] - self.r * other.dr[2])
                fval[5] = (self.dr[0] * other.z - self.dr[1] * other.r +
                           self.r * other.dr[1] - self.z * other.dr[0])
                fval[6] = (self.dz[1] * other.p - self.dz[2] * other.z +
                           self.z * other.dz[2] - self.p * other.dz[1])
                fval[7] = (self.dz[2] * other.r - self.dz[0] * other.p +
                           self.p * other.dz[0] - self.r * other.dz[2])
                fval[8] = (self.dz[0] * other.z - self.dz[1] * other.r +
                           self.r * other.dz[1] - self.z * other.dz[0])
                fval[9] = (self.dp[1] * other.p - self.dp[2] * other.z +
                           self.z * other.dp[2] - self.p * other.dp[1])
                fval[10] = (self.dp[2] * other.r - self.dp[0] * other.p +
                            self.p * other.dp[0] - self.r * other.dp[2])
                fval[11] = (self.dp[0] * other.z - self.dp[1] * other.r +
                            self.r * other.dp[1] - self.z * other.dp[0])
            if dmode > 1:
                raise Exception('Vector: implement cross with dmode=2')
            res = Vector(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        elif type(other) is list:
            dmode = min(self.__dmode, dmode)
            if dmode == 0:
                size = (3,) + np.shape(self.__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(self.__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(self.__fval[0])

            fval = np.zeros(size)
            fval[0] = self.z * other[2] - self.p * other[1]
            fval[1] = self.p * other[0] - self.r * other[2]
            fval[2] = self.r * other[1] - self.z * other[0]
            if dmode > 0:
                fval[3] = self.dr[1] * other[2] - self.dr[2] * other[1]
                fval[4] = self.dr[2] * other[0] - self.dr[0] * other[2]
                fval[5] = self.dr[0] * other[1] - self.dr[1] * other[0]
                fval[6] = self.dz[1] * other[2] - self.dz[2] * other[1]
                fval[7] = self.dz[2] * other[0] - self.dz[0] * other[2]
                fval[8] = self.dz[0] * other[1] - self.dz[1] * other[0]
                fval[9] = self.dp[1] * other[2] - self.dp[2] * other[1]
                fval[10] = self.dp[2] * other[0] - self.dp[0] * other[2]
                fval[11] = self.dp[0] * other[1] - self.dp[1] * other[0]
            if dmode > 1:
                raise Exception('Vector: implement cross with dmode=2')
            res = Vector(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)

        elif type(other) is Tensor:
            dmode = min(self.__dmode, other._Tensor__dmode, dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(other._Tensor__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(other._Tensor__fval[0, 0])

            fval = np.zeros(size)
            fval[0, 0] = self.z * other.pr - self.p * other.zr
            fval[0, 1] = self.z * other.pz - self.p * other.zz
            fval[0, 2] = self.z * other.pp - self.p * other.zp
            fval[1, 0] = self.p * other.rr - self.r * other.pr
            fval[1, 1] = self.p * other.rz - self.r * other.pz
            fval[1, 2] = self.p * other.rp - self.r * other.pp
            fval[2, 0] = self.r * other.zr - self.z * other.rr
            fval[2, 1] = self.r * other.zz - self.z * other.rz
            fval[2, 2] = self.r * other.zp - self.z * other.rp
            if dmode > 0:
                fval[0, 3] = (self.dr[1] * other.pr - self.dr[2] * other.zr +
                              self.z * other.dr[6] - self.p * other.dr[3])
                fval[0, 4] = (self.dr[1] * other.pz - self.dr[2] * other.zz +
                              self.z * other.dr[7] - self.p * other.dr[4])
                fval[0, 5] = (self.dr[1] * other.pp - self.dr[2] * other.zp +
                              self.z * other.dr[8] - self.p * other.dr[5])
                fval[0, 6] = (self.dr[2] * other.rr - self.dr[0] * other.pr +
                              self.p * other.dr[0] - self.r * other.dr[6])
                fval[0, 7] = (self.dr[2] * other.rz - self.dr[0] * other.pz +
                              self.p * other.dr[1] - self.r * other.dr[7])
                fval[0, 8] = (self.dr[2] * other.rp - self.dr[0] * other.pp +
                              self.p * other.dr[2] - self.r * other.dr[8])
                fval[0, 9] = (self.dr[0] * other.zr - self.dr[1] * other.rr +
                              self.r * other.dr[3] - self.z * other.dr[0])
                fval[0, 10] = (self.dr[0] * other.zz - self.dr[1] * other.rz +
                               self.r * other.dr[4] - self.z * other.dr[1])
                fval[0, 11] = (self.dr[0] * other.zp - self.dr[1] * other.rp +
                               self.r * other.dr[5] - self.z * other.dr[2])
                fval[1, 3] = (self.dz[1] * other.pr - self.dz[2] * other.zr +
                              self.z * other.dz[6] - self.p * other.dz[3])
                fval[1, 4] = (self.dz[1] * other.pz - self.dz[2] * other.zz +
                              self.z * other.dz[7] - self.p * other.dz[4])
                fval[1, 5] = (self.dz[1] * other.pp - self.dz[2] * other.zp +
                              self.z * other.dz[8] - self.p * other.dz[5])
                fval[1, 6] = (self.dz[2] * other.rr - self.dz[0] * other.pr +
                              self.p * other.dz[0] - self.r * other.dz[6])
                fval[1, 7] = (self.dz[2] * other.rz - self.dz[0] * other.pz +
                              self.p * other.dz[1] - self.r * other.dz[7])
                fval[1, 8] = (self.dz[2] * other.rp - self.dz[0] * other.pp +
                              self.p * other.dz[2] - self.r * other.dz[8])
                fval[1, 9] = (self.dz[0] * other.zr - self.dz[1] * other.rr +
                              self.r * other.dz[3] - self.z * other.dz[0])
                fval[1, 10] = (self.dz[0] * other.zz - self.dz[1] * other.rz +
                               self.r * other.dz[4] - self.z * other.dz[1])
                fval[1, 11] = (self.dz[0] * other.zp - self.dz[1] * other.rp +
                               self.r * other.dz[5] - self.z * other.dz[2])
                fval[2, 3] = (self.dp[1] * other.pr - self.dp[2] * other.zr +
                              self.z * other.dp[6] - self.p * other.dp[3])
                fval[2, 4] = (self.dp[1] * other.pz - self.dp[2] * other.zz +
                              self.z * other.dp[7] - self.p * other.dp[4])
                fval[2, 5] = (self.dp[1] * other.pp - self.dp[2] * other.zp +
                              self.z * other.dp[8] - self.p * other.dp[5])
                fval[2, 6] = (self.dp[2] * other.rr - self.dp[0] * other.pr +
                              self.p * other.dp[0] - self.r * other.dp[6])
                fval[2, 7] = (self.dp[2] * other.rz - self.dp[0] * other.pz +
                              self.p * other.dp[1] - self.r * other.dp[7])
                fval[2, 8] = (self.dp[2] * other.rp - self.dp[0] * other.pp +
                              self.p * other.dp[2] - self.r * other.dp[8])
                fval[2, 9] = (self.dp[0] * other.zr - self.dp[1] * other.rr +
                              self.r * other.dp[3] - self.z * other.dp[0])
                fval[2, 10] = (self.dp[0] * other.zz - self.dp[1] * other.rz +
                               self.r * other.dp[4] - self.z * other.dp[1])
                fval[2, 11] = (self.dp[0] * other.zp - self.dp[1] * other.rp +
                               self.r * other.dp[5] - self.z * other.dp[2])
            if dmode > 1:
                raise Exception('Tensor: implement cross with dmode=2')
            res = Tensor(fval, other._Tensor__grid, dmode=dmode,
                         torgeom=other._Tensor__torgeom)
        else:
            raise Exception('Cannot cross with a variable of type'
                            + ' {}'.format(type(other)))

        return res

    def mag(self, dmode=2):
        '''
        Magnitude of a :class:`Vector` type.

        :param dmode: output differential order (optional)
        :return: :class:`Scalar` type
        '''
        dmode = min(dmode, self.__dmode)
        if dmode == 0:
            size = (1,) + np.shape(self.__fval[0])
        elif dmode == 1:
            size = (4,) + np.shape(self.__fval[0])
        elif dmode == 2:
            size = (10,) + np.shape(self.__fval[0])

        fval = np.zeros(size)
        fval[0] = self.mod

        if dmode > 0:
            fval[1] = ((self.r * self.dr[0] + self.z * self.dr[1]
                        + self.p * self.dr[2]) / self.mod)
            fval[2] = ((self.r * self.dz[0] + self.z * self.dz[1]
                        + self.p * self.dz[2]) / self.mod)
            fval[3] = ((self.r * self.dp[0] + self.z * self.dp[1]
                        + self.p * self.dp[2]) / self.mod)

        if dmode > 1:
            raise Exception('Implement Vector::mag with dmode=2')

        res = Scalar(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)
        return res

    def hat(self, dmode=2):
        '''
        Return the unit vector in the direction of :class:`Vector`.

        :param dmode: output differential order (optional)
        :return: :class:`Vector` type
        '''
        dmode = min(dmode, self.__dmode)
        res = self / self.mag(dmode)
        return res

    pass


class Tensor:
    '''
    Basic field class for a tensor variable.
    '''

    def __init__(self, fval, grid, dmode=1, torgeom=True):
        '''
        Create an instance of the Tensor class.

        :param fval: input scalar values
        :param grid: corresponding R,Z,Phi values
        :param dmode: differential order of fval
        :param torgeom: True if toroidal geometry
        '''

        self.__fval = fval
        self.__grid = grid
        self.__dmode = dmode
        self.__torgeom = torgeom
        if torgeom:
            self.__h3 = grid[0:1]
        else:
            self.__h3 = np.zeros(np.shape(grid[0:1])[0])
            self.__h3[:] = 1.0
        self.__set_field()
        return

    def __set_field(self):
        '''
        Set views to the values in fval.
        '''
        self.data = self.__fval[:, 0:3]
        self.rr = self.__fval[0, 0]
        self.rz = self.__fval[0, 1]
        self.rp = self.__fval[0, 2]
        self.zr = self.__fval[1, 0]
        self.zz = self.__fval[1, 1]
        self.zp = self.__fval[1, 2]
        self.pr = self.__fval[2, 0]
        self.pz = self.__fval[2, 1]
        self.pp = self.__fval[2, 2]

        if self.__dmode > 0:
            self.dr = self.__fval[0, 3:]
            self.dz = self.__fval[1, 3:]
            self.dp = self.__fval[2, 3:]
        if self.__dmode > 1:
            raise Exception("Tensor not implemented with dmode > 1")
        return

    def _test_almost_eq(self, other, rtol=1e-7, atol=0):
        ''' Test two tensor instances to see if they are equal '''
        if not isinstance(other, Tensor):
            raise TypeError('Can only compare two Tensors')
        self.__test_atr_eq(other)
        self.__test_data_almost_eq(other, rtol=rtol, atol=atol)

    def __test_atr_eq(self, other):
        '''
        Test if self and other have equal attributes
        '''
        assert self.__dmode == other._Tensor__dmode
        assert self.__grid.all() == other._Tensor__grid.all()
        assert self.__torgeom == other._Tensor__torgeom
        assert self.__h3.all() == other._Tensor__h3.all()

    def __test_data_almost_eq(self, other, rtol=1e-7, atol=0):
        '''
        Test data arrays in self and other to see if they are equal within
        tolerance.

        Note that the allclose asseration does not commute
        a ~ b and b ~ a can yield different when atol != 0
        '''
        np.testing.assert_allclose(self.__fval, other._Tensor__fval,
                                   rtol, atol)
        np.testing.assert_allclose(other._Tensor__fval, self.__fval,
                                   rtol, atol)
        np.testing.assert_allclose(self.data, other.data, rtol, atol)
        np.testing.assert_allclose(other.data, self.data, rtol, atol)
        np.testing.assert_allclose(self.rr, other.rr, rtol, atol)
        np.testing.assert_allclose(other.rr, self.rr, rtol, atol)
        np.testing.assert_allclose(self.rz, other.rz, rtol, atol)
        np.testing.assert_allclose(other.rz, self.rz, rtol, atol)
        np.testing.assert_allclose(self.rp, other.rp, rtol, atol)
        np.testing.assert_allclose(other.rp, self.rp, rtol, atol)
        np.testing.assert_allclose(self.zr, other.zr, rtol, atol)
        np.testing.assert_allclose(other.zr, self.zr, rtol, atol)
        np.testing.assert_allclose(self.zz, other.zz, rtol, atol)
        np.testing.assert_allclose(other.zz, self.zz, rtol, atol)
        np.testing.assert_allclose(self.zp, other.zp, rtol, atol)
        np.testing.assert_allclose(other.zp, self.zp, rtol, atol)
        np.testing.assert_allclose(self.pr, other.pr, rtol, atol)
        np.testing.assert_allclose(other.pr, self.pr, rtol, atol)
        np.testing.assert_allclose(self.pz, other.pz, rtol, atol)
        np.testing.assert_allclose(other.pz, self.pz, rtol, atol)
        np.testing.assert_allclose(self.pp, other.pp, rtol, atol)
        np.testing.assert_allclose(other.pp, self.pp, rtol, atol)
        if self.__dmode > 0:
            np.testing.assert_allclose(self.dr, other.dr, rtol, atol)
            np.testing.assert_allclose(other.dr, self.dr, rtol, atol)
            np.testing.assert_allclose(self.dz, other.dz, rtol, atol)
            np.testing.assert_allclose(other.dz, self.dz, rtol, atol)
            np.testing.assert_allclose(self.dp, other.dp, rtol, atol)
            np.testing.assert_allclose(other.dp, self.dp, rtol, atol)

    def cross(self, other, dmode=2):
        '''
        Cross :class:`Tensor` type with another :class:`Vector` type
        when :class:`Tensor` is on the left of :class:`Vector`

        :param other: other :class:`Vector` type
        :param dmode: output differential order (optional)
        :return: :class:`Tensor` type
        '''
        if type(other) is Vector:
            dmode = min(self.__dmode, other._Vector__dmode, dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0, 0] = self.rz * other.p - self.rp * other.z
            fval[0, 1] = self.rp * other.r - self.rr * other.p
            fval[0, 2] = self.rr * other.z - self.rz * other.r
            fval[1, 0] = self.zz * other.p - self.zp * other.z
            fval[1, 1] = self.zp * other.r - self.zr * other.p
            fval[1, 2] = self.zr * other.z - self.zz * other.r
            fval[2, 0] = self.pz * other.p - self.pp * other.z
            fval[2, 1] = self.pp * other.r - self.pr * other.p
            fval[2, 2] = self.pr * other.z - self.pz * other.r
            if dmode > 0:
                fval[0, 3] = (self.dr[1] * other.p - self.dr[2] * other.z +
                              self.rz * other.dr[2] - self.rp * other.dr[1])
                fval[0, 4] = (self.dr[2] * other.r - self.dr[0] * other.p +
                              self.rp * other.dr[0] - self.rr * other.dr[2])
                fval[0, 5] = (self.dr[0] * other.z - self.dr[1] * other.r +
                              self.rr * other.dr[1] - self.rz * other.dr[0])
                fval[0, 6] = (self.dr[4] * other.p - self.dr[5] * other.z +
                              self.zz * other.dr[2] - self.zp * other.dr[1])
                fval[0, 7] = (self.dr[5] * other.r - self.dr[3] * other.p +
                              self.zp * other.dr[0] - self.zr * other.dr[2])
                fval[0, 8] = (self.dr[3] * other.z - self.dr[4] * other.r +
                              self.zr * other.dr[1] - self.zz * other.dr[0])
                fval[0, 9] = (self.dr[7] * other.p - self.dr[8] * other.z +
                              self.pz * other.dr[2] - self.pp * other.dr[1])
                fval[0, 10] = (self.dr[8] * other.r - self.dr[6] * other.p +
                               self.pp * other.dr[0] - self.pr * other.dr[2])
                fval[0, 11] = (self.dr[6] * other.z - self.dr[7] * other.r +
                               self.pr * other.dr[1] - self.pz * other.dr[0])
                fval[1, 3] = (self.dz[1] * other.p - self.dz[2] * other.z +
                              self.rz * other.dz[2] - self.rp * other.dz[1])
                fval[1, 4] = (self.dz[2] * other.r - self.dz[0] * other.p +
                              self.rp * other.dz[0] - self.rr * other.dz[2])
                fval[1, 5] = (self.dz[0] * other.z - self.dz[1] * other.r +
                              self.rr * other.dz[1] - self.rz * other.dz[0])
                fval[1, 6] = (self.dz[4] * other.p - self.dz[5] * other.z +
                              self.zz * other.dz[2] - self.zp * other.dz[1])
                fval[1, 7] = (self.dz[5] * other.r - self.dz[3] * other.p +
                              self.zp * other.dz[0] - self.zr * other.dz[2])
                fval[1, 8] = (self.dz[3] * other.z - self.dz[4] * other.r +
                              self.zr * other.dz[1] - self.zz * other.dz[0])
                fval[1, 9] = (self.dz[7] * other.p - self.dz[8] * other.z +
                              self.pz * other.dz[2] - self.pp * other.dz[1])
                fval[1, 10] = (self.dz[8] * other.r - self.dz[6] * other.p +
                               self.pp * other.dz[0] - self.pr * other.dz[2])
                fval[1, 11] = (self.dz[6] * other.z - self.dz[7] * other.r +
                               self.pr * other.dz[1] - self.pz * other.dz[0])
                fval[2, 3] = (self.dp[1] * other.p - self.dp[2] * other.z +
                              self.rz * other.dp[2] - self.rp * other.dp[1])
                fval[2, 4] = (self.dp[2] * other.r - self.dp[0] * other.p +
                              self.rp * other.dp[0] - self.rr * other.dp[2])
                fval[2, 5] = (self.dp[0] * other.z - self.dp[1] * other.r +
                              self.rr * other.dp[1] - self.rz * other.dp[0])
                fval[2, 6] = (self.dp[4] * other.p - self.dp[5] * other.z +
                              self.zz * other.dp[2] - self.zp * other.dp[1])
                fval[2, 7] = (self.dp[5] * other.r - self.dp[3] * other.p +
                              self.zp * other.dp[0] - self.zr * other.dp[2])
                fval[2, 8] = (self.dp[3] * other.z - self.dp[4] * other.r +
                              self.zr * other.dp[1] - self.zz * other.dp[0])
                fval[2, 9] = (self.dp[7] * other.p - self.dp[8] * other.z +
                              self.pz * other.dp[2] - self.pp * other.dp[1])
                fval[2, 10] = (self.dp[8] * other.r - self.dp[6] * other.p +
                               self.pp * other.dp[0] - self.pr * other.dp[2])
                fval[2, 11] = (self.dp[6] * other.z - self.dp[7] * other.r +
                               self.pr * other.dp[1] - self.pz * other.dp[0])
        else:
            raise Exception('Cannot cross with a variable of type'
                            + ' {}'.format(type(other)))
        res = Tensor(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)
        return res

    def dot(self, other, dmode=2):
        '''
        Dot product between :class:`Tensor` and :class:`Vector`
        when :class:`Tensor` is on the left of :class:`Vector`
        or dot product between :class:`Tensor` and :class:`Vector`

        :param other: :class:`Vector` or :class:`Tensor` type
        :param dmode: output differential order (optional)
        :return: :class:`Vector` or :class:`Tensor` type
        '''
        if type(other) is Vector:
            dmode = min(self.__dmode, other._Vector__dmode, dmode)
            if dmode == 0:
                size = (3,) + np.shape(other._Vector__fval[0])
            elif dmode == 1:
                size = (12,) + np.shape(other._Vector__fval[0])
            elif dmode == 2:
                size = (30,) + np.shape(other._Vector__fval[0])

            fval = np.zeros(size)
            fval[0] = self.rr * other.r + self.rz * other.z + self.rp * other.p
            fval[1] = self.zr * other.r + self.zz * other.z + self.zp * other.p
            fval[2] = self.pr * other.r + self.pz * other.z + self.pp * other.p
            if dmode > 0:
                fval[3] = \
                    (self.dr[0] * other.r + self.rr * other.dr[0] +
                     self.dr[1] * other.z + self.rz * other.dr[1] +
                     self.dr[2] * other.p + self.rp * other.dr[2])
                fval[4] = \
                    (self.dr[3] * other.r + self.zr * other.dr[0] +
                     self.dr[4] * other.z + self.zz * other.dr[1] +
                     self.dr[5] * other.p + self.zp * other.dr[2])
                fval[5] = \
                    (self.dr[6] * other.r + self.pr * other.dr[0] +
                     self.dr[7] * other.z + self.pz * other.dr[1] +
                     self.dr[8] * other.p + self.pp * other.dr[2])
                fval[6] = \
                    (self.dz[0] * other.r + self.rr * other.dz[0] +
                     self.dz[1] * other.z + self.rz * other.dz[1] +
                     self.dz[2] * other.p + self.rp * other.dz[2])
                fval[7] = \
                    (self.dz[3] * other.r + self.zr * other.dz[0] +
                     self.dz[4] * other.z + self.zz * other.dz[1] +
                     self.dz[5] * other.p + self.zp * other.dz[2])
                fval[8] = \
                    (self.dz[6] * other.r + self.pr * other.dz[0] +
                     self.dz[7] * other.z + self.pz * other.dz[1] +
                     self.dz[8] * other.p + self.pp * other.dz[2])
                fval[9] = \
                    (self.dp[0] * other.r + self.rr * other.dp[0] +
                     self.dp[1] * other.z + self.rz * other.dp[1] +
                     self.dp[2] * other.p + self.rp * other.dp[2])
                fval[10] = \
                    (self.dp[3] * other.r + self.zr * other.dp[0] +
                     self.dp[4] * other.z + self.zz * other.dp[1] +
                     self.dp[5] * other.p + self.zp * other.dp[2])
                fval[11] = \
                    (self.dp[6] * other.r + self.pr * other.dp[0] +
                     self.dp[7] * other.z + self.pz * other.dp[1] +
                     self.dp[8] * other.p + self.pp * other.dp[2])
            res = Vector(fval, other._Vector__grid, dmode=dmode,
                         torgeom=other._Vector__torgeom)
        elif type(other) is Tensor:
            dmode = min(self.__dmode, other._Tensor__dmode, dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])
            fval = np.zeros(size)
            fval[0, 0] = (self.rr * other.rr + self.rz * other.zr +
                          self.rp * other.pr)
            fval[0, 1] = (self.rr * other.rz + self.rz * other.zz +
                          self.rp * other.pz)
            fval[0, 2] = (self.rr * other.rp + self.rz * other.zp +
                          self.rp * other.pp)
            fval[1, 0] = (self.zr * other.rr + self.zz * other.zr +
                          self.zp * other.pr)
            fval[1, 1] = (self.zr * other.rz + self.zz * other.zz +
                          self.zp * other.pz)
            fval[1, 2] = (self.zr * other.rp + self.zz * other.zp +
                          self.zp * other.pp)
            fval[2, 0] = (self.pr * other.rr + self.pz * other.zr +
                          self.pp * other.pr)
            fval[2, 1] = (self.pr * other.rz + self.pz * other.zz +
                          self.pp * other.pz)
            fval[2, 2] = (self.pr * other.rp + self.pz * other.zp +
                          self.pp * other.pp)
            if dmode > 0:
                fval[0, 3] = (self.dr[0] * other.rr + self.rr * other.dr[0] +
                              self.dr[1] * other.zr + self.rz * other.dr[3] +
                              self.dr[2] * other.pr + self.rp * other.dr[6])
                fval[0, 4] = (self.dr[0] * other.rz + self.rr * other.dr[1] +
                              self.dr[1] * other.zz + self.rz * other.dr[4] +
                              self.dr[2] * other.pz + self.rp * other.dr[7])
                fval[0, 5] = (self.dr[0] * other.rp + self.rr * other.dr[2] +
                              self.dr[1] * other.zp + self.rz * other.dr[5] +
                              self.dr[2] * other.pp + self.rp * other.dr[8])
                fval[0, 6] = (self.dr[3] * other.rr + self.zr * other.dr[0] +
                              self.dr[4] * other.zr + self.zz * other.dr[3] +
                              self.dr[5] * other.pr + self.zp * other.dr[6])
                fval[0, 7] = (self.dr[3] * other.rz + self.zr * other.dr[1] +
                              self.dr[4] * other.zz + self.zz * other.dr[4] +
                              self.dr[5] * other.pz + self.zp * other.dr[7])
                fval[0, 8] = (self.dr[3] * other.rp + self.zr * other.dr[2] +
                              self.dr[4] * other.zp + self.zz * other.dr[5] +
                              self.dr[5] * other.pp + self.zp * other.dr[8])
                fval[0, 9] = (self.dr[6] * other.rr + self.pr * other.dr[0] +
                              self.dr[7] * other.zr + self.pz * other.dr[3] +
                              self.dr[8] * other.pr + self.pp * other.dr[6])
                fval[0, 10] = (self.dr[6] * other.rz + self.pr * other.dr[1] +
                               self.dr[7] * other.zz + self.pz * other.dr[4] +
                               self.dr[8] * other.pz + self.pp * other.dr[7])
                fval[0, 11] = (self.dr[6] * other.rp + self.pr * other.dr[2] +
                               self.dr[7] * other.zp + self.pz * other.dr[5] +
                               self.dr[8] * other.pp + self.pp * other.dr[8])
                fval[1, 3] = (self.dz[0] * other.rr + self.rr * other.dz[0] +
                              self.dz[1] * other.zr + self.rz * other.dz[3] +
                              self.dz[2] * other.pr + self.rp * other.dz[6])
                fval[1, 4] = (self.dz[0] * other.rz + self.rr * other.dz[1] +
                              self.dz[1] * other.zz + self.rz * other.dz[4] +
                              self.dz[2] * other.pz + self.rp * other.dz[7])
                fval[1, 5] = (self.dz[0] * other.rp + self.rr * other.dz[2] +
                              self.dz[1] * other.zp + self.rz * other.dz[5] +
                              self.dz[2] * other.pp + self.rp * other.dz[8])
                fval[1, 6] = (self.dz[3] * other.rr + self.zr * other.dz[0] +
                              self.dz[4] * other.zr + self.zz * other.dz[3] +
                              self.dz[5] * other.pr + self.zp * other.dz[6])
                fval[1, 7] = (self.dz[3] * other.rz + self.zr * other.dz[1] +
                              self.dz[4] * other.zz + self.zz * other.dz[4] +
                              self.dz[5] * other.pz + self.zp * other.dz[7])
                fval[1, 8] = (self.dz[3] * other.rp + self.zr * other.dz[2] +
                              self.dz[4] * other.zp + self.zz * other.dz[5] +
                              self.dz[5] * other.pp + self.zp * other.dz[8])
                fval[1, 9] = (self.dz[6] * other.rr + self.pr * other.dz[0] +
                              self.dz[7] * other.zr + self.pz * other.dz[3] +
                              self.dz[8] * other.pr + self.pp * other.dz[6])
                fval[1, 10] = (self.dz[6] * other.rz + self.pr * other.dz[1] +
                               self.dz[7] * other.zz + self.pz * other.dz[4] +
                               self.dz[8] * other.pz + self.pp * other.dz[7])
                fval[1, 11] = (self.dz[6] * other.rp + self.pr * other.dz[2] +
                               self.dz[7] * other.zp + self.pz * other.dz[5] +
                               self.dz[8] * other.pp + self.pp * other.dz[8])
                fval[2, 3] = (self.dp[0] * other.rr + self.rr * other.dp[0] +
                              self.dp[1] * other.zr + self.rz * other.dp[3] +
                              self.dp[2] * other.pr + self.rp * other.dp[6])
                fval[2, 4] = (self.dp[0] * other.rz + self.rr * other.dp[1] +
                              self.dp[1] * other.zz + self.rz * other.dp[4] +
                              self.dp[2] * other.pz + self.rp * other.dp[7])
                fval[2, 5] = (self.dp[0] * other.rp + self.rr * other.dp[2] +
                              self.dp[1] * other.zp + self.rz * other.dp[5] +
                              self.dp[2] * other.pp + self.rp * other.dp[8])
                fval[2, 6] = (self.dp[3] * other.rr + self.zr * other.dp[0] +
                              self.dp[4] * other.zr + self.zz * other.dp[3] +
                              self.dp[5] * other.pr + self.zp * other.dp[6])
                fval[2, 7] = (self.dp[3] * other.rz + self.zr * other.dp[1] +
                              self.dp[4] * other.zz + self.zz * other.dp[4] +
                              self.dp[5] * other.pz + self.zp * other.dp[7])
                fval[2, 8] = (self.dp[3] * other.rp + self.zr * other.dp[2] +
                              self.dp[4] * other.zp + self.zz * other.dp[5] +
                              self.dp[5] * other.pp + self.zp * other.dp[8])
                fval[2, 9] = (self.dp[6] * other.rr + self.pr * other.dp[0] +
                              self.dp[7] * other.zr + self.pz * other.dp[3] +
                              self.dp[8] * other.pr + self.pp * other.dp[6])
                fval[2, 10] = (self.dp[6] * other.rz + self.pr * other.dp[1] +
                               self.dp[7] * other.zz + self.pz * other.dp[4] +
                               self.dp[8] * other.pz + self.pp * other.dp[7])
                fval[2, 11] = (self.dp[6] * other.rp + self.pr * other.dp[2] +
                               self.dp[7] * other.zp + self.pz * other.dp[5] +
                               self.dp[8] * other.pp + self.pp * other.dp[8])
            if dmode > 1:
                raise Exception('Max. dmode for tensors is dmode = 1')
            res = Tensor(fval, self.__grid, dmode=dmode,
                         torgeom=self.__torgeom)
        else:
            message = 'Cannot dot with a variable of type ' \
                + '{}'.format(type(other))
            raise Exception(message)
        return res

    def __add__(self, other):
        '''
        Add for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data + other
            if dmode > 0:
                fval[0, 3:] = self.dr
                fval[1, 3:] = self.dz
                fval[2, 3:] = self.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Tensor:
            dmode = np.minimum(self.__dmode, other._Tensor__dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data + other.data
            if dmode > 0:
                fval[0, 3:] = self.dr + other.dr
                fval[1, 3:] = self.dz + other.dz
                fval[2, 3:] = self.dp + other.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot add a Tensor with {}'.format(type(other)))

    def __radd__(self, other):
        '''
        Add for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = other + self.data
            if dmode > 0:
                fval[0, 3:] = self.dr
                fval[1, 3:] = self.dz
                fval[2, 3:] = self.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __sub__(self, other):
        '''
        Subtract for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data - other
            if dmode > 0:
                fval[0, 3:] = self.dr
                fval[1, 3:] = self.dz
                fval[2, 3:] = self.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Tensor:
            dmode = np.minimum(self.__dmode, other._Tensor__dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data - other.data
            if dmode > 0:
                fval[0, 3:] = self.dr - other.dr
                fval[1, 3:] = self.dz - other.dz
                fval[2, 3:] = self.dp - other.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot subtract a {}'.format(type(other))
                            + ' from a Tensor')

    def __rsub__(self, other):
        '''
        Subtract for :class:`Tensor` type.
        '''
        dmode = self.__dmode
        if dmode == 0:
            size = (3, 3,) + np.shape(self.__fval[0, 0])
        elif dmode == 1:
            size = (3, 12,) + np.shape(self.__fval[0, 0])

        fval = np.zeros(size)
        fval[0:3, 0:3] = other - self.data
        if dmode > 0:
            fval[0, 3:] = -self.dr
            fval[1, 3:] = -self.dz
            fval[2, 3:] = -self.dp
        return Tensor(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)

    def __mul__(self, other):
        '''
        Multiply for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data * other
            if dmode > 0:
                fval[0, 3:] = self.dr * other
                fval[1, 3:] = self.dz * other
                fval[2, 3:] = self.dp * other
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12) + np.shape(self.__fval[0, 0])
            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data * other.data
            if dmode > 0:
                fval[0, 3:] = \
                    (self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dr + self.dr * other.data)
                fval[1, 3:] = \
                    (self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dz + self.dz * other.data)
                fval[2, 3:] = \
                    (self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dp + self.dp * other.data)
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot multiply a Tensor with '
                            + '{}'.format(type(other)))

    def __rmul__(self, other):
        '''
        Multiply for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = other * self.data
            if dmode > 0:
                fval[0, 3:] = other * self.dr
                fval[1, 3:] = other * self.dz
                fval[2, 3:] = other * self.dp
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)

    def __truediv__(self, other):
        '''
        Divide for :class:`Tensor` type.
        '''
        if type(other) is int or type(other) is float:
            dmode = self.__dmode
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data / other
            if dmode > 0:
                fval[0, 3:] = self.dr / other
                fval[1, 3:] = self.dz / other
                fval[2, 3:] = self.dp / other
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        elif type(other) is Scalar:
            dmode = np.minimum(self.__dmode, other._Scalar__dmode)
            if dmode == 0:
                size = (3, 3,) + np.shape(self.__fval[0, 0])
            elif dmode == 1:
                size = (3, 12,) + np.shape(self.__fval[0, 0])

            fval = np.zeros(size)
            fval[0:3, 0:3] = self.data / other.data
            if dmode > 0:
                fval[0, 3:] = \
                    (self.dr * other.data
                     - self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dr) / other.data**2
                fval[1, 3:] = \
                    (self.dz * other.data
                     - self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dz) / other.data**2
                fval[2, 3:] = \
                    (self.dp * other.data
                     - self.data.reshape((9,) + np.shape(self.data[0, 0]))
                     * other.dp) / other.data**2
            return Tensor(fval, self.__grid, dmode=dmode,
                          torgeom=self.__torgeom)
        else:
            raise Exception('Cannot divide a Tensor by {}'.format(type(other)))

    def __neg__(self):
        '''
        Negative for :class:`Tensor` type.
        '''
        dmode = self.__dmode
        if dmode == 0:
            size = (3, 3,) + np.shape(self.__fval[0, 0])
        elif dmode == 1:
            size = (3, 12,) + np.shape(self.__fval[0, 0])

        fval = np.zeros(size)
        fval[0:3, 0:3] = -self.data
        if dmode > 0:
            fval[0, 3:] = -self.dr
            fval[1, 3:] = -self.dz
            fval[2, 3:] = -self.dp
        return Tensor(fval, self.__grid, dmode=dmode, torgeom=self.__torgeom)

    def div(self, dmode=1):
        '''
        Divergence of a :class:`Tensor` type.

        :param dmode: output differential order (optional)
        :return: :class:`Vector` type
        '''
        dmode = min(self.__dmode, dmode + 1)
        if dmode > 0:
            fval = np.zeros((3,) + np.shape(self.__fval[0, 0]))
            fval[0] = self.dr[0] + self.dz[3] + self.dp[6]
            fval[1] = self.dr[1] + self.dz[4] + self.dp[7]
            fval[2] = self.dr[2] + self.dz[5] + self.dp[8]
            if self.__torgeom:
                fval[0] = fval[0] + (self.rr - self.pp) / self.__h3
                fval[1] = fval[1] + self.rz / self.__h3
                fval[2] = fval[2] + (self.rp + self.pr) / self.__h3
            res = Vector(fval, self.__grid, dmode=dmode - 1,
                         torgeom=self.__torgeom)
        else:
            raise Exception('Cannot calculate divergence with dmode=0')
        return res

    def transpose(self):
        '''
        Transpose of a :class:`Tensor` type.

        :return: :class:`Tensor` type
        '''
        fval = np.zeros(np.shape(self.__fval))
        for iqty in range(3):
            for jqty in range(3):
                fval[iqty, jqty] = self.data[jqty, iqty]
        if self.__dmode > 0:
            for iqty in range(3):
                for jqty in range(3):
                    fval[0, iqty * 3 + jqty + 3] = \
                        self.__fval[0, jqty * 3 + iqty + 3]
                    fval[1, iqty * 3 + jqty + 3] = \
                        self.__fval[1, jqty * 3 + iqty + 3]
                    fval[2, iqty * 3 + jqty + 3] = \
                        self.__fval[2, jqty * 3 + iqty + 3]

        res = Tensor(fval, self.__grid, dmode=self.__dmode,
                     torgeom=self.__torgeom)
        return res

    pass


def grad(field, dmode=1):
    '''
    Calls appropriate gradient function for field.

    :param dmode: output differential order (optional)
    :return: resulting field
    '''
    if type(field) is Scalar:
        res = field.grad(dmode=dmode)
    elif type(field) is Vector:
        res = field.grad(dmode=dmode)
    else:
        raise Exception('Gradient operator is not defined for type '
                        + '{}'.format(type(field)))
    return res


def div(field, dmode=1):
    '''
    Calls appropriate divergence function for field.

    :param dmode: output differential order (optional)
    :return: resulting field
    '''
    if type(field) is Vector:
        res = field.div(dmode=dmode)
    elif type(field) is Tensor:
        res = field.div(dmode=dmode)
    else:
        raise Exception('Divergence operator is not defined for type '
                        + '{}'.format(type(field)))
    return res


def curl(field, dmode=1):
    '''
    Calls appropriate curl function for field.

    :param dmode: output differential order (optional)
    :return: resulting field
    '''
    if type(field) is Vector:
        res = field.curl(dmode=dmode)
    else:
        raise Exception('Curl operator is not defined for type '
                        + '{}'.format(type(field)))
    return res


def eye(grid):
    '''
    Returns the identity tensor.

    :return: identity tensor
    '''
    fval = np.zeros((3, 12,) + np.shape(grid[0]))
    for iqty in range(3):
        fval[iqty, iqty] = 1.0
    res = Tensor(fval, grid)
    return res


def basis_vector(direction, grid, torgeom=True):
    '''
    Returns a basis vector in the desired direction.

    :param direction: direction index for basis
        0->e_R
        1->e_Z
        2->e_phi
    :param grid: corresponding R,Z,Phi values
    :param torgeom: True if toroidal geometry
    :return: resulting :class:`Vector`
    '''
    fval = np.zeros((30,) + np.shape(grid[0]))
    if direction in ['r', 'R', 0]:
        fval[0] = 1.0
    elif direction in ['z', 'Z', 1]:
        fval[1] = 1.0
    elif direction in ['p', 'P', 2]:
        fval[2] = 1.0
    else:
        message = 'Basis direction not reconized {}'.format(direction)
        raise Exception(message)

    res = Vector(fval, grid, dmode=2, torgeom=torgeom)
    return res
