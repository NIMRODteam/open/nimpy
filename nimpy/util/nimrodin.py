'''
Read NIMROD nimrod.in input file and save namelists as dictonary
with default inputs
'''

import f90nml
import numpy as np
import warnings


class NimrodIn:
    '''
    Read and parse nimrod.in input file

    The class breaks the namelist into the following public dictionaries:
    closure_nml, const_nml, equil_nml, grid_nml, physics_nml, output_nml,
    solver_nml

    Creates a combined namelist, note that a exception is thrown if a key
    is defined in different namelists with different values

    The following variables are also added to the combined namelist
    multispecies, torgeom, modelist, nmodes, nmodes_peq

    The standard usage is to use a NimrodIn class using the [] indexing

    value = nimrodin[key]

    The indexing assignment method is not defined by design.

    nimrodin[key] = value will raise an exception

    The dunder dictionaries contain default nimrod namelist options used
    by nimpy methods. This avoids the verbose dict.get(key, default)
    The dictionaries do not contain all the nimrod input variables
    '''

    __GRID_INPUT = {'dealiase': 3,
                    'nphi': None,
                    'lphi': 2,
                    'lin_nmodes': 1,
                    'lin_nmax': 0,
                    'geom': 'tor',
                    'per_length': 1.0}

    __EQUIL_INPUT = {'nisp': None,
                     'ndnq': 1,
                     'nnsp': 0,
                     'ds_nqty': 1}

    __PHYSICS_INPUT = {'nonlinear': True}

    __CONST_INPUT = {"charge_input": 1.60217733e-19,
                     "zeff": 1.,
                     "mi": 3.3435860e-27,
                     "me": 9.1093898e-31,
                     "gamma": 5./3.,
                     "kblz": 1.60217733e-19,
                     "mu0": np.pi * 4.e-7,
                     "c": 2.99792458e8,
                     "mc2_kT": 1.,
                     "mc2_kTb": 1.}

    __CLOSURE_INPUT = dict()

    __SOLVER_INPUT = dict()

    __OUTPUT_INPUT = dict()

    def __init__(self, filename="nimrod.in", path=None, multispecies=None):
        '''
        Initializes a NimrodIn class. First check to see if nimrod.in exists
        Then read the file, and set up dictionaries.

        :param filename: nimrod input file name (default: nimrod.in)
        :param path: path to file names (Warning evalfield requries path = ./)
        :param multispecies: Set to override multispecies detection
        '''

        if path is not None:
            in_file = path + filename
        else:
            in_file = filename

        try:
            self.nml = f90nml.read(in_file)
        except Exception as err:
            print("NimrodIn: Error reading nimrod input file")
            raise err

        if multispecies is None:
            if 'nisp' in self.nml['equil_input']:
                multispecies = True
            else:
                multispecies = False

        self.__combined_nml = {'multispecies': multispecies}
        self.__combined_keys = set(['multispecies'])
        self.__process_closure()
        self.__process_const()
        self.__process_equil()
        self.__process_output()
        self.__process_physics()
        self.__process_solver()
        # process_grid is called last because it uses nonlinear which is set
        # process_physicss
        self.__process_grid()

    def __getitem__(self, key):
        '''
        Enable [] indexing

        :param key: nimrod.in variable name
        :return : value corresponding to key
        '''

        try:
            return self.__combined_nml[key]
        except Exception:
            raise KeyError(f"NimrodIN: Key {key} not found")

    def keys(self):
        ''' Return keys in combined nml '''
        return self.__combined_nml.keys()

    def __add_combined_nml(self, nml):
        '''
        Add nml to __combined_nml
        Checks for keys with multiple definitions, and raises an exception

        :param nml: namelist dictionary
        '''

        nml_key = set(nml.keys())
        intersection = self.__combined_keys.intersection(nml_key)
        for key in intersection:
            if self.__combined_nml[key] != nml[key]:
                msg = f"NimrodIn: namelist variable {key} has multipe values"
                raise ValueError(msg)
        self.__combined_nml.update(nml)
        self.__combined_keys.update(nml_key)

    def __process_closure(self):
        ''' Process closure namelist '''
        closure_in = self.nml.get('closure_input', {})
        self.closure_nml = self.__CLOSURE_INPUT
        self.closure_nml.update(closure_in)
        self.__add_combined_nml(self.closure_nml)

    def __process_const(self):
        ''' Process const namelist and set aux values '''

        const_in = self.nml.get('const_input', {})
        self.const_nml = self.__CONST_INPUT
        self.const_nml.update(const_in)
        self.const_nml['mtot'] = (self.const_nml['me'] + self.const_nml['mi']
                                  / self.const_nml['zeff'])
        self.__add_combined_nml(self.const_nml)

    def __process_equil(self):
        ''' Process equil namelist and set aux values '''

        eq_in = self.nml.get('equil_input', {})
        self.equil_nml = self.__EQUIL_INPUT
        self.equil_nml.update(eq_in)
        nisp = self.equil_nml['nisp']
        if nisp is not None:
            ndnq = nisp + 1
            self.equil_nml['ndnq'] = ndnq
        self.__add_combined_nml(self.equil_nml)

    def __process_grid(self):
        ''' Process grid namlist and set aux values
            process uses the nonlinear flag, and it must be called after
            __process_physics
        '''

        grid_in = self.nml.get('grid_input', {})
        self.grid_nml = self.__GRID_INPUT
        self.grid_nml.update(grid_in)
        self.torgeom = False
        if self.grid_nml['geom'] == 'tor':
            self.torgeom = True

        if self.__combined_nml['nonlinear']:
            if self.grid_nml['dealiase'] != 3:
                msg = "Dealise is not 3, skip setting of modelist"
                warnings.warn(msg)
                self.grid_nml['modelist'] = None
                self.grid_nml['nmodes'] = None
                self.grid_nml['nmodes_peq'] = None
                return
            nphi = self.grid_nml['nphi']
            if nphi is None:
                lphi = self.grid_nml['lphi']
                nphi = 2**lphi
            self.grid_nml['nmodes'] = nphi // 3 + 1
            self.grid_nml['nmodes_peq'] = self.grid_nml['nmodes'] + 1
            # modelist uses -1 to indicate eq
            self.grid_nml['modelist'] = [i for i in range(-1,
                                         self.grid_nml['nmodes'])]
        else:
            lin_nmodes = self.grid_nml['lin_nmodes']
            lin_nmax = self.grid_nml['lin_nmodes']
            if lin_nmax > 0:
                modelist = [-1]
                for idx in range(0, lin_nmodes):
                    modelist.append(lin_nmax - (lin_nmodes-1)+idx)
                    self.grid_nml['modelist'] = modelist
            else:
                self.grid_nml['modelist'] = [idx for idx in
                                             range(-1, lin_nmodes)]
            self.grid_nml['nmodes_peq'] = lin_nmodes + 1
            self.grid_nml['nmodes'] = lin_nmodes
            self.__add_combined_nml(self.grid_nml)

    def __process_output(self):
        ''' Process output namelist '''

        out_in = self.nml.get('output_input', {})
        self.output_nml = self.__OUTPUT_INPUT
        self.output_nml.update(out_in)
        self.__add_combined_nml(self.output_nml)

    def __process_physics(self):
        ''' Process physics namelist '''

        physics_in = self.nml.get('physics_input', {})
        self.physics_nml = self.__PHYSICS_INPUT
        self.physics_nml.update(physics_in)
        self.__add_combined_nml(self.physics_nml)

    def __process_solver(self):
        ''' Process solver namelist '''

        solver_in = self.nml.get('solver_input', {})
        self.solver_nml = self.__SOLVER_INPUT
        self.solver_nml.update(solver_in)
        self.__add_combined_nml(self.solver_nml)
