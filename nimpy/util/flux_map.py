#!/usr/bin/env python3
'''
fluxMap.py is used for calculating a mapping psi(R,Z) and Theta(R,Z)

class fluxMap
   class for storing the mapping the forward and inverse mapping
   forward map takes (R,Z) are returns flux coordinates (psi, theta)
   inverse map takes flux coordinates (psi,theta) are returns (R,Z)
   currently only PEST coordinates are used, but future development
   could enable the use of additional coordinates

read_map
 Reads a fluxmap from file

write_map
 Writes a fluxmap to file

computePolAngle
Computes the poloidal anagle along the contours generated in fsa.py,

When generating the mapping, the FSA uses a large number of surfaces and eta
values to create a high-resolution mapping. Testing showed that using 350
surfaces and 1001 eta points produced errors on the order of 1e-9.
'''

# Notes
# The accuracy of the mapping can be tested using the method _test_distance
# the method applies the forward map and then the inverse map
# If the mappings are perfect then it should return to the starting point
#
# When applied to a KSTAR equilibrium the points are observed to move
# a distance on the order of 1.1e-9 or less. However, there are issues
# mapping point that are too close to the magnetic axis. This has not been
# an issue in early usage.

import os
import matplotlib.pyplot as plt
import numpy as np
import pickle
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import CloughTocher2DInterpolator as CloughTocher
from scipy.spatial import Delaunay
import shapely.geometry as geo
import nimpy.eval_nimrod as eval
import nimpy.fsa as fsa


class fluxMap:
    '''
    Class for evaluating mappings between NIMROD RZ coordinates and
    equilibrium flux coordinates $\psi_n$ and $\Theta$
    ''' # noqa

    def __init__(self, rz, psi, theta, debug=False, **kwargs):
        '''
        Initialize the mapping

        :param rz: Array of RZ locations, dimension [2,ith, ipsi]
        :param  psi: Array of psi at RZ, dimension [ith, ipsi]
        :param  theta: Array of theta at RZ, dimension [ith, ipsi]
        :param  debug: flag to output debugging info

        The mapping uses three coordinate systems:
        rz : nimrod rz
        geo_r, geo_t: geometric polar r and theta
        psi, theta: flux coordinate psi and theta

        The mapping uses a region last_psi to identify the LCFS
        '''
#        Development Notes:
#
#        Todo: To support an annulus we could also identify a region firstpsi
#
#        The mapping first calculates geo_r(r,z) and geo_t(r,z), this uses the
#        first location in rz to identify the magnetic axis, if not in kwargs
#
#        Triangulations then map create two mappings
#          Forward map: psi(geo_r,geo_t) and theta(geo_r,geo_t)
#          Inverse map: geo_r(psi,theta) and geo_t(psi,theta)
#
#        The methods __compute_geo_rt and __compute_rz handle the transform
#        between NIMROD coordinates and polar coordinates
#

        rzo = rz[:, 0, 0]
        self.__rzo = kwargs.get('rzo', rzo)
        coords = [(rz[0, ix, -1], rz[1, ix, -1]) for ix in range(rz.shape[1])]
        self.__last_psi = geo.Polygon(coords)
        geo_r, geo_t = self.__compute_geo_rt(rz[0, :, :], rz[1, :, :])
        # determine the forward map
        nodes = np.stack((geo_r.flatten(), geo_t.flatten()), axis=0)
        tri = Delaunay(nodes.transpose())
        self.__psi_map = CloughTocher(tri, psi.flatten(), tol=1e-8)
        self.__theta_map = CloughTocher(tri, theta.flatten(), tol=1e-8)
        # determine the inverse map
        inv_nodes = np.stack((psi.flatten(), theta.flatten()), axis=0)
        inv_tri = Delaunay(inv_nodes.transpose())
        self.__geo_r_map = CloughTocher(inv_tri, geo_r.flatten(), tol=1e-8)
        self.__geo_t_map = CloughTocher(inv_tri, geo_t.flatten(), tol=1e-8)
        if debug:
            x2, y2 = self.__last_psi.exterior.xy
            # Plot contours that define the different regions
            plt.plot(x2, y2, c="green")
            plt.show()
            # Plot polar r
            plt.plot(x2, y2, c="black")
            plt.contourf(rz[0, :, :], rz[1, :, :], geo_r[:, :])
            plt.show()
            # Plot theta in the right region
            plt.plot(x2, y2, c="black")
            plt.contourf(rz[0, :, :], rz[1, :, :], geo_t[:, :])
            plt.show()

    def __compute_rz(self, geo_r, geo_t):
        '''
            Compute NIMROD coordinates from polar coordinates

            :param geo_r: polar minor radius
            :param geo_t: polar angle theta
            :return: NIMROD R,Z
        '''

        r = self.__rzo[0] + geo_r * np.cos(geo_t)
        z = self.__rzo[1] + geo_r * np.sin(geo_t)
        return r, z

    def __compute_geo_rt(self, r, z):
        '''
            Compute polar coordinates from NIMROD coordinates

            :param r: NIMROD R
            :param z: NIMROD Z
            :return polar r, theta
        '''

        dr = r - self.__rzo[0]
        dz = z - self.__rzo[1]
        geo_r = np.sqrt(dr**2 + dz**2)
        geo_t = np.arctan2(dz, dr)
        geo_t = np.where(geo_t < 0, geo_t + 2 * np.pi, geo_t)
        return geo_r, geo_t

    def _forward_map(self, rz, debug=False):
        '''
        Compute the flux coordinates psi_N(R,Z), Theta_p(R,Z)

        :param rz: rz locations
        :param debug: display extra info for debugging
        :return: flux coordinates $\psi$, $\theta$

        Raises an ValueError exception if outside domain
        Previous version passed None, None

        The returned theta should be in the domain [0, 2pi]
        ''' # noqa
        pp = geo.Point(rz[0], rz[1])
        # use ingon to determine appropriate domain and then evaluate map
        if not (self.__last_psi.contains(pp)):
            if debug:
                print(f"r = {rz[0]}, z= {rz[1]} outside domain")
            raise ValueError
        geo_r, geo_t = self.__compute_geo_rt(rz[0], rz[1])
        psi = self.__psi_map(geo_r, geo_t)
        theta = self.__theta_map(geo_r, geo_t)
        return psi, theta

    def _inverse_map(self, pt, debug=False):
        '''
        return RZ as a function of normalized psi and poloidal theta

        Inputs:
        :param pt: normalized psi and theta
        :param debug: display extra info for debugging
        :return: R, Z
        '''
        if pt[0] < 0 or pt[0] > 1:
            if debug:
                print(f"psi = {pt[0]}, theta = {pt[1]} outside domain")
            raise ValueError
        if pt[1] < 0 or pt[1] > np.pi * 2.0:
            if debug:
                print(f"psi = {pt[0]}, theta = {pt[1]} outside domain")
            raise ValueError

        geo_r = self.__geo_r_map(pt[0], pt[1])
        geo_t = self.__geo_t_map(pt[0], pt[1])

        return self.__compute_rz(geo_r, geo_t)

    def _test_distance(self, start):
        '''
        Function for testing the accuracy of the mapping
        compute the distance a point moves after applying the mapping and
        then the inverse mapping

        :param start: 2D np array either rz
        :return: distance the point moves
        '''
        try:
            pt = map.getpt(start, debug=True,
                           direction="forward")
        except ValueError:
            print(f"Forward mapping failed on point {start}")
            return np.inf
        except Exception as e:
            print(f"Unknowm error {e}")
            raise
        try:
            end = map.getpt(pt, debug=True, direction="inverse")
        except ValueError:
            print(f"Inverse mapping failed on point {pt}")
            return np.inf
        except Exception as err:
            print(f"Unknown error {err}")
            raise
        diff = end - start
        distance = np.linalg.norm(diff)
        print("Testing accuracy of mapping")
        print(f"Staring point: r = {start[0]}, z = {start[1]}")
        print(f"Point in psi theta: psi = {pt[0]}, theta = {pt[1]}")
        print(f"End point: r = {end[0]}, z = {end[1]}")
        print(f"Point moved: r_diff = {diff[0]}, zdiff = {diff[1]}")
        print(f"Total error: {distance}")
        print("End Test")
        print("---------------------------")

        return distance

    def getpt(self, pt, debug=False, direction="forward"):
        '''
        wrapper function for forward and inverse map

        :param pt: point
        :param debug: display extra info for debugging
        :param direction: forward or inverse (mapping)
        :return: RZ or psi,theta

        NOTE: Rasie a key error if the direction is not
        forward of inverse. This avoids raising a ValueError
        and preserves to ability to use the following design

        try: getpt
        except ValueError: pass

        to naturally loop over points outside the closed flux region
        '''

        if direction == "forward":
            return self._forward_map(pt, debug=debug)
        elif direction == "inverse":
            return self._inverse_map(pt, debug=debug)
        else:
            print(f"getpt direction {direction} not recognized")
            raise KeyError


def write_map(mapping, filename="fluxmap.pickle"):
    '''
    Write a fluxmap instance to file

    :param mapping: instance of fluxMap
    :param filename: optional file name

    '''
    file = open(filename, 'wb')
    pickle.dump(mapping, file)
    file.close()


def read_map(filename="fluxmap.pickle"):
    '''
    Read a fluxmap instance from file

    :param filename: optional file name
    :return: fluxMap instance

    '''
    try:
        file = open(filename, 'rb')
        mapping = pickle.load(file)
        file.close()
    except Exception:
        print(f"Unable to load file {filename}")
    if not isinstance(mapping, fluxMap):
        print(f"File {filename} does not contain a fluxMap")
        mapping = None
        raise TypeError
    return mapping


def __eta_dtheta(rz, rzo, qval, evalnim, fdict):
    '''
    Compute eta and dtheta_p /deta at location rz

    :param rz:         rz location on a flux surface
    :param rzo:        rz location of the o-point
    :param qval:       q value on the flux surface
    :param evalnim: instance of eval nimrod
    :param fdict:      dictionary of additional arguments
    :return: eta, dtheta/deta: geometric angle. derivative of pooloidal angle

    Notes:
        fdict avoids the uses of kwargs for compatibility with ode integrator
        currently use "PEST" Jacobian, in the future we can generalize to other
        flux coordinates using fdict

        dtheta/deta = 1 / (bdgeta * jac)
        bdgeta = (bz * cos eta - br * sin eta) / r
        R = R_0 + r cos eta
        Z = Z_0 + r sin eta
        jac = q R^2 / F = R * q / b_phi
    '''
    addpert = fdict.get("addpert", True)
    debug = fdict.get("debug", False)
    b0 = fsa.get_b0(evalnim, rz, addpert=addpert, abort=True)
    # compute geometric factors
    rminor = np.sqrt((rz[0] - rzo[0])**2 + (rz[1] - rzo[1])**2)
    if rminor == 0.0:
        return 0.0, 0.0
    cosfac = (rz[0] - rzo[0]) / rminor
    sinfac = (rz[1] - rzo[1]) / rminor
    eta = np.arctan2(rz[1] - rzo[1], rz[0] - rzo[0])
    if eta < 0:
        eta += 2 * np.pi
    if debug:
        # when debugging check the calculation of trig factors for consistency
        tol = 1.0e-10
        cosfac2 = np.cos(eta)
        sinfac2 = np.sin(eta)
        if np.abs(cosfac2 - cosfac) > tol or np.abs(sinfac2 - sinfac) > tol:
            print(f"RZ = {rz[0:1]}")
            print(f"RZ o-point = {rzo[0:1]}")
            print(f"eta = {eta}")
            print(f"cosfac = {cosfac}, cosfac2 = {cosfac2}")
            print(f"sinfac = {sinfac}, sinfac2 = {cosfac2}")
            raise Exception('eta_dtheta: Error computing trig functions')
    # compute eta and dtheta/deta
    bdgth = (b0[1] * cosfac - b0[0] * sinfac) / rminor
    jac = rz[0] * qval / b0[2]
    dtheta = 1.0 / (bdgth * jac)
    return eta, dtheta


def compute_pol_angle(dumpfile, rzo=None, rzx=None, nsurf=300,
                      addpert=True, fargs={}, **kwargs):
    '''
    Compute theta_p on a grid use fsa

    :param  dumpfile:   h5 dumpfile
    :param  rzo:        guess for rz location of the o-point
    :param  rzx:        guess for rz location of the x-point
    :param  nsurf:      maximum number of surfaces for FSA (default=300)
    :param  addpert:    flag to include b(n=0) perturbation to B_eq
    :param  fargs:      dictionary for extra arguments
    :return: contours[2,ntheta,npsi]: RZ locations that trace out flux contours
    :return: psi[ntheta,npsi]: normalized poloidal flux
    :return: theta[ntheta,npsi]: flux-aligned poloidal angle
    :return: q[npsi] safety factor on flux surfaces

    '''
    # call FSA to get RZ locations of flux contours and q(psi)
    plot = fargs.get("plot", False)
    fargs['addpert'] = addpert
    evalnim = eval.EvalNimrod(dumpfile, fieldlist='b')
    dvar, yvar, contours = fsa.FSA(evalnim, rzo, fsa.dummy_int,
                                   1, nsurf=nsurf, depvar='eta',
                                   dpow=1, rzx=rzx, normalize=True,
                                   addpert=addpert, neta=1001)
    iend = -1
    while np.isnan(yvar[:, iend]).any():
        iend -= 1
    iend += yvar.shape[1]+1
    qval = dvar[7, :iend]
    # preliminary calculations to compute Theta
    ntheta = contours[:, :, :iend].shape[1]
    npsi = contours[:, :, :iend].shape[2]
    rzo = np.array([contours[0, 0, 0], contours[1, 0, 0], 0.0])
    psi = np.zeros([ntheta, npsi])
    theta = np.zeros([ntheta, npsi])
    # work arrays
    dtheta = np.zeros([ntheta])
    eta = np.zeros([ntheta])
    psi0 = dvar[2, 0]
    psi1 = dvar[2, iend-1]
    dpsi = psi1 - psi0
    for ipsi in range(npsi):
        # renormalize psi
        psi[:, ipsi] = (dvar[2, ipsi] - psi0) / dpsi
        qval = dvar[7, ipsi]
        if ipsi == 0:  # magnetic axis
            continue
        for ith in range(ntheta):
            # compute eta and dtheta / deta one contour at a time
            rz = np.array([contours[0, ith, ipsi], contours[1, ith, ipsi], 0.])
            eta[ith], dtheta[ith] = __eta_dtheta(rz, rzo, qval, evalnim, fargs)
        try:
            # set up a spline to integrate dtheta on contour
            dtSpl = UnivariateSpline(eta, dtheta, k=5, s=0.0,
                                     check_finite=True)
        except Exception as inst:
            print(f'ipsi = {ipsi}')
            print(f'eta = {eta}')
            print(f'dtheta = {theta}')
            raise inst
        # integrate spline to compute Theta
        for ith in range(ntheta):
            if ith == 0:
                continue
            theta[ith, ipsi] = dtSpl.integral(0, eta[ith])
        # renormalize Theta to be in [0,2pi] accounting for numerical errors
        theta[:, ipsi] = theta[:, ipsi] / theta[-1, ipsi] * (2 * np.pi)
    if plot:
        plt.plot(contours[0, :, :iend:5], contours[1, :, :iend:5],
                 color='k', alpha=0.2)
        plt.plot(contours[0, :, iend-1], contours[1, :, iend-1], color='k',
                 alpha=1)
        plt.contour(contours[0, :, 1:iend], contours[1, :, 1:iend],
                    theta[:, 1:], levels=100)
        plt.title("Poloidal Angle")
        plt.show()
    return contours[:, :, :iend], psi, theta, dvar[7, :iend]


if __name__ == "__main__":
    homeDir = os.environ['HOME']
    scratchDir = homeDir + '/SCRATCH'
    dumpfile = scratchDir + '/KSTAR/22030301/dumpgll.00000.h5'
    rz, psi, theta, q = compute_pol_angle(dumpfile,
                                          rzo=[1.768, -0.018831, 0.0],
                                          nsurf=350,
                                          addpert=False,
                                          fargs={'plot': True})
    map = fluxMap(rz, psi, theta, debug=True)
    map._test_distance(np.array([2, 0]))
    map._test_distance(np.array([1.517, 0.6]))
    map._test_distance(np.array([1.986, -.489]))
    map._test_distance(np.array([1.79471, -0.0095]))
    map._test_distance(np.array([2.155, -.2]))
    write_map(map)
    map = None
    map = read_map()
    map._test_distance(np.array([2, 0]))
