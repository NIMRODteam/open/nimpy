#!/usr/bin/env python3
import argparse
import importlib
import h5py
from matplotlib import axes
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from nimpy.eval_nimrod import EvalNimrod
from nimpy.fsa import FSA, FSAInput, dummy_int
import nimpy.util.nimrodin
import numpy as np
import os
from scipy.interpolate import interp1d
from typing import Type
import yaml


'''
Function for generating 2D contour plots of nimrod field data
'''


def __get_dims(mesh: list):
    '''
    Get physical dimensions of the mesh

    Inputs
    ------
    mesh: list[nbl] of block rz arrays

    Outputs
    --------
    r[min,max]
    z[min,max]
    '''
    rmin, rmax = np.inf, -np.inf
    zmin, zmax = np.inf, -np.inf
    for rz in mesh:
        rmax = np.amax([rmax, np.amax(rz[..., 0])])
        rmin = np.amin([rmin, np.amin(rz[..., 0])])
        zmax = np.amax([zmax, np.amax(rz[..., 1])])
        zmin = np.amin([zmin, np.amin(rz[..., 1])])
    return rmin, rmax, zmin, zmax


def __read_field(rblocks: Type[h5py._hl.group.Group], fldspec: dict,
                 keff: Type[np.ndarray], nmode: int) -> (list, str):
    '''
    Read a nodal field from dumpfile

    Inputs
    ------
    rblocks: h5 rblocks group
    fldspec: dict containing field info
    keff: array of mode numbers in dump
    nmode: mode number of field plotted

    Outputs
    --------
    list[nbl] containing field data
    str: substring to append to plot title if requested
    '''

    contour = []
    constr = fldspec['name']
    nmode = fldspec.get('nmode', nmode)
    qty = fldspec['qty']
    project_RZ_rperp = fldspec.get('project_RZ_rperp', False)
    add_eq_sign = fldspec.get('add_eq_sign', 0)
    if add_eq_sign != 0 and nmode == 0:
        eq_dict = {'nd': 'nq', 've': 'vq', 'te': 'teq', 'ti': 'tiq', 'pr':
                   'prq', 'pe': 'peq', 'be': 'bq', 'ja': 'jq', 'ee': 'eq'}
        eq_name = eq_dict[constr]
    plot_title_append = f" n={nmode}"

    compute_qty = False
    if type(nmode) is str:
        if nmode.lower() == 'eq':
            qtyidx = qty
            plot_title_append = ' eq'
        else:
            msg = f"nmode {nmode} not recognized"
            raise KeyError(msg)
    elif type(nmode) is not int:
        msg = f"nmode {nmode} not recognized"
        raise KeyError(msg)
    else:
        part = fldspec.get('part', 're').lower()
        if part == 're':
            constr = "re" + constr
        elif part == 'im':
            constr = "im" + constr
        else:
            msg = f"Part {part} not valid. Must be (re, or im)"
        imode = np.where(np.abs(keff - nmode) < 1.e-4)[0]
        if len(imode) != 1:
            msg = f"nmode {nmode} is not in dumpfile, keff= {keff}"
            raise KeyError(msg)
        imode = imode[0]
        qtyidx = qty
        compute_qty = True

    for id, block in rblocks.items():
        conid = constr + id
        con = np.array(block[conid][...])
        if compute_qty:
            # compute qty index
            nqty, rmd = divmod(con.shape[-1], len(keff))
            if rmd != 0:
                msg = f"Unable to compute nqty for {constr}"
                raise ValueError(msg)
            qtyidx = imode * nqty + qtyidx
            compute_qty = False
        # Add or subtract eq field as appropriate
        if add_eq_sign != 0 and nmode == 0:
            coneqid = eq_name + id
            coneq = np.array(block[coneqid][...])
            con[..., qtyidx] += add_eq_sign * coneq[..., qty]
            if project_RZ_rperp:
                if qty == 0:
                    con[..., qtyidx + 1] += add_eq_sign * coneq[..., qty + 1]
                if qty == 1:
                    con[..., qtyidx - 1] += add_eq_sign * coneq[..., qty - 1]
        # Project as appropriate
        if project_RZ_rperp:
            beqid = "bq" + id
            bn0id = "rebe" + id
            if fldspec.get('project_add_B_n0', (keff[0] == 0)):
                b0 = np.array(block[beqid][..., 0:2]) \
                    + np.array(block[bn0id][..., 0:2])
            else:
                b0 = np.array(block[beqid][..., 0:2])
            norm = np.linalg.norm(b0, axis=-1)
            b0[..., 0] = np.divide(b0[..., 0], norm, out=np.zeros_like(norm),
                                   where=(norm != 0))
            b0[..., 1] = np.divide(b0[..., 1], norm, out=np.zeros_like(norm),
                                   where=(norm != 0))
            if con.shape[-1] != 3*len(keff):
                msg = f"Unable to determine nqty for {constr} projection"
                raise ValueError(msg)
            if qty == 0:
                con[..., qtyidx] = \
                        - con[..., qtyidx] * b0[..., 1] \
                        + con[..., qtyidx + 1] * b0[..., 0]
            if qty == 1:
                con[..., qtyidx] = \
                        + con[..., qtyidx - 1] * b0[..., 0] \
                        + con[..., qtyidx] * b0[..., 1]
        contour.append(con[..., qtyidx])
    return contour, plot_title_append


def __read_mesh(rblocks: Type[h5py._hl.group.Group]) -> list:
    '''
    Loops over blocks in dump and extracts the mesh.

    Inputs
    -------
    rblocks: h5 rblocks group

    Outputs
    -------
    list[nbl] of block rz arrays

    '''
    mesh = []
    for id, block in rblocks.items():
        meshstr = f"rz{id}"
        mesh.append(np.array(block[meshstr][:]))
    return mesh


def __read_nimpy(nimpyin: str = 'nimpy.yaml') -> dict:
    '''
    Read the plot field data from a nimpy yaml input file

    Inputs:
    -------
    nimpy.yaml file name (Default nimpy.yaml)

    Output:
    -------
    dictionary of input variables
    '''
    # check if file exists, and if so read the file
    if not os.path.isfile(nimpyin):
        msg = f"Unable to find nimpy yaml file {nimpyin}\n" \
            + "Find an example in the source at \n" \
            + "https://gitlab.com/NIMRODteam/open/nimpy/-/" \
            + "blob/main/scripts/examples/nimpy.plot_field.yaml"
        raise FileNotFoundError(msg)

    with open(nimpyin, 'r') as file:
        inputdict = yaml.safe_load(file)

    # search for correct "namelist" in yaml file
    NAME_LIST = 'plot_field'
    inputs = inputdict.get(NAME_LIST, None)
    if inputs is None:
        msg = f"Namelist {NAME_LIST} not found in file {nimpyin}"
        raise KeyError(msg)
    return inputs


def __get_surface_contours(surf_input: dict, dumpfile: str,
                           nimpy_input: str) -> list:
    '''
    Read surfaces sol, sep, and wall from files

    Determine q, ne, te contours as appropriate

    Inputs
    -----
    surf_input: dictionary of surface contour input from nimpy.yaml
    dumpfile: NIMROD dump file name
    nimpy_input: nimpy yaml input file name

    Outputs
    -------
    tuple of surface contours
    '''

    # contour and sol files are generated by fgnimeq
    contours_file = surf_input.get("contours_file", None)
    sol_file = surf_input.get("sol_file", None)
    # these files are machine specific
    limiter_file = surf_input.get("limiter_file", None)
    vac_vessel_file = surf_input.get("vac_vessel_file", None)
    if contours_file and sol_file:
        msg = "Both contours_file and sol_files are defined"
        raise KeyError(msg)

    # Read contours from file
    RZsol = None
    RZsep = None
    RZgfile_sep = None
    RZwall = None
    RZlim = None
    RZvac = None
    if contours_file:
        with h5py.File(contours_file, 'r') as hf:
            try:
                RZsol = hf['/sol/points']
                RZsol = np.vstack([RZsol, RZsol[0, :]])
            except Exception:
                pass
            RZsep = hf['/LCFS/points']
            RZsep = np.vstack([RZsep, RZsep[0, :]])
            RZwall = hf['/wall/points']
            RZwall = np.vstack([RZwall, RZwall[0, :]])
            try:
                RZgfile_sep = hf['/gfile_lcfs/points']
                RZgfile_sep = np.vstack([RZgfile_sep, RZgfile_sep[0, :]])
            except Exception:
                pass
            try:
                RZlim = hf['/gfile_limiter/points']
                RZlim = np.vstack([RZlim, RZlim[0, :]])
            except Exception:
                pass
    elif sol_file:
        with open(sol_file, 'r') as file:
            lines = file.readlines()
        nsep = int(lines[0].split()[1])
        RZsol = np.zeros([nsep, 2])
        for idx in range(nsep):
            RZsol[idx, 0] = float(lines[1].split()[idx])
            RZsol[idx, 1] = float(lines[3].split()[idx])
        nsep = int(lines[4].split()[1])
        RZsep = np.zeros([nsep, 2])
        for idx in range(nsep):
            RZsep[idx, 0] = float(lines[5].split()[idx])
            RZsep[idx, 1] = float(lines[7].split()[idx])
        try:
            nsep = int(lines[8].split()[1])
            RZwall = np.zeros([nsep, 2])
            for idx in range(nsep):
                RZwall[idx, 0] = float(lines[9].split()[idx])
                RZwall[idx, 1] = float(lines[11].split()[idx])
        except IndexError:
            RZwall = None
    if limiter_file:
        with h5py.File(limiter_file, 'r') as hf:
            RZlim = hf['/grContours/grContour0/points']
            RZlim = np.vstack([RZlim, RZlim[0, :]])
    if vac_vessel_file:
        with h5py.File(vac_vessel_file, 'r') as hf:
            RZvac = hf['/grContours/grContour0/points']
            RZvac = np.vstack([RZvac, RZvac[0, :]])

    sol_plot = surf_input.get("sol_plot", True)
    sep_plot = surf_input.get("sep_plot", True)
    gfile_sep_plot = surf_input.get("gfile_sep_plot", False)
    wall_plot = surf_input.get("wall_plot", True)
    lim_plot = surf_input.get("lim_plot", True)
    vac_plot = surf_input.get("vac_plot", True)
    out_contours = []
    for surface, add_surf in zip([RZsol, RZsep, RZwall, RZlim, RZvac,
                                  RZgfile_sep],
                                 [sol_plot, sep_plot, wall_plot, lim_plot,
                                  vac_plot, gfile_sep_plot]):
        if surface is not None and add_surf:
            out_contours.append(surface)

    # Trace constant contours
    psin_vals = surf_input.get("psin_vals", [])
    rhon_vals = surf_input.get("rhon_vals", None)
    q_vals = surf_input.get("q_vals", None)
    use_abs_q = surf_input.get("use_abs_q", False)
    if psin_vals or rhon_vals or q_vals:
        # Do FSA to get surfaces
        eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbjd')
        FSAInput.read_fsa_input(nimpy_input)
        dvar, yvars, contours = FSA(eval_nimrod, FSAInput.d['rzo'],
                                    dummy_int, 1)

        psin = dvar[0, :]
        rhon = dvar[1, :]
        R = dvar[4, :]
        Z = dvar[5, :]
        q = dvar[7, :]
        if use_abs_q:
            q = np.fabs(q)

        R_interp = interp1d(psin, R)
        Z_interp = interp1d(psin, Z)
        q_interp = interp1d(q, psin)
        rhon_interp = interp1d(rhon, psin)

        if q_vals:
            for q_val in q_vals:
                psin_vals.append(q_interp(q_val))
        if rhon_vals:
            for rhon_val in rhon_vals:
                psin_vals.append(rhon_interp(rhon_val))

        for psin_val in psin_vals:
            RZpt = [R_interp(psin_val), Z_interp(psin_val), 0.0]
            yvars, contours = FSA(eval_nimrod, FSAInput.d['rzo'], dummy_int, 1,
                                  nsurf=1, rzp=RZpt)
            out_contours.append(contours.T)

    return out_contours


def __plot_contour(ax: Type[axes._axes.Axes],
                   mesh: list, fld: list, flddict: dict,
                   lmts: tuple, inputs: dict, pd: int,
                   plot_title_append: str) -> None:
    '''
    Plot individual field

    Inputs:
    -------
    ax : maplotlib ax instance
    mesh: list[nbl] of block rz arrays
    fld: list[nbl] of block field arrays
    flddict: dictionary specifing field inputs
    lmts: min and max values of field across all blocks
    inputs: plot_field "namelist" from nimpy.yaml
    pd: nimrod poly_degree
    plot_title_append: string to append to plot title
    '''
    grid_input = inputs.get('grid', {})
    show_nodes = grid_input.get('show_nodes', False)
    show_elements = grid_input.get('show_elements', False)
    vertex_only = grid_input.get('vertex_only', False)
    xlabel = grid_input.get('xlabel', 'R(m)')
    ylabel = grid_input.get('ylabel', 'Z(m)')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    # Set limits with a list: [min, max]
    xlim = grid_input.get('xlim', None)
    ylim = grid_input.get('ylim', None)
    if xlim is not None:
        ax.set_xlim(*xlim)
    if ylim is not None:
        ax.set_ylim(*ylim)
    title = flddict.get('label', flddict.get('name'))
    plot_input = inputs.get('plot_options', {})
    title_append_mode = plot_input.get('title_append_mode', False)
    if title_append_mode:
        title += plot_title_append
    ax.set_title(title)
    nlevels = plot_input.get('levels', 50)
    cbar = plot_input.get('cbar', False)
    if cbar not in (False, True):
        msg = f"cbar key value {cbar} not recognized"
        raise KeyError(msg)
    cmap = plot_input.get('color_map', None)
    color_map_module = plot_input.get('color_map_module', None)
    if color_map_module:
        color_module = importlib.import_module(color_map_module)
        cmap = getattr(color_module, cmap)

    if vertex_only:
        ie = pd
    else:
        ie = 1

    minfld, maxfld = lmts
    minfld = float(flddict.get('cmin', minfld))
    maxfld = float(flddict.get('cmax', maxfld))
    levels = np.linspace(minfld, maxfld, nlevels, endpoint=True)
    for rz, con in zip(mesh, fld):
        img = ax.contourf(rz[::ie, ::ie, 0], rz[::ie, ::ie, 1],
                          con[::ie, ::ie], levels, cmap=cmap)
        if show_elements:
            ax.plot(rz[::pd, ::pd, 0], rz[::pd, ::pd, 1],
                    color='k', alpha=0.1)
            ax.plot(np.transpose(rz[::pd, ::pd, 0]),
                    np.transpose(rz[::pd, ::pd, 1]),
                    color='k', alpha=0.1)
        if show_nodes:
            size = 1
            ax.scatter(rz[..., 0], rz[..., 1], s=size,
                       marker='+', color='k', alpha=0.1)
    if cbar:
        last_axes = plt.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(img, cax=cax)
        plt.sca(last_axes)


def __plot_surfaces(ax: Type[axes._axes.Axes],
                    surfaces: Type[Type[np.ndarray]]) -> None:
    '''
    Add sep, wall, and sol surfaces to plot

    Inputs
    ------
    ax: matplotlib axes instance
    surfaces: array of surfaces
    '''
    for surface in surfaces:
        if surface is not None:
            ax.plot(surface[:, 0], surface[:, 1],
                    alpha=0.5, color='r', linestyle='-')


def parse_arguments() -> dict:
    '''
    Parse command line arguments

    Inputs
    ------
    None

    Outputs
    -------
    args dictionary
    '''
    parser = argparse.ArgumentParser(
                    prog='plot_field',
                    description='Plot 2D field using nodal field data',
                    usage="%(prog)s [options] dumpfile")
    parser.add_argument('--nimpyin', '-p', default='nimpy.yaml',
                        help="nimpy yaml input file")
    parser.add_argument('dumpfile', help="NIMROD dump file")

    args = parser.parse_args()
    return args


def plot_field(dumpfile: str, nimpyin: dict) -> None:
    '''
    Generate contour plots of nodal field data

    Inputs
    ------
    dumpfile: nimrod h5 dump file
    nimpyin: nimpy.yaml input file

    Outputs
    -------
    None

    '''

    if not os.path.isfile(dumpfile):
        msg = f"Unable to find dumpfile {dumpfile}"
        raise FileNotFoundError(msg)
    inputs = __read_nimpy(nimpyin)

    example_string = \
        "See example in source: https://gitlab.com/NIMRODteam/open/nimpy" \
        + "/-/blob/main/scripts/examples/nimpy.plot_field.yaml"
    # Get sub-section dictionaries -- fields is required
    fields = inputs.get('fields', None)
    if fields is None:
        msg = f"List of fields to plot not found in file {nimpyin}.\n" \
                + example_string
        raise KeyError(msg)
    # check items at the wrong level to help adjustment with new format
    grid_input = ['show_nodes', 'show_elements', 'vertex_only',
                  'xlabel', 'ylabel', 'xlim', 'ylim']
    if any(item in grid_input for item in fields.keys()):
        msg = f"Input {grid_input} should be nested inside the" \
                 + " grid subblock.\n" + example_string
        raise KeyError(msg)
    grid_input = inputs.get('grid', {})
    surf_input = ["contours_file", "sol_file", "limiter_file",
                  "vac_vessel_file", "sol_plot", "sep_plot", "wall_plot",
                  "lim_plot", "vac_plot", "q_vals", "add_n0", "use_abs_q"]
    if any(item in surf_input for item in fields.keys()):
        msg = f"Input {surf_input} should be nested inside the" \
                + " surface_contours subblock.\n" + example_string
        raise KeyError(msg)
    surf_input = inputs.get('surface_contours', {})
    plot_input = ['margfac', 'vmargfac', 'hmargfac', 'equal_axes',
                  'levels', 'cbar', 'color_map', 'title_append_mode']
    if any(item in plot_input for item in fields.keys()):
        msg = f"Input {plot_input} should be nested inside the plot_options" \
                + " subblock.\n" + example_string
        raise KeyError(msg)
    plot_input = inputs.get('plot_options', {})

    nplots = (len(fields))
    ncol = inputs.get('ncol', -1)
    show_plot = inputs.get('show_plot', True)
    save_file = inputs.get('save_file', None)
    nmode = inputs.get('nmode', None)

    nimrodin = nimpy.util.nimrodin.NimrodIn()
    geom = nimrodin['geom']
    per_length = nimrodin['per_length']

    surfaces = __get_surface_contours(surf_input, dumpfile, nimpyin)

    fldlist = {}
    fldlmts = {}
    plot_title_append = {}
    with h5py.File(dumpfile, 'r') as dump:
        keff = np.array(dump['keff'][:])
        if geom == 'lin':
            keff = keff*per_length/(2.*np.pi)  # convert to mode index
        pd = dump.attrs['poly_degree'][0]
        rblocks = dump['/rblocks']
        mesh = __read_mesh(rblocks)
        rmin, rmax, zmin, zmax = __get_dims(mesh)
        for id, fldspec in fields.items():
            fldlist[id], plot_title_append[id] = \
                    __read_field(rblocks, fldspec, keff, nmode)
            maxfld = -np.inf
            minfld = np.inf
            for fld in fldlist[id]:
                fldmax = np.amax(fld)
                fldmin = np.amin(fld)
                maxfld = np.amax([fldmax, maxfld])
                minfld = np.amin([fldmin, minfld])
            if minfld == 0. and maxfld == 0.:
                minfld = -1.
                maxfld = 1.
            fldlmts[id] = (minfld, maxfld)

    aspect_ratio = (rmax-rmin)/(zmax-zmin)
    screen_ratio = 16/9
    if ncol < 1:
        ncol = int(nplots//(screen_ratio*aspect_ratio)+1)
    nrow = int(-1 * (-nplots // ncol))  # ceiling division
    margfac = plot_input.get('margfac', 0.3)  # 0.15
    vmargfac = plot_input.get('vmargfac', 0.15)
    hmargfac = plot_input.get('hmargfac', 0.2)
    figsize = tuple(plot_input.get('fig_size', (16, 9)))
    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(nrow, ncol,
                           wspace=margfac, hspace=margfac,
                           top=1.-vmargfac/(nrow+2*vmargfac),
                           bottom=vmargfac/(nrow+2*vmargfac),
                           left=hmargfac/(ncol+2*hmargfac),
                           right=1.-hmargfac/(ncol+2*vmargfac))

    equal_axes = plot_input.get('equal_axes', False)
    iplt = 0
    for id, fld in fldlist.items():
        row, col = divmod(iplt, ncol)
        ax = plt.subplot(gs[row, col])
        if equal_axes:
            ax.axis('equal')
        __plot_contour(ax, mesh, fld, fields[id], fldlmts[id],
                       inputs, pd, plot_title_append[id])
        __plot_surfaces(ax, surfaces)
        iplt += 1

    if show_plot:
        plt.show()
    if save_file:
        msg = f"Saving figure to file {save_file}"
        fig.savefig(save_file)


def main():
    args = parse_arguments()
    plot_field(args.dumpfile, args.nimpyin)


if __name__ == "__main__":
    main()
