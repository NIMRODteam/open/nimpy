#!/usr/bin/env python3
'''
 Example usage:

.. code-block:: python

   import f90nml
   from nimpy.eval_nimrod import EvalNimrod
   from nimpy.fsa import FSA

   dumpfile = 'dumpgll.00000.h5'
   nml = f90nml.read('nimrod.in')
   gmt = nml['grid_input']['geom']
   dmf = EvalNimrod(dumpfile, fieldlist='nvptbj')

   def myfsa(rzc, y, dy, eval_nimrod, fdict):
       \'\'\'
       Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
       Set neq to number of outputs in FSA call
       and fill dy[4:4+neq]
       \'\'\'
       p = eval_nimrod.eval_field('p', rzc, dmode=0, eq=2)
       dy[4] = p[0]*dy[2]
       n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=2)
       dy[5] = n[0]*dy[2]
       return dy

   dvar, yvars, contours = FSA(dmf, [1.7, -0.2, 0], myfsa, 2,
                               nsurf=500, depvar='eta', dpow=0.5, **kwargs)
'''
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.integrate import ode
from scipy.integrate import quad
from scipy.interpolate import interp1d
from nimpy.plot_nimrod import PlotNimrod as pn
import traceback
import yaml


class FSAInput:
    '''
    FSA control data that can set nimpy yaml input file based input parameters.
    Up to external scripts to call FSAInput.read_fsa_input().
    '''
    d = {
        # o-point RZPhi location
        'rzo': None,
        # x-point RZPhi location
        'rzx': None,
        # starting FSA RZPhi location (useful for annulus)
        'rzs': None,
        # starting FSA psi (useful for annulus)
        'psis': None,
        # starting FSA phi (useful for annulus)
        'phis': None,
        # number of FSA contours
        'nsurf': 50,
        # set to 'eta' or 'len' to define field line ODE equations
        'depvar': 'eta',
        # add plot contours to next plot
        'plot_contours': False,
        # set less than 1 to pack surfaces outboard and greater than 1 to pack
        # inboard
        'dpow': 1.0,
        # if true and n=0 perturbation to B_eq
        'addpert': True,
    }
    # true if read_fsa_input has been called
    __nimpy_read = False

    def __read_nimpy(nimpyin: str = 'nimpy.yaml') -> dict:
        '''
        Read the fsa data from a nimpy yaml input file

        :param nimpyin: nimpy.yaml file name (Default nimpy.yaml)
        :return: dictionary of input variables
        '''
        # check if file exists, and if so read the file
        if not os.path.isfile(nimpyin):
            msg = f"Unable to find nimpy yaml file {nimpyin}\n" \
                + "Find an example in the source at \n" \
                + "https://gitlab.com/NIMRODteam/open/nimpy/-/" \
                + "blob/main/scripts/examples/nimpy.plot_field.yaml"
            raise FileNotFoundError(msg)

        with open(nimpyin, 'r') as file:
            inputdict = yaml.safe_load(file)

        # search for correct "namelist" in yaml file
        NAME_LIST = 'fsa'
        inputs = inputdict.get(NAME_LIST, None)
        if inputs is None:
            msg = f"Namelist {NAME_LIST} not found in file {nimpyin}"
            raise KeyError(msg)
        return inputs

    def read_fsa_input(nimpyin: str = 'nimpy.yaml') -> dict:
        '''
        Set class data based on read data from a nimpy yaml file.

        :param nimpyin: nimpy.yaml file name (Default nimpy.yaml)
        '''
        if FSAInput.__nimpy_read:
            return
        FSAInput.__nimpy_read = True
        fsa_dict = FSAInput.__read_nimpy(nimpyin)
        for key, val in fsa_dict.items():
            if key not in FSAInput.d.keys():
                msg = f"nimpy.yaml fsa invalid entry {key}"
                raise KeyError(msg)
            if isinstance(FSAInput.d[key], bool) and not isinstance(val, bool):
                msg = f"nimpy.yaml fsa expected boolean for {key}"
                raise KeyError(msg)
            FSAInput.d[key] = val


def get_b0(eval_nimrod, rzn, addpert=True, abort=False):
    '''
    Find the axisymmetric magnetic field at a given point.

    :param eval_nimrod: :class:`nimpy.eval_nimrod.EvalNimrod` instance
    :param rzn: field RZ location
    :param addpert: if False only use eq, if True add n=0 to eq
    :param abort: raise an exception if true and can't find b
    :return: magnetic field vector
    '''
    if addpert:
        eq = 3
    else:
        eq = 1
    b0 = eval_nimrod.eval_field('b', rzn, dmode=0, eq=eq)
    if abort and np.isnan(b0).any():
        raise Exception('fsa.py get_b0: Hit wall')
    return b0


def find_pf_null(eval_nimrod, rzn, addpert=True):
    '''
    Find a poloidal field null.

    :param eval_nimrod: :class:`nimpy.eval_nimrod.EvalNimrod` instance
    :param rzn: initial guess for poloidal field null
    :param addpert: if False only use eq, if True add n=0 to eq
    :return: RZ location of null
    '''
    rzn = np.array(rzn)
    maxsteps = 1000
    it = 0
    rtol = 1.e-8
    drz0 = 0.125 * rzn[0]
    while True:
        b = get_b0(eval_nimrod, rzn, addpert=addpert)
        norm0 = np.sqrt(b[0]**2 + b[1]**2)
        rvn = -rzn[0] * b[1] / norm0
        zvn = rzn[0] * b[0] / norm0
        drz = drz0 * (1.0 - float(it) / maxsteps) + rtol * rzn[0]
        while True:
            rr = rzn[0] + rvn * drz
            zz = rzn[1] + zvn * drz
            rzng = np.array([rr, zz, 0])
            try:
                b = get_b0(eval_nimrod, rzng, addpert=addpert)
                norm = np.sqrt(b[0]**2 + b[1]**2)
                if norm < norm0:
                    rzn = np.array([rr, zz, 0])
                    break
            except Exception:
                pass
            rr = rzn[0] - rvn * drz
            zz = rzn[1] - zvn * drz
            rzng = np.array([rr, zz, 0])
            try:
                b = get_b0(eval_nimrod, rzng, addpert=addpert)
                norm = np.sqrt(b[0]**2 + b[1]**2)
                if norm < norm0:
                    rzn = np.array([rr, zz, 0])
                    break
            except Exception:
                pass
            drz = drz / 2.0
            if drz / rzn[0] < rtol:
                return rzn  # done
        it = it + 1
        if it >= maxsteps:
            raise Exception('fsa.py find_pf_null: No convergence')


def find_xpoint(eval_nimrod, rz_guess=[1.3, -1.1, 0.], addpert=True):
    '''
    Find the x-point. Use file-based or previous result if available as a
    guess.

    :param rz_guess: Guess to use if not provided by `fsa_vals.txt` file.
    :param addpert: if False only use eq, if True add n=0 to eq
    :return: RZ location of x-point.
    '''
    if FSAInput.d['rzx'] is not None:
        rz_guess = [FSAInput.d['rzx'][0], FSAInput.d['rzx'][1], 0.]
    else:
        rz_guess = [rz_guess[0], rz_guess[1], 0.]
    try:
        FSAInput.d['rzx'] = find_pf_null(eval_nimrod, rz_guess,
                                         addpert=addpert)
        print('Found x-point at ', FSAInput.d['rzx'])
    except Exception as e:
        if FSAInput.d['rzx'] is not None:
            print('Unable to find x-point, using value ', FSAInput.d['rzx'])
        else:
            raise Exception('fsa.py find_xpoint: Unable to find x-point,'
                            + ' using ' + str(rz_guess) + ', ' + str(e))
    return FSAInput.d['rzx']


def find_opoint(eval_nimrod, rz_guess=[1.7, 0., 0.], addpert=True):
    '''
    Find the o-point. Use file-based or previous result if available as a
    guess.

    :param rz_guess: Guess to use if not provided by `fsa_vals.txt` file.
    :param addpert: if False only use eq, if True add n=0 to eq
    :return: RZ location of o-point.
    '''
    if FSAInput.d['rzo'] is not None:
        rz_guess = [FSAInput.d['rzo'][0], FSAInput.d['rzo'][1], 0.]
    else:
        rz_guess = [rz_guess[0], rz_guess[1], 0.]
    try:
        FSAInput.d['rzo'] = find_pf_null(eval_nimrod, rz_guess,
                                         addpert=addpert)
        print('Found o-point at ', FSAInput.d['rzo'])
    except Exception as e:
        if FSAInput.d['rzo'] is not None:
            print('Unable to find o-point, using value ', FSAInput.d['rzo'])
        else:
            raise Exception('fsa.py find_opoint: Unable to find o-point,'
                            + ' using ' + str(rz_guess) + ', ' + str(e))
    return FSAInput.d['rzo']


def __int_polflux(delta, rzst, rz, norm, tang, eval_nimrod, addpert):
    '''
    Integrand for poloidal flux.

    :param delta: distance from rzst
    :param rzst: starting rzp location
    :param rz: work-space for rzp
    :param norm: normal unit vector to line of integration in rzp
    :param tang: tangential unit vector to line of integration in rzp
    :param eval_nimrod: instance of :class:`nimpy.eval_nimrod.EvalNimrod`
    :param addpert: if true add n=0 perturbation to beq
    :return: poloidal flux integrand
    '''
    rz = rzst + tang * delta
    b = get_b0(eval_nimrod, rz, addpert=addpert, abort=True)
    return -2.0 * np.pi * rz[0] * np.dot(b[0:3], norm)


def compute_polflux(rzo, rz_arr, eval_nimrod, psis, addpert=True):
    '''
    Compute the poloidal flux at rz_arr given rzo.

    :param rzo: o-point rz location
    :param rz_arr: return polflux at these points
    :param eval_nimrod: instance of :class:`nimpy.eval_nimrod.EvalNimrod`
    :param addpert: if true add n=0 perturbation to beq
    '''
    rzo = np.array(rzo)
    polflux = np.zeros(rz_arr.shape[1])
    rz = np.zeros(3)
    rz[:] = rzo
    delta = np.linalg.norm(rz_arr[:, -1] - rzo)
    tang = (rz_arr[:, -1] - rzo) / delta
    norm = np.cross(tang, np.array([0, 0, 1]))
    delta0 = 0
    rzst = rzo
    for ii in range(rz_arr.shape[1]):
        delta1 = np.linalg.norm(rzst - rz_arr[:, ii])
        if (psis != 0 and ii == 0):
            delta0 = delta1
            continue
        polflux[ii], err = quad(__int_polflux, delta0, delta1,
                                args=(rzst, rz, norm, tang,
                                      eval_nimrod, addpert))
        delta0 = delta1
    return np.cumsum(polflux) + 2.*np.pi*psis


def compute_torflux(polflux, phis, q):
    '''
    Compute the toroidal flux from the poloidal flux and the safety factor.

    :param polflux: computed poloidal flux
    :param phis: starting value of the toroidal flux
    :param q: safety factor profile
    :return: toroidal flux
    '''
    qpsispl = interp1d(polflux[0:len(q)], q, kind='cubic',
                       fill_value='extrapolate')
    torflux = np.zeros(len(polflux))
    for ipsi in range(len(polflux)):
        if ipsi == 0:
            if phis != 0:
                continue
            pfst = 0.0
        else:
            pfst = polflux[ipsi - 1]
        torflux[ipsi], err = quad(qpsispl, pfst, polflux[ipsi])
    return np.cumsum(torflux) + phis


def dummy_int(rzc, y, dy, evalnim, fdict):
    """
    Dummy FSA integrand for calling FSA without specifing a new integrand
    :param rzc: pre-allocated space for rzc (current rz) for optimization
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param eval_nimrod: eval_nimrod class instance
    :param fdict: additional arguments to send to integrand

    There are times when it's useful to call the fsa without specifing
    new quatities to be integrated. For example, this happens when we
    want to trace a surface, or determine q(psi) or torflux(psi)

    When using dummy integrand set neq=1

    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    dy(0)=dl/deta or deta/dl
    dy(1)=dr/deta or dr/dl
    dy(2)=1/bdgth :v'
    dy(3)=dq: q
    dy(4)=dummy
    """
    dy[4] = 1.0
    return dy


def __field_line_eta(eta, y, dy, rzc, eval_nimrod, rzo, odedfdt, offeta,
                     fdict):
    '''
    Integrates a field line with a independent variable of eta.

    :param eta: independent variable
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param rzc: pre-allocated space for rzc (current rz) for optimization
    :param eval_nimrod: :class:`nimpy.eval_nimrod.EvalNimrod` instance
    :param odedfdt: functor to equations for FSA
    :param rzo: o-point rz location
    :param offeta: off set eta for transform from eta, r to rz
    :param fdict: additional arguments to send to integrand
    '''
    addpert = fdict.get("addpert", True)
    cosfac = np.cos(eta + offeta)
    sinfac = np.sin(eta + offeta)
    rzc[0] = rzo[0] + y[1] * cosfac
    rzc[1] = rzo[1] + y[1] * sinfac
    b = get_b0(eval_nimrod, rzc, addpert, abort=True)
    dy[0] = y[1] / (b[1] * cosfac - b[0] * sinfac)
    dy[1] = dy[0] * (b[0] * cosfac + b[1] * sinfac)  # dr/deta
    dy[0] = np.sqrt(b[0]**2 + b[1]**2) * dy[0]  # dl/deta
    bdgth = (b[1] * cosfac - b[0] * sinfac) / y[1]
    dy[2] = 1.0 / bdgth  # 1/bdgth
    dy[3] = b[2] * dy[2] / rzc[0]  # q
    return odedfdt(rzc, y, dy, eval_nimrod, fdict)


def __field_line_len(length, y, dy, rzc, eval_nimrod, rzo, odedfdt, offeta,
                     fdict):
    '''
    Integrates a field line with a independent variable of length.

    :param eta: independent variable
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param rzc: pre-allocated space for rzc (current rz) for optimization
    :param eval_nimrod: :class:`nimpy.eval_nimrod.EvalNimrod` instance
    :param odedfdt: functor to equations for FSA
    :param rzo: o-point rz location
    :param offeta: off set eta for transform from eta, r to rz
    :param fdict: additional arguments to send to integrand
    '''
    addpert = fdict("addpert", True)
    cosfac = np.cos(y[0] + offeta)
    sinfac = np.sin(y[0] + offeta)
    rzc[0] = rzo[0] + y[1] * cosfac
    rzc[1] = rzo[1] + y[1] * sinfac
    b = get_b0(eval_nimrod, rzc, addpert, abort=True)
    bp = np.sqrt(b[0]**2 + b[1]**2)
    dy[0] = (-b[0] * sinfac + b[1] * cosfac) / (y[1] * bp)  # deta/dl
    dy[1] = (b[0] * cosfac + b[1] * sinfac) / bp  # dr/dl
    bdgth = (rzc[0] * b[1] - rzc[1] * b[0]) / y[1]**2
    dy[2] = 1.0 / bdgth  # 1/bdgth
    dy[3] = b[2] * dy[2] / rzc[0]  # q
    return odedfdt(rzc, y, dy, eval_nimrod, fdict)


def FSA(eval_nimrod, rzo, func, neq, nsurf=None, depvar=None,
        plot_contours=None, dpow=None, rzx=None, rzp=None, normalize=True,
        addpert=None, fargs={}, **kwargs):
    '''
    Perform flux-surface-average integration on func.

    :param eval_nimrod: :class:`nimpy.eval_nimrod.EvalNimrod` instance
    :param rzo: initial guess for o-point as array [r, z, 0]
    :param func: user defined function with signature
                 func(rzc, dy, eval_nimrod), where rzc is the current rzp
                 location, dy is to be filled neq times, and eval_nimrod is the
                 instance of the EvalNimrod class
    :param neq: number of user defined equations
    :param nsurf: number of surfaces in the FSA
    :param depvar: set to 'eta' or 'len' to define field line ODE equations
    :param plot_contours: plots contours if true
    :param dpow: set less than 1 to pack outboard and
                 greater than 1 to pack inboard
    :param rzx: guess for x-point to be used as endpoint of integration
                use this for diverted cases
    :param rzp: start point if nsurf=1
    :param normalize: normalize the fsa output
    :param addpert: if true and n=0 perturbation to B_eq
    :param fargs: an additional dictonary of arguments to pass to integrand
    :return (dvar,yvals,contours): tuple of returned numpy arrays
                 dvar - dependent vars psin,rhon,psi,phi,R,Z,V',q
                 yvals - user equations of size neq
                 contours - contours of size (rz,:,nsurf)
    '''
    if nsurf is None:
        nsurf = FSAInput.d['nsurf']
    if depvar is None:
        depvar = FSAInput.d['depvar']
    if addpert is None:
        addpert = FSAInput.d['addpert']
    if rzx is None:
        rzx = FSAInput.d['rzx']
    if plot_contours is None:
        plot_contours = FSAInput.d['plot_contours']
    if dpow is None:
        dpow = FSAInput.d['dpow']
    psis = 0.
    phis = 0.
    if nsurf > 1:
        # determine starting locations
        rzo = find_opoint(eval_nimrod, rzo, addpert)
        if None not in [FSAInput.d['rzs'], FSAInput.d['psis'],
                        FSAInput.d['phis']]:
            rzs = FSAInput.d['rzs']
            psis = FSAInput.d['psis']
            phis = FSAInput.d['phis']
        else:
            # use o-point by default
            rzs = rzo
        # Find x-point
        try:
            rzx1 = find_xpoint(eval_nimrod, rzx, addpert)
            rze = rzx1
        except Exception:
            # find the wall on the outboard midplane
            fac = 1.1
            rzwg1 = rzs
            while True:
                rzwg2 = np.array([rzs[0] * fac, rzs[1], 0])
                b = get_b0(eval_nimrod, rzwg2, addpert, abort=False)
                if np.isnan(b).any():
                    break
                rzwg1 = rzwg2
                fac = fac * 1.1
            while True:
                rzw = np.array([(rzwg1[0] + rzwg2[0]) * 0.5, rzs[1], 0])
                b = get_b0(eval_nimrod, rzw, addpert, abort=False)
                if np.isnan(b).any():
                    rzwg2 = rzw
                else:
                    rzwg1 = rzw
                if (np.fabs(rzwg2[0] - rzwg1[0]) / rzw[0] < 1.e-8):
                    break
            print('rzw=', rzw)
            rze = rzw
        # set startpoint locations
        dwall = np.linalg.norm(rze - rzs)
        tang = (rze - rzs) / dwall
        delta = dwall / float(nsurf + 1)
        rzs1 = np.array(rzs + tang * delta)
        rze1 = np.array(rze)
        start_points = pn.grid_1d_gen(rzs1, rze1, nsurf, dpow)
        # determine poloidal flux
        polflux = compute_polflux(rzo, start_points, eval_nimrod, psis,
                                  addpert)
        # to collect output
        dvar = np.zeros([8, nsurf])  # psin,rhon,psi,phi,R,Z,V',q
        dvar[2, :] = polflux / (2.0 * np.pi)
        dvar[4, :] = start_points[0][:]
        dvar[5, :] = start_points[1][:]
    yvals = np.zeros([neq, nsurf])
    yvals[:, :] = np.nan
    # preallocate arrays used in integrand for optimization
    dy = np.zeros(4 + neq)
    rzc = np.zeros(3)
    # loop over surfaces and integrate
    hit_wall = False
    rtol = kwargs.get("rtol", 1.e-8)
    nsteps_max = kwargs.get("nsteps_max", 1e5)
    if depvar == 'eta':
        lsode = ode(__field_line_eta).set_integrator('lsoda', rtol=rtol,
                                                     nsteps=nsteps_max)
        neta = kwargs.get("neta", 201)
        dt = np.pi * 2 / (neta - 1)
        contours = np.zeros([2, neta, nsurf])
    else:
        lsode = ode(__field_line_len).set_integrator('lsoda', rtol=rtol,
                                                     nsteps=nsteps_max)
        dt = 0.05
        contours = np.zeros([2, 1001, nsurf])
    if nsurf > 1:
        for isurf in range(nsurf):
            contours[0, :, isurf] = start_points[0][isurf]
            contours[1, :, isurf] = start_points[1][isurf]
    else:
        if rzp is None:
            raise Exception('nsurf=1 but rzp not set')
        contours[0, :, 0] = rzp[0]
        contours[1, :, 0] = rzp[1]
        dvar = np.zeros([8, nsurf])  # psin,rhon,psi,phi,R,Z,V',q
        dvar[2, :] = np.nan
        dvar[4, :] = rzp[0]
        dvar[5, :] = rzp[1]
    for isurf in range(nsurf):
        rzp = np.array([contours[0, 0, isurf], contours[1, 0, isurf], 0])
        if (all(rzp == FSAInput.d['rzx'])):
            continue  # Don't try to integrate the x-point
        y20 = np.sqrt((rzp[0] - rzo[0])**2 + (rzp[1] - rzo[1])**2)
        y0 = [0.0, y20, 0.0, 0.0]
        for ii in range(neq):
            y0.append(0.0)
        offeta = np.arctan2(rzp[1] - rzo[1], rzp[0] - rzo[0])
        lsode.set_initial_value(y0)
        fdict = fargs.copy()
        fdict.update(kwargs)
        fdict['psi'] = dvar[2, isurf]
        fdict['isurf'] = isurf
        fdict['addpert'] = addpert
        lsode.set_f_params(dy, rzc, eval_nimrod, rzo, func, offeta, fdict)
        keep_running = True
        ii = 1
        # collect contour locations during integration
        contours[:, 0, isurf] = rzp[0:2]
        while keep_running:
            try:
                lsode.integrate(lsode.t + dt)
            except Exception:
                hit_wall = True
                traceback.print_exc()
                break
            if depvar == "eta":
                eta = lsode.t
            else:
                eta = lsode.y[0]
            keep_running = lsode.successful() and eta - 2.0 * np.pi < 1.e-5
            if keep_running and ii < contours.shape[1]:
                eta = lsode.t
                cosfac = np.cos(eta + offeta)
                sinfac = np.sin(eta + offeta)
                rzc = np.array([rzo[0] + lsode.y[1] * cosfac,
                                rzo[1] + lsode.y[1] * sinfac, 0])
                # print(eta,rzc,ii)
                contours[:, ii, isurf] = rzc[0:2]
                ii = ii + 1
        if hit_wall:
            cosfac = np.cos(eta + offeta)
            sinfac = np.sin(eta + offeta)
            rzc = np.array([rzo[0] + lsode.y[1] * cosfac,
                            rzo[1] + lsode.y[1] * sinfac, 0])
            contours[0, ii:, isurf] = rzc[0]
            contours[1, ii:, isurf] = rzc[1]
            print("FSA: Hit wall, eta=", lsode.t, ", rz=", rzc[0], rzc[1])
            break
        if eta - 2.0 * np.pi < 1.e-5:
            print("FSA: too many steps in integration")
            break
        # normalize output
        if normalize:
            yvals[:, isurf] = lsode.y[4:4 + neq] / lsode.y[2]
        else:
            yvals[:, isurf] = lsode.y[4:4 + neq]
        if nsurf > 1:
            dvar[6, isurf] = lsode.y[2]  # V'
            dvar[7, isurf] = lsode.y[3] / (2.0 * np.pi)  # q
            print('isurf=', isurf + 1, 'RZ=', "{0:0.4f}".format(rzp[0]),
                  "{0:0.4f}".format(rzp[1]),
                  'q=', "{0:0.4f}".format(dvar[7, isurf]),
                  'psi=', "{0:0.4f}".format(dvar[2, isurf]))
    if nsurf == 1:
        return yvals[:, 0], contours[:, :, 0]
    print('integrated isurf=', isurf + 1, ' of nsurf=', nsurf)
    if hit_wall:
        dvar[6, isurf:] = dvar[6, isurf - 1]  # V'
        dvar[7, isurf:] = dvar[7, isurf - 1]  # q
    # Determine where the FSA failed
    iend = -1
    while np.isnan(yvals[:, iend]).any():
        iend -= 1
    iend += yvals.shape[1]
    # compute toroidal flux with good q values
    dvar[3, :] = compute_torflux(polflux, phis, dvar[7, :iend])
    # compute normalized fluxes
    dvar[0, :] = dvar[2, :] / dvar[2, -1]  # psin
    dvar[1, :] = np.sqrt(dvar[3, :] / dvar[3, -1])  # rhon
    if plot_contours:
        plt.plot(contours[0, :, :], contours[1, :, :])

    return (dvar[:, :iend], yvals[:, :iend], contours[:, :, :iend])
