### eval_nimrod.py
Call the eval_nimrod library to do field evaluation of the NIMROD basis
functions at arbitrary R,Z locations within the domain in either real or
complex (toroidally decomposed by Fourier mode). Returns NaNs if outside the
domain. Contains a grid class that assists with finding a good logical to
mapped coordinate solution.

### field_class.py
The results of the real-space eval_nimrod call can be stored as a Scalar,
Vector or Tensor which enables mathematical operations (dot product, cross
product, etc.) and derivatives (gradient, divergence, curl) as appropriate for
constructing derived quantities

### fsa.py
Contains routines to evaluate a flux-surface average and other quantities
associated with the magnetic topology.

### plot2D.py
Routine to create two-dimensional, RZ plots of real (sliced) or complex
(Fourier-mode decomposed) dump file quantities.

### plot_field.py
Routine to create two-dimensional RZ plots of real (sliced) or complex
(Fourier-mode decomposed) dump file quantities. Unlike plot2D this
uses the nodal values of the field. This is faster, but less
flexible.

### globaleq.py 
Computes global equilibrium quantities including those associated with the
geometry, the pressure, current, safety factor, etc.

### plot_nimrod.py
Plotting helper routines
