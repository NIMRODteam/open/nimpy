import copy
import ctypes.util  # Fixes odd bug with python 3.6
import ctypes
import f90nml
import itertools
import numpy as np
import os
import pickle
import sys


# load shared libraries
# ctypes.util.find_library does not work on mac, use nimsoft directory
if sys.platform == 'darwin':
    nimlibdir = os.environ['NIMSOFT'] + '/nimdevel/lib/'
    nimlib_shared = nimlibdir + "libnimlib.dylib"
    nimeval = nimlibdir + "libnimeval.dylib"
    try:
        nimlibobj = ctypes.CDLL(nimlib_shared)
    except OSError:
        print("Library nimlib_shared not found, set NIMSOFT")
        raise
    try:
        evalobj = ctypes.CDLL(nimeval)
    except OSError:
        print("Library nimeval not found, set NIMSOFT")
        raise
else:
    nimlib_shared = ctypes.util.find_library("nimlib")
    if nimlib_shared is None:
        message = 'Library nimlib_shared not found, set LD_LIBRARY_PATH.'
        raise Exception(message)
    nimlibobj = ctypes.CDLL(nimlib_shared, mode=os.RTLD_LAZY)
    nimeval = ctypes.util.find_library("nimeval")
    if nimeval is None:
        raise Exception('Library nimeval not found, set LD_LIBRARY_PATH.')
    evalobj = ctypes.CDLL(nimeval)


class EvalNimrod:
    '''
    Read a NIMROD dump file and evaluate fields at specified locations.
    '''

    def __init__(self, dumpfile, fieldlist="bvnpf", coord='rzp', path="./"):
        '''
        Initializes library to read file, and requires nimrod.in to be present.
        Only one file can be opened at a time because of the shared library
        nature of this interface. Opening a second file will automatically
        close the first.

        :param dumpfile: NIMROD dump file to read
        :param fieldlist: Fields to initialize within the NIMROD library
        :param coord: Coordinate system to use
        :param path: path to nimrod.in file
        '''

        # initialize libraries
        cstr_dumpfile = ctypes.create_string_buffer(dumpfile.encode('utf-8'))
        cstr_fieldlist = ctypes.create_string_buffer(fieldlist.encode('utf-8'))
        evalobj.c_nimfield_init(ctypes.byref(cstr_dumpfile),
                                ctypes.byref(cstr_fieldlist))

        # intialize stored guess (array of size 2)
        self.c_xyguess = (ctypes.c_double * 2)(-1., -1.)

        # read and store namelist variables
        in_file = path + 'nimrod.in'
        try:
            nml = f90nml.read(in_file)
        except Exception:
            raise IOError("Can't open nimrod.in")

        self.ndnq = nml['equil_input'].get('nisp', 0) + 1
        self.nnsp = nml['equil_input'].get('nnsp', 0)
        self.ds_nqty = nml['equil_input'].get('ds_nqty', 1)
        self.nonlinear = nml['physics_input'].get('nonlinear', True)
        self.dealiase = nml['grid_input'].get('dealiase', 3)
        self.nphi = nml['grid_input'].get('nphi', -1)
        self.lphi = nml['grid_input'].get('lphi', 2)
        if self.nphi < 0:
            self.nphi = 2**self.lphi
        self.lin_nmodes = nml['grid_input'].get('lin_nmodes', 1)
        self.lin_nmax = nml['grid_input'].get('lin_nmax', 0)
        self.zimp = nml['closure_input'].get('zimp', 0)

        if self.nnsp > 0:
            nimlibobj.c_set_degas_tables()

        # determine nmodes_total
        if self.nonlinear:  # nonlinear is true
            self.nmodes = self.nphi//self.dealiase + 1
            self.nmodes_peq = self.nmodes + 1
            self.modelist = [i for i in range(-1, self.nmodes)]  # -1 for eq
        else:  # linear
            if self.lin_nmax > 0:
                self.modelist = [-1]
                for i in range(0, self.lin_nmodes):
                    self.modelist.append(self.lin_nmax-(self.lin_nmodes-1)+i)
            else:
                self.modelist = [i for i in range(-1, self.lin_nmodes)]
            self.nmodes_peq = self.lin_nmodes + 1

        # set the coordinate system
        if coord == 'rzp':
            self.__transform = False
        elif coord == 'xyz':
            self.__transform = True
        else:
            raise Exception('Unsupported coordinate system.')

        return

    @staticmethod
    def __rzp_to_xyz(rzp_coord):
        '''
        Transforms rzp coordinate system to xyz coordinate system.

        :param rzp_coord: coordinates of grid points in (R, Z, Phi)
        :return: coordinates of grid points in (X, Y, Z)
        '''

        # create xyz array with the same dimension as rzp
        xyz_coord = np.zeros(np.shape(rzp_coord))

        # transform the coordinate system
        xyz_coord[0] = rzp_coord[0] * np.cos(rzp_coord[2])
        xyz_coord[1] = rzp_coord[0] * np.sin(rzp_coord[2])
        xyz_coord[2] = rzp_coord[1]

        return xyz_coord

    @staticmethod
    def __xyz_to_rzp(xyz_coord):
        '''
        Transforms xyz coordinate system to rzp coordinate system.

        :param xyz_coord: coordinates of grid points in (X, Y, Z)
        :return: coordinates of grid points in (R, Z, Phi)
        '''

        # create rzp array with the same dimension as xyz
        rzp_coord = np.zeros(np.shape(xyz_coord))

        # transform the coordinate system
        rzp_coord[0] = np.sqrt(xyz_coord[0]**2 + xyz_coord[1]**2)
        rzp_coord[1] = xyz_coord[2]
        rzp_coord[2] = np.arctan2(xyz_coord[1], xyz_coord[0])

        return rzp_coord

    @staticmethod
    def __eval_field_on_point(c_xyguess, field, rzp, fsize, nqty, dmode, eq):
        '''
        Evaluate the specified field at a single point given by rzp.

        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of the point in R-Z-Phi coordinate
                    (array with shape(3))
        :return: value of the field at point rzp
                 (nan if point is out of bound)
        '''

        # convert data to C
        c_rzp = (ctypes.c_double * 3)(*rzp)
        cstr_fname = ctypes.create_string_buffer(field.encode('utf-8'))
        c_fval = (ctypes.c_double * fsize)(*np.zeros(fsize))
        c_nqty = (ctypes.c_int)(nqty)
        c_ifail = (ctypes.c_int)(0)
        c_dmode = (ctypes.c_int)(dmode)
        c_eq = (ctypes.c_int)(eq)

        # call the eval function
        evalobj.c_eval_field_dm(
                ctypes.byref(c_rzp), ctypes.byref(cstr_fname),
                ctypes.byref(c_fval), ctypes.byref(c_nqty),
                ctypes.byref(c_xyguess), ctypes.byref(c_ifail),
                ctypes.byref(c_dmode), ctypes.byref(c_eq))

        # copy results to output
        res = np.array(c_fval[0:fsize])

        # change the value to NaN if the point is out of bound
        ifail = c_ifail.value
        if ifail != 0:
            res = np.nan * res

        return res

    @staticmethod
    def __eval_comp_field_on_point(c_xyguess, field, rzp, fsize, nqty, dmode):
        '''
        Evaluate the specified field at a single point given by rzp.

        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of the point in R-Z-Phi coordinate
                    (array with shape(3))
        :return: value of the field at point rz
                 (nan if point is out of bound)
        '''

        tot_size = 2*fsize[0]*fsize[1]*fsize[2]

        # convert data to C
        rz = rzp[0:2]  # This gives you rz[0] and rz[1]
        c_rz = (ctypes.c_double * 2)(*rz)
        cstr_fname = ctypes.create_string_buffer(field.encode('utf-8'))
        c_fval = (ctypes.c_double * tot_size)(*np.zeros(tot_size))
        c_nqty = (ctypes.c_int)(nqty)
        c_ifail = (ctypes.c_int)(0)
        c_dmode = (ctypes.c_int)(dmode)

        # call the eval function
        evalobj.c_eval_comp_field_dm(
                ctypes.byref(c_rz), ctypes.byref(cstr_fname),
                ctypes.byref(c_fval), ctypes.byref(c_nqty),
                ctypes.byref(c_xyguess), ctypes.byref(c_ifail),
                ctypes.byref(c_dmode))

        # copy results to output
        res = np.array(c_fval[0:tot_size])
        ifail = c_ifail.value
        if ifail != 0:
            res = np.nan*res

        res2 = np.zeros([fsize[0], fsize[1], fsize[2]], dtype=np.complex64)

        for iqty in range(0, fsize[0]):
            for imode in range(0, fsize[1]):
                for ideriv in range(0, fsize[2]):
                    cindex = 2*((iqty+1)+imode*fsize[0]+ideriv*nqty*fsize[1])-2
                    res2[iqty, imode, ideriv] = res[cindex]+1j*res[cindex+1]

        return res2

    def __eval_field_on_line(self, field, rzp, fsize, nqty, dmode, eq, nr,
                             xygrid, comp=False):
        '''
        Evaluate the specified field along a line given by rzp.

        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on line in R-Z-Phi coordinate
                    (array with shape(3, nr))
        :return: value of the field along the line defined by rzp
                 (nan if point is out of bound)
        '''

        # initialize the result with NaNs
        if comp:
            res = np.full([fsize[0], fsize[1], fsize[2], nr], np.nan,
                          dtype=np.complex64)
        else:
            res = np.full([fsize, nr], np.nan)

        # call the eval_field at each point
        for index in range(nr):
            if xygrid is not None:
                if np.isnan(xygrid[:, index]).any():
                    continue
                guess = \
                    (ctypes.c_double * 2)(xygrid[0, index], xygrid[1, index])
            else:
                guess = self.c_xyguess

            if comp:
                res[:, :, :, index] = \
                    self.__eval_comp_field_on_point(guess, field,
                                                    rzp[:, index], fsize, nqty,
                                                    dmode)
            else:
                res[:, index] = \
                    self.__eval_field_on_point(guess, field, rzp[:, index],
                                               fsize, nqty, dmode, eq)

        return res

    def __eval_field_on_2d_grid(self, field, rzp, fsize, nqty, dmode, eq, nr,
                                nz, xygrid, comp=False):
        '''
        Evaluate the specified field on a 2d grid given by rzp.

        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on grid in R-Z-Phi coordinate
                    (array with shape(3, nr, nz))
        :return: value of the field on the 2d grid defined by rzp
                 (nan if point is out of bound)
        '''

        # initialize the result with NaNs
        if comp:
            res = np.full([fsize[0], fsize[1], fsize[2], nr, nz], np.nan,
                          dtype=np.complex64)
        else:
            res = np.full([fsize, nr, nz], np.nan)

        # call each eval_field_on_line for each line
        for index in range(nz):
            xy = None
            if xygrid is not None:
                xy = xygrid[:, :, index]
            if comp:
                res[:, :, :, :, index] = \
                    self.__eval_field_on_line(field, rzp[:, :, index],
                                              fsize, nqty, dmode, eq, nr,
                                              xy, comp)
            else:
                res[:, :, index] = \
                    self.__eval_field_on_line(field, rzp[:, :, index],
                                              fsize, nqty, dmode, eq, nr,
                                              xy, comp)

        return res

    def __eval_field_on_3d_grid(self, field, rzp, fsize, nqty, dmode, eq, nr,
                                nz, nphi, xygrid):
        '''
        Evaluate the specified field on a 3d grid given by rzp.

        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on grid in R-Z-Phi coordinate
                    (array with shape(3, nr, nz, nphi))
        :return: value of the field on the 3d grid defined by rzp
                 (nan if point is out of bound)
        '''

        # initialize the result with NaNs
        res = np.full([fsize, nr, nz, nphi], np.nan)

        # call each eval_field_on_2d_grid for each plane
        for index in range(nphi):
            xy = None
            if xygrid is not None:
                xy = xygrid[:, :, :, index]
            res[:, :, :, index] = \
                self.__eval_field_on_2d_grid(field, rzp[:, :, :, index],
                                             fsize, nqty, dmode, eq, nr, nz,
                                             xy)

        return res

    def __get_nqty(self, field, dmode=0, comp=False):
        '''
        Function to get nqty and fize.

        :param field: the specified field
        :param dmode: derivative order
        :param comp: true if complex data
        :return: integer nqty, fsize
        '''

        vector_list = ['b', 'v', 'j', 'f', 'e', 'qi', 'qe', 'dp']
        nqty = 1
        if field in vector_list:
            nqty = 3
        elif field == 'n':
            nqty = self.ndnq
        elif field == 'ndn' or field == 'tn':
            nqty = self.nnsp
        elif field == 'vn':
            nqty = 3 * self.nnsp
        elif field == 'd':
            nqty = self.ds_nqty
        elif field == 'r':
            nqty = 4
        elif field == 'neo_diff':
            nqty = 2
        elif field == 'nz':
            nqty = self.zimp + 1

        if dmode == 0:
            if comp:
                fsize = [nqty, self.nmodes_peq, 1]
            else:
                fsize = nqty
        elif dmode == 1:
            if comp:
                fsize = [nqty, self.nmodes_peq, 4]
            else:
                fsize = nqty * 4
        elif dmode == 2:
            if comp:
                fsize = [nqty, self.nmodes_peq, 10]
            else:
                fsize = nqty * 10
        else:
            raise Exception('incorrect order of derivative.')

        return nqty, fsize

    def __transform_rzp(self, rzp):
        '''
        Transform rzp if self.__transform is true

        :param rzp: grid coordinates
        :return: transformed grid
        '''
        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rzp(rzp)
        return rzp

    def eval_field(self, field, rzp, dmode=0, eq=2):
        '''
        Evaluate a real field at RZP point(s).

        :param field: the specified field
        :param rzp: coordinate of the points in following format:
                    (3)                : single point
                    (3, nx1)           : line
                    (3, nx1, nx2)      : plane
                    (3, nx1, nx2, nx3) : cube
        :param dmode: order of derivatives required
                      0 : field only
                      1 : field + first derivatives
                      2 : field + first derivatives + second derivatives
        :param eq: integer value determining equilibrium and perturbation part
                   4 : perturbation - n0 perturbation
                   3 : equilibrium + n0 perturbation
                   2 : equilibrium + perturbation
                   1 : equilibrium
                   0 : perturbation
        :return: array of field values (nqty*nderiv, rzp.shape)
        '''

        if isinstance(rzp, EvalGrid):
            dimension = rzp.rzp.ndim - 1
            rzpgrid = rzp.rzp
            evalgrid = True
            if not rzp.initalized:
                rzp.set_logical_grid(field, self)
            xygrid = rzp.xy
        else:
            dimension = rzp.ndim - 1
            rzpgrid = rzp
            xygrid = None
            evalgrid = False

        nqty, fsize = self.__get_nqty(field, dmode)
        # set the grid size
        if dimension > 0:
            nr = rzpgrid.shape[1]
        if dimension > 1:
            nz = rzpgrid.shape[2]
        if dimension > 2:
            np = rzpgrid.shape[3]
        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rzp(rzp)

        # call eval_* functions according to dimension

        if dimension == 0:
            if evalgrid:
                # copy prevents changes to xygrid
                temp = copy.copy(xygrid)
                guess = (ctypes.c_double * 2)(temp[0], temp[1])
            else:
                guess = self.c_xyguess
            fval = self.__eval_field_on_point(guess, field, rzpgrid, fsize,
                                              nqty, dmode, eq)
        elif dimension == 1:
            fval = self.__eval_field_on_line(field, rzpgrid, fsize, nqty,
                                             dmode, eq, nr, xygrid)
        elif dimension == 2:
            fval = self.__eval_field_on_2d_grid(field, rzpgrid, fsize, nqty,
                                                dmode, eq, nr, nz, xygrid)
        else:
            fval = self.__eval_field_on_3d_grid(field, rzpgrid, fsize, nqty,
                                                dmode, eq, nr, nz, np, xygrid)
        return fval

    def eval_comp_field(self, field, rzp, dmode=0):
        '''
        Evaluate a complex field at RZ point(s).

        :param field: the specified field
        :param rzp: coordinate of the points in following format:
                    (3)                : single point
                    (3, nx1)           : line
                    (3, nx1, nx2)      : plane
        :param dmode: order of derivatives required
                      0 : field only
                      1 : field + first derivatives
                      2 : field + first derivatives + second derivatives
        :return: array of complex field values
                 size (nqty, nmodes, nderiv, rzp.shape)
        '''

        if isinstance(rzp, EvalGrid):
            dimension = rzp.rzp.ndim - 1
            rzpgrid = rzp.rzp
            evalgrid = True
            if not rzp.initalized:
                rzp.set_logical_grid(field, self)
            xygrid = rzp.xy
        else:
            dimension = rzp.ndim - 1
            rzpgrid = rzp
            xygrid = None
            evalgrid = False

        nqty, fsize = self.__get_nqty(field, dmode, comp=True)
        # set the grid size
        if dimension > 0:
            nr = rzpgrid.shape[1]
        if dimension > 1:
            nz = rzpgrid.shape[2]
        if dimension > 2:
            raise Exception('eval_comp_field only works for 2D planes')
        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rzp(rzp)

        # call eval_* functions according to dimension

        if dimension == 0:
            if evalgrid:
                # copy prevents changes to xygrid
                guess = copy.copy(xygrid)
            else:
                guess = self.c_xyguess
            fval = self.__eval_comp_field_on_point(guess, field, rzpgrid,
                                                   fsize, nqty, dmode)
        elif dimension == 1:
            fval = self.__eval_field_on_line(field, rzpgrid, fsize, nqty,
                                             dmode, None, nr, xygrid,
                                             comp=True)
        else:
            fval = self.__eval_field_on_2d_grid(field, rzpgrid, fsize, nqty,
                                                dmode, None, nr, nz,
                                                xygrid, comp=True)
        return fval

    def eval_field_with_guess(self, field, rzp, c_xyguess, dmode=0, eq=2):
        '''
        Evaluate a real field at an rzp point with a guess.

        :param field: the specified field
        :param rzp: coordinate of the points in following format:
                    (3)                : single point
        :param c_xyguess: ctype logical coordinates to use as an initial guess:
                    (2)                : single point
        :param dmode: order of derivatives required
                      0 : field only
                      1 : field + first derivatives
                      2 : field + first derivatives + second derivatives
        :param eq: integer value determining equilibrium and perturbation part
                   4 : perturbation - n0 perturbation
                   3 : equilibrium + n0 perturbation
                   2 : equilibrium + perturbation
                   1 : equilibrium
                   0 : perturbation
        :return: array of field values (nqty, nderiv)
        '''

        dimension = rzp.ndim - 1
        if dimension != 0:
            print("eval_field_with_guess only works on a point")
            raise ValueError

        nqty, fsize = self.__get_nqty(field, dmode)

        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rzp(rzp)

        fval = self.__eval_field_on_point(c_xyguess, field, rzp, fsize,
                                          nqty, dmode, eq)
        return fval

    @staticmethod
    def eval_degas(ne, te):
        '''
        Evaluate the deuterium Degas2 tables.

        :param ne: electron density (input)
        :param te: electron temperature (input)
        :return (sion, srec): <sigma_ion v>, <sigma_rec v>
        '''
        c_ne = (ctypes.c_double)(ne)
        c_te = (ctypes.c_double)(te)
        c_sion = (ctypes.c_double)(0.0)
        c_srec = (ctypes.c_double)(0.0)

        nimlibobj.c_eval_degas_tables(ctypes.byref(c_ne),
                                      ctypes.byref(c_te),
                                      ctypes.byref(c_sion),
                                      ctypes.byref(c_srec))

        return c_sion.value, c_srec.value


class EvalGrid:
    '''
    Grid class for saving NIMROD logical data associated with a coordinate
    grid.  This is useful when evaluating a multiple fields on the same
    coordinate grid as it avoids multiple searches for the logical to
    coordinate mapping.  The logic in the `set_logical_grid` routine also
    assists in giving more guesses to find the logical solution for coordinate
    grid locations.
    '''

    def __init__(self, rzp, max_levels=2, filename=None):
        '''
        Initialize grid class.

        :param rzp: coordinates at which to find logical mapping
        :param max_levels: number of iterative recursions to refine mapping
        '''
        self.rzp = rzp
        self._dim = 0
        xyshape = [2]
        if rzp.ndim > 1:
            xyshape.append(rzp.shape[1])
            self._dim = 1
        if rzp.ndim > 2:
            xyshape.append(rzp.shape[2])
            self._dim = 2
        if rzp.ndim > 3:
            xyshape.append(rzp.shape[3])
            self._dim = 3
        if rzp.ndim > 4:
            print("Higher dimension grids are not supported in EvalGrid")
            raise ValueError

        self.xy = np.full(xyshape, np.nan)
        self.initalized = False
        # test shows 2 or 3 is usually enough, keep >7 for 3D
        self.__max_levels = max_levels
        self.print_debug = {'line': False, "plane": False, "volume": False}
        self.symm_3d = False
        self.filename = filename

    def set_debug(self, level):
        '''
        Set debug statement level.

        :param level:
            0 no debug
            1 debug on line, plane, volume
            2 debug on plane, volume
            3 debug on volume
        '''
        if level in [1, 2, 3]:
            self.print_debug["volume"] = True
        if level in [1, 2]:
            self.print_debug["plane"] = True
        if level in [1]:
            self.print_debug["line"] = True

    def set_3d_symm(self, symm=True):
        '''
        Set 3d_symm boolean variable that uses a 2D logical to coordinate
        mapping for 3D grids.

        :param symm: set to true if mesh is symmetric in 3rd direction
        '''
        self.symm_3d = symm

    def _load(self):
        '''
        Load mapping class from pickle file.
        '''
        try:
            f = open(self.filename, 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.__dict__.update(tmp_dict)
        except Exception:
            raise Exception(f'File {self.filename} not found')

    def _save(self):
        '''
        Save mapping class to pickle file.
        '''
        f = open(self.filename, 'wb')
        pickle.dump(self.__dict__, f, 2)
        f.close()

    def set_logical_grid(self, field, eval_nimrod):
        '''
        Main function that sets logical grid.

        :param field: a physical field initialized in eval_nimrod
        :param eval_nimrod: instance of :class:`EvalNimrod` to use
        '''

        # try to read object from file
        if self.filename:
            try:
                self._load()
                return
            except Exception as e:
                print('Generating logical mapping for EvalGrid as '+str(e))

        level = 1
        if self._dim == 0:
            self.xy = self.__set_logical_on_point(field, eval_nimrod)
        elif self._dim == 1:
            self.xy = self.__set_logical_on_1D_grid(level, field, self.xy,
                                                    self.rzp, eval_nimrod)
        elif self._dim == 2:
            self.xy = self.__set_logical_on_2D_grid(level, field, self.xy,
                                                    self.rzp, eval_nimrod)
        elif self._dim == 3:
            self.xy = self.__set_logical_on_3D_grid(level, field, self.xy,
                                                    self.rzp, eval_nimrod)
        else:
            print("Higher dimension grids are not supported")
            raise ValueError
        self.initalized = True

        # write object to file
        if self.filename:
            self._save()

    def __set_logical_on_point(self, field, eval_nimrod):
        '''
        Set the logical grid value on a point using eval_nimrod.c_xyguess
        '''
        xy = np.full([2], np.nan)
        res = eval_nimrod.eval_field_with_guess(field, self.rzp,
                                                eval_nimrod.c_xyguess)
        if not np.isnan(res).any():
            xy = np.array(eval_nimrod.c_xyguess[0:2])
        return xy

    def __set_logical_on_1D_grid(self, level, field, xy, rzp, eval_nimrod):
        '''
        Set the logical grid value on a 1d grid. March forward and then
        backward along grid.  First try eval_nimrod guess, second try previous
        point on grid, and third march from the previous point to current
        point.
        '''
        substeps = 5  # number of sub steps to use when marching
        startnan = np.count_nonzero(np.isnan(xy))
        if startnan == 0:
            return xy
        first_attempt = (startnan == xy.size)

        for direction in ['forward', 'backward']:
            if direction == 'forward':
                iincr = 1
            else:
                iincr = -1
            for ii in range(xy.shape[1]):
                # move on if point has already been found
                if not np.isnan(xy[:, ii]).any():
                    continue
                else:
                    if first_attempt:
                        # first try with native guess
                        res = eval_nimrod.eval_field_with_guess(
                                field, rzp[:, ii], eval_nimrod.c_xyguess)
                        # check for success
                        if not np.isnan(res).any():
                            xy[:, ii] = np.array(eval_nimrod.c_xyguess[0:2])
                            continue
                    # now try previous point
                    try:
                        thisxy = xy[:, ii - iincr]
                        lastrzp = rzp[:, ii - iincr]
                        # move on if previous point was nan
                        if np.isnan(thisxy).any():
                            continue
                        guess = (ctypes.c_double * 2)(thisxy[0], thisxy[1])
                    # move on if previous point was not found
                    except Exception:
                        continue
                    # now try with using the previous point as a guess
                    res = eval_nimrod.eval_field_with_guess(
                            field, rzp[:, ii], guess)
                    if not np.isnan(res).any():
                        xy[:, ii] = np.array(guess[0:2])
                        continue
                    # try marching forward from previous point
                    guess = \
                        (ctypes.c_double*2)(xy[0, ii-iincr], xy[1, ii-iincr])
                    for thisrzp in np.linspace(lastrzp, rzp[:, ii],
                                               num=substeps, endpoint=True):
                        res = eval_nimrod.eval_field_with_guess(
                                field, thisrzp, guess)
                        # break at first instance of failure
                        if np.isnan(res).any():
                            break
                    # test for march success
                    if not np.isnan(res).any():
                        xy[:, ii] = np.array(guess[0:2])

        endnan = np.count_nonzero(np.isnan(xy))
        if self.print_debug["line"]:
            print("In set_logical_on_1D_grid")
            print(f"level = {level}")
            print(f"startnan = {startnan}")
            print(f"endnan = {endnan}")
        if np.isnan(xy).any() and (level < self.__max_levels) \
                and startnan != endnan:
            # call function recursively if sensible
            xy = self.__set_logical_on_1D_grid(level + 1, field, xy, rzp,
                                               eval_nimrod)
        return xy

    def __set_logical_on_2D_grid(self, level, field, xy, rzp, eval_nimrod):
        '''
        Set the logical grid value on a 2d grid. Call set_logical_on_1D_grid
        alternating between the two axes. Note that the weaving between axes is
        more efficient.
        '''
        startnan = np.count_nonzero(np.isnan(xy))
        # set initial recursion level for set_logical_on_1D_grid call
        linelevel = self.__max_levels - 1
        # call eval_field_on_line for each line;
        # alternate horizontal and vertical
        for index in range(xy.shape[2]):
            xy[:, :, index] = \
                self.__set_logical_on_1D_grid(linelevel, field,
                                              xy[:, :, index],
                                              rzp[:, :, index], eval_nimrod)
        for index in range(xy.shape[1]):
            xy[:, index, :] = \
                self.__set_logical_on_1D_grid(linelevel, field,
                                              xy[:, index, :],
                                              rzp[:, index, :], eval_nimrod)

        endnan = np.count_nonzero(np.isnan(xy))
        if self.print_debug["plane"]:
            print("In set_logical_on_2D_grid")
            print(f"level = {level}")
            print(f"startnan = {startnan}")
            print(f"endnan = {endnan}")
        if np.isnan(xy).any() and (level < self.__max_levels) \
                and startnan != endnan:
            # call function recrusively
            xy = self.__set_logical_on_2D_grid(level + 1, field, xy, rzp,
                                               eval_nimrod)
        return xy

    def __set_logical_on_3D_grid(self, level, field, xy, rzp, eval_nimrod):
        '''
        Set the logical grid value on a 3d grid using calls to set logical on
        1D and 2D grids.  self.symm_3d indicates that the python grid respects
        NIMROD's symmetry.  If true solve for xy points on the plane and
        project, otherwise try a plane perpendicular to the first axis (use the
        middle index) and then for each point in plane. Try setting logical 1d
        for 1st axis repeat for a plane perpendicular to 2nd axis and then 3rd
        axis.  As a last resort, iterate through each plane.
        '''
        if self.symm_3d:
            xy[:, :, :, 0] = \
                self.__set_logical_on_2D_grid(level, field, xy[:, :, :, 0],
                                              rzp[:, :, :, 0], eval_nimrod)
            for index in range(1, xy.shape[3]):
                xy[:, :, :, index] = xy[:, :, :, 0]
            return xy

        planelevel = self.__max_levels
        linelevel = self.__max_levels - 2
        startnan = np.count_nonzero(np.isnan(xy))
        # call eval_field_on_plane alternate between the three planes
        # on call set_logical on a plane,
        # the call set_logical on lines perpendicular to plane
        # after iterating through all 3 axes twice,
        # systematically call set_logical on all planes
        ii = level % 3
        if level in [1, 4]:
            index = int(xy.shape[1] / 2)
            xy[:, index, :, :] = \
                self.__set_logical_on_2D_grid(planelevel, field,
                                              xy[:, index, :, :],
                                              rzp[:, index, :, :], eval_nimrod)
            for jdex, kdex in \
                    itertools.product(range(xy.shape[2]), range(xy.shape[3])):
                xy[:, :, jdex, kdex] = \
                    self.__set_logical_on_1D_grid(linelevel, field,
                                                  xy[:, :, jdex, kdex],
                                                  rzp[:, :, jdex, kdex],
                                                  eval_nimrod)
        elif level in [2, 5]:
            index = int(xy.shape[2] / 2)
            xy[:, :, index, :] = \
                self.__set_logical_on_2D_grid(planelevel, field,
                                              xy[:, :, index, :],
                                              rzp[:, :, index, :], eval_nimrod)
            for jdex, kdex in \
                    itertools.product(range(xy.shape[1]), range(xy.shape[3])):
                xy[:, jdex, :, kdex] = \
                    self.__set_logical_on_1D_grid(linelevel, field,
                                                  xy[:, jdex, :, kdex],
                                                  rzp[:, jdex, :, kdex],
                                                  eval_nimrod)
        elif level in [3, 6]:
            index = int(xy.shape[3] / 2)
            xy[:, :, :, index] = \
                self.__set_logical_on_2D_grid(planelevel, field,
                                              xy[:, :, :, index],
                                              rzp[:, :, :, index], eval_nimrod)
            for jdex, kdex in \
                    itertools.product(range(xy.shape[1]), range(xy.shape[2])):
                xy[:, jdex, kdex, :] = \
                    self.__set_logical_on_1D_grid(linelevel, field,
                                                  xy[:, jdex, kdex, :],
                                                  rzp[:, jdex, kdex, :],
                                                  eval_nimrod)
        elif ii == 0:
            for index in range(xy.shape[1]):
                xy[:, index, :, :] = \
                    self.__set_logical_on_2D_grid(planelevel, field,
                                                  xy[:, index, :, :],
                                                  rzp[:, index, :, :],
                                                  eval_nimrod)
        elif ii == 1:
            for index in range(xy.shape[2]):
                xy[:, :, index, :] = \
                    self.__set_logical_on_2D_grid(planelevel, field,
                                                  xy[:, :, index, :],
                                                  rzp[:, :, index, :],
                                                  eval_nimrod)
        elif ii == 2:
            for index in range(xy.shape[3]):
                xy[:, :, :, index] = \
                    self.__set_logical_on_2D_grid(planelevel, field,
                                                  xy[:, :, :, index],
                                                  rzp[:, :, :, index],
                                                  eval_nimrod)

        endnan = np.count_nonzero(np.isnan(xy))
        if self.print_debug["volume"]:
            print("In set_logical_on_3D_grid")
            print(f"level = {level}")
            print(f"startnan = {startnan}")
            print(f"endnan = {endnan}")
        if np.isnan(xy).any() and (level < self.__max_levels) \
                and startnan != endnan:
            # call function recursively
            xy = self.__set_logical_on_3D_grid(level + 1, field, xy, rzp,
                                               eval_nimrod)
        return xy
