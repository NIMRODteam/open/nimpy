#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np


class PlotNimrod:
    '''
    Provides tools for generating grids and plotting data.
    '''

    @staticmethod
    def set_pyplot_params():
        '''
        Set good defaults for pyplot.
        '''

        ftsize = 12
        lnwidth = 3
        params = {'backend': 'Qt5Agg',
                  'axes.labelsize': ftsize,
                  'axes.linewidth': 2,
                  # 'axes.labelweight': 'bold',
                  # 'axes.titleweight': 'bold',
                  'xtick.major.size': 4,
                  'xtick.minor.size': 2,
                  'xtick.major.width': 2,
                  'xtick.minor.width': 2,
                  'xtick.direction': 'in',
                  'ytick.major.size': 4,
                  'ytick.minor.size': 2,
                  'ytick.major.width': 2,
                  'ytick.minor.width': 2,
                  'ytick.direction': 'in',
                  'font.size': ftsize,
                  # 'font.weight': 'bold',
                  'legend.fontsize': ftsize,
                  'legend.frameon': False,
                  'xtick.labelsize': ftsize,
                  'ytick.labelsize': ftsize,
                  # 'text.usetex': True,
                  'text.latex.preamble':
                  r'\newcommand{\mathdefault}[1][]{}',
                  'lines.linewidth': lnwidth}
        plt.rcParams.update(params)

    @staticmethod
    def grid_1d_gen(p1, p2, nx1, dpow=1):
        '''
        Generate a 1-D grid in general coordinate (xyz or rzp) compatible with
        :class:`nimpy.eval_nimrod.EvalNimrod`.

        :param p1: start point in the form (^1, ^2, ^3) in general coordinate
        :param p2: end point in the form (^1, ^2, ^3) in general coordinate
        :param nx1: number of points in first direction
        :param dpow: set less than 1 to pack towards the endpoint and
                     greater than 1 to pack towards the startpoint
        :return: array of coordinates with shape (3, nx1)
        '''

        if dpow == 1:
            grid = np.linspace(p1, p2, nx1).transpose()
        else:
            direction = np.array(p2) - np.array(p1)
            grid = np.zeros([3, nx1])
            for ix in range(0, nx1):
                grid[:, ix] = \
                    p1 + direction * (float(ix) / float(nx1 - 1))**dpow

        return grid

    @staticmethod
    def grid_2d_gen(p1, p2, p3, nx1, nx2):
        '''
        Generate a 2-D grid in general coordinate (xyz or rzp) compatible with
        :class:`nimpy.eval_nimrod.EvalNimrod`.

        :param p1: start point in the form (^1, ^2, ^3) for both directions
                   (e.g. lower left)
        :param p2: end point in the form (^1, ^2, ^3) for first direction
                   (e.g. upper left)
        :param p3: end point in the form (^1, ^2, ^3) for second direction
                   (e.g. lower right)
        :param nx1: number of points in first direction
        :param nx2: number of points in second direction
        :return: array of coordinates with shape (3, nx1, nx2)
        '''

        line1 = np.linspace(p1, p2, nx1).transpose()
        line2 = np.linspace(p1, p3, nx2).transpose()
        grid = np.zeros((3, nx1, nx2))
        grid[:, :, 0] = line1
        grid[:, 0, :] = line2
        for i in range(1, nx1):
            for j in range(1, nx2):
                grid[0, i, j] = line1[0, i] + (line2[0, j] - line2[0, 0])
                grid[1, i, j] = line1[1, i] + (line2[1, j] - line2[1, 0])
                grid[2, i, j] = line1[2, i] + (line2[2, j] - line2[2, 0])

        return grid

    @staticmethod
    def grid_3d_gen(p1, p2, p3, p4, nx1, nx2, nx3):
        '''
        Generate a 3-D grid in general coordinate (xyz or rzp) compatible with
        :class:`nimpy.eval_nimrod.EvalNimrod`.

        :param p1: start point in the form (^1, ^2, ^3) for both directions
                   (e.g. lower back left)
        :param p2: end point in the form (^1, ^2, ^3) for first direction
                   (e.g. upper back left)
        :param p3: end point in the form (^1, ^2, ^3) for second direction
                   (e.g. lower back right)
        :param p4: end point in the form (^1, ^2, ^3) for third direction
                   (e.g. lower front left)
        :param nx1: number of points in first direction
        :param nx2: number of points in second direction
        :param nx3: number of points in third direction
        :return: array of coordinates with shape (3, nx1, nx2, nx3)
        '''
        line1 = np.linspace(p1, p2, nx1)
        line2 = np.linspace(p1, p3, nx2)
        line3 = np.linspace(p1, p4, nx3)
        grid = np.zeros((3, nx1, nx2, nx3))
        for i in range(nx1):
            for j in range(nx2):
                for k in range(nx3):
                    grid[0, i, j, k] = \
                        line1[i, 0] + (line2[j, 0] - line2[0, 0]) \
                        + (line3[k, 0] - line3[0, 0])
                    grid[1, i, j, k] = \
                        line1[i, 1] + (line2[j, 1] - line2[0, 1]) \
                        + (line3[k, 1] - line3[0, 1])
                    grid[2, i, j, k] = \
                        line1[i, 2] + (line2[j, 2] - line2[0, 2]) \
                        + (line3[k, 2] - line3[0, 2])

        return grid

    @staticmethod
    def plot_grid(points):
        '''
        Plot the grid points in 3D space.

        :param points: coordinate of the grid points compatible with
                       :class:`nimpy.eval_nimrod.EvalNimrod`
        :return: 3-D scatter plot of grid points
        '''

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.scatter(points[0], points[1], points[2])
        ax.set_xlabel("X1 Axis")
        ax.set_ylabel("X2 Axis")
        ax.set_zlabel("X3 Axis")
        ax.set_title("Grid Points")
        plt.show()

    @staticmethod
    def add_arrow(line, position=None, direction='right'):
        '''
        Add an arrow to a line.

        :param line: Line2D object
        :param position: x-position of the arrow. If None, mean of xdata is
                         taken
        :param direction: 'left' or 'right'
        '''

        # color = line.get_color()
        color = 'k'
        xdata = line.get_xdata()
        ydata = line.get_ydata()

        if position is None:
            position = xdata.mean()
        # find closest index
        start_ind = np.argmin(np.absolute(xdata - position))
        if direction == 'right':
            end_ind = start_ind + int(len(xdata)/10)
        else:
            end_ind = start_ind - int(len(xdata)/10)

        line.axes.annotate('',
                           xytext=(xdata[start_ind], ydata[start_ind]),
                           xy=(xdata[end_ind], ydata[start_ind]),
                           arrowprops=dict(arrowstyle="->", color=color),
                           size=30)

    @staticmethod
    def plot_dict_scalar_line(field_dict, xlabel="x",
                              ylabel="y", style='-o', xticks=None, yticks=None,
                              legend_loc='upper left', vert_legend_gap=False,
                              legend_ncol=1, arrow_dir=None, arrow_xloc=None,
                              ax=None, title=None, show=True, logy=False,
                              yscilabel=True):
        '''
        1D plot of a scalar field along a (potentially curved) line.

        :param field_dict: values of the fields as a dictionary of
                           {label : xvar(nx1), yvar(nx1)}
        :param xlabel: label for x axis
        :param ylabel: label for y axis
        :param style: line style (set to varied for cylic by field)
        :param legend_loc: legend location (None to skip legend)
        :param vert_legend_gap: split legend vertically in half
        :param legend_ncol: legend number of columns
        :param arrow_dir: add arrow to plot, should be "left" or "right"
        :param arrow_xloc: x value at which to add arrow
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param show: if False do not call show (optional)
        :param logy: if true use semilogy (optional)
        :param yscilabel: set false to skip setting y axis labels to
                          scientific notation (optional)
        '''

        if style == "varied":
            linestyles = ['-', '--', '-.', ':', '-', '--', '-.']
        else:
            linestyles = [style] * 7

        fig_size = [12, 6.75]
        if ax is None:
            fig, ax = plt.subplots(figsize=fig_size)

        ii = 0
        for flabel, xyvars in field_dict.items():
            if logy:
                line = ax.semilogy(xyvars[0], xyvars[1], linestyles[ii],
                                   alpha=0.7, label=flabel)
            else:
                line = ax.plot(xyvars[0], xyvars[1], linestyles[ii],
                               alpha=0.7, label=flabel)
            ii += 1
            ii = ii % len(linestyles)

        if arrow_dir is not None:
            PlotNimrod.add_arrow(line[0], position=arrow_xloc,
                                 direction=arrow_dir)

        ax.set(ylabel=ylabel, xlabel=xlabel)
        if xticks:
            if xticks == 'skip':
                ax.tick_params(labelbottom=False)
            else:
                ax.set_xticks(ticks=xticks)
        if yticks:
            if xticks == 'skip':
                ax.tick_params(labelleft=False)
            else:
                ax.set_yticks(ticks=yticks)

        if legend_loc:
            if vert_legend_gap:
                handles, labels = ax.get_legend_handles_labels()
                isplit = int(len(labels)/2)
                if "left" in legend_loc:
                    loc_up = "upper left"
                else:
                    loc_up = "upper right"
                l1 = ax.legend(handles[:isplit], labels[:isplit], loc=loc_up)
                ax.add_artist(l1)
            else:
                ax.legend(loc=legend_loc, ncol=legend_ncol)
        if title is not None:
            ax.set_title(title)

        if not logy and yscilabel:
            ax.ticklabel_format(axis='both', style='sci',
                                scilimits=(10**3, 10**-3), useOffset=None,
                                useLocale=None, useMathText=True)
        if show:
            plt.tight_layout()
            plt.show()

    @staticmethod
    def plot_scalar_line(line, field, flabel=None, f2=None, f2label=None,
                         f3=None, f3label=None, f4=None, f4label=None, f5=None,
                         f5label=None, f6=None, f6label=None, f7=None,
                         f7label=None, xvar=None, xlabel="x", ylabel="y",
                         style='-o', xticks=None, yticks=None,
                         legend_loc='upper left', vert_legend_gap=False,
                         arrow_dir=None, arrow_xloc=None, ax=None, title=None,
                         show=True):
        '''
        1D plot of a scalar field along a (potentially curved) line.

        :param line: coordinates of the grid points on the curve in (3, nx1)
                     format
        :param field: values of the field in (f_size, nx1)
        :param flabel: legend label for field
        :param f2: values of a second field in (f_size, nx1)
        :param f2label: legend label for f2
        :param f3: values of a third field in (f_size, nx1)
        :param f3label: legend label for f3
        :param f4: values of a fourth field in (f_size, nx1)
        :param f4label: legend label for f4
        :param f5: values of a fifth field in (f_size, nx1)
        :param f5label: legend label for f5
        :param f6: values of a fifth field in (f_size, nx1)
        :param f6label: legend label for f6
        :param f7: values of a fifth field in (f_size, nx1)
        :param f7label: legend label for f7
        :param xvar: dependent variable in (nx1)
        :param xlabel: label for x axis
        :param ylabel: label for y axis
        :param style: line style (set to varied for cylic by field)
        :param legend_loc: legend location
        :param vert_legend_gap: split legend vertically in half
        :param arrow_dir: add arrow to plot, should be "left" or "right"
        :param arrow_xloc: x value at which to add arrow
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param show: if False do not call show (optional)
        '''

        if xvar is None:
            xvar = np.sqrt((line[0, :] - line[0, 0])**2 +
                           (line[1, :] - line[1, 0])**2 +
                           (line[2, :] - line[2, 0])**2)
            if xlabel == "x":
                xlabel = "distance"

        if style == "varied":
            linestyles = ['-', '--', '-.', ':', '-', '--', '-.']
        else:
            linestyles = [style] * 7

        fig_size = [12, 6.75]
        if ax is None:
            fig, ax = plt.subplots(figsize=fig_size)

        line = ax.plot(xvar, field, linestyles[0], alpha=0.7, label=flabel)
        if f2 is not None:
            ax.plot(xvar, f2, linestyles[1], alpha=0.7, label=f2label)
        if f3 is not None:
            ax.plot(xvar, f3, linestyles[2], alpha=0.7, label=f3label)
        if f4 is not None:
            ax.plot(xvar, f4, linestyles[3], alpha=0.7, label=f4label)
        if f5 is not None:
            ax.plot(xvar, f5, linestyles[4], alpha=0.7, label=f5label)
        if f6 is not None:
            ax.plot(xvar, f6, linestyles[5], alpha=0.7, label=f6label)
        if f7 is not None:
            ax.plot(xvar, f7, linestyles[6], alpha=0.7, label=f7label)

        if arrow_dir is not None:
            PlotNimrod.add_arrow(line[0], position=arrow_xloc,
                                 direction=arrow_dir)

        ax.set(ylabel=ylabel, xlabel=xlabel)
        if xticks is not None:
            ax.set_xticks(ticks=xticks)
        if yticks is not None:
            ax.set_yticks(ticks=yticks)
        if (flabel is not None or f2label is not None or f3label is not None or
                f4label is not None or f5label is not None or
                f6label is not None or f7label is not None):
            if vert_legend_gap:
                handles, labels = ax.get_legend_handles_labels()
                isplit = int(len(labels)/2)
                if "left" in legend_loc:
                    loc_up = "upper left"
                else:
                    loc_up = "upper right"
                l1 = ax.legend(handles[:isplit], labels[:isplit], loc=loc_up)
                ax.add_artist(l1)
            else:
                ax.legend(loc=legend_loc)
        if title is not None:
            ax.set_title(title)

        ax.ticklabel_format(axis='both', style='sci',
                            scilimits=(10**3, 10**-3),
                            useOffset=None, useLocale=None, useMathText=True)
        if show:
            plt.tight_layout()
            plt.show()

    @staticmethod
    def plot_vector_line(line, field, comp=0, flabel=None, f2=None,
                         f2label=None, f3=None, f3label=None, f4=None,
                         f4label=None, f5=None, f5label=None, f6=None,
                         f6label=None, f7=None, f7label=None, xvar=None,
                         xlabel="x", ylabel="y", style='-o',
                         legend_loc='upper left', vert_legend_gap=False,
                         ax=None, title=None, show=True):
        '''
        1D plot of one component of a vector field along a (potentially curved)
        line

        :param line: coordinates of the grid points on the curve in (3, nx1)
                     format
        :param field: values of the field in (f_size, nx1)
        :param comp: specifies the component being plotted, possible values:
                     0, 1, 2 or "tang", "perp", and "phi"
        :param flabel: legend label for field
        :param f2: values of a second field in (f_size, nx1)
        :param f2label: legend label for f2
        :param f3: values of a third field in (f_size, nx1)
        :param f3label: legend label for f3
        :param f4: values of a fourth field in (f_size, nx1)
        :param f4label: legend label for f4
        :param f5: values of a fifth field in (f_size, nx1)
        :param f5label: legend label for f5
        :param f6: values of a fifth field in (f_size, nx1)
        :param f6label: legend label for f6
        :param f7: values of a fifth field in (f_size, nx1)
        :param f7label: legend label for f7
        :param xvar: dependent variable in (nx1)
        :param xlabel: label for x axis
        :param ylabel: label for y axis
        :param style: line style (set to varied for cylic by field)
        :param legend_loc: legend location
        :param vert_legend_gap: split legend vertically in half
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param show: if False do not call show (optionsal)
        '''

        if comp == "tang" or comp == "perp" or xvar is None:
            distance = np.sqrt((line[0, :] - line[0, 0])**2 +
                               (line[1, :] - line[1, 0])**2 +
                               (line[2, :] - line[2, 0])**2)
            # works for straight line
            norm = np.array([line[0, -1] - line[0, 0],
                             line[1, -1] - line[1, 0],
                             line[2, -1] - line[2, 0]]) / distance[-1]
            if xvar is None:
                xvar = distance
                if xlabel == "x":
                    xlabel = "distance"

        if comp == "phi":
            comp = 2

        pf2 = None
        pf3 = None
        pf4 = None
        pf5 = None
        pf6 = None
        pf7 = None
        if comp == "tang":
            pfield = np.dot(norm, field[0:3])
            if f2 is not None:
                pf2 = np.dot(norm, f2[0:3])
            if f3 is not None:
                pf3 = np.dot(norm, f3[0:3])
            if f4 is not None:
                pf4 = np.dot(norm, f4[0:3])
            if f5 is not None:
                pf5 = np.dot(norm, f5[0:3])
            if f6 is not None:
                pf6 = np.dot(norm, f6[0:3])
            if f7 is not None:
                pf7 = np.dot(norm, f7[0:3])
        elif comp == "perp":
            perp = np.cross(norm, [0, 0, 1])
            pfield = np.dot(perp, field[0:3])
            if f2 is not None:
                pf2 = np.dot(perp, f2[0:3])
            if f3 is not None:
                pf3 = np.dot(perp, f3[0:3])
            if f4 is not None:
                pf4 = np.dot(perp, f4[0:3])
            if f5 is not None:
                pf5 = np.dot(perp, f5[0:3])
            if f6 is not None:
                pf6 = np.dot(perp, f6[0:3])
            if f7 is not None:
                pf7 = np.dot(perp, f7[0:3])
        else:
            pfield = field[comp]
            if f2 is not None:
                pf2 = f2[comp]
            if f3 is not None:
                pf3 = f3[comp]
            if f4 is not None:
                pf4 = f4[comp]
            if f5 is not None:
                pf5 = f5[comp]
            if f6 is not None:
                pf6 = f6[comp]
            if f7 is not None:
                pf7 = f7[comp]

        PlotNimrod.plot_scalar_line(xvar, pfield, flabel=flabel, f2=pf2,
                                    f2label=f2label, f3=pf3, f3label=f3label,
                                    f4=pf4, f4label=f4label, f5=pf5,
                                    f5label=f5label, f6=pf6, f6label=f6label,
                                    f7=pf7, f7label=f7label, xvar=xvar,
                                    xlabel=xlabel, ylabel=ylabel, style=style,
                                    legend_loc=legend_loc,
                                    vert_legend_gap=vert_legend_gap, ax=ax,
                                    title=title, show=show)

    @staticmethod
    def __colorbar(ax, fig, labels=None, units=None):
        '''
        Make contour plot color bars nice.

        :param ax: figure axes subplot
        :param fig: contour plot
        :param labels: colorbar labels (optional)
        :param units: variable units (optional)
        '''

        from mpl_toolkits.axes_grid1 import make_axes_locatable
        last_axes = plt.gca()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(fig, cax=cax)
        plt.sca(last_axes)
        if labels is not None:
            cbar.set_ticks(labels)
            cbar.set_ticklabels(['{:.0e}'.format(it) for it in labels])
        if units is not None:
            cbar.ax.set_title(units)
        return cbar

    @staticmethod
    def plot_scalar_plane(plane, field, ax=None, title=None, fmin=None,
                          fmax=None, units=None, nlvls=50, log=False,
                          labels=None, cbar=True, show=True):
        '''
        2D plot of a scalar field on an arbitrary plane.

        :param plane: coordinates of the grid points on the plane in
                      (3, nx1, nx2) format
        :param field: values of the field in (f_size, nx1, nx2)
                      val_nimrod gives the field value as the first
                      component of f_size
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param fmin: minimum contour value (optional)
        :param fmax: maximum contour value (optional)
        :param units: variable units (optional)
        :param nlvls: number of contour levels (optional)
        :param log: contours in log space, requires fmin/fmax (optional)
        :param labels: colorbar labels (optional)
        :param cbar: show colorbar (optional, default True)
        :param show: if False do not call show (optionsal)
        :return:
        '''

        grid = plane

        if ax is None:
            fig, ax = plt.subplots()
            ax.set_aspect('equal')
        norm = None
        if (fmin is not None and fmax is not None):
            if log and fmin > 0.:
                levels = np.logspace(np.log10(fmin),
                                     np.log10(fmax), nlvls)
                norm = colors.LogNorm()
            else:
                levels = np.linspace(fmin, fmax, nlvls)
        else:
            levels = nlvls
        cf = ax.contourf(grid[0], grid[1], field, levels, norm=norm)
        cb = None
        if (cbar):
            cb = PlotNimrod.__colorbar(ax, cf, labels, units)
        if labels is not None:
            cf2 = ax.contour(cf, levels=labels, colors='r')
            if cb is not None:
                cb.add_lines(cf2)
        if title is not None:
            ax.set_title(title)
        if show:
            plt.tight_layout()
            plt.show()

        return

    @staticmethod
    def plot_vector_plane_contour(plane, field, comp=0, ax=None, title=None,
                                  fmin=None, fmax=None, units=None, nlvls=50,
                                  log=False, labels=None, cbar=True,
                                  show=True):
        '''
        2D plot of one component of a vector field on an arbitrary plane.

        :param plane: coordinates of the grid points on the plane in
                      (3, nx1, nx2) format
        :param field: values of the field in (f_size, nx1, nx2)
                      :class:`nimpy.eval_nimrod.EvalNimrod` gives the field
                      value as the first 3 components of f_size
        :param comp: specifies the component being plotted
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param fmin: minimum contour value (optional)
        :param fmax: maximum contour value (optional)
        :param units: variable units (optional)
        :param nlvls: number of contour levels (optional)
        :param log: contours in log space, requires fmin/fmax (optional)
        :param labels: colorbar labels (optional)
        :param cbar: show colorbar (optional, default True)
        :param show: if False do not call show (optionsal)
        '''

        PlotNimrod.plot_scalar_plane(plane, field[comp], ax=ax, title=title,
                                     fmin=fmin, fmax=fmax, units=units,
                                     nlvls=nlvls, log=log, labels=labels,
                                     cbar=cbar, show=show)

    @staticmethod
    def plot_vector_plane_quiver(plane, field, comp1=0, comp2=1, ax=None,
                                 title=None, npts=None, fmin=None, fmax=None,
                                 units=None, labels=None, cbar=True,
                                 show=True):
        '''
        2D quiver plot of two components of a vector field on an arbitrary
        plane.

        :param plane: coordinates of the grid points on the plane in
                      (3, nx1, nx2) format
        :param field: values of the field in (f_size, nx1, nx2)
                      :class:`nimpy.eval_nimrod.EvalNimrod` gives the field
                      value as the first 3 components of f_size
        :param comp1: specifies the first component of the quiver
        :param comp2: specifies the second component of the quiver
        :param ax: if ax is specified do not create new figure (optional)
        :param title: plot title (optional)
        :param npts: number of points in x/y directions. Can be a list.
                     (optional)
        :param fmin: minimum contour value (optional)
        :param fmax: maximum contour value (optional)
        :param units: variable units (optional)
        :param labels: colorbar labels (optional)
        :param cbar: show colorbar (optional, default True)
        :param show: if False do not call show (optionsal)
        '''

        # transform to perpendicular coordinate
        # perp = np.cross(plane[:, 0, -1] - plane[:, 0, 0],
        #                    plane[:, -1, 0] - plane[:, 0, 0])
        # perp = perp / np.linalg.norm(perp)
        # grid = np.zeros(np.shape(plane))
        # for i in range(plane.shape[1]):
        #     for j in range(plane.shape[2]):
        #         grid[:, i, j] = np.cross(plane[:, i, j], perp)
        grid = plane

        if npts is not None:
            if type(npts) is list:
                nxpts = npts[0]
                nypts = npts[1]
            elif type(npts) is int:
                nxpts = npts
                nypts = npts
            else:
                raise Exception('plot_vector_plane_quiver:: unrecognized npts')
            slx = int(grid.shape[1] / nxpts)
            sly = int(grid.shape[2] / nypts)
        else:
            slx = 1
            sly = 1

        # plot 2D quiver plot
        mag = np.hypot(field[comp1, ::slx, ::sly],
                       field[comp2, ::slx, ::sly])
        if ax is None:
            fig, ax = plt.subplots()
            ax.set_aspect('equal')
        qp = ax.quiver(grid[0, ::slx, ::sly], grid[1, ::slx, ::sly],
                       field[comp1, ::slx, ::sly], field[comp2, ::slx, ::sly],
                       mag, units='width', pivot='tail', width=0.01)
        if cbar:
            cb = PlotNimrod.__colorbar(ax, qp, labels, units)
        if (fmin is not None and fmax is not None):
            qp.set_clim(fmin, fmax)
        if title is not None:
            ax.set_title(title)
        if units is not None:
            cb.ax.set_title(units)
        if show:
            plt.tight_layout()
            plt.show()
