'''
Defines unit tests for yamp_input class :class:`_YamlInput`,

To run the test using command pytest -n [nprocs] (requires pytest-xdist)

Add options -v -> -vvv for verbose output
    option -x stops on first error
    option -rP displays terminal output for all tests
'''

import pytest
import yaml
from nimpy.util.yaml_input import _YamlInput, yaml_input

# base test dict
dict1 = {
    'a': 1,
    'b': True,
    'c': 'house'
}

# base default test dict (make sure there are more keys then dict1)
dict2 = {
    'a': 2,
    'b': False,
    'c': 'cat',
    'd': 23.5
}

# base dict for testing None
dict3 = {
    'none': "None",
}


class Test_YAML():
    '''
    Test yaml_input :class: '_YamlInput' and public yaml_input
    '''
    def test_init(self):
        input1 = _YamlInput()
        assert input1.input is None
        assert input1.get_filename() is None

    def test_read_input(self, tmpdir):
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(dict1, file, default_flow_style=False)
        input1 = _YamlInput()
        input1.read_input(filename)
        assert input1.get_filename() == filename
        assert input1.input == dict1

    def test_reread(self, tmpdir):
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(dict1, file, default_flow_style=False)
        input1 = _YamlInput()
        input1.read_input(filename)
        with pytest.raises(AssertionError):
            input1.read_input(filename)

    def test_read_namelist(self, tmpdir):
        tmpdict = {'fsa': dict1}
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(tmpdict, file, default_flow_style=False)
        input1 = _YamlInput()
        input1.read_input(filename)
        nml1 = input1.read_namelist('fsa', dict2, True)
        temp = dict2.copy()
        temp.update(dict1)
        assert nml1 == temp

    def test_check_keys(self, tmpdir):
        tmpdict = {'fsa': dict1}
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(tmpdict, file, default_flow_style=False)
        input1 = _YamlInput()
        input1.read_input(filename)
        with pytest.raises(KeyError):
            input1.read_namelist('fsa', {}, True)

    def test_fix_input(self, tmpdir):
        tmpdict = {'fsa': dict3}
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(tmpdict, file, default_flow_style=False)
        input1 = _YamlInput()
        input1.read_input(filename)
        nml1 = input1.read_namelist('fsa', dict3, True)
        assert nml1['none'] is None

    def test_public_input(self, tmpdir):
        tmpdict = {'fsa': dict1}
        assert yaml_input.input is None
        assert yaml_input.get_filename() is None
        filename = tmpdir+"/nimpy.yaml"
        with open(filename, 'w') as file:
            yaml.dump(tmpdict, file, default_flow_style=False)
        yaml_input.read_input(filename)
        assert yaml_input.input == tmpdict
        assert yaml_input.get_filename() == filename
        nml1 = yaml_input.read_namelist('fsa', dict1, True)
        assert nml1 == dict1
