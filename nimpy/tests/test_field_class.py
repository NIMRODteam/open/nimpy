'''
Defines unit tests for field_class classes :class:`Scalar`,
`Vector` and `Tensor`

To run the test using command pytest -n [nprocs] (requires pytest-xdist)

Add options -v -> -vvv for verbose output
    option -x stops on first error
    option -rP displays terminal output for all tests
'''

import pytest
import numpy as np

from nimpy.field_class import Scalar, Vector, Tensor
from nimpy.plot_nimrod import PlotNimrod as pn
import sympy


# Use Sympy to generate expressions and compute results for testing
r, z, p = sympy.symbols('r z p')
expr1 = r * z * p
expr2 = r**2 * z - sympy.sin(r * p)
expr3 = 2 * r * z + r * sympy.exp(p)
expr4 = sympy.cos(r * z * p)
expr5 = sympy.cosh(2 * r * z) - sympy.sinh(z)
expr6 = sympy.sqrt(r**2 + z**2 + p**2)

# Generate several lists of expressions to test
# Some methods require special consideration: e.g. to avoid division by zero
expr_list = [expr1, expr2, expr5, expr6]
int_list = [1, 2]
float_list = [4.0, 6.0, 8.0]
number_list = int_list + float_list
full_list = expr_list + number_list
non_zero_list = number_list + [expr3, expr6]
full_vec_list = []
full_vec_non_zero_list = []
llen = len(full_list)
for ii in range(0, llen):
    full_vec_list.append([full_list[ii], full_list[(ii+1) % llen],
                          full_list[(ii+2) % llen]])

llen2 = len(non_zero_list)
for ii in range(llen2):
    full_vec_non_zero_list.append([non_zero_list[ii],
                                   non_zero_list[(ii+3) % llen2],
                                   non_zero_list[(ii+5) % llen2]])
full_ten_list = []
for ii in range(llen):
    full_ten_list.append([full_list[ii], full_list[(ii+1) % llen],
                          full_list[(ii+2) % llen],
                          full_list[(ii+3) % llen],
                          full_list[(ii+4) % llen],
                          full_list[(ii+5) % llen],
                          full_list[(ii+6) % llen],
                          full_list[(ii+7) % llen],
                          full_list[(ii+8) % llen]])


def function_constant(value, grid):
    '''
        Generate fval consistent with eval_nimrod output for constant value
    '''
    size = (10, ) + np.shape(grid[0])
    fval = np.zeros(size)
    fval[0] = value
    return fval


def vector_constant(value, grid):
    '''
      Generate fval consistent with eval_nimrod output for
      vector of constant components
    '''
    size = (30, ) + np.shape(grid[0])
    fval = np.zeros(size)
    fval[0] = value[0]
    fval[1] = value[1]
    fval[2] = value[2]
    return fval


def function_expression(expr, grid, torgeom):
    '''
        Generate fval consistent with eval_nimrod output for sympy expression
    '''
    size = (10, ) + np.shape(grid[0])
    fval = np.zeros(size)
    dr = sympy.diff(expr, 'r')
    drr = sympy.diff(dr, 'r')
    drz = sympy.diff(dr, 'z')
    drp = sympy.diff(dr, 'p')
    dz = sympy.diff(expr, 'z')
    dzz = sympy.diff(dz, 'z')
    dzp = sympy.diff(dz, 'p')
    dp = sympy.diff(expr, 'p')
    dpp = sympy.diff(dp, 'p')
    if torgeom:
        dp = dp / r
        drp = drp / r
        dzp = dzp / r
        dpp = dpp / r**2
    fval[0] = sympy.utilities.lambdify((r, z, p), expr)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), dr)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), dz)(*grid[0:3])
    fval[3] = sympy.utilities.lambdify((r, z, p), dp)(*grid[0:3])
    fval[4] = sympy.utilities.lambdify((r, z, p), drr)(*grid[0:3])
    fval[5] = sympy.utilities.lambdify((r, z, p), drz)(*grid[0:3])
    fval[6] = sympy.utilities.lambdify((r, z, p), drp)(*grid[0:3])
    fval[7] = sympy.utilities.lambdify((r, z, p), dzz)(*grid[0:3])
    fval[8] = sympy.utilities.lambdify((r, z, p), dzp)(*grid[0:3])
    fval[9] = sympy.utilities.lambdify((r, z, p), dpp)(*grid[0:3])
    return fval


def tensor_transpose_expression(expr, grid, torgeom):
    '''
       Generate fval consistent with eval_nimrod output for
       a transposed tensor using sympy
    '''
    size = (3, 12,) + np.shape(grid[0])
    fval = np.zeros(size)
    rrcomp = expr[0]
    rzcomp = expr[1]
    rpcomp = expr[2]
    zrcomp = expr[3]
    zzcomp = expr[4]
    zpcomp = expr[5]
    prcomp = expr[6]
    pzcomp = expr[7]
    ppcomp = expr[8]
    dr1 = sympy.diff(rrcomp, 'r')
    dr2 = sympy.diff(zrcomp, 'r')
    dr3 = sympy.diff(prcomp, 'r')
    dr4 = sympy.diff(rzcomp, 'r')
    dr5 = sympy.diff(zzcomp, 'r')
    dr6 = sympy.diff(pzcomp, 'r')
    dr7 = sympy.diff(rpcomp, 'r')
    dr8 = sympy.diff(zpcomp, 'r')
    dr9 = sympy.diff(ppcomp, 'r')
    dz1 = sympy.diff(rrcomp, 'z')
    dz2 = sympy.diff(zrcomp, 'z')
    dz3 = sympy.diff(prcomp, 'z')
    dz4 = sympy.diff(rzcomp, 'z')
    dz5 = sympy.diff(zzcomp, 'z')
    dz6 = sympy.diff(pzcomp, 'z')
    dz7 = sympy.diff(rpcomp, 'z')
    dz8 = sympy.diff(zpcomp, 'z')
    dz9 = sympy.diff(ppcomp, 'z')
    dp1 = sympy.diff(rrcomp, 'p')
    dp2 = sympy.diff(zrcomp, 'p')
    dp3 = sympy.diff(prcomp, 'p')
    dp4 = sympy.diff(rzcomp, 'p')
    dp5 = sympy.diff(zzcomp, 'p')
    dp6 = sympy.diff(pzcomp, 'p')
    dp7 = sympy.diff(rpcomp, 'p')
    dp8 = sympy.diff(zpcomp, 'p')
    dp9 = sympy.diff(ppcomp, 'p')
    if torgeom:
        dp1 = dp1 / r
        dp2 = dp2 / r
        dp3 = dp3 / r
        dp4 = dp4 / r
        dp5 = dp5 / r
        dp6 = dp6 / r
        dp7 = dp7 / r
        dp8 = dp8 / r
        dp9 = dp9 / r
    fval[0, 0] = sympy.utilities.lambdify((r, z, p), rrcomp)(*grid[0:3])
    fval[0, 1] = sympy.utilities.lambdify((r, z, p), zrcomp)(*grid[0:3])
    fval[0, 2] = sympy.utilities.lambdify((r, z, p), prcomp)(*grid[0:3])
    fval[0, 3] = sympy.utilities.lambdify((r, z, p), dr1)(*grid[0:3])
    fval[0, 4] = sympy.utilities.lambdify((r, z, p), dr2)(*grid[0:3])
    fval[0, 5] = sympy.utilities.lambdify((r, z, p), dr3)(*grid[0:3])
    fval[0, 6] = sympy.utilities.lambdify((r, z, p), dr4)(*grid[0:3])
    fval[0, 7] = sympy.utilities.lambdify((r, z, p), dr5)(*grid[0:3])
    fval[0, 8] = sympy.utilities.lambdify((r, z, p), dr6)(*grid[0:3])
    fval[0, 9] = sympy.utilities.lambdify((r, z, p), dr7)(*grid[0:3])
    fval[0, 10] = sympy.utilities.lambdify((r, z, p), dr8)(*grid[0:3])
    fval[0, 11] = sympy.utilities.lambdify((r, z, p), dr9)(*grid[0:3])
    fval[1, 0] = sympy.utilities.lambdify((r, z, p), rzcomp)(*grid[0:3])
    fval[1, 1] = sympy.utilities.lambdify((r, z, p), zzcomp)(*grid[0:3])
    fval[1, 2] = sympy.utilities.lambdify((r, z, p), pzcomp)(*grid[0:3])
    fval[1, 3] = sympy.utilities.lambdify((r, z, p), dz1)(*grid[0:3])
    fval[1, 4] = sympy.utilities.lambdify((r, z, p), dz2)(*grid[0:3])
    fval[1, 5] = sympy.utilities.lambdify((r, z, p), dz3)(*grid[0:3])
    fval[1, 6] = sympy.utilities.lambdify((r, z, p), dz4)(*grid[0:3])
    fval[1, 7] = sympy.utilities.lambdify((r, z, p), dz5)(*grid[0:3])
    fval[1, 8] = sympy.utilities.lambdify((r, z, p), dz6)(*grid[0:3])
    fval[1, 9] = sympy.utilities.lambdify((r, z, p), dz7)(*grid[0:3])
    fval[1, 10] = sympy.utilities.lambdify((r, z, p), dz8)(*grid[0:3])
    fval[1, 11] = sympy.utilities.lambdify((r, z, p), dz9)(*grid[0:3])
    fval[2, 0] = sympy.utilities.lambdify((r, z, p), rpcomp)(*grid[0:3])
    fval[2, 1] = sympy.utilities.lambdify((r, z, p), zpcomp)(*grid[0:3])
    fval[2, 2] = sympy.utilities.lambdify((r, z, p), ppcomp)(*grid[0:3])
    fval[2, 3] = sympy.utilities.lambdify((r, z, p), dp1)(*grid[0:3])
    fval[2, 4] = sympy.utilities.lambdify((r, z, p), dp2)(*grid[0:3])
    fval[2, 5] = sympy.utilities.lambdify((r, z, p), dp3)(*grid[0:3])
    fval[2, 6] = sympy.utilities.lambdify((r, z, p), dp4)(*grid[0:3])
    fval[2, 7] = sympy.utilities.lambdify((r, z, p), dp5)(*grid[0:3])
    fval[2, 8] = sympy.utilities.lambdify((r, z, p), dp6)(*grid[0:3])
    fval[2, 9] = sympy.utilities.lambdify((r, z, p), dp7)(*grid[0:3])
    fval[2, 10] = sympy.utilities.lambdify((r, z, p), dp8)(*grid[0:3])
    fval[2, 11] = sympy.utilities.lambdify((r, z, p), dp9)(*grid[0:3])
    return fval


def tensor_transpose_function(expr, grid, dmode, torgeom):
    '''
      Create a tensor object based on expr
    '''
    if dmode == 1:
        size = (3, 12, ) + np.shape(grid[0])
    else:
        size = (3, 3, ) + np.shape(grid[0])
    fval = np.zeros(size)
    result = tensor_transpose_expression(expr, grid, torgeom)
    fval[:, 0:3] = result[:, 0:3]
    if dmode > 0:
        fval[:, 3:12] = result[:, 3:12]
    tensor = Tensor(fval, grid, dmode, torgeom)
    return tensor


def tensor_expression(expr, grid, torgeom):
    '''
      Generate fval consistent with eval_nimrod output for a tensor
      using simpy
    '''
    size = (3, 12,) + np.shape(grid[0])
    fval = np.zeros(size)
    rrcomp = expr[0]
    rzcomp = expr[1]
    rpcomp = expr[2]
    zrcomp = expr[3]
    zzcomp = expr[4]
    zpcomp = expr[5]
    prcomp = expr[6]
    pzcomp = expr[7]
    ppcomp = expr[8]
    dr1 = sympy.diff(rrcomp, 'r')
    dr2 = sympy.diff(rzcomp, 'r')
    dr3 = sympy.diff(rpcomp, 'r')
    dr4 = sympy.diff(zrcomp, 'r')
    dr5 = sympy.diff(zzcomp, 'r')
    dr6 = sympy.diff(zpcomp, 'r')
    dr7 = sympy.diff(prcomp, 'r')
    dr8 = sympy.diff(pzcomp, 'r')
    dr9 = sympy.diff(ppcomp, 'r')
    dz1 = sympy.diff(rrcomp, 'z')
    dz2 = sympy.diff(rzcomp, 'z')
    dz3 = sympy.diff(rpcomp, 'z')
    dz4 = sympy.diff(zrcomp, 'z')
    dz5 = sympy.diff(zzcomp, 'z')
    dz6 = sympy.diff(zpcomp, 'z')
    dz7 = sympy.diff(prcomp, 'z')
    dz8 = sympy.diff(pzcomp, 'z')
    dz9 = sympy.diff(ppcomp, 'z')
    dp1 = sympy.diff(rrcomp, 'p')
    dp2 = sympy.diff(rzcomp, 'p')
    dp3 = sympy.diff(rpcomp, 'p')
    dp4 = sympy.diff(zrcomp, 'p')
    dp5 = sympy.diff(zzcomp, 'p')
    dp6 = sympy.diff(zpcomp, 'p')
    dp7 = sympy.diff(prcomp, 'p')
    dp8 = sympy.diff(pzcomp, 'p')
    dp9 = sympy.diff(ppcomp, 'p')
    if torgeom:
        dp1 = dp1 / r
        dp2 = dp2 / r
        dp3 = dp3 / r
        dp4 = dp4 / r
        dp5 = dp5 / r
        dp6 = dp6 / r
        dp7 = dp7 / r
        dp8 = dp8 / r
        dp9 = dp9 / r
    fval[0, 0] = sympy.utilities.lambdify((r, z, p), rrcomp)(*grid[0:3])
    fval[0, 1] = sympy.utilities.lambdify((r, z, p), rzcomp)(*grid[0:3])
    fval[0, 2] = sympy.utilities.lambdify((r, z, p), rpcomp)(*grid[0:3])
    fval[0, 3] = sympy.utilities.lambdify((r, z, p), dr1)(*grid[0:3])
    fval[0, 4] = sympy.utilities.lambdify((r, z, p), dr2)(*grid[0:3])
    fval[0, 5] = sympy.utilities.lambdify((r, z, p), dr3)(*grid[0:3])
    fval[0, 6] = sympy.utilities.lambdify((r, z, p), dr4)(*grid[0:3])
    fval[0, 7] = sympy.utilities.lambdify((r, z, p), dr5)(*grid[0:3])
    fval[0, 8] = sympy.utilities.lambdify((r, z, p), dr6)(*grid[0:3])
    fval[0, 9] = sympy.utilities.lambdify((r, z, p), dr7)(*grid[0:3])
    fval[0, 10] = sympy.utilities.lambdify((r, z, p), dr8)(*grid[0:3])
    fval[0, 11] = sympy.utilities.lambdify((r, z, p), dr9)(*grid[0:3])
    fval[1, 0] = sympy.utilities.lambdify((r, z, p), zrcomp)(*grid[0:3])
    fval[1, 1] = sympy.utilities.lambdify((r, z, p), zzcomp)(*grid[0:3])
    fval[1, 2] = sympy.utilities.lambdify((r, z, p), zpcomp)(*grid[0:3])
    fval[1, 3] = sympy.utilities.lambdify((r, z, p), dz1)(*grid[0:3])
    fval[1, 4] = sympy.utilities.lambdify((r, z, p), dz2)(*grid[0:3])
    fval[1, 5] = sympy.utilities.lambdify((r, z, p), dz3)(*grid[0:3])
    fval[1, 6] = sympy.utilities.lambdify((r, z, p), dz4)(*grid[0:3])
    fval[1, 7] = sympy.utilities.lambdify((r, z, p), dz5)(*grid[0:3])
    fval[1, 8] = sympy.utilities.lambdify((r, z, p), dz6)(*grid[0:3])
    fval[1, 9] = sympy.utilities.lambdify((r, z, p), dz7)(*grid[0:3])
    fval[1, 10] = sympy.utilities.lambdify((r, z, p), dz8)(*grid[0:3])
    fval[1, 11] = sympy.utilities.lambdify((r, z, p), dz9)(*grid[0:3])
    fval[2, 0] = sympy.utilities.lambdify((r, z, p), prcomp)(*grid[0:3])
    fval[2, 1] = sympy.utilities.lambdify((r, z, p), pzcomp)(*grid[0:3])
    fval[2, 2] = sympy.utilities.lambdify((r, z, p), ppcomp)(*grid[0:3])
    fval[2, 3] = sympy.utilities.lambdify((r, z, p), dp1)(*grid[0:3])
    fval[2, 4] = sympy.utilities.lambdify((r, z, p), dp2)(*grid[0:3])
    fval[2, 5] = sympy.utilities.lambdify((r, z, p), dp3)(*grid[0:3])
    fval[2, 6] = sympy.utilities.lambdify((r, z, p), dp4)(*grid[0:3])
    fval[2, 7] = sympy.utilities.lambdify((r, z, p), dp5)(*grid[0:3])
    fval[2, 8] = sympy.utilities.lambdify((r, z, p), dp6)(*grid[0:3])
    fval[2, 9] = sympy.utilities.lambdify((r, z, p), dp7)(*grid[0:3])
    fval[2, 10] = sympy.utilities.lambdify((r, z, p), dp8)(*grid[0:3])
    fval[2, 11] = sympy.utilities.lambdify((r, z, p), dp9)(*grid[0:3])
    return fval


def vector_expression(expr, grid, torgeom):
    '''
       Generate fval consistent with eval_nimrod output for a vector
       using simpy
    '''
    size = (30, ) + np.shape(grid[0])
    fval = np.zeros(size)
    rcomp = expr[0]
    zcomp = expr[1]
    pcomp = expr[2]
    dr1 = sympy.diff(rcomp, 'r')
    dr2 = sympy.diff(zcomp, 'r')
    dr3 = sympy.diff(pcomp, 'r')
    dz1 = sympy.diff(rcomp, 'z')
    dz2 = sympy.diff(zcomp, 'z')
    dz3 = sympy.diff(pcomp, 'z')
    dp1 = sympy.diff(rcomp, 'p')
    dp2 = sympy.diff(zcomp, 'p')
    dp3 = sympy.diff(pcomp, 'p')
    drr1 = sympy.diff(dr1, 'r')
    drr2 = sympy.diff(dr2, 'r')
    drr3 = sympy.diff(dr3, 'r')
    drz1 = sympy.diff(dz1, 'r')
    drz2 = sympy.diff(dz2, 'r')
    drz3 = sympy.diff(dz3, 'r')
    drp1 = sympy.diff(dp1, 'r')
    drp2 = sympy.diff(dp2, 'r')
    drp3 = sympy.diff(dp3, 'r')
    dzz1 = sympy.diff(dz1, 'z')
    dzz2 = sympy.diff(dz2, 'z')
    dzz3 = sympy.diff(dz3, 'z')
    dzp1 = sympy.diff(dp1, 'z')
    dzp2 = sympy.diff(dp2, 'z')
    dzp3 = sympy.diff(dp3, 'z')
    dpp1 = sympy.diff(dp1, 'p')
    dpp2 = sympy.diff(dp2, 'p')
    dpp3 = sympy.diff(dp3, 'p')
    if torgeom:
        dp1 = dp1 / r
        dp2 = dp2 / r
        dp3 = dp3 / r
        drp1 = drp1 / r
        drp2 = drp2 / r
        drp3 = drp3 / r
        dzp1 = dzp1 / r
        dzp2 = dzp2 / r
        dzp3 = dzp3 / r
        dpp1 = dpp1 / r**2
        dpp2 = dpp2 / r**2
        dpp3 = dpp3 / r**2
    fval[0] = sympy.utilities.lambdify((r, z, p), rcomp)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), zcomp)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), pcomp)(*grid[0:3])
    fval[3] = sympy.utilities.lambdify((r, z, p), dr1)(*grid[0:3])
    fval[4] = sympy.utilities.lambdify((r, z, p), dr2)(*grid[0:3])
    fval[5] = sympy.utilities.lambdify((r, z, p), dr3)(*grid[0:3])
    fval[6] = sympy.utilities.lambdify((r, z, p), dz1)(*grid[0:3])
    fval[7] = sympy.utilities.lambdify((r, z, p), dz2)(*grid[0:3])
    fval[8] = sympy.utilities.lambdify((r, z, p), dz3)(*grid[0:3])
    fval[9] = sympy.utilities.lambdify((r, z, p), dp1)(*grid[0:3])
    fval[10] = sympy.utilities.lambdify((r, z, p), dp2)(*grid[0:3])
    fval[11] = sympy.utilities.lambdify((r, z, p), dp3)(*grid[0:3])
    fval[12] = sympy.utilities.lambdify((r, z, p), drr1)(*grid[0:3])
    fval[13] = sympy.utilities.lambdify((r, z, p), drr2)(*grid[0:3])
    fval[14] = sympy.utilities.lambdify((r, z, p), drr3)(*grid[0:3])
    fval[15] = sympy.utilities.lambdify((r, z, p), drz1)(*grid[0:3])
    fval[16] = sympy.utilities.lambdify((r, z, p), drz2)(*grid[0:3])
    fval[17] = sympy.utilities.lambdify((r, z, p), drz3)(*grid[0:3])
    fval[18] = sympy.utilities.lambdify((r, z, p), drp1)(*grid[0:3])
    fval[19] = sympy.utilities.lambdify((r, z, p), drp2)(*grid[0:3])
    fval[20] = sympy.utilities.lambdify((r, z, p), drp3)(*grid[0:3])
    fval[21] = sympy.utilities.lambdify((r, z, p), dzz1)(*grid[0:3])
    fval[22] = sympy.utilities.lambdify((r, z, p), dzz2)(*grid[0:3])
    fval[23] = sympy.utilities.lambdify((r, z, p), dzz3)(*grid[0:3])
    fval[24] = sympy.utilities.lambdify((r, z, p), dzp1)(*grid[0:3])
    fval[25] = sympy.utilities.lambdify((r, z, p), dzp2)(*grid[0:3])
    fval[26] = sympy.utilities.lambdify((r, z, p), dzp3)(*grid[0:3])
    fval[27] = sympy.utilities.lambdify((r, z, p), dpp1)(*grid[0:3])
    fval[28] = sympy.utilities.lambdify((r, z, p), dpp2)(*grid[0:3])
    fval[29] = sympy.utilities.lambdify((r, z, p), dpp3)(*grid[0:3])
    return fval


def divergence_tensor_expression(expr, grid, torgeom):
    '''
      Generate divergence of tensor
    '''
    size = (30, ) + np.shape(grid[0])
    fval = np.zeros(size)
    rrcomp = expr[0]
    rzcomp = expr[1]
    rpcomp = expr[2]
    zrcomp = expr[3]
    zzcomp = expr[4]
    zpcomp = expr[5]
    prcomp = expr[6]
    pzcomp = expr[7]
    ppcomp = expr[8]
    dr1 = sympy.diff(rrcomp, 'r')
    dr2 = sympy.diff(rzcomp, 'r')
    dr3 = sympy.diff(rpcomp, 'r')
    dz1 = sympy.diff(zrcomp, 'z')
    dz2 = sympy.diff(zzcomp, 'z')
    dz3 = sympy.diff(zpcomp, 'z')
    dp1 = sympy.diff(prcomp, 'p')
    dp2 = sympy.diff(pzcomp, 'p')
    dp3 = sympy.diff(ppcomp, 'p')
    if torgeom:
        dr1 = dr1 + (rrcomp / r) - (ppcomp / r)
        dr2 = dr2 + (rzcomp / r)
        dr3 = dr3 + (rpcomp / r) + (prcomp / r)
        dp1 = dp1 / r
        dp2 = dp2 / r
        dp3 = dp3 / r
    c0 = dr1 + dp1 + dz1
    c1 = dr2 + dp2 + dz2
    c2 = dr3 + dp3 + dz3
    fval[0] = sympy.utilities.lambdify((r, z, p), c0)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), c1)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), c2)(*grid[0:3])
    return fval


def divergence_vector_expression(expr, grid, torgeom):
    '''
       Generate fval consistent wth eval_nimrod output
       for divergence of a vector v = (expr1, expr2, expr3)
    '''
    size = (4, ) + np.shape(grid[0])
    fval = np.zeros(size)
    rcomp = expr[0]
    zcomp = expr[1]
    pcomp = expr[2]
    dr = sympy.diff(rcomp, 'r')
    dz = sympy.diff(zcomp, 'z')
    dp = sympy.diff(pcomp, 'p')
    drr = sympy.diff(dr, 'r')
    drz = sympy.diff(dz, 'r')
    drp = sympy.diff(dp, 'r')
    dzr = sympy.diff(dr, 'z')
    dzz = sympy.diff(dz, 'z')
    dzp = sympy.diff(dp, 'z')
    dpr = sympy.diff(dr, 'p')
    dpz = sympy.diff(dz, 'p')
    dpp = sympy.diff(dp, 'p')
    if torgeom:
        dzaux = sympy.diff(rcomp, 'z')
        dpaux = sympy.diff(rcomp, 'p')
        drp = drp / r
        dzp = dzp / r
        dpp = dpp / r**2
        drr = drr - (dp / r**2) + (dr / r) - (rcomp / r**2)
        dzz = dzz + (dzaux / r)
        dpp = dpp + (dpaux / r**2)
        dr = dr + rcomp / r
        dp = dp / r
        dpr = dpr / r
        dpz = dpz / r
    c0 = dr + dz + dp
    c1 = drr + drz + drp
    c2 = dzr + dzz + dzp
    c3 = dpr + dpz + dpp
    fval[0] = sympy.utilities.lambdify((r, z, p), c0)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), c1)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), c2)(*grid[0:3])
    fval[3] = sympy.utilities.lambdify((r, z, p), c3)(*grid[0:3])
    return fval


def gradient_constant(value, grid):
    '''
        Generate fval consistent with eval_nimrod output for grad of a unifom
        scalar value
    '''
    size = (12, ) + np.shape(grid[0])
    fval = np.zeros(size)
    return fval


def dyadic_product(expr1, expr2, grid, dmode, torgeom):
    '''
      Generate dyadic product between two vectors
    '''
    r1 = expr1[0]
    z1 = expr1[1]
    p1 = expr1[2]
    r2 = expr2[0]
    z2 = expr2[1]
    p2 = expr2[2]
    rr0 = r1 * r2
    rz0 = r1 * z2
    rp0 = r1 * p2
    zr0 = z1 * r2
    zz0 = z1 * z2
    zp0 = z1 * p2
    pr0 = p1 * r2
    pz0 = p1 * z2
    pp0 = p1 * p2
    result = [rr0, rz0, rp0, zr0, zz0, zp0, pr0, pz0, pp0]
    tensor = tensor_function(result, grid, dmode, torgeom)
    return tensor


def dot_product(expr1, expr2, grid, dmode, torgeom):
    '''
       Dot product between vectors
       v1 = (expr1[0], expr1[1], expr1[2])
       v2 = (expr2[0], expr2[1], expr2[2])
    '''
    result = expr1[0] * expr2[0] + expr1[1] * expr2[1] + expr1[2] * expr2[2]
    dotp = scalar_function(result, grid, dmode, torgeom)
    return dotp


def curl_expression(expr, grid, torgeom):
    '''
      Calculates curl(expr) when expr is vector
    '''
    size = (12, ) + np.shape(grid[0])
    fval = np.zeros(size)
    rcomp = expr[0]
    zcomp = expr[1]
    pcomp = expr[2]
    dpdz = sympy.diff(pcomp, 'z')
    dzdp = sympy.diff(zcomp, 'p')
    if torgeom:
        dzdp = dzdp / r
    cr = dpdz - dzdp
    drdp = sympy.diff(rcomp, 'p')
    if torgeom:
        drdp = drdp / r
    dpdr = sympy.diff(pcomp, 'r')
    cz = drdp - dpdr
    if torgeom:
        cz = cz - (pcomp / r)
    dzdr = sympy.diff(zcomp, 'r')
    drdz = sympy.diff(rcomp, 'z')
    cp = dzdr - drdz
    dcrdr = sympy.diff(cr, 'r')
    dczdr = sympy.diff(cz, 'r')
    dcpdr = sympy.diff(cp, 'r')
    dcrdz = sympy.diff(cr, 'z')
    dczdz = sympy.diff(cz, 'z')
    dcpdz = sympy.diff(cp, 'z')
    dcrdp = sympy.diff(cr, 'p')
    dczdp = sympy.diff(cz, 'p')
    dcpdp = sympy.diff(cp, 'p')
    if torgeom:
        dcrdp = dcrdp / r
        dczdp = dczdp / r
        dcpdp = dcpdp / r
    fval[0] = sympy.utilities.lambdify((r, z, p), cr)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), cz)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), cp)(*grid[0:3])
    fval[3] = sympy.utilities.lambdify((r, z, p), dcrdr)(*grid[0:3])
    fval[4] = sympy.utilities.lambdify((r, z, p), dczdr)(*grid[0:3])
    fval[5] = sympy.utilities.lambdify((r, z, p), dcpdr)(*grid[0:3])
    fval[6] = sympy.utilities.lambdify((r, z, p), dcrdz)(*grid[0:3])
    fval[7] = sympy.utilities.lambdify((r, z, p), dczdz)(*grid[0:3])
    fval[8] = sympy.utilities.lambdify((r, z, p), dcpdz)(*grid[0:3])
    fval[9] = sympy.utilities.lambdify((r, z, p), dcrdp)(*grid[0:3])
    fval[10] = sympy.utilities.lambdify((r, z, p), dczdp)(*grid[0:3])
    fval[11] = sympy.utilities.lambdify((r, z, p), dcpdp)(*grid[0:3])
    return fval


def dot_vector_tensor(expr1, expr2, grid, dmode, torgeom):
    '''
      Generate dot product bewteen vector (expr1) and
      tensor (expr2)
    '''
    rrcomp = expr2[0]
    rzcomp = expr2[1]
    rpcomp = expr2[2]
    zrcomp = expr2[3]
    zzcomp = expr2[4]
    zpcomp = expr2[5]
    prcomp = expr2[6]
    pzcomp = expr2[7]
    ppcomp = expr2[8]
    rcomp = expr1[0]
    zcomp = expr1[1]
    pcomp = expr1[2]
    r0 = rcomp * rrcomp + zcomp * zrcomp + pcomp * prcomp
    z0 = rcomp * rzcomp + zcomp * zzcomp + pcomp * pzcomp
    p0 = rcomp * rpcomp + zcomp * zpcomp + pcomp * ppcomp
    result = [r0, z0, p0]
    vector = vector_function(result, grid, dmode, torgeom)
    return vector


def dot_tensor_vector(expr1, expr2, grid, dmode, torgeom):
    '''
      Generate dot product between tensor (expr1) and
      vector (expr2)
    '''
    rrcomp = expr1[0]
    rzcomp = expr1[1]
    rpcomp = expr1[2]
    zrcomp = expr1[3]
    zzcomp = expr1[4]
    zpcomp = expr1[5]
    prcomp = expr1[6]
    pzcomp = expr1[7]
    ppcomp = expr1[8]
    rcomp = expr2[0]
    zcomp = expr2[1]
    pcomp = expr2[2]
    r0 = rrcomp * rcomp + rzcomp * zcomp + rpcomp * pcomp
    z0 = zrcomp * rcomp + zzcomp * zcomp + zpcomp * pcomp
    p0 = prcomp * rcomp + pzcomp * zcomp + ppcomp * pcomp
    result = [r0, z0, p0]
    vector = vector_function(result, grid, dmode, torgeom)
    return vector


def dot_tensor_tensor(expr1, expr2, grid, dmode, torgeom):
    '''
      Calculates dot product between two tensors
    '''
    rr1 = expr1[0]
    rz1 = expr1[1]
    rp1 = expr1[2]
    zr1 = expr1[3]
    zz1 = expr1[4]
    zp1 = expr1[5]
    pr1 = expr1[6]
    pz1 = expr1[7]
    pp1 = expr1[8]
    rr2 = expr2[0]
    rz2 = expr2[1]
    rp2 = expr2[2]
    zr2 = expr2[3]
    zz2 = expr2[4]
    zp2 = expr2[5]
    pr2 = expr2[6]
    pz2 = expr2[7]
    pp2 = expr2[8]
    rr0 = rr1 * rr2 + rz1 * zr2 + rp1 * pr2
    rz0 = rr1 * rz2 + rz1 * zz2 + rp1 * pz2
    rp0 = rr1 * rp2 + rz1 * zp2 + rp1 * pp2
    zr0 = zr1 * rr2 + zz1 * zr2 + zp1 * pr2
    zz0 = zr1 * rz2 + zz1 * zz2 + zp1 * pz2
    zp0 = zr1 * rp2 + zz1 * zp2 + zp1 * pp2
    pr0 = pr1 * rr2 + pz1 * zr2 + pp1 * pr2
    pz0 = pr1 * rz2 + pz1 * zz2 + pp1 * pz2
    pp0 = pr1 * rp2 + pz1 * zp2 + pp1 * pp2
    result = [rr0, rz0, rp0, zr0, zz0, zp0, pr0, pz0, pp0]
    tensor = tensor_function(result, grid, dmode, torgeom)
    return tensor


def cross_vector_vector(expr1, expr2, grid, dmode, torgeom):
    '''
       Generate fval consistent with eval_nimrod output for cross
       product between vectors, i.e., vector x vector
       v1 = (expr1[0], expr1[1], expr1[2]) (R, z, phi)
       v2 = (expr2[0], expr2[1], expr2[2]) (R, z, phi)
    '''
    rcomp = (expr1[1] * expr2[2] - expr1[2] * expr2[1])
    zcomp = (expr1[2] * expr2[0] - expr1[0] * expr2[2])
    pcomp = (expr1[0] * expr2[1] - expr1[1] * expr2[0])
    result = [rcomp, zcomp, pcomp]
    vector = vector_function(result, grid, dmode, torgeom)
    return vector


def cross_vector_tensor(expr1, expr2, grid, dmode, torgeom):
    '''
      Calculate vector (expr1) x tensor (expr2)
    '''
    rrcomp = expr2[0]
    rzcomp = expr2[1]
    rpcomp = expr2[2]
    zrcomp = expr2[3]
    zzcomp = expr2[4]
    zpcomp = expr2[5]
    prcomp = expr2[6]
    pzcomp = expr2[7]
    ppcomp = expr2[8]
    rcomp = expr1[0]
    zcomp = expr1[1]
    pcomp = expr1[2]
    rr0 = zcomp * prcomp - pcomp * zrcomp
    rz0 = zcomp * pzcomp - pcomp * zzcomp
    rp0 = zcomp * ppcomp - pcomp * zpcomp
    zr0 = pcomp * rrcomp - rcomp * prcomp
    zz0 = pcomp * rzcomp - rcomp * pzcomp
    zp0 = pcomp * rpcomp - rcomp * ppcomp
    pr0 = rcomp * zrcomp - zcomp * rrcomp
    pz0 = rcomp * zzcomp - zcomp * rzcomp
    pp0 = rcomp * zpcomp - zcomp * rpcomp
    result = [rr0, rz0, rp0, zr0, zz0, zp0, pr0, pz0, pp0]
    tensor = tensor_function(result, grid, dmode, torgeom)
    return tensor


def cross_tensor_vector(expr1, expr2, grid, dmode, torgeom):
    '''
      Calculate tensor (expr1) x vector (exprs)
    '''
    rrcomp = expr1[0]
    rzcomp = expr1[1]
    rpcomp = expr1[2]
    zrcomp = expr1[3]
    zzcomp = expr1[4]
    zpcomp = expr1[5]
    prcomp = expr1[6]
    pzcomp = expr1[7]
    ppcomp = expr1[8]
    rcomp = expr2[0]
    zcomp = expr2[1]
    pcomp = expr2[2]
    rr0 = rzcomp * pcomp - rpcomp * zcomp
    rz0 = rpcomp * rcomp - rrcomp * pcomp
    rp0 = rrcomp * zcomp - rzcomp * rcomp
    zr0 = zzcomp * pcomp - zpcomp * zcomp
    zz0 = zpcomp * rcomp - zrcomp * pcomp
    zp0 = zrcomp * zcomp - zzcomp * rcomp
    pr0 = pzcomp * pcomp - ppcomp * zcomp
    pz0 = ppcomp * rcomp - prcomp * pcomp
    pp0 = prcomp * zcomp - pzcomp * rcomp
    result = [rr0, rz0, rp0, zr0, zz0, zp0, pr0, pz0, pp0]
    tensor = tensor_function(result, grid, dmode, torgeom)
    return tensor


def gradient_vector_expression(expr, grid, torgeom):
    '''
      Generate fval for gradient of vector
    '''
    size = (3, 12, ) + np.shape(grid[0])
    fval = np.zeros(size)
    rcomp = expr[0]
    zcomp = expr[1]
    pcomp = expr[2]
    rr0 = sympy.diff(rcomp, 'r')
    rz0 = sympy.diff(zcomp, 'r')
    rp0 = sympy.diff(pcomp, 'r')
    zr0 = sympy.diff(rcomp, 'z')
    zz0 = sympy.diff(zcomp, 'z')
    zp0 = sympy.diff(pcomp, 'z')
    pr0 = sympy.diff(rcomp, 'p')
    if torgeom:
        pr0 = pr0 / r
    pz0 = sympy.diff(zcomp, 'p')
    if torgeom:
        pz0 = pz0 / r
    pp0 = sympy.diff(pcomp, 'p')
    if torgeom:
        pp0 = pp0 / r
    if torgeom:
        pr0 = pr0 - (pcomp / r)
        pp0 = pp0 + (rcomp / r)
    dr1 = sympy.diff(rr0, 'r')
    dr2 = sympy.diff(rz0, 'r')
    dr3 = sympy.diff(rp0, 'r')
    dr4 = sympy.diff(zr0, 'r')
    dr5 = sympy.diff(zz0, 'r')
    dr6 = sympy.diff(zp0, 'r')
    dr7 = sympy.diff(pr0, 'r')
    dr8 = sympy.diff(pz0, 'r')
    dr9 = sympy.diff(pp0, 'r')
    dz1 = sympy.diff(rr0, 'z')
    dz2 = sympy.diff(rz0, 'z')
    dz3 = sympy.diff(rp0, 'z')
    dz4 = sympy.diff(zr0, 'z')
    dz5 = sympy.diff(zz0, 'z')
    dz6 = sympy.diff(zp0, 'z')
    dz7 = sympy.diff(pr0, 'z')
    dz8 = sympy.diff(pz0, 'z')
    dz9 = sympy.diff(pp0, 'z')
    dp1 = sympy.diff(rr0, 'p')
    dp2 = sympy.diff(rz0, 'p')
    dp3 = sympy.diff(rp0, 'p')
    dp4 = sympy.diff(zr0, 'p')
    dp5 = sympy.diff(zz0, 'p')
    dp6 = sympy.diff(zp0, 'p')
    dp7 = sympy.diff(pr0, 'p')
    dp8 = sympy.diff(pz0, 'p')
    dp9 = sympy.diff(pp0, 'p')
    if torgeom:
        dp1 = dp1 / r
        dp2 = dp2 / r
        dp3 = dp3 / r
        dp4 = dp4 / r
        dp5 = dp5 / r
        dp6 = dp6 / r
        dp7 = dp7 / r
        dp8 = dp8 / r
        dp9 = dp9 / r
    fval[0, 0] = sympy.utilities.lambdify((r, z, p), rr0)(*grid[0:3])
    fval[0, 1] = sympy.utilities.lambdify((r, z, p), rz0)(*grid[0:3])
    fval[0, 2] = sympy.utilities.lambdify((r, z, p), rp0)(*grid[0:3])
    fval[0, 3] = sympy.utilities.lambdify((r, z, p), dr1)(*grid[0:3])
    fval[0, 4] = sympy.utilities.lambdify((r, z, p), dr2)(*grid[0:3])
    fval[0, 5] = sympy.utilities.lambdify((r, z, p), dr3)(*grid[0:3])
    fval[0, 6] = sympy.utilities.lambdify((r, z, p), dr4)(*grid[0:3])
    fval[0, 7] = sympy.utilities.lambdify((r, z, p), dr5)(*grid[0:3])
    fval[0, 8] = sympy.utilities.lambdify((r, z, p), dr6)(*grid[0:3])
    fval[0, 9] = sympy.utilities.lambdify((r, z, p), dr7)(*grid[0:3])
    fval[0, 10] = sympy.utilities.lambdify((r, z, p), dr8)(*grid[0:3])
    fval[0, 11] = sympy.utilities.lambdify((r, z, p), dr9)(*grid[0:3])
    fval[1, 0] = sympy.utilities.lambdify((r, z, p), zr0)(*grid[0:3])
    fval[1, 1] = sympy.utilities.lambdify((r, z, p), zz0)(*grid[0:3])
    fval[1, 2] = sympy.utilities.lambdify((r, z, p), zp0)(*grid[0:3])
    fval[1, 3] = sympy.utilities.lambdify((r, z, p), dz1)(*grid[0:3])
    fval[1, 4] = sympy.utilities.lambdify((r, z, p), dz2)(*grid[0:3])
    fval[1, 5] = sympy.utilities.lambdify((r, z, p), dz3)(*grid[0:3])
    fval[1, 6] = sympy.utilities.lambdify((r, z, p), dz4)(*grid[0:3])
    fval[1, 7] = sympy.utilities.lambdify((r, z, p), dz5)(*grid[0:3])
    fval[1, 8] = sympy.utilities.lambdify((r, z, p), dz6)(*grid[0:3])
    fval[1, 9] = sympy.utilities.lambdify((r, z, p), dz7)(*grid[0:3])
    fval[1, 10] = sympy.utilities.lambdify((r, z, p), dz8)(*grid[0:3])
    fval[1, 11] = sympy.utilities.lambdify((r, z, p), dz9)(*grid[0:3])
    fval[2, 0] = sympy.utilities.lambdify((r, z, p), pr0)(*grid[0:3])
    fval[2, 1] = sympy.utilities.lambdify((r, z, p), pz0)(*grid[0:3])
    fval[2, 2] = sympy.utilities.lambdify((r, z, p), pp0)(*grid[0:3])
    fval[2, 3] = sympy.utilities.lambdify((r, z, p), dp1)(*grid[0:3])
    fval[2, 4] = sympy.utilities.lambdify((r, z, p), dp2)(*grid[0:3])
    fval[2, 5] = sympy.utilities.lambdify((r, z, p), dp3)(*grid[0:3])
    fval[2, 6] = sympy.utilities.lambdify((r, z, p), dp4)(*grid[0:3])
    fval[2, 7] = sympy.utilities.lambdify((r, z, p), dp5)(*grid[0:3])
    fval[2, 8] = sympy.utilities.lambdify((r, z, p), dp6)(*grid[0:3])
    fval[2, 9] = sympy.utilities.lambdify((r, z, p), dp7)(*grid[0:3])
    fval[2, 10] = sympy.utilities.lambdify((r, z, p), dp8)(*grid[0:3])
    fval[2, 11] = sympy.utilities.lambdify((r, z, p), dp9)(*grid[0:3])
    return fval


def gradient_expression(expr, grid, torgeom):
    '''
        Generate fval consistent with eval_nimrod output for grad of scalar
        sympy expression
    '''
    size = (12, ) + np.shape(grid[0])
    fval = np.zeros(size)
    dr = sympy.diff(expr, 'r')
    drr = sympy.diff(dr, 'r')
    drz = sympy.diff(dr, 'z')
    drp = sympy.diff(dr, 'p')
    dz = sympy.diff(expr, 'z')
    dzz = sympy.diff(dz, 'z')
    dzp = sympy.diff(dz, 'p')
    dp = sympy.diff(expr, 'p')
    dpp = sympy.diff(dp, 'p')
    if torgeom:
        dp = dp / r
        drp = drp / r
        dzp = dzp / r
        dpp = dpp / r**2
        dpr = drp
        drp = drp - dp / r
    else:
        dpr = drp

    fval[0] = sympy.utilities.lambdify((r, z, p), dr)(*grid[0:3])
    fval[1] = sympy.utilities.lambdify((r, z, p), dz)(*grid[0:3])
    fval[2] = sympy.utilities.lambdify((r, z, p), dp)(*grid[0:3])
    #  derivative terms neglect derivatives of unit verctors
    fval[3] = sympy.utilities.lambdify((r, z, p), drr)(*grid[0:3])
    fval[4] = sympy.utilities.lambdify((r, z, p), drz)(*grid[0:3])
    fval[5] = sympy.utilities.lambdify((r, z, p), drp)(*grid[0:3])
    fval[6] = fval[4]
    fval[7] = sympy.utilities.lambdify((r, z, p), dzz)(*grid[0:3])
    fval[8] = sympy.utilities.lambdify((r, z, p), dzp)(*grid[0:3])
    fval[9] = sympy.utilities.lambdify((r, z, p), dpr)(*grid[0:3])
    fval[10] = fval[8]
    fval[11] = sympy.utilities.lambdify((r, z, p), dpp)(*grid[0:3])
    return fval


# @pytest.fixture
def grid_1d():
    '''
      1D grid for testing
    '''
    p1 = [0.75, 0.1, 0]
    p2 = [1.5, 0.3, 0.546]
    nx1 = 3
    return pn.grid_1d_gen(p1, p2, nx1)


def grid_2d():
    '''
      2D grid for testing
    '''
    p1 = [1.25, -0.9, 0.25]
    p2 = [1.25, 1.8, 0.25]
    p3 = [3.5, -0.9, 0.25]
    nx1 = 3
    nx2 = 4
    return pn.grid_2d_gen(p1, p2, p3, nx1, nx2)


def grid_3d():
    '''
      3D grid for testing
    '''
    p1 = [2.68, -1.33, 1.14]
    p2 = [2.68, 0.89, 1.14]
    p3 = [4.8, -1.33, 1.14]
    p4 = [2.68, -1.33, -1.14]
    nx1 = 4
    nx2 = 2
    nx3 = 3
    return pn.grid_3d_gen(p1, p2, p3, p4, nx1, nx2, nx3)


def scalar_function(expr, grid, dmode, torgeom, iqnq=(0, 1)):
    '''
      Create a scalar object based on expr
    '''
    iqty = iqnq[0]
    nqty = iqnq[1]
    if dmode == 1:
        size = (4*nqty,) + np.shape(grid[0])
    elif dmode == 2:
        size = (10*nqty,) + np.shape(grid[0])
    else:
        size = (nqty,) + np.shape(grid[0])
    fval = np.zeros(size)
    if isinstance(expr, sympy.Basic):
        result = function_expression(expr, grid, torgeom)
    else:
        result = function_constant(expr, grid)
    for iq in range(nqty):
        fval[iq] = (iq+1)*result[0]
        if dmode > 0:
            fval[nqty+iq:4*nqty:nqty] = (iq+1)*result[1:4]
        if dmode > 1:
            fval[4*nqty+iq:10*nqty:nqty] = (iq+1)*result[4:10]
    scalar = Scalar(fval, grid, dmode, torgeom, iqty)
    return scalar


def vector_modulus(expr, grid, dmode, torgeom):
    '''
      Calculate modulus of vector
    '''
    vector_mod = sympy.sqrt(expr[0]**2 + expr[1]**2 + expr[2]**2)
    scalar = scalar_function(vector_mod, grid, dmode, torgeom)
    return scalar


def vector_function(expr, grid, dmode, torgeom):
    '''
      Create a vector object based on expr
    '''
    if dmode == 1:
        size = (12, ) + np.shape(grid[0])
    elif dmode == 2:
        size = (30, ) + np.shape(grid[0])
    else:
        size = (3, ) + np.shape(grid[0])
    fval = np.zeros(size)
#    if cond1:
    result = vector_expression(expr, grid, torgeom)
#    elif cond5:
#        result = vector_constant(expr1, expr2, expr3, grid)
#    else:
#        raise Exception("Cannot create vector with mixed
#                         expressions and constant components")
    fval[0:3] = result[0:3]
    if dmode > 0:
        fval[3:12] = result[3:12]
    if dmode > 1:
        fval[12:30] = result[12:30]
    vector = Vector(fval, grid, dmode, torgeom)
    return vector


def tensor_function(expr, grid, dmode, torgeom):
    '''
      Create a tensor object based on expr
    '''
    if dmode == 1:
        size = (3, 12, ) + np.shape(grid[0])
    else:
        size = (3, 3, ) + np.shape(grid[0])
    fval = np.zeros(size)
    result = tensor_expression(expr, grid, torgeom)
    fval[:, 0:3] = result[:, 0:3]
    if dmode > 0:
        fval[:, 3:12] = result[:, 3:12]
    tensor = Tensor(fval, grid, dmode, torgeom)
    return tensor


def divergence_vector_function(expr, grid, dmode, torgeom):
    '''
      Create a scalar object based on div(expr), for vector expr
    '''
    result = divergence_vector_expression(expr, grid, torgeom)
    if dmode == 0:
        raise ValueError("Divergence calculation needs dmode>0")
    elif dmode == 1:
        size = (1, ) + np.shape(grid[0])
        newdmode = 0
    else:
        size = (4, ) + np.shape(grid[0])
        newdmode = 1
    fval = np.zeros(size)
    fval[0] = result[0]
    if dmode > 1:
        fval[1:4] = result[1:4]
    scalar = Scalar(fval, grid, newdmode, torgeom)
    return scalar


def curl_function(expr, grid, dmode, torgeom):
    '''
      Yields curl of vector
    '''
    if dmode == 0:
        raise ValueError("Cannot calculate curl with dmode = 0")
    elif dmode == 1:
        size = (3, ) + np.shape(grid[0])
        newdmode = dmode - 1
    else:
        size = (12, ) + np.shape(grid[0])
        newdmode = dmode - 1
    fval = np.zeros(size)
    result = curl_expression(expr, grid, torgeom)
    fval[0:3] = result[0:3]
    if dmode > 1:
        fval[3:12] = result[3:12]
    vector = Vector(fval, grid, newdmode, torgeom)
    return vector


def divergence_tensor_function(expr, grid, dmode, torgeom):
    '''
      Create a vector object based on div(expr), for tensor expr
    '''
    result = divergence_tensor_expression(expr, grid, torgeom)
    if dmode == 0 or dmode == 2:
        raise ValueError("Divergence of tensor needs dmode = 1")
    else:
        size = (3, ) + np.shape(grid[0])
        newdmode = 0
    fval = np.zeros(size)
    if dmode == 1:
        fval[0:3] = result[0:3]
    vector = Vector(fval, grid, newdmode, torgeom)
    return vector


def vector_grad_function(expr, grid, dmode, torgeom):
    '''
      Create a tensor object based on grad(expr), for vector expr
    '''
    result = gradient_vector_expression(expr, grid, torgeom)
    if dmode == 0:
        raise ValueError("Gradient calculation requires dmode>0")
    elif dmode == 1:
        size = (3, 3, ) + np.shape(grid[0])
        newdmode = 0
    else:
        size = (3, 12, ) + np.shape(grid[0])
        newdmode = 1
    fval = np.zeros(size)
    fval[0:3, 0:3] = result[0:3, 0:3]
    if dmode > 1:
        fval[0:3, 3:12] = result[0:3, 3:12]
    tensor = Tensor(fval, grid, newdmode, torgeom)
    return tensor


def scalar_grad_function(expr, grid, dmode, torgeom):
    '''
      Create a vector object based on grad(expr), for scalar expr
    '''
    if isinstance(expr, sympy.Basic):
        result = gradient_expression(expr, grid, torgeom)
    else:
        result = gradient_constant(expr, grid)
    if dmode == 0:
        raise ValueError("Gradient calculation requires dmode>0")
    elif dmode == 1:
        size = (3,) + np.shape(grid[0])
        newdmode = 0
    else:
        size = (12,) + np.shape(grid[0])
        newdmode = 1
    fval = np.zeros(size)
    fval[0:3] = result[0:3]
    if dmode > 1:
        fval[3:12] = result[3:12]
    vector = Vector(fval, grid, newdmode, torgeom)
    return vector


@pytest.mark.parametrize("grid", [grid_1d(), grid_2d(), grid_3d()])
@pytest.mark.parametrize("torgeom", [False, True])
class Test_Scalar():
    '''
    Test field_class classes :class:`Scalar`
    '''
    @pytest.mark.parametrize("v1", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    @pytest.mark.parametrize("iqnq", [(0, 1), (0, 3), (1, 2), (4, 6)])
    def test_init(self, v1, grid, dmode, torgeom, iqnq):
        v1scalar = scalar_function(v1, grid, dmode, torgeom, iqnq)
        v1scalar._test_almost_eq(v1scalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    @pytest.mark.parametrize("iqnq", [(0, 1), (0, 3), (1, 2), (4, 6)])
    def test_add_number(self, v1, v2, grid, dmode, torgeom, iqnq):
        sum = v1 + v2
        v2iq = v2 * (iqnq[0] + 1)
        v1scalar = scalar_function(v1, grid, dmode, torgeom, iqnq)
        sumscalar = scalar_function(sum, grid, dmode, torgeom, iqnq)
        result = v1scalar + v2iq
        result._test_almost_eq(sumscalar, rtol=1e-7, atol=1.0e-8)
        result = v2iq + v1scalar
        result._test_almost_eq(sumscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    @pytest.mark.parametrize("iqnq", [(0, 1), (0, 3), (1, 2), (4, 6)])
    def test_add_scalar(self, v1, v2, grid, dmode, torgeom, iqnq):
        sum = v1 + v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom, iqnq)
        v2scalar = scalar_function(v2, grid, dmode, torgeom, iqnq)
        sumscalar = scalar_function(sum, grid, dmode, torgeom, iqnq)
        result = v1scalar + v2scalar
        result._test_almost_eq(sumscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_sub_number(self, v1, v2, grid, dmode, torgeom):
        diff = v1 - v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        diffscalar = scalar_function(diff, grid, dmode, torgeom)
        result = v1scalar - v2
        result._test_almost_eq(diffscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", number_list)
    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_rsub_number(self, v1, v2, grid, dmode, torgeom):
        diff = v1 - v2
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        diffscalar = scalar_function(diff, grid, dmode, torgeom)
        result = v1 - v2scalar
        result._test_almost_eq(diffscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_sub_scalar(self, v1, v2, grid, dmode, torgeom):
        diff = v1 - v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        diffscalar = scalar_function(diff, grid, dmode, torgeom)
        result = v1scalar - v2scalar
        result._test_almost_eq(diffscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_mul_number(self, v1, v2, grid, dmode, torgeom):
        prod = v1 * v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        prodscalar = scalar_function(prod, grid, dmode, torgeom)
        result = v1scalar * v2
        result._test_almost_eq(prodscalar, rtol=1e-7, atol=1.0e-8)
        result = v2 * v1scalar
        result._test_almost_eq(prodscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_mul_scalar(self, v1, v2, grid, dmode, torgeom):
        prod = v1 * v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        prodscalar = scalar_function(prod, grid, dmode, torgeom)
        result = v1scalar * v2scalar
        result._test_almost_eq(prodscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_truediv_number(self, v1, v2, grid, dmode, torgeom):
        quot = v1 / v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        quotscalar = scalar_function(quot, grid, dmode, torgeom)
        result = v1scalar / v2
        result._test_almost_eq(quotscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", non_zero_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_truediv_scalar(self, v1, v2, grid, dmode, torgeom):
        quot = v1 / v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        quotscalar = scalar_function(quot, grid, dmode, torgeom)
        result = v1scalar / v2scalar
        result._test_almost_eq(quotscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", number_list)
    @pytest.mark.parametrize("v2", non_zero_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_rtruediv_number(self, v1, v2, grid, dmode, torgeom):
        quot = v1 / v2
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        quotscalar = scalar_function(quot, grid, dmode, torgeom)
        result = v1 / v2scalar
        result._test_almost_eq(quotscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_neg(self, v1, grid, dmode, torgeom):
        neg = -v1
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        negscalar = scalar_function(neg, grid, dmode, torgeom)
        result = -v1scalar
        result._test_almost_eq(negscalar, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("v2", int_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_pow_number(self, v1, v2, grid, dmode, torgeom):
        power = v1 ** v2
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        powerscalar = scalar_function(power, grid, dmode, torgeom)
        result = v1scalar ** v2
        result._test_almost_eq(powerscalar, rtol=1.e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v1", full_list)
    @pytest.mark.parametrize("dmode", [1, 2])
    def test_grad(self, v1, grid, dmode, torgeom):
        v1scalar = scalar_function(v1, grid, dmode, torgeom)
        gradvector = scalar_grad_function(v1, grid, dmode, torgeom)
        result = v1scalar.grad()
        result._test_almost_eq(gradvector, rtol=1.e-7, atol=1.0e-8)


@pytest.mark.parametrize("grid", [grid_1d(), grid_2d(), grid_3d()])
@pytest.mark.parametrize("torgeom", [False, True])
@pytest.mark.parametrize("v1", full_vec_list)
class Test_Vector():
    '''
      Test field class for :class: `Vector`
    '''
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_init(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        vector._test_almost_eq(vector, rtol=1e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_add_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v1[i] + v2
        sumvector = vector_function(z, grid, dmode, torgeom)
        result = vector + v2
        result._test_almost_eq(sumvector, rtol=1.e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_radd_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v2 + v1[i]
        sumvector = vector_function(z, grid, dmode, torgeom)
        result = v2 + vector
        result._test_almost_eq(sumvector, rtol=1.e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_add_vector(self, v1, v2, grid, dmode, torgeom):
        vector1 = vector_function(v1, grid, dmode, torgeom)
        vector2 = vector_function(v2, grid, dmode, torgeom)
        vectorsum = vector1 + vector2
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] + v2[i]
        result = vector_function(z, grid, dmode, torgeom)
        result._test_almost_eq(vectorsum, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_sub_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v1[i] - v2
        subvector = vector_function(z, grid, dmode, torgeom)
        result = vector - v2
        result._test_almost_eq(subvector, rtol=1.e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_rsub_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v2 - v1[i]
        subvector = vector_function(z, grid, dmode, torgeom)
        result = v2 - vector
        result._test_almost_eq(subvector, rtol=1.e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_sub_vector(self, v1, v2, grid, dmode, torgeom):
        vector1 = vector_function(v1, grid, dmode, torgeom)
        vector2 = vector_function(v2, grid, dmode, torgeom)
        result = vector1 - vector2
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] - v2[i]
        subvector = vector_function(z, grid, dmode, torgeom)
        result._test_almost_eq(subvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_mul_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v1[i] * v2
        mulvector = vector_function(z, grid, dmode, torgeom)
        result = vector * v2
        result._test_almost_eq(mulvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_rmul_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        vector = vector_function(v1, grid, dmode, torgeom)
        for i in range(dim):
            z[i] = v2 * v1[i]
        mulvector = vector_function(z, grid, dmode, torgeom)
        result = v2 * vector
        result._test_almost_eq(mulvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_mul_scalar(self, v1, v2, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] * v2
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        mulvector = vector_function(z, grid, dmode, torgeom)
        result = vector * v2scalar
        result._test_almost_eq(mulvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_div_number(self, v1, v2, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] / v2
        divvector = vector_function(z, grid, dmode, torgeom)
        result = vector / v2
        result._test_almost_eq(divvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", non_zero_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_div_scalar(self, v1, v2, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] / v2
        divvector = vector_function(z, grid, dmode, torgeom)
        v2scalar = scalar_function(v2, grid, dmode, torgeom)
        result = vector / v2scalar
        result._test_almost_eq(divvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_neg(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = -1 * v1[i]
        negvector = vector_function(z, grid, dmode, torgeom)
        result = -vector
        result._test_almost_eq(negvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1, 2])
    def test_dot_vector(self, v1, v2, grid, dmode, torgeom):
        dot = dot_product(v1, v2, grid, dmode, torgeom)
        vector1 = vector_function(v1, grid, dmode, torgeom)
        vector2 = vector_function(v2, grid, dmode, torgeom)
        result = vector1.dot(vector2)
        result._test_almost_eq(dot, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_ten_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_dot_tensor(self, v1, v2, grid, dmode, torgeom):
        r1 = dot_vector_tensor(v1, v2, grid, dmode, torgeom)
        vector = vector_function(v1, grid, dmode, torgeom)
        tensor = tensor_function(v2, grid, dmode, torgeom)
        r2 = vector.dot(tensor)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_dyadic(self, v1, v2, grid, dmode, torgeom):
        r1 = dyadic_product(v1, v2, grid, dmode, torgeom)
        vector1 = vector_function(v1, grid, dmode, torgeom)
        vector2 = vector_function(v2, grid, dmode, torgeom)
        r2 = vector1.dyadic(vector2)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_cross_vector(self, v1, v2, grid, dmode, torgeom):
        vector1 = vector_function(v1, grid, dmode, torgeom)
        vector2 = vector_function(v2, grid, dmode, torgeom)
        crossvector = cross_vector_vector(v1, v2, grid, dmode, torgeom)
        result = vector1.cross(vector2)
        result._test_almost_eq(crossvector, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_ten_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_cross_tensor(self, v1, v2, grid, dmode, torgeom):
        r1 = cross_vector_tensor(v1, v2, grid, dmode, torgeom)
        vector = vector_function(v1, grid, dmode, torgeom)
        tensor = tensor_function(v2, grid, dmode, torgeom)
        r2 = vector.cross(tensor)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [0, 1])
    def test_mag(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        abs_val = vector_modulus(v1, grid, dmode, torgeom)
        abs_vec = vector.mag()
        abs_val._test_almost_eq(abs_vec, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [0, 1])
    def test_hat(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        vector_mod = vector_modulus(v1, grid, dmode, torgeom)
        u1 = vector / vector_mod
        vector_hat = vector.hat()
        u1._test_almost_eq(vector_hat, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [1, 2])
    def test_divergence(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dv1 = divergence_vector_function(v1, grid, dmode, torgeom)
        dv2 = vector.div()
        dv1._test_almost_eq(dv2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [1, 2])
    def test_curl(self, v1, grid, dmode, torgeom):
        vector = vector_function(v1, grid, dmode, torgeom)
        dv1 = curl_function(v1, grid, dmode, torgeom)
        dv2 = vector.curl()
        dv1._test_almost_eq(dv2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [1, 2])
    def test_grad(self, v1, grid, dmode, torgeom):
        r1 = vector_grad_function(v1, grid, dmode, torgeom)
        vector = vector_function(v1, grid, dmode, torgeom)
        r2 = vector.grad()
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)


@pytest.mark.parametrize("grid", [grid_1d(), grid_2d(), grid_3d()])
@pytest.mark.parametrize("torgeom", [False, True])
@pytest.mark.parametrize("v1", full_ten_list)
class Test_Tensor():
    '''
      Test field class for :class: `Tensor`
    '''
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_init(self, v1, grid, dmode, torgeom):
        tensor = tensor_function(v1, grid, dmode, torgeom)
        tensor._test_almost_eq(tensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_add_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] + v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        sumtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor + v2
        result._test_almost_eq(sumtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_radd_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v2 + v1[i]
        tensor = tensor_function(v1, grid, dmode, torgeom)
        sumtensor = tensor_function(z, grid, dmode, torgeom)
        result = v2 + tensor
        result._test_almost_eq(sumtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_ten_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_add_tensor(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] + v2[i]
        tensor1 = tensor_function(v1, grid, dmode, torgeom)
        tensor2 = tensor_function(v2, grid, dmode, torgeom)
        sumtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor1 + tensor2
        result._test_almost_eq(sumtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_sub_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] - v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        subtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor - v2
        result._test_almost_eq(subtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_rsub_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v2 - v1[i]
        tensor = tensor_function(v1, grid, dmode, torgeom)
        subtensor = tensor_function(z, grid, dmode, torgeom)
        result = v2 - tensor
        result._test_almost_eq(subtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_ten_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_sub_tensor(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] - v2[i]
        tensor1 = tensor_function(v1, grid, dmode, torgeom)
        tensor2 = tensor_function(v2, grid, dmode, torgeom)
        subtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor1 - tensor2
        result._test_almost_eq(subtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_mul_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] * v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        multensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor * v2
        result._test_almost_eq(multensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_mul_scalar(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] * v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        multensor = tensor_function(z, grid, dmode, torgeom)
        scalar = scalar_function(v2, grid, dmode, torgeom)
        result = tensor * scalar
        result._test_almost_eq(multensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_rmul_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v2 * v1[i]
        tensor = tensor_function(v1, grid, dmode, torgeom)
        multensor = tensor_function(z, grid, dmode, torgeom)
        result = v2 * tensor
        result._test_almost_eq(multensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", number_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_div_number(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] / v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        divtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor / v2
        result._test_almost_eq(divtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", non_zero_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_div_scalar(self, v1, v2, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = v1[i] / v2
        tensor = tensor_function(v1, grid, dmode, torgeom)
        scalar = scalar_function(v2, grid, dmode, torgeom)
        divtensor = tensor_function(z, grid, dmode, torgeom)
        result = tensor / scalar
        result._test_almost_eq(divtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [0, 1])
    def test_neg(self, v1, grid, dmode, torgeom):
        dim = len(v1)
        z = [0] * dim
        for i in range(dim):
            z[i] = -v1[i]
        tensor = tensor_function(v1, grid, dmode, torgeom)
        negtensor = tensor_function(z, grid, dmode, torgeom)
        result = -tensor
        result._test_almost_eq(negtensor, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [1])
    def test_divergence(self, v1, grid, dmode, torgeom):
        tensor = tensor_function(v1, grid, dmode, torgeom)
        dt1 = divergence_tensor_function(v1, grid, dmode, torgeom)
        dt2 = tensor.div()
        dt1._test_almost_eq(dt2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("dmode", [0, 1])
    def test_transpose(self, v1, grid, dmode, torgeom):
        t1 = tensor_transpose_function(v1, grid, dmode, torgeom)
        tensor = tensor_function(v1, grid, dmode, torgeom)
        t2 = tensor.transpose()
        t1._test_almost_eq(t2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_dot_vector(self, v1, v2, grid, dmode, torgeom):
        r1 = dot_tensor_vector(v1, v2, grid, dmode, torgeom)
        tensor = tensor_function(v1, grid, dmode, torgeom)
        vector = vector_function(v2, grid, dmode, torgeom)
        r2 = tensor.dot(vector)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_ten_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_dot_tensor(self, v1, v2, grid, dmode, torgeom):
        r1 = dot_tensor_tensor(v1, v2, grid, dmode, torgeom)
        tensor1 = tensor_function(v1, grid, dmode, torgeom)
        tensor2 = tensor_function(v2, grid, dmode, torgeom)
        r2 = tensor1.dot(tensor2)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)

    @pytest.mark.parametrize("v2", full_vec_list)
    @pytest.mark.parametrize("dmode", [0, 1])
    def test_cross(self, v1, v2, grid, dmode, torgeom):
        r1 = cross_tensor_vector(v1, v2, grid, dmode, torgeom)
        tensor = tensor_function(v1, grid, dmode, torgeom)
        vector = vector_function(v2, grid, dmode, torgeom)
        r2 = tensor.cross(vector)
        r1._test_almost_eq(r2, rtol=1.0e-7, atol=1.0e-8)
