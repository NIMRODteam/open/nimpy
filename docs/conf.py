# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'nimpy'
copyright = '2024, The NIMROD Team'
author = 'The NIMROD Team'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# requires pip install sphinx-mdinclude sphinx-math-dollar
extensions = ['sphinx.ext.todo', 'sphinx.ext.viewcode',
              'sphinx.ext.autodoc', 'sphinx_mdinclude',
              'sphinx_math_dollar', 'sphinx.ext.mathjax',
              'sphinx.ext.napoleon'] 

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'README.md']
source_suffix = ['.rst']

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__'
}

latex_engine = 'xelatex'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'bizstyle'
html_theme_options = {
        'sidebarwidth' : '25%'
}
html_static_path = ['_static']
html_css_files = ["custom.css"]
