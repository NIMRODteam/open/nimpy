.. nimpy documentation master file, created by
   sphinx-quickstart on Wed Jun  7 16:11:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nimpy documentation
===================

.. mdinclude:: ../README.md

File descriptions
-----------------

.. mdinclude:: ../nimpy/README.md
.. mdinclude:: ../nimpy/util/README.md

Code API documentation
----------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   nimpy.rst
   nimpy.util.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
