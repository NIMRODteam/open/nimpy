import importlib.util
from pathlib import Path
import yaml

'''
This module automatically generates example nimpy.yaml files
for each python script in nimpy

It assumes that a module has the variables
<NAME>_NML
<NAME>_INPUTS

If the variables are found, then a example file will be
written to the directory nimpy/yaml

This functionality could be extended to include a default header for
each module.

@TODO The script should also add additional dependencies.
For example nimglobaleq uses fsa

'''


def find_nimpy():
    '''
    Find the nimpy directory
    Assume directory structure is nimpy/yaml/thisfile

    :returns: nimpy_dir, yaml_dir

    '''

    currentfile = Path(__file__).resolve()
    yamldir = currentfile.parent
    nimpydir = currentfile.parent.parent / 'nimpy'

    return nimpydir, yamldir


def exclude_file(path):
    '''
    Check path to see if it should be exclude
    '''

    #  Exclude init files and testing files
    #  Also exclude files in eigen (for now)
    EXCLUDE_SET = {'__init__.py',
                   'eigen',
                   'tests'}

    #  Exclude objects that are not files
    exclude = not(path.is_file())

    #  Use set intersection to find files to exclude
    if (EXCLUDE_SET & set(path.parts)):
        exclude = True

    return exclude


def write_example_input(path, yaml_dir) -> None:
    '''
    Load the file path as a python module
    Reads the default namlist and writes it to a file

    :params path: path to python file
    :params yaml_dir: path to nimpy/yaml directory
    '''

    name = path.name
    root = name.split('.')[0]
    nml_name = root.upper() + '_NML'
    dict_name = root.upper() + '_INPUTS'

    nml = None
    inputs = None
    spec = importlib.util.spec_from_file_location(root, path)
    this = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(this)

    try:
        nml = getattr(this, nml_name)
        inputs = getattr(this, dict_name)
    except AttributeError:
        return
    except Exception as e:
        raise e

    outfile = "nimpy." + str(name) + ".yaml"
    msg1 = f"# This is an example yaml file for {root}.\n"
    msg2 = f"# See the file {name} for the definition of the inputs.\n \n"

    with open(outfile, 'w') as file:
        file.write(msg1)
        file.write(msg2)
        yaml.dump({nml: inputs}, file, default_flow_style=False)

    return


def main():
    nimpy_dir, yaml_dir = find_nimpy()
    for path in nimpy_dir.rglob('*.py'):
        if exclude_file(path):
            continue
        write_example_input(path, yaml_dir)


if __name__ == '__main__':
    main()
