title: Contributing
# NIMROD Contributing Guide

The [NIMROD team](https://www.nimrodteam.org) values the support and
contributions of the fusion community.
As a user or developer with access to the source code we invite you to share
your knowledge and ideas.
In order to maintain a healthy development community and quality codebase we
provide the following guidelines for requesting features, reporting bugs, and
contributing code.
Community members are encouraged to apply this development workflow to add
personalized analysis scripts to the scripts directory.
Development in personal scripts directories will not be scrutinized in a merge
request unless code review is requested.

### Table of Contents
* [Code of Conduct](#code-of-conduct)
* [Steps for Contributing](#steps-for-contributing)
  1. [Identify or Create a New Issue](#1-identify-or-create-a-new-issue)
  2. [Start a New Branch](#2-start-a-new-branch)
  3. [Work on the Feature](#3-work-on-the-feature)
  4. [Open a Merge Request](#4-open-a-merge-request)

## Code of Conduct
Familiarize yourself with the 
[code of conduct](https://gitlab.com/NIMRODteam/open/nimpy/-/blob/main/code_of_conduct.md)

## Steps for Contributing

### 1. Identify or Create a New Issue
Whether you have something to contribute or would just like to request a feature
or report a bug, the first thing you should do is search the
[existing issues](https://gitlab.com/NIMRODteam/open/nimpy/issues) for
relevant topics.

* If an appropriate issue already exits:

  Comment with additional information or :thumbsup: the discussion that you
  find important.

* If no relevant issue is currently open:

  Open a new issue.
  Be sure to include relevant context and information.
  It may be good to allow some time for discussion of your new issue to develop.

Features will be added at the discretion of developers.
Work that is mission critical or has a funding source will be prioritized.

### 2. Start a New Branch
When working on a new feature in Git, it is customary to put it in a new branch.
One can create a new branch based on the current working branch using
```
git checkout -b "my_feature_branch"
```
Currently, we are using the convention `<username>-<issue#>-<feature>` for
branch names.

### 3. Work on the Feature
For information about git commands and workflow refer to the
[git resources](https://gitlab.com/NIMRODteam/open/nimpy/-/wikis/git).
In this branch follow the "edit, add, commit" cycle and "push as needed.
Be sure to include unit tests that check the correctness of any code that you
have added.

### 4. Open a Merge Request
On the GitLab site, open a
[merge request](https://gitlab.com/NIMRODteam/open/nimpy/-/merge_requests) (MR)
Please include a concise description of the feature and all issues that it addresses.
To solicit feedback based on code review, prepend [WIP] or [Draft] to the MR
title indicate that while the feature branch is a work in progress or draft.
Using the appropriate keywords will
[automatically close](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
the associated issues once the merge request is finalized.

At this stage your code will be reviewed by the community.
You may also choose an Assignee who you would like to review your code.
Once you have addressed all any comments or concerns,
then your merge request will be merged by another developer.
If `main` is your target branch,
then only a select set of developers will be able to complete the merge request.
Otherwise, any number of participants could complete the merge.
It is considered bad practice to merge your own merge request,
but if there is consensus of approval then you probably won't get in trouble.
There are also safeguards in place to prevent merges that do not pass CI
or have conflicts with the target branch.
