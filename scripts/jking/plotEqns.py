from nimpy.plot_nimrod import PlotNimrod as pn
import f90nml, optparse
from nimpy.eval_nimrod import *
from nimpy.field_class import *
import matplotlib.pyplot as pl
from matplotlib import gridspec


### Basic sanity check ###
def sanity_check(eval_nimrod, grid, eval_grid, gmt, nml):
    nrow = 3
    ncol = 3
    figfac = 8
    margfac = 0.15
    vmargfac = 0.15
    hmargfac = 0.2
    fig = pl.figure(figsize=(figfac*ncol+1,figfac*nrow+1)) 
    gs = gridspec.GridSpec(nrow, ncol,
         wspace=margfac, hspace=margfac, 
         top=1.-vmargfac/(nrow+2*vmargfac), bottom=vmargfac/(nrow+2*vmargfac), 
         left=hmargfac/(ncol+2*hmargfac), right=1.-hmargfac/(ncol+2*vmargfac)) 
    
    # density
    fval = eval_nimrod.eval_field('n', eval_grid, dmode=2, eq=2)
    ne = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=0)
    nd = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=1)
    fval = eval_nimrod.eval_field('ndn', eval_grid, dmode=2, eq=2)
    ndn = Scalar(fval, grid, torgeom=gmt, dmod=2)
    ax = pl.subplot(gs[0,0])
    pn.plot_scalar_line(grid, ne.data, flabel='ne', f2=nd.data, f2label='nd',
                        f3=ndn.data, f3label='nn', ax=ax, show=False,
                        title=r'density ($m^{-3}$)')
    
    # grad density
    ax = pl.subplot(gs[1,0])
    pn.plot_scalar_line(grid, grad(ne).data[0], flabel='dne/dr', f2=grad(nd).data[0], f2label='dnd/dr',
                        f3=grad(ndn).data[0], f3label='dnn/dr', ax=ax, show=False,
                        title=r'deriv ($m^{-4}$)')
    
    # div(grad) density
    ax = pl.subplot(gs[2,0])
    pn.plot_scalar_line(grid, div(grad(ne)).data, flabel='d^2ne',
                        f2=div(grad(nd)).data, f2label='d^2 nd',
                        f3=div(grad(ndn)).data, f3label='d^2 nn', ax=ax, show=False,
                        title=r'div grad ($m^{-5}$)')
    
    # velocity
    fval = eval_nimrod.eval_field('v', eval_grid, dmode=2, eq=2)
    vcom = Vector(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('vn', eval_grid, dmode=2, eq=2)
    vn = Vector(fval, grid, torgeom=gmt, dmod=2)
    ax = pl.subplot(gs[0,1])
    pn.plot_scalar_line(grid, vcom.data[0], flabel='vcom',
                        f3=vn.data[0], f3label='vn', ax=ax, show=False,
                        title=r'velocity ($m/s$)')
    
    # div
    ax = pl.subplot(gs[1,1])
    pn.plot_scalar_line(grid, div(vcom).data, flabel='div vcom',
                        f3=div(vn).data, f3label='div vn', ax=ax, show=False,
                        title=r'div ($1/s$)')
    
    # grad(div)
    ax = pl.subplot(gs[2,1])
    pn.plot_scalar_line(grid, grad(div(vcom)).data[0], flabel='d^2 vcom',
                        f3=grad(div(vn)).data[0], f3label='d^2 vn', ax=ax, show=False,
                        title=r'grad div ($1/m*s$)')
    
    # temperature
    fval = eval_nimrod.eval_field('ti', eval_grid, dmode=2, eq=2)
    ti = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('te', eval_grid, dmode=2, eq=2)
    te = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('tn', eval_grid, dmode=2, eq=2)
    tn = Scalar(fval, grid, torgeom=gmt, dmod=2)
    ax = pl.subplot(gs[0,2])
    pn.plot_scalar_line(grid, te.data, flabel='te',
                        f2=ti.data, f2label='ti',
                        f3=tn.data, f3label='tn', ax=ax, show=False,
                        title=r'temp ($eV$)')
    
    # grad
    ax = pl.subplot(gs[1,2])
    pn.plot_scalar_line(grid, grad(te).data[0], flabel='grad te',
                        f2=grad(ti).data[0], f2label='grad ti',
                        f3=grad(tn).data[0], f3label='grad tn', ax=ax, show=False,
                        title=r'grad ($eV/m$)')
    
    # div(grad)
    ax = pl.subplot(gs[2,2])
    pn.plot_scalar_line(grid, div(grad(te)).data, flabel='d^2 te',
                        f2=div(grad(ti)).data, f2label='d^2 ti',
                        f3=div(grad(tn)).data, f3label='d^2 tn', ax=ax, show=False,
                        title=r'div grad ($eV/m^2$)')

    pl.show()


### Plot neutral density/velocity equations ###
def plot_neutral_eqn(eval_nimrod, grid, eval_grid, gmt, nml):
    ### field evals ###
    fval = eval_nimrod.eval_field('b', eval_grid, dmode=2, eq=2)
    b = Vector(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('n', eval_grid, dmode=2, eq=2)
    ne = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=0)
    nd = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=1)
    fval = eval_nimrod.eval_field('ndn', eval_grid, dmode=2, eq=2)
    ndn = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('v', eval_grid, dmode=2, eq=2)
    vcom = Vector(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('vn', eval_grid, dmode=2, eq=2)
    vn = Vector(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('ti', eval_grid, dmode=2, eq=2)
    ti = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('te', eval_grid, dmode=2, eq=2)
    te = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('tn', eval_grid, dmode=2, eq=2)
    tn = Scalar(fval, grid, torgeom=gmt, dmod=2)
    fval = eval_nimrod.eval_field('d', eval_grid, dmode=2, eq=1)
    diffv = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=1)
    diffn = Scalar(fval, grid, torgeom=gmt, dmod=2, nqty=2)
    
    ### density equation ###
    si = numpy.empty_like(ne.data)
    sr = numpy.empty_like(ne.data)
    scshape = (-1, ne.data.shape[0])
    for ix in numpy.ndindex(ne.data.shape):
        si[ix], sr[ix] = eval_nimrod.eval_degas(ne.data[ix], te.data[ix])
    sion = Scalar(numpy.reshape(si, scshape), grid, torgeom=gmt, dmod=0)
    srec = Scalar(numpy.reshape(sr, scshape), grid, torgeom=gmt, dmod=0)
    
    ndn_diff = nml['physics_input']['ndn_diff']
    d2nn = div(ndn_diff*diffn*grad(ndn))
    divnvn = -div(vn*ndn)
    ionization = sion*ne*ndn
    recombination = srec*ne*ne
    
    ### velocity equation ###
    mass = nml['const_input']['misp_input']
    m = mass[0]
    vn_iso_visc = nml['physics_input']['vn_iso_visc']
    neutral_dens = nml['equil_input']['neutral_dens']
    nu_n = m*neutral_dens*vn_iso_visc*diffv
    kb=1.60217733e-19
    pi=numpy.pi
    vn_adv = -m*ndn*vn.dot(grad(vn))
    grpn = -grad(ndn*kb*tn)
    divpin = div(nu_n*(grad(vn)+grad(vn).transpose()-2./3.*eye(grid)*div(vn)))
    vrel2 = (vn - vcom).dot(vn - vcom, dmod=0)
    vnth2 = 2.*kb*tn/m
    vith2 = 2.*kb*ti/m
    vcx = numpy.sqrt(4.*(vith2.data+vnth2.data)/pi+vrel2.data)
    acx = 1.09e-18
    bcx = -7.15e-20
    cx_section = Scalar(numpy.reshape(acx+bcx*numpy.log(vcx), scshape),
                        grid, torgeom=gmt, dmod=0)
    facin = numpy.power(4.*(4./pi*vith2.data+vrel2.data)+9.*pi/4.*vnth2.data,-0.5)
    facin = Scalar(numpy.reshape(facin, scshape), grid, torgeom=gmt, dmod=0)
    facni = numpy.power(4.*(4./pi*vnth2.data+vrel2.data)+9.*pi/4.*vith2.data,-0.5)
    facni = Scalar(numpy.reshape(facni, scshape), grid, torgeom=gmt, dmod=0)
    rcxin = -m*cx_section*ne*ndn*(vcom-vn)*vnth2*facin
    rcxni =  m*cx_section*ne*ndn*(vcom-vn)*vith2*facni
    faccx = numpy.sqrt(4.*vith2.data/pi+4.*vnth2.data/pi+vrel2.data)
    faccx = Scalar(numpy.reshape(faccx, scshape), grid, torgeom=gmt, dmod=0)
    gamma_cx = cx_section*ne*ndn*faccx
    gamvrel = m*(gamma_cx+recombination)*(vcom-vn) 
    
    ### plot ###
    nrow = 1
    ncol = 2
    figfac = 8
    margfac = 0.2
    vmargfac = 0.15
    hmargfac = 0.25
    fig = pl.figure(figsize=(figfac*ncol+1,figfac*nrow+1)) 
    gs = gridspec.GridSpec(nrow, ncol,
         wspace=margfac, hspace=margfac, 
         top=1.-vmargfac/(nrow+2*vmargfac), bottom=vmargfac/(nrow+2*vmargfac), 
         left=hmargfac/(ncol+2*hmargfac), right=1.-hmargfac/(ncol+2*vmargfac)) 
    
    ax = pl.subplot(gs[0,0])
    pn.plot_scalar_line(grid, divnvn.data, flabel=r'$-\nabla \cdot n_n v_n$',
                        f2=d2nn.data, f2label=r'$\nabla \cdot D_n \nabla n_n$',
                        f3=(0.-ionization).data, f3label=r'$-\Gamma^{ion}$',
                        f4=recombination.data, f4label=r'$\Gamma^{rec}$',
                        #f5=(divnvn-ionization+recombination+d2nn).data, f5label='sum',
                        style='varied', ax=ax, show=False, 
                        ylabel=r'$\partial n_n / \partial t$ ($m^{-3}s^{-1}$)', 
                        xlabel=r'R ($m$)')
    ax.set_ylim([-1.2e23,1.2e23])
    
    ax = pl.subplot(gs[0,1])
    pn.plot_vector_line(grid, vn_adv.data, flabel=r'$-\rho_n v_n \cdot \nabla v_n$',
                        f2=grpn.data, f2label=r'$-\nabla p_n$',
                        #f3=divpin.data, f3label=r'$-\nabla \cdot \Pi_n$',
                        #f4=(-rcxin).data, f4label=r'$-R^{cx}_{in}$',
                        f4=divpin.data, f4label=r'$-\nabla \cdot \Pi_n$',
                        f3=(-rcxin).data, f3label=r'$-R^{cx}_{in}$',
                        f5=rcxni.data, f5label=r'$R^{cx}_{ni}$',
                        f6=gamvrel.data, f6label=r'$m (\Gamma^{rec} + \Gamma^{cx}) (v - v_n)$',
                        #f7=(vn_adv+grpn+divpin-rcxin+rcxni+gamvrel).data, f7label='sum',
                        style='varied', ax=ax, show=False,
                        #vert_legend_gap=True,
                        ylabel=r'$\rho \partial v_n / \partial t \cdot \hat{R}$ ($kg\; m^{-2} s^{-2}$)',
                        xlabel='R (m)')
    ax.set_ylim([-8,8])
    pl.show()

def main():
    parser = optparse.OptionParser(usage="%prog [options] h5 file")
    parser.add_option('-d', '--debug', dest='debug',
                        help='Print out debug information',
                        action='store_true')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.print_usage()
        return
      
    dumpfile=args[0]
    
    grid = pn.grid_1d_gen([0, 0.1, 0], [0.8, 0.1, 0], 800)
    eval_grid = EvalGrid(grid)
    eval_nimrod = EvalNimrod(dumpfile, fieldlist='vnbtd')
    
    # parse namelist
    nml = f90nml.read('nimrod.in')
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False
    
    plot_neutral_eqn(eval_nimrod, grid, eval_grid, gmt, nml)

if __name__ == "__main__":
    main()

