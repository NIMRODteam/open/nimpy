#!/usr/bin/env python3

import f90nml,os
import matplotlib.pyplot as pl
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.field_class import Scalar,Vector,Tensor
from nimpy.fsa import FSA,find_pf_null

# load dump file
dumpfile = 'dumpgll.78244.h5'
nml = f90nml.read('nimrod.in')
gmt = nml['grid_input']['geom']
zeff = nml['const_input']['zeff_input']
mass = 3.3435860e-27
if gmt == 'tor':
    gmt=True
else:
    gmt=False
eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvbj')
rzo = find_pf_null(eval_nimrod, [1.7, -0.2, 0])

nsurf = 150 # FSA surfaces
nphi = 150

# Do FSA
def basefsa(rzc, y, dy, eval_nimrod, fdict):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call 
    and fill dy[4:4+neq]
    '''
    #### field evals ###
    rzf = rzc + [0., 0., 2.*numpy.pi]
    grid = pn.grid_1d_gen(rzc, rzf, nphi)
    fval = eval_nimrod.eval_field('n', grid, dmode=0, eq=2)
    nd = Scalar(fval, grid, torgeom=gmt, dmode=0)
    fval = eval_nimrod.eval_field('n', rzc, dmode=0, eq=3)
    nd0 = Scalar(fval, rzc, torgeom=gmt, dmode=0)
    fval = eval_nimrod.eval_field('v', grid, dmode=1, eq=2)
    ve = Vector(fval, grid, torgeom=gmt, dmode=1)
    fval = eval_nimrod.eval_field('v', rzc, dmode=1, eq=3)
    ve0 = Vector(fval, rzc, torgeom=gmt, dmode=1)
    fval = eval_nimrod.eval_field('b', grid, dmode=0, eq=2)
    be = Vector(fval, grid, torgeom=gmt, dmode=0)
    fval = eval_nimrod.eval_field('b', rzc, dmode=0, eq=3)
    be0 = Vector(fval, rzc, torgeom=gmt, dmode=0)
    fval = eval_nimrod.eval_field('j', grid, dmode=0, eq=2)
    ja = Vector(fval, grid, torgeom=gmt, dmode=0)
    fval = eval_nimrod.eval_field('j', rzc, dmode=0, eq=3)
    ja0 = Vector(fval, rzc, torgeom=gmt, dmode=0)

    #### Maxwell stress ###
    jxb =  numpy.average((ja.cross(be)).data, axis=1)
    jxb = Vector(jxb, rzc, torgeom=gmt, dmode=0)
    maxwell = be0.dot(jxb - ja0.cross(be0))
    dy[4] = maxwell.data*dy[2]

    #### Reynolds stress ###
    nvdgv = numpy.average((nd*ve.dot(grad(ve))).data, axis=1)
    nvdgv = Vector(nvdgv, rzc, torgeom=gmt, dmode=0)
    reynolds = (-mass/zeff)*be0.dot(nvdgv-nd0*ve0.dot(grad(ve0)))
    dy[5] = reynolds.data*dy[2]

    print(rzc, maxwell.data, reynolds.data, end = "\r")

    return dy

fsafilename = 'fsa_nl.npz'
if os.path.exists(fsafilename):
    fsaDict = numpy.load(fsafilename)
    dvar = fsaDict['arr_0']
    yvars = fsaDict['arr_1']
    contours = fsaDict['arr_2']
else:
    dvar, yvars, contours = FSA(eval_nimrod, rzo, basefsa, 2, nsurf=nsurf, \
                                depvar='eta', dpow=0.5, rzx=[1.3, -1.14, 0],
                                nsteps_max=5000)
    fsaArr = [dvar, yvars, contours]
    numpy.savez(fsafilename,*fsaArr)

# Determine where the FSA failed
iend=-1
while numpy.isnan(yvars[:,iend]).any():
    iend -= 1
iend += yvars.shape[1]+1
ist = 75

# set fields as views to the arrays
vprime = dvar[6, ist:iend]
maxwell = yvars[0,ist:iend]
reynolds = yvars[1,ist:iend]
rhon = dvar[1,ist:iend]
psi = dvar[2,ist:iend]
psix = dvar[2,-1]
q = numpy.fabs(dvar[7,ist:iend])
xticks=numpy.linspace(0.85,1,4)

### plot ###
fig, ax = pl.subplots()

pn.plot_scalar_line(None, maxwell, flabel=r'Maxwell',
                    f2=reynolds, f2label=r'Reynolds',
                    #f3=vprime, f3label=r'Vprime',
                    style='varied', ax=ax, show=False,
                    ylabel=r'$B \cdot \rho \partial v / \partial t$ ($T N m^{-3}$)',
                    xticks=xticks,
                    xvar=rhon, xlabel=r'$\rho_N$')
#ax.set_ylim([-8,8])
pl.show()

