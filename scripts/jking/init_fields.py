#!/usr/bin/env python
import h5py, optparse, re
import numpy as np
import f90nml

class fieldDefinitions(object):
    ''' Class to define field initial conditions '''

    def set_zero(self, outdat, rzdat):
        ''' Set field to zero (default if field is not defined)'''
        outdat[:,:,:] = 0.

    def set_field(self, field, outdat, rzdat):
        ''' Dispatch method '''
        method_name = 'set_' + str(field)
        # Get the method from 'self'. Default to zero.
        method = getattr(self, method_name, self.set_zero)
        # Call the method as we return it
        return method(outdat, rzdat)

    def set_rz(self, outdat, rzdat):
        ''' set RZ '''
        outdat[:,:,:] = rzdat

    def set_rebe(self, outdat, rzdat):
        ''' Set magnetic field '''
        outdat[:,:,0] = 1.
        outdat[:,:,1] = 2.
        outdat[:,:,2] = 3.
        return 

    def set_rend(self, outdat, rzdat):
        ''' Set number density '''
        outdat[:,:,0] = 4.*rzdat[:,:,0]

    def set_reti(self, outdat, rzdat):
        ''' Set ion temperature '''
        outdat[:,:,0] = 5.*rzdat[:,:,0]

    def set_rete(self, outdat, rzdat):
        ''' Set electron temperature '''
        outdat[:,:,0] = 6.*rzdat[:,:,0]

    def set_repr(self, outdat, rzdat):
        ''' Set pressure '''
        outdat[:,:,0] = 7.*rzdat[:,:,1]

    def set_repe(self, outdat, rzdat):
        ''' Set electron pressure '''
        outdat[:,:,0] = 8.*rzdat[:,:,1]

def setFields(field, nxbl, nybl, infile, outfile):
  '''
    Loop over fields from infile and set them as desired
  '''
  try:
    bldat = infile['/rblocks/0001/'+field+'0001']
  except:
    return # skip variable if it does not exist

  setter = fieldDefinitions()

  for ixbl in np.arange(nxbl):
    for iybl in np.arange(nybl):
      ib = ixbl*nybl+iybl+1
      blstr = str(ib)
      while(len(blstr) < 4):
        blstr='0'+blstr
      rzdat = infile['/rblocks/'+blstr+'/rz'+blstr]
      bldat = infile['/rblocks/'+blstr+'/'+field+blstr]
      outdat = outfile.create_dataset('/rblocks/'+blstr+'/'+field+blstr, bldat.shape, dtype='f')

      # set the field here
      setter.set_field(field, outdat, rzdat)

      if (field=='rz'):
        outdat.attrs.create("vsKind","structured")
        outdat.attrs.create("vsType","mesh")
        outdat.attrs.create("vsLabels","R, Z")
      else:
        outdat.attrs.create("vsMesh","/rz")
        outdat.attrs.create("vsType","variable")
        if (bldat.shape[2]==3):
          outdat.attrs.create("vsLabels",field+"R, "+field+"Z, "+field+"Phi")

  return

def main():
  '''
    Parse input and open files
  '''
  parser = optparse.OptionParser(usage="%prog [options] h5 file")
  parser.add_option('-d', '--debug', dest='debug',
                      help='Print out debug information',
                      action='store_true')
  options, args = parser.parse_args()
  if len(args) > 1 or len(args)==0:
    parser.print_usage()
    return
  else:
    infilename=args[0]

  infile = h5py.File(infilename, 'r')
  outfilename = re.sub(r'.h5', '.init.h5', infilename)
  outfile = h5py.File(outfilename, 'w')
  outfile.attrs.create("nbl", infile.attrs.get("nbl"))
  outfile.attrs.create("nrbl", infile.attrs.get("nrbl"))
  outfile.attrs.create("poly_degree", infile.attrs.get("poly_degree"))
  outfile.attrs.create("nmodes", infile.attrs.get("nmodes"))
  inTimeGroup = infile["/dumpTime"]
  outTimeGroup = outfile.create_group("/dumpTime")
  outTimeGroup.attrs.create("vsStep", inTimeGroup.attrs.get("vsStep"))
  outTimeGroup.attrs.create("vsTime", inTimeGroup.attrs.get("vsTime"))
  outTimeGroup.attrs.create("vsType", inTimeGroup.attrs.get("vsType"))
  infile.copy('/seams', outfile)
  nml = f90nml.read('nimrod.in')
  nxbl = nml['grid_input']['nxbl']
  nybl = nml['grid_input']['nybl']

  fields=["bq", "diff", "jq", "nq", "peq", "prq", "rz", "vq", \
          "imbe", "imconc", "imnd", "impe", "impr", "imte", "imti", "imve", \
          "rebe", "reconc", "rend", "repe", "repr", "rete", "reti", "reve", \
          "reja", "imja", "psi_eq", "eq", "reee", "imee", \
          "F_diff", "Fhot_eq", "nhot_eq", "thot_eq", \
          "imgei", "imppe", "imppi", "imqpe", "imqpi", "imrpe", "impri", \
          "regei", "reppe", "reppi", "reqpe", "reqpi", "rerpe", "repri", \
          "reFhot", "imFhot", "rephot", "imphot" \
          "ndn_eq", "vn_eq", "tn_eq", \
          "rendn", "revn", "retn", "imndn", "imvn", "imtn"]

  for field in fields:
    setFields(field, nxbl, nybl, infile, outfile)

  for ixbl in np.arange(nxbl):
    for iybl in np.arange(nybl):
      ib = ixbl*nybl+iybl+1
      blstr = str(ib)
      while(len(blstr) < 4):
        blstr='0'+blstr
      grp = '/rblocks/'+blstr
      igrp = infile[grp]
      outfile[grp].attrs.create("degenerate", igrp.attrs.get("degenerate"))
      outfile[grp].attrs.create("id", igrp.attrs.get("id"))
      outfile[grp].attrs.create("mx", igrp.attrs.get("mx"))
      outfile[grp].attrs.create("mxe", igrp.attrs.get("mxe"))
      outfile[grp].attrs.create("my", igrp.attrs.get("my"))
      outfile[grp].attrs.create("nfour", igrp.attrs.get("nfour"))
      outfile[grp].attrs.create("poly_degree", igrp.attrs.get("poly_degree"))

  return

if __name__ == "__main__":
        main()
