#!/usr/bin/env python3

import os,h5py,argparse,f90nml
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from datetime import date
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.fsa import FSA,find_opoint,find_xpoint

def basefsa(rzc, y, dy, eval_nimrod, fdict):
    '''
    Flux surface averge quantities
    Set neq to number of outputs in FSA call
    and fill dy[4:4+neq]
    '''
    eval_eq=fdict['eval_eq']
    n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=eval_eq)
    dy[4] = n[0]*dy[2] # ne
    dy[5] = n[1]*dy[2] # nd
    dy[6] = n[2]*dy[2] # nc
    ti = eval_nimrod.eval_field('ti', rzc, dmode=0, eq=eval_eq)
    dy[7] = ti[0]*dy[2] # ti
    te = eval_nimrod.eval_field('te', rzc, dmode=0, eq=eval_eq)
    dy[8] = te[0]*dy[2] # te
    bf = eval_nimrod.eval_field('b', rzc, dmode=0, eq=eval_eq)
    vf = eval_nimrod.eval_field('v', rzc, dmode=0, eq=eval_eq)
    dy[9] = vf[2]*dy[2]/rzc[0] # omega
    dy[10] = (vf[0]*bf[0]+vf[1]*bf[1])*dy[2]/(bf[0]*bf[0]+bf[1]*bf[1]) # Kpol
    j = eval_nimrod.eval_field('j', rzc, dmode=0, eq=eval_eq)
    dy[11] = np.sqrt(j[0]*j[0]+j[1]*j[1]+j[2]*j[2]) # J
    return dy

def create_figure(nrows=1, ncols=1, rhoq_list=None):
    fig_size = [ncols*12,nrows*6.75]
    fig,axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=fig_size)
    if nrows>1 or ncols>1:
        for ax in axs.reshape(-1):
            for rhoq in rhoq_list:
                ax.axvline(rhoq, ls=':')
    else:
        for rhoq in rhoq_list:
            axs.axvline(rhoq, ls=':')
    return fig,axs

def run_fsa(dumpfile, step):
    '''
    Compute the fsa for each file
    '''
    nsurf = 150 # FSA surfaces
    dpow=1.0
    stepstr=str(step)
    fsafilename = 'fsa'+stepstr+'.npz'
    if os.path.exists(fsafilename):
        fsaDict = np.load(fsafilename)
        dvar = fsaDict['arr_0']
        yvars = fsaDict['arr_1']
        contours = fsaDict['arr_2']
    else:
        eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbjd')
        rzo = find_opoint(eval_nimrod)
        rzx = find_xpoint(eval_nimrod)
        dvar, yvars, contours = FSA(eval_nimrod, rzo, basefsa, 8, nsurf=nsurf, 
                                    depvar='eta', dpow=dpow, rzx=rzx,
                                    eval_eq=3)
        fsaArr = [dvar, yvars, contours]
        np.savez(fsafilename,*fsaArr)

    return dvar,yvars

def plot_profs(var_list):
    '''
    Plot fsa quantities
    '''

    # Constants
    echrg=1.609e-19 # C
    kb=1.609e-19    # eV/J
    eps0=8.85418782e-12
    mu0=4.e-7*np.pi
    nml = f90nml.read('nimrod.in')
    nisp=nml['equil_input']['nisp']
    nsp=nisp+1
    misp=nml['const_input']['misp_input']
    zisp=nml['const_input']['zisp_input']
    md=misp[0]
    mc=misp[1]
    zd=zisp[0]
    zc=zisp[1]
    me=nml['const_input']['me_input']
    qe=echrg
    qd=zd*echrg
    qc=zc*echrg

    # dictionaries for plots
    ne_dict = {}
    ti_dict = {}
    te_dict = {}
    omega_dict = {}
    kpol_dict = {}
    j_dict = {}

    # iterate over files
    for label,dvar,yvars in var_list:
        # Determine where the FSA failed
        iend=-1
        while np.isnan(yvars[:,iend]).any():
            iend -= 1
        iend += yvars.shape[1]+1

        iend += -2 # adjust

        ne = yvars[0,:iend]
        nd = yvars[1,:iend]
        nc = yvars[2,:iend]
        ti = yvars[3,:iend]
        te = yvars[4,:iend]
        omega = yvars[5,:iend]
        kpol = yvars[6,:iend]
        ja = yvars[7,:iend]
        psin = dvar[0,:iend]
        rhon = dvar[1,:iend]
        psi = dvar[2,:iend]
        psix = dvar[2,-1]
        q = np.fabs(dvar[7,:iend])

        xvar = rhon
        ne_dict[label] = rhon,ne
        te_dict[label] = rhon,te
        ti_dict[label] = rhon,ti
        omega_dict[label] = rhon,omega
        kpol_dict[label] = rhon,kpol
        j_dict[label] = rhon,ja

    rhoofq=interp1d(q,rhon)
    psiofq=interp1d(q,psin)
    rhoq_list=[rhoofq(4),rhoofq(5),rhoofq(6),rhoofq(7),rhoofq(8)]
    psiq_list=[psiofq(4),psiofq(5),psiofq(6),psiofq(7),psiofq(8)]

    # Plot density, temperature (2x), omega, kpol, j
    # 
    fig,axs = create_figure(nrows=3, ncols=2, rhoq_list=rhoq_list)
    pn.plot_dict_scalar_line(ne_dict, xlabel=None, xticks='skip',
                             ylabel=r'$\langle n_e \rangle$ ($m^{-3}$)',
                             style='varied', legend_loc=None, ax=axs[0,0], show=False)
    pn.plot_dict_scalar_line(te_dict, xlabel=None, xticks='skip',
                             ylabel=r'$\langle T_e \rangle$ (eV)',
                             style='varied', legend_loc=None, ax=axs[1,0], show=False)
    pn.plot_dict_scalar_line(ti_dict, xlabel=r'$\rho_N$',
                             ylabel=r'$\langle T_i \rangle$ (eV)',
                             style='varied', legend_loc=None, ax=axs[2,0], show=False)
    pn.plot_dict_scalar_line(omega_dict, xlabel=None, xticks='skip',
                             ylabel=r'$\langle v_\Phi/R \rangle$ ($1/s$)',
                             style='varied', legend_loc='lower left', ax=axs[0,1], show=False)
    pn.plot_dict_scalar_line(kpol_dict, xlabel=None, xticks='skip',
                             ylabel=r'$\langle v\cdot B/B^2 \rangle$ ($m/sT$)',
                             style='varied', legend_loc=None, ax=axs[1,1], show=False)
    pn.plot_dict_scalar_line(j_dict, xlabel=r'$\rho_N$',
                             ylabel=r'$\langle J \rangle$ ($A/m^2$)',
                             style='varied', legend_loc=None, ax=axs[2,1]) 

    fig.tight_layout()
    fig.savefig('profs.png')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Calculate and plot profiles'
    )
    parser.add_argument('-f', '--files', nargs='+', default=['dumpgll.00000.h5'])
    args = vars(parser.parse_args())

    var_list=[]
    for file in args['files']:
        dfile = h5py.File(file,'r')
        timeGroup = dfile["/dumpTime"]
        time = timeGroup.attrs.get("vsTime")
        step = timeGroup.attrs.get("vsStep")[0]
        dvar, yvars = run_fsa(file, step)
        label = "{0:0.3f}".format(time*1000.)+' ms'
        var_list.append((label, dvar, yvars))

    plot_profs(var_list)
