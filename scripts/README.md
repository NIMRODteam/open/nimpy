Place personal scripts in named subdirectories. Development in personal scripts
directories will not be scrutinized in a merge request unless code review is
requested.
