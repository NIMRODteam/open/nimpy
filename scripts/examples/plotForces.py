#!/usr/bin/env python3

import f90nml
from nimpy.plot_nimrod import PlotNimrod as pn
from nimpy.eval_nimrod import EvalNimrod
from nimpy.field_class import Scalar,Vector,Tensor

dumpfile = 'dumpgll.68000.h5'
nml = f90nml.read('nimrod.in')
gmt = nml['grid_input']['geom']
if gmt == 'tor':
    gmt=True
else:
    gmt=False
eval_nimrod = EvalNimrod(dumpfile, fieldlist='nvptbj') #, coord='xyz')

rzc = pn.grid_1d_gen([1.8,0.,0.],[2.,0.,0.],10)

#print(rzc[0])
#n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=2) # total
#print(n[0])
#
#pn.plot_scalar_line(rzc, n[0], flabel="my title")

grid = pn.grid_1d_gen([1.8, -0.035, 0], [2.15, 0.74, 0], 1000)
fval = eval_nimrod.eval_field('p', grid, dmode=2, eq=1)
p = Scalar(fval, grid, torgeom=gmt, dmode=2)
fval = eval_nimrod.eval_field('b', grid, dmode=1, eq=1)
B = Vector(fval, grid, torgeom=gmt, dmode=1)
fval = eval_nimrod.eval_field('j', grid, dmode=1, eq=1)
j = Vector(fval, grid, torgeom=gmt, dmode=1)
fval = eval_nimrod.eval_field('v', grid, dmode=1, eq=1)
v = Vector(fval, grid, torgeom=gmt, dmode=1)
fval = eval_nimrod.eval_field('n', grid, dmode=1, eq=1)
nd = Scalar(fval, grid, torgeom=gmt, dmode=1, nqty=1)
# computed quantities
jxb=j.cross(B)
grp=p.grad()
md=3.3435860e-27
mc=1.9944235e-26
rhovdgrdv = (md*nd)*v.dot(grad(v))
force=jxb-grp
pn.plot_vector_line(grid, force.data, flabel=r'Force', f2=jxb.data,
                    f2label=r'\mathbf{J}\times\mathbf{B}',
                    f3=grp.data, f3label=r'\nabla p',
                    f4=rhovdgrdv.data, f4label=r'v\cdot \nabla v',
                    comp='perp', style='varied',
                    legend_loc='lower left', ylabel=r'Force density N/m^3')

