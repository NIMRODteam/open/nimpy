import numpy as np
from nimpy.plot_nimrod import PlotNimrod as pn
from scipy.integrate import ode
from scipy.integrate import quad
from scipy.interpolate import interp1d
import traceback
import warnings

msg = "comp_fsa is deprecated and will be removed. Use fsa instead."
warnings.warn(msg, category=FutureWarning)

# Example usage:
#
# import f90nml
# from eval_nimrod import *
# from fsa import *
#
# dumpfile = 'dumpgll.00000.h5'
# nml = f90nml.read('nimrod.in')
# gmt = nml['grid_input']['geom']
# dmf = EvalNimrod(dumpfile, fieldlist='nvptbj')
#
# def myfsa(rzc, dy, eval_nimrod, isurf):
#     '''
#     Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
#     Set neq to number of outputs in FSA call
#     and fill dy[4:4+neq]
#     '''
#     p = eval_nimrod.eval_field('p', rzc, dmode=0, eq=2)
#     dy[4] = p[0]*dy[2]
#     n = eval_nimrod.eval_field('n', rzc, dmode=0, eq=2)
#     dy[5] = n[0]*dy[2]
#     return dy
#
# dvar, yvars, contours = FSA(dmf, [1.7, -0.2, 0], myfsa, 2, nsurf=500, depvar='eta', dpow=0.5)
#


def get_b0(eval_nimrod,rzn,flag,abort=False):
    """
    Find b at a given point
    :param eval_nimrod: eval_nimrod class instance
    :param rzn: initial guess for poloidal field null
    :param flag: if 0 only use eq, if 1 add n=0 to eq
    :param abort: raise an exception if true and can't find b
    """
    b = eval_nimrod.eval_comp_field('b', rzn, dmode=0)
    b0=np.real(b[:,0])
    if flag==1:
      b0+=np.real(b[:,1])
    if (abort and np.isnan(b0).any()):
      print(b)
      raise Exception('CFSA_get_b0: Hit Wall')
    return b0

def find_pf_null(eval_nimrod, rzn, flag=0):
    """
    Find a poloidal field null
    :param eval_nimrod: eval_nimrod class instance
    :param rzn: initial guess for poloidal field null
    :param flag: if 0 only use eq, if 1 add n=0 to eq
    """
    rzn = np.array(rzn)
    maxsteps=1000
    it=0
    rtol=1.e-8
    drz0=0.125*rzn[0]
    while True:
        b = get_b0(eval_nimrod,rzn,flag,abort=False)
        norm0=np.sqrt(b[0]**2+b[1]**2)
        rvn=-rzn[0]*b[1]/norm0
        zvn= rzn[0]*b[0]/norm0
        drz=drz0*(1.0-float(it)/maxsteps)+rtol*rzn[0]
        while True:
            rr=rzn[0]+rvn*drz
            zz=rzn[1]+zvn*drz
            rzng=np.array([rr[0], zz[0], 0.0])
            b = get_b0(eval_nimrod,rzng,flag,abort=False)
            if not np.isnan(b).any():
                norm=np.sqrt(b[0]**2+b[1]**2)
                if (norm < norm0):
                    rzn[:]=rzng[:]
                    break
            rr=rzn[0]-rvn*drz
            zz=rzn[1]-zvn*drz
            rzng=np.array([rr[0], zz[0], 0.0])
            b = get_b0(eval_nimrod,rzng,flag,abort=False)
            if not np.isnan(b).any():
                norm=np.sqrt(b[0]**2+b[1]**2)
                if (norm < norm0):
                    rzn[:]=rzng[:]
                    break
            drz=drz/2.0
            if (drz/rzn[0] < rtol):
                return rzn # done
        it=it+1
        if it>=maxsteps:
            raise Exception('CFSA find_pf_null: No convergence')
            return

def int_polflux(delta, rzst, rz, norm, tang, eval_nimrod,flag=0):
    '''
    Integrand for poloidal flux.
    :param delta: distance from rzst
    :param rzst: starting rzp location
    :param rz: work-space for rzp
    :param norm: normal unit vector to line of integration in rzp
    :param tang: tangential unit vector to line of integration in rzp
    :param eval_nimrod: instance of eval_nimrod class
    :param flag: if 0 only use eq, if 1 add n=0 to eq
    :return: poloidal flux integrand
    '''
    rz = rzst+tang*delta
    b = get_b0(eval_nimrod,rz,flag,abort=True)
    return -2.0*np.pi*rz[0]*np.dot(b[0:3,0],norm)

def compute_polflux(rzo, rz_arr, eval_nimrod, flag):
    '''
    Compute the poloidal flux at rz_arr given rzo.
    :param rzo: o-point rz location
    :param rz_arr: return polflux at these points
    :param eval_nimrod: instance of eval_nimrod class
    :param flag: if 0 only trace B_eq, if 1 include n=0
    '''
    rzo = np.array(rzo)
    polflux = np.zeros(rz_arr.shape[1])
    rz = np.zeros(3)
    rz[:] = rzo
    delta = np.linalg.norm(rz_arr[:,-1]-rzo)
    tang = (rz_arr[:,-1]-rzo)/delta
    norm = np.cross(tang,np.array([0,0,1]))
    delta0 = 0.0
    rzst = rzo
    for ii in range(rz_arr.shape[1]):
        delta1=np.linalg.norm(rzst-rz_arr[:,ii])
        polflux[ii], err = quad(int_polflux, delta0, delta1,
                                args=(rzst, rz, norm, tang, eval_nimrod,flag))
        delta0 = delta1
    return np.cumsum(polflux)

def compute_torflux(polflux, q):
    '''
    Compute the toroidal flux from the poloidal and the safety factor
    '''
    polfluxnew = np.zeros(len(q)+1)
    qnew = np.zeros(len(q)+1)
    polfluxnew[1:] = polflux
    qnew[1:] = q
    qnew[0] = q[1]-polflux[0]*(q[2]-q[1])/(polflux[1]-polflux[0])
    qpsispl = interp1d(polfluxnew, qnew, kind='cubic')
    torflux = np.zeros(len(q))
    ii = 0
    for ipsi in range(len(q)):
        if ipsi == 0:
            pfst=0.0
        else:
            pfst=polflux[ipsi-1]
        torflux[ipsi], err = quad(qpsispl, pfst, polflux[ipsi])
    return np.cumsum(torflux)

def field_line_eta(eta, y, dy, rzc, eval_nimrod, rzo, odedfdt, offeta, flag=0,fargs={}):
    '''
    Integrates a field line with a independent variable of eta
    :param eta: independent variable
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param rzc: pre-allocated space for rzc (current rz) for optimization
    :param eval_nimrod: eval_nimrod class instance
    :param odedfdt: functor to equations for FSA
    :param rzo: o-point rz location
    :param offeta: off set eta for transform from eta, r to rz
    :param flag: if 0 only trace B_eq, if 1 include n=0
    :param fargs: additional arguments to send to integrand
    '''
    cosfac = np.cos(eta+offeta)
    sinfac = np.sin(eta+offeta)
    rzc[0] = rzo[0]+y[1]*cosfac
    rzc[1] = rzo[1]+y[1]*sinfac
    b = get_b0(eval_nimrod,rzc,flag,abort=True)
    if (b[1]*cosfac-b[0]*sinfac)==0:
        raise Exception('FSA_field_line_eta: Hit poloidal Null')
    dy[0] = y[1]/(b[1]*cosfac-b[0]*sinfac)
    dy[1] = dy[0]*(b[0]*cosfac+b[1]*sinfac)   # dr/deta
    dy[0] = np.sqrt(b[0]**2+b[1]**2)*dy[0] # dl/deta
    bdgth = (b[1]*cosfac-b[0]*sinfac)/y[1]
    dy[2] = 1.0/bdgth                         # 1/bdgth
    dy[3] = b[2]*dy[2]/rzc[0]                 # q
    return odedfdt(rzc, y, dy, eval_nimrod, flag, fargs)

def field_line_len(length, y, dy, rzc, eval_nimrod, rzo, odedfdt, offeta, flag=0,fargs={}):
    '''
    Integrates a field line with a independent variable of length
    :param eta: independent variable
    :param y: dependent variables (r, l, V', etc.)
    :param dy: pre-allocated space for dy for optimization
    :param rzc: pre-allocated space for rzc (current rz) for optimization
    :param eval_nimrod: eval_nimrod class instance
    :param odedfdt: functor to equations for FSA
    :param rzo: o-point rz location
    :param offeta: off set eta for transform from eta, r to rz
    :param flag: if 0 only trace B_eq, if 1 include n=0
    :param fargs: additional arguments to send to integrand
    '''
    cosfac = np.cos(y[0]+offeta)
    sinfac = np.sin(y[0]+offeta)
    rzc[0] = rzo[0]+y[1]*cosfac
    rzc[1] = rzo[1]+y[1]*sinfac
    b = get_b0(eval_nimrod,rzc,flag,abort=True)
    bp = np.sqrt(b[0]**2+b[1]**2)
    dy[0] = (-b[0]*sinfac+b[1]*cosfac)/(y[1]*bp) # deta/dl
    dy[1] = (b[0]*cosfac+b[1]*sinfac)/bp         # dr/dl
    bdgth = (rzc[0]*b[1]-rzc[1]*b[0])/y[1]**2
    dy[2] = 1.0/bdgth                         # 1/bdgth
    dy[3] = b[2]*dy[2]/rzc[0]                 # q
    return odedfdt(rzc, y, dy, eval_nimrod,flag,fargs)


def FSA(eval_nimrod, rzo, func, neq, nsurf=50, depvar='eta', plt=False, dpow=1.0, rzx=None, rzp=None, normalize=True,flag=0,fargs={}):
    """
    Perform flux-surface-average integration on func
    :param eval_nimrod: instance of eval nimrod class
    :param rzo: initial guess for o-point as array [r, z, 0]
    :param func: user defined function with signature func(rzc, dy, eval_nimrod)
                 where rzc is the current rzp location
                 dy is to be filled neq times
                 eval_nimrod is the instance of the EvalNimrod class
    :param neq: number of user defined equations
    :param nsurf: number of surfaces in the FSA
    :param depvar: set to 'eta' or 'len' to define field line ODE equations
    :param plt: plots contours if true
    :param dpow: set less than 1 to pack outboard and
                 greater than 1 to pack inboard
    :param rzx: guess for x-point to be used as endpoint of integration
                use this for diverted cases
    :param rzp: start point if nsurf=1
    :param normalize: normalize the fsa by
    :param flag: if 0 only trace B_eq, if 1 include n=0
    :param an additional dictonary of arguments to pass to integrand
    :return (dvar,yvals,contours): tuple of returned numpy arrays
                 dvar - dependent vars psin,rhon,psi,phi,R,Z,V',q
                 yvals - user equations of size neq
                 contours - contours of size (rz,:,nsurf)
    """
    # determine starting locations
    rzo = find_pf_null(eval_nimrod, rzo, flag)
    if nsurf>1:
        print('rzo=',rzo)
        if rzx is not None:
            # Find x-point
            try:
              rzx1 = find_pf_null(eval_nimrod, rzx, flag)
              print('rzx=',rzx1)
              rze=rzx1
            except:
              rze=rzx
        else:
            # find the wall on the outboard midplane
            fac=1.1
            rzwg1=rzo
            while True:
                rzwg2=np.array([rzo[0]*fac,rzo[1],0])
                b = get_b0(eval_nimrod,rzwg2,flag,abort=False)
                if np.isnan(b).any():
                    break
                rzwg1=rzwg2
                fac=fac*1.1
            while True:
                rzw=np.array([(rzwg1[0]+rzwg2[0])*0.5,rzo[1],0])
                b = get_b0(eval_nimrod,rzw,flag,abort=False)
                if np.isnan(b).any():
                    rzwg2=rzw
                else:
                    rzwg1=rzw
                if (np.fabs(rzwg2[0]-rzwg1[0])/rzw[0] < 1.e-8):
                    break
            print('rzw=',rzw)
            rze=rzw
        dwall = np.linalg.norm(rze-rzo)
        tang = (rze-rzo)/dwall
        delta = dwall/float(nsurf+1)
        rzo1 = np.array(rzo+tang*delta)
        rze1 = np.array(rze)
        start_points = pn.grid_1d_gen(rzo1, rze1, nsurf, dpow)
        # determine poloidal flux
        polflux = compute_polflux(rzo, start_points, eval_nimrod, flag)
        # to collect output
        dvar = np.zeros([8,nsurf]) # psin,rhon,psi,phi,R,Z,V',q
        dvar[2,:] = polflux/(2.0*np.pi)
        dvar[4,:] = start_points[0][:]
        dvar[5,:] = start_points[1][:]
    yvals = np.zeros([neq,nsurf])
    yvals[:,:] = np.nan
    # preallocate arrays used in integrand for optimization
    dy = np.zeros(4+neq)
    rzc = np.zeros(3)
    # loop over surfaces and integrate
    hit_wall=False
    rtol=1.e-10
    rtol=1.e-8
    nsteps_max=4e4
    if depvar == 'eta':
        lsode = ode(field_line_eta).set_integrator('lsoda', rtol=rtol, nsteps=nsteps_max)
        dt = np.pi/100.0
        contours = np.zeros([2,201,nsurf])
    else:
        lsode = ode(field_line_len).set_integrator('lsoda', rtol=rtol, nsteps=nsteps_max)
        dt = 0.05
        contours = np.zeros([2,1001,nsurf])
    if nsurf>1:
        for isurf in range(nsurf):
            contours[0,:,isurf] = start_points[0][isurf]
            contours[1,:,isurf] = start_points[1][isurf]
    else:
        if rzp is None:
            raise Exception('nsurf=1 but rzp not set')
        contours[0,:,0]=rzp[0]
        contours[1,:,0]=rzp[1]
    for isurf in range(nsurf):
        rzp = np.array([contours[0,0,isurf], contours[1,0,isurf], 0])
        eta0 = 0.0
        y20 = np.sqrt((rzp[0]-rzo[0])**2+(rzp[1]-rzo[1])**2)
        y0 = [0.0, y20, 0.0, 0.0]
        for ii in range(neq):
            y0.append(0.0)
        offeta = np.arctan2(rzp[1]-rzo[1],rzp[0]-rzo[0])
        lsode.set_initial_value(y0)
        ifargs=fargs
        ifargs['psi']=dvar[2,isurf]
        ifargs['isurf']=isurf
        lsode.set_f_params(dy, rzc, eval_nimrod, rzo, func, offeta, flag,ifargs)
        # collect contour locations during integration
        keep_running = True
        ii=1
        contours[:,0,isurf] = rzp[0:2]
        while keep_running:
            try:
                y = lsode.integrate(lsode.t+dt)
            except Exception:
                hit_wall=True
                traceback.print_exc()
                break
            if depvar == "eta":
                eta = lsode.t
            else:
                eta = lsode.y[0]
            keep_running = lsode.successful() and eta-2.0*np.pi < 1.e-5
            if keep_running and ii < contours.shape[1]:
                eta = lsode.t
                cosfac = np.cos(eta+offeta)
                sinfac = np.sin(eta+offeta)
                rzc = np.array([rzo[0]+lsode.y[1]*cosfac, rzo[1]+lsode.y[1]*sinfac, 0])
                #print(eta,rzc,ii)
                contours[:,ii,isurf] = rzc[0:2]
                ii = ii+1
        if hit_wall:
            cosfac = np.cos(eta+offeta)
            sinfac = np.sin(eta+offeta)
            rzc = np.array([rzo[0]+lsode.y[1]*cosfac, rzo[1]+lsode.y[1]*sinfac, 0])
            contours[0,ii:,isurf] = rzc[0]
            contours[1,ii:,isurf] = rzc[1]
            print("FSA: Hit wall, eta=", lsode.t, ", rz=", rzc[0], rzc[1])
            break
        if eta -2.0*np.pi < 1.e-5:
            print("FSA: too many steps in integration")
            break
        # normalize output
        if normalize:
          yvals[:,isurf] = lsode.y[4:4+neq]/lsode.y[2]
        else:
          yvals[:,isurf] = lsode.y[4:4+neq]
        if nsurf>1:
            dvar[6,isurf] = lsode.y[2] # V'
            dvar[7,isurf] = lsode.y[3]/(2.0*np.pi) # q
            print('isurf=',isurf+1,'RZ=',rzp[0:2],', q=',dvar[7,isurf])
    if nsurf==1:
        return yvals[:,0], contours[:,:,0]
    # compute toroidal flux
    print('integrated isurf=',isurf+1,' of nsurf=',nsurf)
    if hit_wall:
        dvar[6,isurf:] = dvar[6,isurf-1]
        dvar[7,isurf:] = dvar[7,isurf-1]
    dvar[3,:] = compute_torflux(polflux,dvar[7,:])
    # compute normalized fluxes
    dvar[0,:] = dvar[2,:]/dvar[2,-1]
    dvar[1,:] = np.sqrt(dvar[3,:]/dvar[3,-1])
    if plt:
        pl.plot(contours[0,:,:],contours[1,:,:])
    return (dvar,yvals,contours)
