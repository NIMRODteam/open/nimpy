import numpy as np


class TripClass:
    ''' Base class for storing and reading trip3D data '''
    probeRzFile = ""
    probeBFile = ""
    probeAFile = ""
    bData = np.zeros(1)
    aData = np.zeros(1)
    npts = 0
    nphi = 0
    rr = np.zeros(1)
    zz = np.zeros(1)
    phi = np.zeros(1)
    brReal = np.zeros(1)
    bzReal = np.zeros(1)
    btReal = np.zeros(1)
    brPhase = np.zeros(1, dtype=np.complex_)
    bzPhase = np.zeros(1, dtype=np.complex_)
    shift = 0
    complexConj = False

    def __init__(self, rzFile, aFile, bFile, shift=0, complexConj=True):
        ''' Initialize TripClass '''
        self.probeRzFile = rzFile
        self.probeBFile = bFile
        self.probeAFile = aFile
        self.shift = shift
        self.complexConj = complexConj

    def readBFile(self):
        ''' Read probeBFile as Save Data '''
        # b data: phi, r, z, B_phi, B_R, B_Z, B_p, B_mag, PsiN_pol
        self.bData = np.loadtxt(self.probeBFile, comments='%')

    def reorderBFile(self):
        ''' Reorder the bfile to account for a shift in the data'''
        if (self.shift == 0):
            return
        else:
            # reorder data due to indexing change in NIMDEVEL
            # not needed unless probe_g data was created before index shift
            tempData = self.bData
            for ip in range(self.nphi):
                start = ip * self.npts
                for ii in range(self.npts):
                    if(ii >= self.shift):
                        self.bData[ii - self.shift + start, :] = \
                            tempData[ii + start, :]
                    else:
                        self.bData[self.npts - self.shift + ii + start, :] = \
                            tempData[ii + 1 + start, :]

    def readAFile(self):
        ''' Read probeAFile as Save Data '''
        # data is stored as phi, r, z, A_phi, A_R, A_Z, A_p, A_mag, PsiN_pol
        self.aData = np.loadtxt(self.probeAFile, comments='%')

    def findNumPoints(self, data):
        phi0 = data[0, 0]
        for ii in range(data.shape[0]):
            if (data[ii, 0] != phi0):
                self.npts = ii
                break
        self.nphi = int(data.shape[0]/self.npts)

    def flipPhi(self):
        for ii, iphi in enumerate(self.phi):
            print(ii, iphi)
            self.phi[ii] = 360.0 - iphi
            if (self.phi[ii] == 360.0):
                self.phi[ii] = 0.0

    def processBFile(self):
        self.readBFile()
        self.findNumPoints(self.bData)
        self.reorderBFile()
        self.phi = np.zeros(self.nphi)
        self.brReal = np.zeros([self.npts, self.nphi])
        self.bzReal = np.zeros([self.npts, self.nphi])
        self.btReal = np.zeros([self.npts, self.nphi])
        self.rr = self.bData[:self.npts, 1]
        self.zz = self.bData[:self.npts, 2]
        for ii in range(self.nphi):
            start = ii * self.npts
            self.phi[ii] = self.bData[start, 0]
            self.brReal[:, ii] = self.bData[start:start + self.npts, 4]
            self.bzReal[:, ii] = self.bData[start:start + self.npts, 5]
            self.btReal[:, ii] = self.bData[start:start + self.npts, 3]
        # The following options were needed to test conversion between probe_g
        # physics space coordinates and nimrod fourier space
        # testing shows that complexConj=True and flipPhi = false correctly
        # converts between the different coordinates

        # i don't need to flip phi or change the sign of Bphi
        # self.flipPhi()
        # Testing shows complexConj=True is correct
        if self.complexConj:
            self.brPhase = np.fft.ifft(self.brReal, axis=1)
            self.bzPhase = np.fft.ifft(self.bzReal, axis=1)
            self.btPhase = np.fft.ifft(self.btReal, axis=1)
        else:
            self.brPhase = np.fft.fft(self.brReal, axis=1) / (float(self.nphi))
            self.bzPhase = np.fft.fft(self.bzReal, axis=1) / (float(self.nphi))
            self.btPhase = np.fft.fft(self.btReal, axis=1) / (float(self.nphi))

    def writeNimrodBext(self, path, fileName, ext):
        if (self.nphi % 2 == 0):  # even
            maxnphi = int(self.nphi/2)
        else:  # odd
            maxnphi = int((self.nphi + 1) / 2)
        for ii in range(maxnphi + 1):
            if ii == maxnphi:
                # todo check if the is correct for both even and odd maxnphi
                fac = 0.5
            else:
                fac = 1.0
            print(ii, maxnphi, fac)
            tempFileName = path + fileName + "{0:0=2d}".format(ii) + ext
            thisFile = open(tempFileName, 'w')
            for jj in range(self.brPhase.shape[0]):
                line = '{: 16.16e}'.format(fac * self.brPhase[jj, ii].real)
                line += ", "
                line += '{: 16.16e}'.format(fac * self.brPhase[jj, ii].imag)
                line += ", "
                line += '{: 16.16e}'.format(fac * self.bzPhase[jj, ii].real)
                line += ", "
                line += '{: 16.16e}'.format(fac * self.bzPhase[jj, ii].imag)
                line += ", "
                line += '{: 16.16e}'.format(fac * self.btPhase[jj, ii].real)
                line += ", "
                line += '{: 16.16e}'.format(fac * self.btPhase[jj, ii].imag)
                line += "\n"
                thisFile.write(line)
            thisFile.close()
