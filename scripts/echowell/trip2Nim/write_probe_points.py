import argparse
import numpy as np


def write_probeg_file(nimbdry: str, nphi: int) -> None:
    '''
    Write a PROBEG rz input file from nimrod node locations

    Inputs
    ------
    nimbdry : nimrod_bdry_rz.txt file
    nphi : number of phi planes

    Outputs
    -------
    None
    '''

    RZFILE = 'probe.points.rz.in'
    nodeRZ = np.loadtxt(nimbdry, delimiter=',', skiprows=1)
    rzpts = nodeRZ.shape[0]
    probeFile = open(RZFILE, 'w')
    probeFile.write("&startptsrz\n")
    probeFile.write("\n")
    probeFile.write("  nptsrz = " + str(rzpts) + "\n")
    probeFile.write("  phinrz = 0 \n")
    probeFile.write("  nphinrz = " + str(nphi) + "\n")
    probeFile.write("\n")
    probeFile.write("  ptsrz(1:2, 1:" + str(rzpts) + ") = \n")
    for ii in range(rzpts):
        thisStr = "   " + str(nodeRZ[ii, 0]) + "   " + str(nodeRZ[ii, 1])
        probeFile.write(thisStr + "\n")
    probeFile.write("\n")
    probeFile.write("&END")
    probeFile.close()


def parse_arguments() -> dict:
    '''
    Parse command line arguments

    Inputs
    ------
    None

    Outputs
    -------
    args dictionary
    '''

    msg = 'Write Probe G rz in file from NIMROD boundary node locations'
    parser = argparse.ArgumentParser(
                    prog='write probe points',
                    description=msg,
                    usage="%(prog)s [options]")
    parser.add_argument('--boundary', '-b', default='nimrod_bdry_rz.txt',
                        help="NIMROD boundary rz file")
    parser.add_argument('--nphi', '-n', default=256,
                        help="Number of phi planes for Probe G")
    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    write_probeg_file(args.boundary, args.nphi)


if __name__ == "__main__":
    main()
