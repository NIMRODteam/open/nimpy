#!/usr/bin/env python3
''' This module adds lagr edge rmp fields to the h5dump file


Inputs in nimpy yaml
---------------------
yaml_dir: add_rmp_dump
base_dump - name of reference h5 dumpfile
new_dump - write dumpfile name
rmp_dir - path to drirectory containing rmp field files
rmp_file_prefix - rmp data stored in text files named {prefix}##.{suffix}
rmp_file_suffix - see above
nnmodes - list of toroidal mode numbers to add RMP data too
nxbl - number of xblocks in dump
nybl - number of yblocks in dump
debug - Run in debug mode
test_dump - dumpfile to use as a reference for testing

Assumptions
-----------
Assumes dim nlist matches dump file
Does not modify lagr data

'''

import argparse
import shutil
import yaml
import h5py
import numpy as np


DEFAULT_INPUTS = {
    'base_dump': 'dumpgll.00000.h5',
    'new_dump': 'dumpgll_rmp.00000.h5',
    'rmp_dir': './',
    'rmp_file_prefix': 'brmpn',
    'rmp_file_suffix': 'dat',
    'nmodes': [1, ],
    'nxbl': 1,
    'nybl': 1,
    'debug': False,
    'test_dump': 'dumpref.h5'
}


def block_str(id):
    ''' Generate a block id string

        Inputs
        ------
        id : block id number

        Outputs
        -------
        blkstr : block id as a string

    '''
    block_str = str(id).zfill(4)
    return block_str


def compare_rzedge(rblocks, inputs):
    ''' Compare data in rzedge with nimrod_bdry data
        Used to check the ordering of the data when debugging
        requires nimrod_brdy_rz.txt file in dir

        Inputs
        -------
        rblocks: hdf5 rblocks Group
        inputs: inputs dictionary
    '''
    rz_bdry = np.loadtxt('nimrod_bdry_rz.txt',
                         delimiter=',',
                         skiprows=1)
    nxbl = inputs['nxbl']
    nybl = inputs['nybl']
    bl_start = nxbl * (nybl - 1) + 1
    bl_end = nxbl * nybl
    bl_name = block_str(bl_start)
    rz_name = 'rzedge' + bl_name
    bl_rz = rblocks[bl_name][rz_name]
    rz_start = 0
    print('Testing: Computing difference in edge node location')
    print('block id, norm(rz_error)')
    for bl in range(bl_start, bl_end+1):
        bl_name = block_str(bl)
        rz_name = 'rzedge'+bl_name
        mxe = rblocks[bl_name].attrs['mxe'][0]
        poly_degree = rblocks[bl_name].attrs['poly_degree'][0]
        nv = mxe * poly_degree + 1
        this_rz_bdry = rz_bdry[rz_start:rz_start+nv, :]
        bl_rz = np.array(rblocks[bl_name][rz_name])
        rz_start += nv - 1
        diff = this_rz_bdry - bl_rz
        print(bl, ',', np.linalg.norm(diff))


def compare_brmp(new_dump, inputs):
    ''' Compare data in brmp with a data in a reference dumpfile.
        Used to check the ordering of the data when debugging

        Inputs
        -------
        newdump: hdf5 dumpfile
        inputs: inputs dictionary
    '''

    nxbl = inputs['nxbl']
    nybl = inputs['nybl']
    bl_start = nxbl*(nybl-1)+1
    bl_end = nxbl * nybl
    print(f'Begin test of brmp data in {new_dump}.')
    print(f"Use dumpfile {inputs['test_dump']} as a reference")
    with h5py.File(new_dump, 'r') as h1:
        with h5py.File(inputs['test_dump'], 'r') as h2:
            print('Computing difference in brmp')
            print('block id, norm(re_rmp_error), norm(im_rmp_error)')
            for bl in range(bl_start, bl_end+1):
                bl_name = block_str(bl)
                rermp_name = 'rebrmp' + bl_name
                imrmp_name = 'imbrmp' + bl_name
                re_rmp1 = np.array(h1['rblocks'][bl_name][rermp_name])
                re_rmp2 = np.array(h2['rblocks'][bl_name][rermp_name])
                im_rmp1 = np.array(h1['rblocks'][bl_name][imrmp_name])
                im_rmp2 = np.array(h2['rblocks'][bl_name][imrmp_name])
                re_diff = re_rmp1 - re_rmp2
                im_diff = im_rmp1 - im_rmp2
                re_norm = np.linalg.norm(re_diff)
                im_norm = np.linalg.norm(im_diff)
                print(f'{bl}, {re_norm} , {im_norm}')


def add_rmp_dump(args={}):
    ''' Main method for adding a rmp data to a dumpfile

    Inputs
    --------
    args: dict of input arguments

    Outputs
    --------
    None

    '''
    # Read inputs from file, and set default values
    try:
        with open(args['file'], 'r') as file:
            nimpy_in = yaml.safe_load(file)
    except FileNotFoundError:
        print(f"Could not find file {args['file']}, using default inputs")
        nimpy_in = {}
    inputs = DEFAULT_INPUTS
    inputs.update(nimpy_in.get('add_rmp_dump', {}))
    # Copy dumpfile, this overwrites a pre_exsiting new dump
    new_dump = shutil.copy(inputs['base_dump'], inputs['new_dump'])
    thisn = float(inputs['nmodes'][0])

    with h5py.File(new_dump, 'r+') as hf:
        # update keff in the dumpfile to reflect new n
        hf['keff'][0] = thisn
        rblocks = hf['rblocks']
        if inputs['debug']:
            compare_rzedge(rblocks, inputs)
        rmp_file = inputs['rmp_file_prefix']
        rmp_file += str(inputs['nmodes'][0]).zfill(2)
        rmp_file += '.' + inputs['rmp_file_suffix']
        print(f'Reading RMP file {rmp_file}')
        rebr, imbr, rebz, imbz, rebt, imbt = np.loadtxt(rmp_file,
                                                        delimiter=',',
                                                        unpack=True)
        re_rmp = np.vstack((rebr, rebz, rebt)).T
        im_rmp = np.vstack((imbr, imbz, imbt)).T
        print('Applying RMP data to dumpfile')
        nxbl = inputs['nxbl']
        nybl = inputs['nybl']
        bl_start = nxbl*(nybl-1)+1
        bl_end = nxbl * nybl
        rmp_start = 0
        for bl in range(bl_start, bl_end+1):
            bl_name = block_str(bl)
            rermp_name = 'rebrmp' + bl_name
            imrmp_name = 'imbrmp' + bl_name
            mxe = rblocks[bl_name].attrs['mxe'][0]
            poly_degree = rblocks[bl_name].attrs['poly_degree'][0]
            nv = mxe * poly_degree + 1
            print(f'Adding rmp to block {bl_name}')
            rbl = rblocks[bl_name]
            rbl[rermp_name][:, :] = re_rmp[rmp_start:rmp_start+nv, :]
            rbl[imrmp_name][:, :] = im_rmp[rmp_start:rmp_start+nv, :]
            rmp_start += nv - 1

    if inputs['debug']:
        compare_brmp(new_dump, inputs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Flux decomp runner.')
    parser.add_argument('--file', default='nimpy.yaml', help='yaml input file')
    args = vars(parser.parse_args())
    add_rmp_dump(args=args)
