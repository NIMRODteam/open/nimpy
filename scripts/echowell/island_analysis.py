#!/usr/bin/env python3
#from plot_nimrod import PlotNimrod as pn
#import f90nml
from eval_comp_nimrod import *
from field_class import *
import comp_fsa as cfsa
import matplotlib.pyplot as pl
from scipy.interpolate import interp1d,splev,UnivariateSpline
import os
import h5py
import sys
import numpy as np

def basefsa(rzc, dy, eval_nimrod, isurf):
    '''
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    Set neq to number of outputs in FSA call 
    and fill dy[4:4+neq]
    dy(0)=dl/deta or d eta/dl
    dy(1)=dr/deta or dr/dl
    dy(2)=1/bdgth
    dy(3)=dq
    '''
    v = eval_nimrod.eval_field('v',rzc,dmode=0,) 
    b = eval_nimrod.eval_field('b', rzc, dmode=0)

    veq=np.real(v[:,0])
    beq=np.real(b[:,0])
    v0=np.real(v[:,0]+v[:,1])
    b0=np.real(b[:,0]+b[:,1])

    dy[4] = veq[2]*dy[2]/rzc[0] 
    dy[5] = v0[2]*dy[2]/rzc[0]

    modBp = np.sqrt(beq[0]*beq[0]+beq[1]*beq[1])
    vpeq = (veq[0]*beq[0]+veq[1]*beq[1])/modBp
    vp0 = (v0[0]*beq[0]+v0[1]*beq[1])/modBp
    smallNum=1.0e-6
    if np.fabs(vpeq) < smallNum:
      vpeq=smallNum
    if np.fabs(vp0) < smallNum:
      print("FSA small num")
      print(vp0)
      vp0=smallNum

    dy[6] = dy[2]/vpeq
    dy[7] = dy[2]/vp0
    bigr[isurf] = max(bigr[isurf], rzc[0])
    return dy

def cross_field(field1,field2,n0_only=True):
  '''
  evaluate the cross product of two fields
  if n0_only, then only calculate n0 cross product
  '''
  if field1.shape[0]!=3 or field2.shape[0]!=3:
    raise Exception("Cross field: input fields must be three vectors")
  conj1=np.conj(field1)
  conj2=np.conj(field2)
  if n0_only==True:
    result = np.zeros([3,2],dtype=np.complex64)
  else:
    raise Exception("Cross field: todo n0_only=False not implamented")
# calculate equilbirium cross product
  result[0,0] = field1[1,0,0]*field2[2,0,0]-field1[2,0,0]*field2[1,0,0]
  result[1,0] = field1[2,0,0]*field2[0,0,0]-field1[0,0,0]*field2[2,0,0]
  result[2,0] = field1[0,0,0]*field2[1,0,0]-field1[1,0,0]*field2[0,0,0]
# calculate perturbed n0 cross product
# do i need to divide n0 component by 2?
  result[0,1] = field1[1,1,0]*field2[2,1,0]-field1[2,1,0]*field2[1,1,0]
  result[1,1] = field1[2,1,0]*field2[0,1,0]-field1[0,1,0]*field2[2,1,0]
  result[2,1] = field1[0,1,0]*field2[1,1,0]-field1[1,1,0]*field2[0,1,0]
# the other n0 componets are due to B_n * J_-n products
  for ii in range(2,field1.shape[1]):
    result[0,1] += field1[1,ii,0]*conj2[2,ii,0]-field1[2,ii,0]*conj2[1,ii,0]
    result[1,1] += field1[2,ii,0]*conj2[0,ii,0]-field1[0,ii,0]*conj2[2,ii,0]
    result[2,1] += field1[0,ii,0]*conj2[1,ii,0]-field1[1,ii,0]*conj2[0,ii,0]
    result[0,1] += conj1[1,ii,0]*field2[2,ii,0]-conj1[2,ii,0]*field2[1,ii,0]
    result[1,1] += conj1[2,ii,0]*field2[0,ii,0]-conj1[0,ii,0]*field2[2,ii,0]
    result[2,1] += conj1[0,ii,0]*field2[1,ii,0]-conj1[1,ii,0]*field2[0,ii,0]
  if n0_only==True:
    return np.real(result)
  raise Exception("Cross field: todo n0_only=False not implamented")

#Do fas
def forcefsa(rzc,dy,eval_comp_nimrod,isuf):
  '''
  Flux surface average quantities (f/bdgth where y[2]=1/bdgrth)
  Set neq to the number of outbuts in FSA call
  and fill dy[4:4+neq]
  '''
  b = eval_comp_nimrod.eval('b',rzc,dmode=1)
  flag=0 #if one add eq to b0
  b0 =np.real(b[:,0])
  if flag==1:
    b0+=real(b[:,0])
  bp = b0[0:1,0]
  
# collect output here
rhon = np.arange(.5,1.0,0.004)
yrhon = []

# load dump files
step_number_list = ['00000','22000','50000','68000']
#step_number_list = ['22000']
time_list = []
for step_number in step_number_list:
    dumpfile = '/home/research/ehowell/test_fsa/dumpgll.'+step_number+'.h5'
    print(dumpfile)
    infile = h5py.File(dumpfile,'r')
    time_list.append(infile["/dumpTime"].attrs.get("vsTime")*1.e6) # in micro-s
    nml = f90nml.read('/home/research/ehowell/test_Fsa/nimrod.in')
    gmt = nml['grid_input']['geom']
    if gmt == 'tor':
        gmt=True
    else:
        gmt=False
    eval_comp_nimrod = EvalCompNimrod(dumpfile, fieldlist='nvptbj')
  



    md=3.3435860e-27
    zc=6
    mc=1.9944235e-26
    echrg=1.609e-19
    kboltz=echrg
    kb=1.609e-19
    eps0=8.85418782e-12
    rzo=[1.5, 0, 0]
    opt=np.array([1.5, 0.12,0])
    xpt=np.array([1.29, -1.16,0])
  #  field=eval_nimrod.eval_field('b',(test_point),dmode=1)


    nsurf = 150 # FSA surfaces
    bigr = numpy.zeros([nsurf])
    bigr[:] = -numpy.inf # max
    
  
    fsafilename = 'cfsa'+step_number+'.npz'
    if os.path.exists(fsafilename):
        fsaDict = numpy.load(fsafilename)
        dvar = fsaDict['arr_0']
        yvars = fsaDict['arr_1']
        Gcontours = fsaDict['arr_2']
        bigr = fsaDict['arr_3']
    else:
        dvar, yvars, contours = cfsa.FSA(eval_comp_nimrod,opt,basefsa,4,rzx=xpt,normalize=False)
        
        #normalize
        yvars[0,:]=yvars[0,:]/dvar[6,:]
        yvars[1,:]=yvars[1,:]/dvar[6,:]
        yvars[2,:]=2.0*numpy.pi/yvars[2,:]
        yvars[3,:]=2.0*numpy.pi/yvars[3,:]

        print (yvars.shape)
        fsaArr = [dvar, yvars, contours, bigr]
        numpy.savez(fsafilename,*fsaArr)
    iend=-1
    while numpy.isnan(yvars[:,iend]).any():
        iend -= 1
    iend += yvars.shape[1]+1


    print(numpy.min(dvar[1,:iend]))
    print(numpy.max(dvar[1,:iend]))
    print(iend)
    print(dvar[4,iend])
    yrhon.append(interp1d(dvar[1,:iend], yvars[:,:iend], kind='cubic')(rhon))

ii=0
uc=1.e-3
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\Omega$ (kHz)',legend_loc='upper right')

ii=1
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$K_{pol}$ (mT/s)',legend_loc='upper center')

ii=2
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$n_{e0} (m^{-3})$',legend_loc='upper right')

ii=3
uc=1.
pn.plot_scalar_line(None, uc*yrhon[0][ii], flabel=f"{time_list[0]:.4f}",
                    f2=uc*yrhon[1][ii], f2label=f"{time_list[1]:.4f}",
                    f3=uc*yrhon[2][ii], f3label=f"{time_list[2]:.4f}",
                    f4=uc*yrhon[3][ii], f4label=f"{time_list[3]:.4f}",
                    xvar=rhon, xlabel=r'$\rho_N$', ylabel=r'$\tilde{n} (m^{-3})$',legend_loc='upper left')