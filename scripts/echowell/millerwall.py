#!/usr/bin/env python3
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import argparse
from scipy.fft import rfft, irfft

def miller(r, theta, r0, kappa, delta, xval = None):
    """ Return R,Z locations for a miller equilibria where
        R = R0 + r cos( theta + sin^-1 (delta) sin theta )
        Z = kappa r sin theta

        inputs:
        r: minor radius coordinate
        theta: geometric polar angle
        r0: major radius
        kappa: elongation
        delta: triangularity
        xval: if set use xval instead of arcsin(delta)

        outputs:
        R: radius in cylinderical coordinates
        Z: height in cylinderical coordinates
    """
    if xval is None:
        xval = np.arcsin(delta)
    R = r0 + r * np.cos(theta + xval * np.sin(theta))
    Z = r * kappa * np.sin(theta)
    return R, Z

def main(r0, a, kappa, delta, npts=250, xval=None, filename=None):
    """ 

    """
    if filename is None:
        BASE_FILE= "miller_boundary."
        FILE_EXT = ".txt"
        now = datetime.now().strftime("%y%m%d%H%M")
        filename = BASE_FILE + now + FILE_EXT
    print(filename)
    #try:
    with open(filename,'w') as file:
        file.write(f"{args['n']}\n")
        for theta in np.linspace(0, 2 * np.pi, npts-1, endpoint=False):
            rr, zz = miller(a, theta, r0, kappa, delta, xval=xval)
            file.write(f"{rr}, {zz}\n")
        rr, zz = miller(a, 0., r0, kappa, delta, xval=xval)
        file.write(f"{rr}, {zz}\n")

#  except IOError as e:
#      print(e)
#  except:
#      print("Unknown error: unable to write boundary file")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate a nimrod wall using Miller's formula")
    parser.add_argument('-r',type=float, default=1., help="wall major radius")
    parser.add_argument('-a',type=float, default=0.33, help="wall minor radius")
    parser.add_argument('-k',type=float, default=1., help="wall elongation")
    parser.add_argument('-d',type=float, default=0., help="wall triangularity")
    parser.add_argument('-x',type=float, default=None,
                        help="alternative wall triangularity")
    parser.add_argument('-n', type=int, default=250, help="number of points")
    parser.add_argument('-f', default=None, help="file name")
    args = vars(parser.parse_args())
    print(args)
    main(r0=args['r'], a=args['a'], kappa=args['k'], delta=args['d'],
         npts=args['n'], xval=args['x'], filename=args['f'])