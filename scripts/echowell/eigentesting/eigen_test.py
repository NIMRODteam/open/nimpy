'''
Module for running plane wave test in NIMROD
The steps for running the module are
    1) Read namelist input
    2) Set up a rectangular mesh by calling rectset
    3) Set equilibrium fields
    4) Compute eigenvector/eigenvalues
    5) Set perturbed fields
    6) Run simulation and compare with eigenvalue
'''

import argparse
import subprocess
import numpy as np
import init_fields


def read_nimsetin(nimsetin: str = None):
    '''
    Read nimset input file and set the namelist

    TODO
    '''
    pass


def main(nimsetin: str = None, rectset: str = None):
    '''
    Perform a plane wave analysis in nimrod
    '''
    __DUMPFILE = "dump.00000.h5"

    # 1) Read namelist input
    #  Read nimset input file and set grid quantities
    # TODO
    #
    # Currently the grid quantities are hardcoded
    # nimset_nml = read_nimsetin(nimsetin)

    mx = str(32)
    my = str(84)
    phik = 1. / 3.
    kx = np.cos(np.pi * phik)
    ky = np.sin(np.pi * phik)
    nx = 2
    ny = 3
    xmin = 3.2
    ymin = 8.2
    if np.abs(kx) < 1.0E-16:
        lx = 1.0
    else:
        lx = 2. * np.pi * nx / kx
    if np.abs(ky) < 1.0E-16:
        ly = 1.0
    else:
        ly = 2. * np.pi * ny / ky
    ymax = ymin + ly
    xmax = xmin + lx

    # 2) Set up a rectangular mesh by calling rectset
    # cmd is an array that represent the shell cmd
    # the first argument is the path to executable
    # command line arguments are then added as pairs

    cmd = [rectset]
    cmd.extend([_ for _ in ["-mx", mx]])
    cmd.extend([_ for _ in ["-my", my]])
    cmd.extend([_ for _ in ["-xmin", str(xmin)]])
    cmd.extend([_ for _ in ["-ymin", str(ymin)]])
    cmd.extend([_ for _ in ["-ymin", str(xmax)]])
    cmd.extend([_ for _ in ["-ymax", str(ymax)]])

    try:
        returncode = subprocess.run(cmd)
        print(returncode)
    except OSError as e:
        print("Execution failed:", e)

    # 3) Set equilibrium fields
    # The design reflects design where the equilibrium
    # and perturbed fields are set as separate steps
    # in the setup of a nimrod run.

    init_fields.set_eqfields(infilename=__DUMPFILE)
    # 4) Compute eigenvector/eigenvalues
    # 

    # 5) Set perturbed fields
    # 6) Run simulation and compare with eigenvalue


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='First stab at setting up a dump file for plane wave test')
    parser.add_argument('-i', '--nimsetin', default=None,
                        help='NIMSET input file %TODO')
    parser.add_argument('-r', '--rectset', default=None,
                        help='path to rectset executable')

    args = parser.parse_args()

    main(nimsetin=args.nimsetin, rectset=args.rectset)
