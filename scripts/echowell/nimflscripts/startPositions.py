import numpy as np
import argparse
import random
import shapely.geometry as geo

SEED = 12456

def writeStart(outfile,wall,nptsPlane,nPlane):
  """ Write a start position file """
  npts = nptsPlane * nPlane
  rmin, zmin, rmax, zmax = wall.bounds
  print(rmin,zmin,rmax,zmax)
  try:
    with open(outfile,'w') as file:
      file.write(str(npts)+'\n')
      for phi in np.linspace(0,2*np.pi,nPlane,endpoint=False):
          for ir in range(nptsPlane):
            inwall = False
            while not inwall:
              rr = random.uniform(rmin, rmax)
              zz = random.uniform(zmin, zmax)
              pp = geo.Point(rr, zz)
              if wall.contains(pp):
                print(rr,'\b,', zz, '\b,', phi)
                file.write(str(rr) + ', ' + str(zz)+ ', ' + str(phi) + '\n')
                inwall = True
  except IOerror as e:
    print(e)
  except:
    print("Unknown error: unable to read wall file")

def readWallFile(wallfile):
  """ Read the wall file at store the rz coordinates """
  coords = []
  try:
    with open(wallfile,'r') as file:
      pts = int(file.readline())
      for i in range(pts):
        line = file.readline().split(',')
        r = float(line[0])
        z = float(line[1])
        coords.append((r,z))
  except IOerror as e:
    print(e)
  except:
    print("Unknown error: unable to read wall file")
  return coords


def main(wallfile,nptsPlane=100,nPlane=1,outfile="start_positions.dat"):
  """ Main function for seeding start positions inside a given wall
      Inputs
      ------
      wallfile: name of nimrod compatable wall file
      nptsPlane: number of seed points per toroidal plane
      nPlane: number of toroidal planes
      outfile: name of file to write rzp locations (default start_positions)
  """
  random.seed(SEED)
  coords = readWallFile(wallfile)
  wall = geo.Polygon(coords)
  writeStart(outfile,wall,nptsPlane,nPlane)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    "Create NIMFL start_positions.dat file"
    )
  parser.add_argument(
    'file',
    action='store',
    help="Wall file",
)
  parser.add_argument(
    '-n',
    action='store',
    default=100,
    help='Number of seed points per plane',
    type=int,
    )
  parser.add_argument(
    '-p',
    action='store',
    default=1,
    help='Number of phi planes',
    type=int,
    )
  parser.add_argument(
    '-o',
    action='store',
    default='start_positions.dat',
    help='Name of start position file',
    )
  args = parser.parse_args()

  main(args.file,nptsPlane=args.n,nPlane=args.p,outfile=args.o)
