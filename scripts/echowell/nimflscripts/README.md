# nimflscripts
This directory contains scripts for setting up nimfl inputs and plotting
the output

## drawpss.py
  - Plots poincare plots

### Inputs:
  - nimfl.dat file

### Outputs:
  - Poincare plot in RZ and rho-theta plane

### Key Steps:
  - TODO

### Status: 
  - Needs to be cleaned up

### Todo:
  - [ ] Clean up
  - [ ] Make todo list

## flux_map.py
  - Creates the mapping psi(R,Z) and Theta(R,Z)

### Inputs:
  - dumpfile
  - nimrod.in file

### Outputs:
  - TODO

### Key Steps:
  - TODO

### Status: 
  - Needs to be cleaned up

### Todo:
  - [ ] Clean up
  - [ ] Add ability to read and write mapping to file

## startPositions.py
  - Create random seed points for NIMFL fieldline integration within a domain

### Inputs:
  - NIMROD compatiable wall file

### Outputs
  - startpositions.dat file

### Key Steps:
  - Read wall file to create domain boundary
  - Randomly sample (R,Z) and check if they are in domain
  - Keep point if in domain
  - Write startpositions file

### Status: 
  - Works for 1 phi plane
  - Statue for multiple phi planes is untested

### Todo:
  - [ ] Test multiple phi planes
  - [ ] Create yaml input
  - [ ] Make several defaul walls

## NIMFL general todo
  - [ ] Make a general wall class (not sure where this belongs)
  - [ ] Move flux_map (it could be useful elsewhere)
  - [ ] Add tests