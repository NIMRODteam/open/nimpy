#!/usr/bin/env python3
import ctypes.util # Fixes odd bug with python 3.6
import ctypes
import numpy
import sys
from plot_nimrod import PlotNimrod as pn
import f90nml
import os
#from numba import jit,jitclass,float32,boolean

# load shared libraries
# ctypes.util.find_library does not work on mac, use nimsoft directory
if (sys.platform=='darwin'):
  nimlibdir=os.environ['NIMSOFT']+'/nimdevel/lib/'
  nimlib_shared=nimlibdir+"libnimlib.dylib"
  nimeval=nimlibdir+"libnimeval.dylib"
  try:
    nimlibobj = ctypes.CDLL(nimlib_shared)
  except OSError:
    print("Library nimlib_shared not found, set NIMSOFT")
    raise 
  try:
    evalobj = ctypes.CDLL(nimeval)
  except OSError:
    print("Library nimeval not found, set NIMSOFT")
    raise
else:
  nimlib_shared = ctypes.util.find_library("nimlib")
  if (nimlib_shared == None):
      raise Exception('Library nimlib_shared not found, set LD_LIBRARY_PATH.')
      raise Exception('Library nimeval not found, set LD_LIBRARY_PATH.')
  nimlibobj = ctypes.CDLL(nimlib_shared)
  nimeval = ctypes.util.find_library("nimeval")
  if (nimeval == None):
      raise Exception('Library nimeval not found, set LD_LIBRARY_PATH.')
  evalobj = ctypes.CDLL(nimeval)


#spec = [('c_xyguess',float32[:]), ('__transform',boolean),
#        ('ndnq',int32)]

#@jitclass(spec)
class EvalCompNimrod:
    '''
    Read a NIMROD dump file and returs the complex fields at RZ locations.
    '''

    def __init__(self, dumpfile, fieldlist="bvnpf", coord='rzp',path="./"):
        '''
          initialize file, requires nimrod.in to be present
          only one file can be opened at a time because of the shared
          library nature of this interface. Opening a second file
          will automatically close the first.
        '''

        # initialize libraries
        cstr_dumpfile = ctypes.create_string_buffer(dumpfile.encode('utf-8'))
        cstr_fieldlist = ctypes.create_string_buffer(fieldlist.encode('utf-8'))
        evalobj.c_nimfield_init(ctypes.byref(cstr_dumpfile),
                                ctypes.byref(cstr_fieldlist))
        #nimlibobj.c_set_degas_tables()

        # intialize stored guess (array of size 2)
        self.c_xyguess = (ctypes.c_double * 2)(-1., -1.)

        # read and store namelist variables
        in_file = path +'nimrod.in'
        try:
          nml = f90nml.read(in_file)
        except:
          raise IOerror("Can't open nimrod.in")
        try:
          self.ndnq = nml['closure_input']['nisp']+1
        except:
          #self.ndnq = 2 # elec + ion
          self.ndnq = 1 # elec + ion
        try:
          self.nnsp = nml['equil_input']['nnsp']
        except:
          self.nnsp = 0

        #determine nmodes_total
        try:
          self.nonlinear = nml['physics_input']['nonlinear']
        except:
          self.nonlinear = True
        if self.nonlinear: #nonlinear is true
          try:
            self.dealiase = nml['grid_input']['dealiase']
          except:
            self.dealiase =3
          if self.dealiase !=3:
            raise Exception("eval comp nimrod requires dealiase =3")
          try:
            self.lphi = nml['grid_input']['lphi']
          except:
            self.lphi=2
          self.nmodes = 2**self.lphi//3 + 1
          self.nmodes_peq = self.nmodes +1
          self.modelist = [i for i in range(-1,self.nmodes)] #-1 for eq 
        else: #linear
          print("Warning: Linear mode setup is not tested")
          try:
            self.lin_nmodes=nml['grid_input'][lin_nmodes]
          except:
            self.lin_nmodes=1
          try:
            self.lin_nmax=nml['grid_input'][lin_nmax]
          except:
            self.lin_nmax=0
          if self.lin_nmax>0:
            self.modelist=[-1]
            for i in range(0,self.lin_nmodes):
              self.modelist.append(self.lin_nmax-(self.lin_nmodes-1)+i)
          else:
            self.modelist = [i for i in range(-1,self.lin_nmode)]
          self.nmodes_peq=self.lin_nmodes+1
        # set the coordinate system
        if coord is 'rzp':
            self.__transform = False
        elif coord is 'xyz':
            self.__transform = True
        else:
            raise Exception('Unsupported coordinate system.')

        return

    def __rzp_to_xyz(self, rz_coord):
        '''
        transforming rzp coordinate system to xyz coordinate system
        :param rzp_coord: coordinates of grid points in (R, Z, Phi)
        :return: coordinates of grid points in (X, Y, Z)
        '''

        # create xyz array with the same dimension as rz
        xyz_coord = numpy.zeros(numpy.shape(rzp_coord))

        # transform the coordinate system
        xyz_coord[0] = rzp_coord[0] * numpy.cos(rzp_coord[2])
        xyz_coord[1] = rzp_coord[0] * numpy.sin(rzp_coord[2])
        xyz_coord[2] = rzp_coord[1]

        return xyz_coord

    def __xyz_to_rzp(self, xyz_coord):
        '''
        transforming xyz coordinate system to rzp coordinate system
        :param xyz_coord: coordinates of grid points in (X, Y, Z)
        :return: coordinates of grid points in (R, Z, Phi)
        '''

        # create rzp array with the same dimension as xyz
        rzp_coord = numpy.zeros(numpy.shape(xyz_coord))

        # transform the coordinate system
        rzp_coord[0] = numpy.sqrt(xyz_coord[0] ** 2 + xyz_coord[1] ** 2)
        rzp_coord[1] = xyz_coord[2]
        rzp_coord[2] = numpy.arctan2(xyz_coord[1], xyz_coord[0])

        return rzp_coord
 
    def __eval_comp_field_on_point(self, c_xyguess, field, rzp, fsize, nqty, dmode):
        '''
        evaluate the specified field at a single point given by rz
        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of the point in R-Z-Phi coordinate
                    (array with shape(3))
        :return: value of the field at point rz
                 (nan if point is out of bound)
        '''
    
        # convert data to C
        rz=rzp[0:2] #This gives you rz[0] and rz[1]
        c_rz = (ctypes.c_double * 2)(*rz)
        cstr_fname = ctypes.create_string_buffer(field.encode('utf-8'))
        c_fval = (ctypes.c_double * fsize)(*numpy.zeros(fsize))
        c_nqty = (ctypes.c_int)(nqty)
        c_ifail = (ctypes.c_int)(0)
        c_dmode = (ctypes.c_int)(dmode)
    
        # call the eval function
        evalobj.c_eval_comp_field_dm(ctypes.byref(c_rz), ctypes.byref(cstr_fname), ctypes.byref(c_fval),ctypes.byref(c_nqty),ctypes.byref(c_xyguess),   ctypes.byref(c_ifail),ctypes.byref(c_dmode))
    
        # copy results to output
        res = numpy.array(c_fval[0:fsize])
        ifail = c_ifail.value
        if ifail != 0:
            res = numpy.nan*res

        nderivs=(fsize//(2*nqty*self.nmodes_peq))
        res2 = numpy.zeros([nqty,self.nmodes_peq,nderivs],dtype=numpy.complex64)

        for iqty in range(0,nqty):
          for imode in range(0,self.nmodes_peq):
            for ider in range (0,nderivs):
              cindex= 2*((iqty+1)+imode*nqty+ider*nqty*self.nmodes_peq)-2
              res2[iqty,imode,ider]=res[cindex]+1j*res[cindex+1]

        return res2


#    def __eval_field_on_line(self, field, rz, fsize, nqty, dmode, eq, nr):
        '''
        evaluate the specified field along a line given by rz
        :param field: desired field given by a single character, e.g. n b v j
        :param rz: coordinates of all the points on line in R-Z-Phi coordinate
                    (array with shape(3, nr))
        :return: value of the field along the line defined by rz
                 (nan if point is out of bound)
        '''

        # initialize the result with zeros
#        res = numpy.zeros([fsize, nr])

        # call the eval_field at each point
#        for index in range(nr):
#            res[:, index] = \
#                    self.__eval_field_on_point(self.c_xyguess, field, rz[:, index], 
#                                               fsize, nqty, dmode, eq)

#        return res

#    def __eval_field_on_2d_grid(self, field, rz, fsize, nqty, dmode, eq, nr, nz):
        '''
        evaluate the specified field on a 2d grid given by rz
        :param field: desired field given by a single character, e.g. n b v j
        :param rz: coordinates of all the points on grid in R-Z-Phi coordinate
                    (array with shape(3, nr, nz))
        :return: value of the field on the 2d grid defined by rz
                 (nan if point is out of bound)
        '''

        # initialize the result with zeros
#        res = numpy.zeros([fsize, nr, nz])

        # call each eval_field_on_line for each line
#        for index in range(nz):
#            res[:, :, index] = \
#                    self.__eval_field_on_line(field, rz[:, :, index], 
#                                              fsize, nqty, dmode, eq, nr)

#        return res


    def eval_field(self, field, rzp, dmode=0):
        '''
        main function to evaluate a field at rzp point(s)
        :param field: the specified field
        :param rzp: coordinate of the points in following format:
                    (3)                : single point
                    (3, nx1)           : line
                    (3, nx1, nx2)      : plane
        :return: array of field values (nqty,nmodes_peq,nder,rzp.shape)
        nmodes_peq (plus eq) is infeered from nimrod.in file
        nder depends on dmode
        '''

        # set number of components for each field
        nqty = 1
        if field == 'b' or field == 'v' or field == 'j' or \
           field == 'f' or field =='e':
            nqty = 3
        elif field == 'n':
            nqty = self.ndnq
        elif field == 'ndn' or field == 'tn':
            nqty = self.nnsp
        elif field == 'vn':
            nqty = 3*self.nnsp

        # set number of components for each field considering the 
        # derivatives and the order itself
        if dmode == 0:
            fsize = 2*nqty*self.nmodes_peq
            dmode = 0
        elif dmode == 1:
            fsize = 8*nqty*self.nmodes_peq
            dmode = 1
        elif dmode == 2:
            fsize = 20*nqty*self.nmodes_peq
            dmode = 2
        else:
            raise Exception('incorrect order of derivative.')

        # set the grid size
        if rzp.ndim>1:
          nr = rzp.shape[1]
        if rzp.ndim>2:
          nz = rzp.shape[2]
        if rzp.ndim>3:
          np = rzp.shape[3]

        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rz(rzp)

        # call eval_* functions according to dimension
        dimension = rzp.ndim - 1
        if dimension == 0:
            fval = self.__eval_comp_field_on_point(self.c_xyguess, field, rzp, fsize, nqty, dmode)
        else: #temp
          raise Exception('eval comp currently only supprots eval field on point')
#        elif dimension == 1:
#            fval = self.__eval_field_on_line(field, rzp, fsize, 
#                                             nqty, dmode, eq, nr)
#        elif dimension == 2:
#            fval = self.__eval_field_on_2d_grid(field, rzp, fsize, 
#                                                nqty, dmode, eq, nr, nz)
#        else:
#            fval = self.__eval_field_on_3d_grid(field, rzp, fsize, 
#                                                nqty, dmode, eq, nr, nz, np)

        return fval

    def eval_degas(self, ne, te):
        '''
        Evaluate the degas tables and expression for CX
        :param ne: electron density (input)
        :param te: electron temperature (input)
        :return (sion, srec): <sigma_ion v>, <sigma_rec v>
        '''
        c_ne = (ctypes.c_double)(ne)
        c_te = (ctypes.c_double)(te)
        c_sion = (ctypes.c_double)(0.0)
        c_srec = (ctypes.c_double)(0.0)

        nimlibobj.c_eval_degas_tables(ctypes.byref(c_ne), ctypes.byref(c_te),
                                      ctypes.byref(c_sion), ctypes.byref(c_srec))

        return c_sion.value, c_srec.value


def main():
    '''
    This is an example of usage
    '''
    try:
        coord = 'rzp'
        dumpfile = './164988/dumpgll.00000.h5'
        nimeval = EvalCompNimrod(dumpfile, fieldlist='nvbp', coord=coord)

        p1 = numpy.array([0.9, -1.5, 0.0])
        p2 = numpy.array([0.9, +1.5, 0.0])
        p3 = numpy.array([2.5, -1.5, 0.0])
#        rz = pn.grid_2d_gen(p1, p2, p3, 300, 300)

        field = nimeval.eval_field('n', p1, dmode=0 )

#        pn.plot_scalar_plane(rz, field)
    except Exception as err:
        print("Failure:", err)
        return

    return


if __name__ == "__main__":
    main()
