#!/usr/bin/env python3
#
#profiles is a class for calculating 1D profiles
# using the flux surface integration
#
#
import nimpy.eval_nimrod as eval
import nimpy.fsa as fsa
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from scipy.interpolate import interp1d,splev,UnivariateSpline
import h5py
import numpy as np
import pickle

class fsadecomp:
  def __init__(self,dumpfile,nimrodin):
    #start with dump file and nimrod.in info
    self.dumpfile=dumpfile
    self.nimrodin=nimrodin
    self.time=None
    self.step=None
    # next include info on how fsa's were performed
    self.mmax=None
    self.ifour=[]
    self.nfour=None
    self.setprofiles=False
    #finally end with the profiles
    self.psin=np.empty([1])
    self.psi=np.empty([1])
    self.rhon=np.empty([1])
    self.q=np.empty([1])
    self.qpsi=np.empty([1])
    self.vprime=np.empty([1])
    self.bmn=np.empty([1])
    self.bcmn=np.empty([1])
    self.bsmn=np.empty([1])
    self.calc_decomp = False
    self.s_decomp = np.empty([1])
    self.phasemn = None

  def dump(self,file):
    pickle.dump(self.dumpfile,file)
    pickle.dump(self.nimrodin,file)
    pickle.dump(self.time,file)
    pickle.dump(self.step,file)
    # next include info on how fsa's were performed
    pickle.dump(self.mmax,file)
    pickle.dump(self.ifour,file)
    pickle.dump(self.nfour,file)
    pickle.dump(self.setprofiles,file)
    #finally end with the profiles
    if self.setprofiles==True:
      pickle.dump(self.psin,file)
      pickle.dump(self.psi,file)
      pickle.dump(self.rhon,file)
      pickle.dump(self.q,file)
      pickle.dump(self.vprime,file)
      pickle.dump(self.bmn,file)
      pickle.dump(self.bcmn,file)
      pickle.dump(self.bsmn,file)
    pickle.dump(self.calc_decomp,file)
    if self.calc_decomp == True:
      pickle.dump(self.s_decomp,file)

  def load(self,file):
    self.dumpfile=pickle.load(file)
    self.nimrodin=pickle.load(file)
    self.time=pickle.load(file)
    self.step=pickle.load(file)
    # next include info on how fsa's were performed
    self.mmax=pickle.load(file)
    self.ifour=pickle.load(file)
    self.nfour=pickle.load(file)
    self.setprofiles=pickle.load(file)
    #finally end with the profiles
    if self.setprofiles==True:
      self.psin=pickle.load(file)
      self.psi=pickle.load(file)
      self.rhon=pickle.load(file)
      self.q=pickle.load(file)
      self.vprime=pickle.load(file)
      self.bmn=pickle.load(file)
      self.bcmn=pickle.load(file)
      self.bsmn=pickle.load(file)
    try:
      self.calc_decomp=pickle.load(file)
    except:
      self.calc_decomp=False
    if self.calc_decomp == True:
      self.s_decomp=pickle.load(file)

  def get_m_index(self,m):
    '''Return the index for the given m
      Return None if m is out of range'''
    if self.mmax==None:
      print("mmax has not be set in get_m_index")
      raise
    else:
      if m>self.mmax:
        return None
      elif m<-1*self.mmax:
        return None
      else:
        return m+self.mmax

  def get_n_index(self,n):
    '''Return the index for the given n'''
    nidx = []
    for idx, nval in enumerate(self.ifour):
        if nval == n:
            nidx.append(idx)
    if len(nidx)==1:
        return nidx[0]
    else:
       print(f"Fourier mode {n} is not in ifour")
       raise ValueError
    
  def dummy_fsa(self,rzc,y,dy,evalnimrod,fdict):
    '''
    Dummy integrand for complex fsa, this is used to get v' and q
    without running a true fsa
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    dy(0)=dl/deta or d eta/dl
    dy(1)=dr/deta or dr/dl
    dy(2)=1/bdgth :v'
    dy(3)=dq: q
    '''
    dy[4]=1.0
    return dy

  def decomp_int(self,rzc,y,dy,evalnimrod,fdict):
    '''
    Integrand for fluxsurface integration
    Flux surface averge quantities (f/bdgrth where y[2]=1/bdgrth)
    dy(0)=dl/deta or d eta/dl
    dy(1)=dr/deta or dr/dl
    dy(2)=1/bdgth
    dy(3)=dq
    dy(4)=dtheta
    '''
    addpert=fdict.get('addpert',False)
    b0 = np.array(fsa.get_b0(evalnimrod,rzc,addpert))
    b = evalnimrod.eval_comp_field('b', rzc, dmode=0)
    rr = rzc[0]
    q = self.qpsi(fdict.get('psi'))
    jac = rr * q / b0[2]
    dy[4] = dy[2] / jac #dtheta
    dy[5] = rr * np.sqrt(b0[0]**2 + b0[1]**2) * dy[2] #add decomp J
    for ii, im in enumerate(self.ifour):
      oset = ii * (4*self.mmax+1)
      ndx = evalnimrod.modelist.index(im)
      reb=np.real(b[:,ndx]) #im+1]) #im+1 is true for nonlinear only
      imb=np.imag(b[:,ndx]) #im+1]) #todo test nonlinear
      rBePsi=rr*(reb[1]*b0[0]-reb[0]*b0[1])
      iBePsi=rr*(imb[1]*b0[0]-imb[0]*b0[1])
      for im in range(self.mmax):
        nmth=-(self.mmax-im)*y[4] #negative m theta
        pmth=(im+1)*y[4] #positive m theta
        dy[6+im+oset]=(rBePsi*np.cos(nmth)-iBePsi*np.sin(nmth))*dy[2]
        dy[7+self.mmax+im+oset]=(rBePsi*np.cos(pmth)-iBePsi*np.sin(pmth))*dy[2]
        dy[7+2*self.mmax+im+oset]=-(rBePsi*np.sin(nmth)+iBePsi*np.cos(nmth))*dy[2]
        dy[7+3*self.mmax+im+oset]=-(rBePsi*np.sin(pmth)+iBePsi*np.cos(pmth))*dy[2]
      dy[6+self.mmax+oset]=rBePsi*dy[2]
    return dy


  def get_rho_q(self,q):
    try:
      return interp1d(self.q, self.rhon, kind='cubic',
                      fill_value="extrapolate")(q)
    except:
      print(f"The safety factor {q} is not in the domain")
      raise

  def get_psi_q(self,q):
    try:
      return interp1d(self.q, self.psi, kind='cubic',
                      fill_value="extrapolate")(q)
    except:
      print(f"The safety factor {q} is not in the domain")
      raise

  def get_field_rho(self, field, rhon):
    try:
      return interp1d(self.rhon, field, kind='cubic')(rhon)
    except:
      print(f"Problem evaluitng field at rhon={rhon}")
      raise

  def compute_phase(self):
    if self.phasemn is None:
      self.phasemn = np.arctan2(self.bsmn, self.bcmn)

  def calculate(self,fsa_in,**kwargs):
    rzo = fsa_in.get('rzo', [1.768,-0.018831,0.0])
    rzx = fsa_in.get('rzx', None)
    nsurf = fsa_in.get('nsurf', 150)
    eqflag = fsa_in.get('eqflag', 0)
    if eqflag == 1:
      addpert = True
    else:
      addpert = False
    nmax = fsa_in.get("nmax", 5)
    nmodes = fsa_in.get('nmodes', -1)

    if nmodes <1 or nmodes>nmax :
      self.ifour=list(range(1,nmax+1))
    else:
      start=nmax-nmodes+1
      self.ifour=list(range(start,nmax+1))
    
    self.mmax=fsa_in.get('mmax', 15)  
    self.nfour=len(self.ifour)
    self.setprofiles=True
    print(rzo)
    print(rzx)
    #first call to fsa is to calcualte q
    evalnimrod=eval.EvalNimrod(self.dumpfile,fieldlist='nvptbj')
    dvar, yvar, contours = fsa.FSA(evalnimrod, rzo, self.dummy_fsa, 1, \
      nsurf=nsurf,depvar='eta',dpow=0.5,rzx=rzx, addpert=addpert,
      normalize=True, fargs=fsa_in)
   
    iend=-1
    while np.isnan(yvar[:,iend]).any():
        iend -= 1
    iend += yvar.shape[1]+1
   
    #set up q interpolate
    self.qpsi=interp1d(dvar[2,:iend], dvar[7,:iend], kind='cubic',
                       fill_value="extrapolate")
  
  #second call to fsa is to calcualte b_ms ans psi_mn
    neq=2+self.nfour*(4*self.mmax+1)    
    dvar,yvar,contours = fsa.FSA(evalnimrod, rzo, self.decomp_int, neq, \
      nsurf=nsurf,depvar='eta', dpow=0.5,rzx=rzx,addpert=addpert,
      normalize=False,fargs=fsa_in)

    iend=-1
    while np.isnan(yvar[:,iend]).any():
        iend -= 1
    iend += yvar.shape[1]+1

    bmn=np.zeros([self.nfour,2*self.mmax+1,iend])
    bcmn=np.zeros([self.nfour,2*self.mmax+1,iend])
    bsmn=np.zeros([self.nfour,2*self.mmax+1,iend])
    for ii in range(self.nfour):
      oset = ii * (4*self.mmax+1)
      bcmn[ii,:,:]= yvar[2+oset:2*self.mmax+3+oset,:iend]*(np.pi*2.0)
      bsmn[ii,0:self.mmax,:]=\
        yvar[3+2*self.mmax+oset:3+3*self.mmax+oset,:iend]*(np.pi*2.0)
      bsmn[ii,self.mmax+1:2*self.mmax+1,:]=\
        yvar[3+3*self.mmax+oset:3+4*self.mmax+oset,:iend]*(np.pi*2.0)
    bmn=np.sqrt(np.square(bcmn)+np.square(bsmn))
    s_decomp = yvar[1,:iend]*np.pi*2

    rhomin=np.min(dvar[1,:iend])
    rhomax=np.max(dvar[1,:iend])
    self.rhon = np.linspace(rhomin,rhomax,200,endpoint=True)
    #dvars
    self.psin=interp1d(dvar[1,:iend], dvar[0,:iend], kind='cubic')(self.rhon)
    self.psi=interp1d(dvar[1,:iend], dvar[2,:iend], kind='cubic')(self.rhon)
    self.vprime=np.pi*2*interp1d(dvar[1,:iend], dvar[6,:iend], kind='cubic')(self.rhon)
    self.q=interp1d(dvar[1,:iend], dvar[7,:iend], kind='cubic')(self.rhon)

    self.bcmn=interp1d(dvar[1,:iend],bcmn, kind='cubic')(self.rhon)
    self.bsmn=interp1d(dvar[1,:iend],bsmn, kind='cubic')(self.rhon)
    self.bmn =interp1d(dvar[1,:iend],bmn , kind='cubic')(self.rhon)

    self.s_decomp = interp1d(dvar[1,:iend],s_decomp, kind='cubic')(self.rhon)
    self.calc_decomp = True

  def get_resonance(self,field,nn,mm):
      ''' Evaluate the resonant component of a field at the given resonces'''
      if nn<1:
        print("nn must be positive by convention in get_resonance")
        raise ValueError
      ndex = self.get_n_index(nn)
      mdex=self.get_m_index(mm)
      if ndex==None:
        print(f"{nn} is not a n number in decomp file")
        raise ValueError
      if mdex==None:
        print(f"{mm} is not an m number in decomp file")
        raise ValueError
      qres=mm/nn
      print(self.bmn.shape, ndex, mdex)
      if field=="psi":
        resfield=interp1d(self.rhon,self.bmn[ndex,mdex,:])
      elif field=="bmn":
        if self.calc_decomp==False:
          raise ValueError("Can not calculate b_mn, no S_decomp")
        else:
          resfield=interp1d(self.rhon,self.bmn[ndex,mdex,:]/self.s_decomp*2e4)
      else:
        raise ValueError("b_flag not reconized")

      return resfield(self.get_rho_q(qres))

  def get_phase(self,nn,mm,):
      ''' Evaluate the phase of psi at the given resonces'''
      if self.phasemn is None: self.compute_phase()
      if nn<1:
        print("nn must be positive by convention in get_phase")
        raise ValueError
      ndex = self.get_n_index(nn)
      mdex=self.get_m_index(mm)
      if ndex==None:
        print(f"{nn} is not a n number in decomp file")
        raise ValueError
      if mdex==None:
        print(f"{mm} is not an m number in decomp file")
        raise ValueError
      qres=mm/nn
      return interp1d(self.rhon,self.phasemn[ndex,mdex,:])(self.get_rho_q(qres))

  def plot(self,pargs={}):
    qmin = np.amin(self.q)
    qmax = np.amax(self.q)
    if (qmin < 0.0 and qmax < 0.0):
      self.signq = -1
    elif (qmin > 0.0 and qmax > 0.0):
      self.signq = +1
    else:
      self.signq = None
    for im,imode in enumerate(self.ifour):
       self.plot_radial(im,imode,pargs)
       self.plot_decomp(im,imode,pargs)
       if self.calc_decomp==True:
         self.plot_radial_bmn(im,imode,pargs)
         self.plot_decomp_bmn(im,imode,pargs)

  def plot_radial(self,ii,imode,pargs={}):

    fig = plt.figure(figsize=(8,6))
    ax=fig.add_subplot(111)
    title=f"$\psi$(n={int(imode)}) at {self.time*1000:.3f}ms"
    ylabel=f"$\psi_m$ [mWb]"
    colorlist = list(mcolors.TABLEAU_COLORS)
    xlabel=r'$\rho_N$'
    fontsize=24
    legendfont=20
    if imode==1:
      if self.signq == -1:
        mlist=range(-4,0)
      else:
        mlist=range(0,4)
    elif imode==2:
      mlist=range(-6,-1)
    else:
      mstart=-2*imode
      mlist=range(mstart,mstart+imode+1)
    if 'mlists' in pargs:
      if ii<len(pargs['mlists']):
        mlist=pargs['mlists'][ii]
    rhomax=np.max(self.rhon)
    for im,this_m in enumerate(mlist):
      this_i = self.get_m_index(this_m)
      if this_i!= None:
        mlbl = "m = " + str(abs(this_m))
        tc=colorlist[im%len(colorlist)]
        ax.plot(self.rhon/rhomax, self.bmn[ii,this_i,:]*1000, color=tc,
                label=mlbl)
    try:
      qlist=pargs['qlists'][ii]
    except:
      if imode==1:
        if self.signq == -1:
          qlist=range(-4,0)
        else:
          qlist=range(2,5)
      elif imode==2:
        if self.signq == -1:
          qlist=[-4,-3,-2.5,-2,-1.5]
        else:
          qlist=range(2,5)
      elif imode==3:
        if self.signq == -1:
          qlist=[-3,-2.33, -2,-1.67,-1.33]
        else:
          qlist=range(2,5)
      elif imode==4:
        if self.signq == -1:
          qlist=[-3,-2,-1.75,-1.5,-1.25]
        else:
          qlist=range(2,5)
      elif imode==5:
        if self.signq == -1:
          qlist=[-3,-2,-1.8,-1.6,-1.4,-1.2]
        else:
          qlist=range(2,5)
      else:
        if self.signq == -1:
          qlist=[-4,-3,-2]
        else:
          qlist=[2,3,4]
    for iq,qq in enumerate(qlist):
      try:
        irho = self.get_rho_q(qq)
        qlbl = f"q = {abs(qq):.1f}"
        tc=colorlist[iq]
        ax.axvline(irho/rhomax,ls=':',color=tc, label=qlbl)
      except:
        print(f"q={qq:.2f} is not in the domain")
    ax.axhline(0,ls='-',c='k')
    ax.legend(loc=0,
              frameon=True,
              fontsize=legendfont,
              ncol=1,
              handlelength=1,
              handletextpad=0.4)
    plt.title(title,fontsize=fontsize)
    plt.xlabel(xlabel,fontsize=fontsize)
    plt.ylabel(ylabel,fontsize=fontsize)
    plt.tight_layout()
    plt.show()
  
  def plot_radial_bmn(self,ii,imode,pargs={}):
    fig = plt.figure(figsize=(8,6))
    ax=fig.add_subplot(111)
    title=f"$B$(n={int(imode)}) at {self.time*1000:.3f}ms"
    title=f"$B$(n={int(imode)})"
    ylabel=f"$B_r$ [G]"
    colorlist = list(mcolors.TABLEAU_COLORS)
    xlabel=r'$\rho_N$'
    fontsize=24
    legendfont=20
    if imode==1:
      if self.signq == -1:
        mlist=range(-6,1)
      else:
        mlist=range(0,7)
    elif imode==2:
      mlist=range(-6,-1)
    else:
      mstart=-2*imode
      mlist=range(mstart,mstart+imode+1)
    if 'mlists' in pargs:
      if ii<len(pargs['mlists']):
        mlist=pargs['mlists'][ii]
    rhomax=np.max(self.rhon)
    for im,this_m in enumerate(mlist):
      this_i = self.get_m_index(this_m)
      if this_i!= None:
        mlbl = "m = " + str(abs(this_m))
        tc=colorlist[im%len(colorlist)]
        ax.plot(self.rhon / rhomax,
                2 * self.bmn[ii,this_i,:] / self.s_decomp * 10000,
                color=tc,
                label=mlbl)
    try:
      qlist=pargs['qlists'][ii]
    except:
      if imode==1:
        if self.signq == -1:
          qlist=range(-4,0)
        else:
          qlist=range(2,5)
      elif imode==2:
        if self.signq == -1:
          qlist=[-4,-3,-2.5,-2,-1.5]
        else:
          qlist=range(2,5)
      elif imode==3:
        if self.signq == -1:
          qlist=[-3,-2.33, -2,-1.67,-1.33]
        else:
          qlist=range(2,5)
      elif imode==4:
        if self.signq == -1:
          qlist=[-3,-2,-1.75,-1.5,-1.25]
        else:
          qlist=range(2,5)
      elif imode==5:
        if self.signq == -1:
          qlist=[-3,-2,-1.8,-1.6,-1.4,-1.2]
        else:
          qlist=range(2,5)
      else:
        if self.signq == -1:
          qlist=[-4,-3,-2]
        else:
          qlist=[2,3,4]
    for iq,qq in enumerate(qlist):
      try:
        irho = self.get_rho_q(qq)
        qlbl = f"q = {abs(qq):.1f}"
        tc=colorlist[iq]
        if qq in [2,-2]:
          tc = colorlist[2]
        if qq in [3,-3]:
          tc = colorlist[3]
        if qq in [4,-4]:
          tc = colorlist[4]
        ax.axvline(irho/rhomax,ls=':',color=tc)
      except:
        print(f"q={qq:.2f} is not in the domain")
    ax.axhline(0,ls='-',c='k')
    ax.legend(loc=0,
              frameon=True,
              fontsize=legendfont,
              ncol=1,
              handlelength=1,
              handletextpad=0.4)
    plt.title(title,fontsize=fontsize)
    plt.xlabel(xlabel,fontsize=fontsize)
    plt.ylabel(ylabel,fontsize=fontsize)
    plt.tight_layout()
    plt.show()


  def plot_decomp(self,im,imode,decomp,pargs={}):
    fig = plt.figure(figsize=(10,8))
    ax=fig.add_subplot(111)
    # Set titles and labels
    title=f"$\psi$(n={int(imode)}) at {self.time*1000:.3f}ms"
    # set contour levels, i could generalize this further if needed
    levels=301
    vmax=np.amax(self.bmn[im,:,:])*1000
    levels=np.linspace(0,vmax,301)
    cbar_ticks=np.linspace(0,vmax,11)
    # Update plot based on keys in pargs
    xlabel="Poloidal Mode Number m"
    fontsize=18
    # set up mrange()
    qmin=np.amin(self.q)
    qmax=np.amax(self.q)
    mrange=np.linspace(qmin,qmax)
    #create the decomp plot
    plt.set_cmap('nipy_spectral')
    m=range(-self.mmax,self.mmax+1)
    rhomax=np.max(self.rhon)
    mv, rv = np.meshgrid(m, self.rhon/rhomax, sparse=False, indexing='ij')
    conf=plt.contourf(mv,rv,np.clip(self.bmn[im,:,:]*1000,0,None),levels=levels,vmax=vmax)
    plt.plot(imode*mrange,self.get_rho_q(mrange)/rhomax,c='w')
    plt.title(title,fontsize=fontsize)
    plt.ylabel(r'$\rho_N$',fontsize=fontsize)
    plt.xlabel(xlabel,fontsize=fontsize)
    cbar=fig.colorbar(conf,ticks=cbar_ticks)
    plt.xlim(-self.mmax,self.mmax)
    plt.show()

  def plot_decomp_bmn(self,im,imode,decomp,pargs={}):
    fig = plt.figure(figsize=(10,8))
    ax=fig.add_subplot(111)
    # Set titles and labels
    title=f"$B_r$(n={int(imode)}) at {self.time*1000:.3f}ms"
    title=f"$B_r$(n={int(imode)}) [G]"
    # set contour levels, i could generalize this further if needed
    levels=301
    vmax = 2 * np.amax(self.bmn[im,:,:] / self.s_decomp) * 10000
#    vmax=4.2
    #vmax=4.8
    levels=np.linspace(0,vmax,301)
    cbar_ticks=np.linspace(0,vmax,11)
    # Update plot based on keys in pargs
    xlabel="Poloidal Mode Number m"
    fontsize=18
    # set up mrange()
    qmin=np.amin(self.q)
    qmax=np.amax(self.q)
    mrange=np.linspace(qmin,qmax)
    #create the decomp plot
    plt.set_cmap('nipy_spectral')
    m=range(-self.mmax,self.mmax+1)
    psimax=np.max(self.psi)
    mv, rv = np.meshgrid(m, self.psi/psimax, sparse=False, indexing='ij')
    conf=plt.contourf(mv,
                      rv,
                      np.clip(2 * self.bmn[im,:,:] / self.s_decomp * 10000, 0, None),
                      levels=levels,
                      vmax=vmax)
    plt.plot(imode*mrange,self.get_psi_q(mrange)/psimax,c='w')
    #plt.plot(imode*mrange+1,self.get_psi_q(mrange)/psimax,c='k',ls='--')
    plt.plot(imode*mrange+2,self.get_psi_q(mrange)/psimax,c='r',ls='-.')
    #plt.plot(imode*mrange+3,self.get_psi_q(mrange)/psimax,c='k',ls=':')
    plt.title(title,fontsize=fontsize)
    plt.ylabel(r'$\psi_N$',fontsize=fontsize)
    plt.xlabel(xlabel,fontsize=fontsize)
    cbar=fig.colorbar(conf,ticks=cbar_ticks)
    plt.xlim(-self.mmax,self.mmax)
    plt.show()

  def get_dumptime(self):
    ''' Open the hdf5 dumpfile read the dump time and dumpstep
    '''
    with h5py.File(self.dumpfile, 'r') as h5file:
      try:
        self.time=h5file["dumpTime"].attrs['vsTime']
        self.step=int(h5file["dumpTime"].attrs['vsStep'])
      except:
        print(f"Error reading time or step in {self.dumpfile}")
        raise
