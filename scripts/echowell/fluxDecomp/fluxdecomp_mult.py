#!/usr/bin/env python3
''' This is a generic script for analysing a multiple dumpfile/timestep
    using the decomp fsa scripts 

    Classes
    --------
    ResonantReponse

    Methods
    --------
    pickle_sort: sorts surfmn pickle file by step number
    dump_sort: sorts dump files by step number
    time_hist: computes time history of resonant field
    decomp_mult: main routine for performing analysis on multiple files
'''
import yaml
import os
import matplotlib.pyplot as plt
import numpy as np
import argparse
import glob
import fsa_decomp as decomp

class ResonantResponse:
  ''' ResonantResponse stores the time history of the resonant field amplitude
      and phase '''

  _first_call = True
  _field = None
  _times = None
  _XSCALE = 1000
  _XLABEL = "Time [ms]"
  _YSCALE_DICT = {"psi" : 1000, 'bmn': 1}
  _YLABEL_DICT = {"psi" : r"$\psi$ [mWb]", 'bmn': r"$B_r$ [G]"}
  _TITLE_DICT = {"psi" : r"Resonant $\psi$", 'bmn': r"Resonant $B_r$"}
  _TITLE_PHASE = "Resonant Phase"
  _YLABEL_PHASE = r"Phase mn"

  def __init__(self,m,n,length,field="psi"):
    ''' Initialize resonant response class

        Inputs:
        m: poloidal mode number
        n: toroidal mode number
        length: lenght of time history 
        field: psi or bmn
    '''
    self._m = m
    self._n = n
    self.amp = np.zeros(length)
    self.phase = np.zeros(length)
    self.label = f"{self._m} / {self._n}"
    if self._first_call:
      self.set_first_call(length, field)

  @classmethod
  def set_first_call(cls, length, field):
    ''' Set up time history array, and set field type
        This must be done as a class method to avoid
        converting class variables into instance variables

        Inputs: 
          length: length of time history
          field: psi or bmn
    '''
    cls._first_call = False
    cls._times = np.zeros(length)
    cls._field = field

  @classmethod
  def set_time(cls, index, time):
    ''' Set the time

        Inputs:
          index: index of times array
          time: time at index
    '''
    cls._times[index] = time

  def get_resonance(self, index, step):
    ''' Compute the amplitude and phase of the resonant field at step 

        Inputs:
          step: decomp.fsadecomp instance
          index: time series index
    '''
    self.amp[index] = step.get_resonance(self._field, self._n, self._m)
    self.phase[index] = step.get_phase(self._n, self._m)
  
  def plot_amp(self, ax, **kwargs):
    ''' plot amplitude vs time 

        Inputs:
          ax: matplotlib axis instance
          
        Comments:
          Plan to use kwargs can be used to override default
    '''
    ax.plot(self._XSCALE * self._times,
            self._YSCALE_DICT[self._field] * self.amp,
            label = self.label)


  def plot_phase(self, ax, **kwargs):
    ''' plot phase vs time 

        Inputs:
          ax: matplotlib axis instance
          
        Comments:
          Plan to use kwargs can be used to override default
    '''
    ax.plot(self._XSCALE * self._times, self.phase, label = self.label)

  @classmethod
  def set_amp_plot(cls, ax, **kwargs):
    ''' set amp plot title and axes labels 

        Inputs:
          ax: matplotlib axis instance
          
        Comments:
          Plan to use kwargs can be used to override default
    '''
    ax.set_title(cls._TITLE_DICT[cls._field],fontsize=16)
    ax.set_ylabel(cls._YLABEL_DICT[cls._field],fontsize=16)
    ax.set_xlabel(cls._XLABEL,fontsize=16)
    ax.legend(loc='best',frameon=True,ncol=2,fontsize=14)

  @classmethod
  def set_phase_plot(cls,ax, **kwargs):
    ''' set phase plot title and axes labels 

        Inputs:
          ax: matplotlib axis instance
          
        Comments:
          Plan to use kwargs can be used to override default
    '''
    ax.set_title(cls._TITLE_PHASE,fontsize=16)
    ax.set_ylabel(cls._YLABEL_PHASE,fontsize=16)
    ax.set_xlabel(cls._XLABEL,fontsize=16)
    ax.legend(loc='best',frameon=True,ncol=2,fontsize=14)

def pickle_sort(file):
  ''' Sorting function that gets step number from pickle file

      inputs
        file: pickle file with name {prefix}.{stepnumber}.{postfix}
  '''
  return int(file.split('.')[1])

def dump_sort(file):
  ''' Sorting function that gets step number from dump file

      inputs
        file: dump file with name {prefix}.{stepnumber}.{postfix}
  '''
  return int(file.split('.')[1])

def time_hist(steplist, timehist_in):
    ''' computes time history of resonant field'''
    #Set some reasonable defaults
    _MLIST = [2,3,4]
    _NLIST = [1,1,1]
    _FIELD = 'bmn'

    mlist = timehist_in.get('mlist',_MLIST)
    nlist = timehist_in.get('nlist',_NLIST)
    field = timehist_in.get('field',_FIELD)

    if (len(mlist) != len(nlist)):
      print("time_hist: mlist and nlist must have the same len")
      raise ValueError

    length = len(steplist)
    responseList = []
    for mm, nn in zip(mlist,nlist):
      responseList.append(ResonantResponse(mm,nn,length,field=field))

    for istep, step in enumerate(steplist):
        print(istep, step.step, step.time)
        ResonantResponse.set_time(istep, step.time)
        for response in responseList:
          response.get_resonance(istep, step)

    #plot amplitude and phase time history
    fig = plt.figure(figsize=(8,6))
    ax=fig.add_subplot(111)
    for response in responseList:
      response.plot_amp(ax)
    ResonantResponse.set_amp_plot(ax)
    plt.tight_layout()
    plt.show()

    fig = plt.figure(figsize=(8,6))
    ax=fig.add_subplot(111)
    for response in responseList:
      response.plot_phase(ax)
    ResonantResponse.set_phase_plot(ax)
    plt.tight_layout()
    plt.show()

def decomp_mult(read_pickle=False,args={}):
  ''' Perform decomp analysie based on the the options
      If read pickle then it will look for pickle files else it will
      look for dump files
  '''

  dump_pre=["dumpgll","dump"]
  dump_suf=["h5"]
  pickle_pre=["surfmn"]
  pickle_suf=["pickle"]
  nimrodin="nimrod.in"

  try: 
    with open(args['file'], 'r') as file:
      nimpy_in = yaml.safe_load(file)
  except:
    nimpy_in = {}
  fc_decomp_in = nimpy_in.get("fc_decomp",{})
  fsa_in = nimpy_in.get('fsa', {})
  steplist=[]
  if fc_decomp_in.get("read_pickle",False):
    pickle_list=glob.glob("surfmn*.pickle")
    pickle_list.sort(key=pickle_sort)
    if len(pickle_list)<=0:
      print("No pickle files found")
      raise IOError
    for iobj in pickle_list:
      with open(iobj,'rb') as file:
        surf=decomp.fsadecomp(None,None)
        surf.load(file)
        steplist.append(surf)
  else: #read from dump files and calculate fsa
    if not os.path.isfile(nimrodin):
      print(f"nimrod.in not found")
      raise IOError
    dumplist=[]
    dumplist=glob.glob("dumpg*.h5")
    dumplist.sort(key=dump_sort)
    for file_name in dumplist:
      print(f"Performing flux decomposition analysis on file: {file_name}")
      surf=decomp.fsadecomp(file_name,nimrodin)
      surf.get_dumptime()
      surf.calculate(fsa_in)
      steplist.append(surf)
      if fc_decomp_in.get("pickle_data",False):
        pfile=pickle_pre[0]+'.'+str(surf.step).zfill(5)+'.'+pickle_suf[0]
        print(f"writing file: {pfile}")
        with open(pfile,'wb') as file:
          surf.dump(file)

  if fc_decomp_in.get("show_plot", False):
    timehist_in = fc_decomp_in.get("timehist", {})
    stepplot_in = fc_decomp_in.get("stepplot", {})
    plotlist = stepplot_in.get("plotlist", [])
    #append last step to plotlist are remove duplicates
    plotlist.append(steplist[-1].step)
    plotlist = [*set(plotlist)]

    time_hist(steplist, timehist_in)
    for step in steplist:
      if step.step in plotlist:
        step.plot(pargs=stepplot_in)

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Flux decomp runner.')
  parser.add_argument('--file', default='nimpy.yaml', help='yaml input file')
  args = vars(parser.parse_args())
  decomp_mult(args=args)
