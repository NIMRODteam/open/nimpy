#!/usr/bin/env python3
import os
import matplotlib.pyplot as plt
import numpy as np
import argparse
import fsa
#import nim_timer as timer
import eval_nimrod as eval
from scipy.interpolate import interp1d,splev,UnivariateSpline,griddata
import qsolver_fsa as qfsa

def qmod(dumpfile, outfile, plot, args):
  psi, psin, qpsi, fsa_qty, f_fsa, f_com = qfsa.q_fsarunner(dumpfile,False,args)
  if plot:
    plt.plot(psin, qpsi(psi))
    plt.show()
    qfsa.write_qcvs("dumpq.txt",psin,qpsi(psi))

  sgn = 1.0
  if qpsi(psi[0]) < 0:
    sgn = -1.0

  q0 = np.abs(args.get('q0',1.0)) * sgn
  qpsin = qpsi(psi)
  knot_psi = args.get('psik',0.2)
  npsin = len(psin)
  iknot = -1
  for idx, ipsi in enumerate(psin):
    if ipsi > knot_psi:
      break
    else:
      iknot = idx

  if iknot < 0:
    print("index of psi knot could not be found")
    raise ValueError
  elif iknot<2:
    print("index of psi knot is too small increase psik or nsurf")
    raise ValueError
  elif npsin-iknot<2:
    print("index of psi knot is too large decrease psik or increase nsurf")
    raise ValueError
    
  #try simple cubic fit by matchign q at idx-1,idx,idx+1
  coef = np.zeros(4)
  bvec = np.zeros(4)
  psin1 = psin[iknot-1]
  psin0 = psin[iknot]
  psip1 = psin[iknot+1]

  amat = np.array([[0,0,0,1],[psin1**3,psin1**2,psin1,1],
                  [psin0**3,psin0**2,psin0,1],[psip1**3,psip1**2,psip1,1]])
  
  bmat = np.array([q0,qpsin[iknot-1],qpsin[iknot],qpsin[iknot+1]])

  coef = np.linalg.solve(amat, bmat)
  newq = qpsin.copy()

  for idx,ipsi in enumerate(psin):
    if idx < iknot:
      newq[idx] = coef[0] * ipsi**3 + coef[1] * ipsi**2 + coef[2] * ipsi \
                + coef[3]

  if plot:
    plt.plot(psin, qpsin)
    plt.plot(psin, newq)

    plt.show()


  newf = 2 * np.pi * newq / ( fsa_qty(psi) )

  if plot:
    plt.plot(psin, fsa_qty(psi))
    plt.show()

    plt.plot(psin, newf, label='new f')
    plt.plot(psin, f_fsa(psi), label='old f')
    plt.legend()
    plt.show()


  qfsa.write_qcvs(outfile,psin,newq)


  print(coef)

  print(q0, npsin)
  print(iknot, psin[iknot], psin[iknot+1])

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='compute q and f.')
  parser.add_argument('dumpfile',help='initial dump file name')
  parser.add_argument('--qfile', default="qfile.txt", help='file name to write q')
  parser.add_argument('--plot', action='store_true',help='shows plots')
  parser.add_argument('--nsurf', type=int, default=150, help="number of surfaces")
  parser.add_argument('--eqflag', type=int, default=1, help="flag to add n=0 perturbation to eq")
  parser.add_argument('--rzo',type=float, nargs=3, default=[1.768,-0.018831,0.0], help="intial guess for o-point")
  parser.add_argument('--q0', type=float, default=1.01, help="desired value of q on axis")
  parser.add_argument('--psik', type=float, default=0.2, help="psi value to spline")
  
  args = vars(parser.parse_args())
  qmod(dumpfile=args['dumpfile'],outfile = args['qfile'], 
       plot=args['plot'],args=args)
  


