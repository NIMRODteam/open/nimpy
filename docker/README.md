Docker files for the project are collected here.

Basic commands:

```
$ docker image pull registry.gitlab.com/nimrodteam/open/nimpy:<image name>

$ docker build -t registry.gitlab.com/nimrodteam/open/nimpy:<image name> .  

$ docker push registry.gitlab.com/nimrodteam/open/nimpy:<image name>

$ docker image ls --all

$ docker run -it --rm -v $(pwd):/source -w /source <image name> 
```
